— — - - - - ReadMe — — - - - - 

You have been provided a .zip file titled “Fraud_data.” The are two directories labeled "Fraudulent" and "Unretracted," each containing 253 full papers. 

By obtaining these files, the recipient acknowledges that the text falls under the U.S. Copyright Law of Fair Use (see: http://www.copyright.gov/fls/fl102.html). Furthermore, these files are only to be used for research purposes. Redistribution of these files is prohibited. The authors send these files to the recipient in good faith that they will not be redistributed to another individual or party.

For information about the research using the text files, please see: http://jls.sagepub.com/content/early/2015/11/05/0261927X15614605

If there are any questions or inquiries about the data, please first contact David Markowitz (markowitz@stanford.edu).
	
— — - - - - ReadMe — — - - - - 
