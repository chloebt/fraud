
Cyclosporine CyA7 and FK506 Tacrolimus are immunosuppressive agents  and
have been widely used in a variety of disease conditions  such  as  clinical
post-organ transplantation and autoimmune diseases 1, 2. CyA is  a  cyclic
polypeptide produced as a fungal metabolite and consists of 11 amino  acids,
whereas FK506 is a macrolide lactone 2. CyA and FK506 bind to  cyclophilin
A and the 12-kDa FK506-binding protein, FKBP12, respectively,  and  suppress
activation of NFAT in T cells 3, 4. CyA and FK506  are  potent  inhibitors
of Ca2+-dependent T cell activation, and the CyA-cyclophilin  A  and  FK506-
FKBP12 complexes  bind  to  the  same  target,  calcineurin  3–5,  thereby
inhibiting  the  T  cell  antigen  receptor-mediated   signal   transduction
pathway. FK506 and CyA modulate the  biological  functions  of  T  cells,  B
cells, macrophages, and dendritic cells DCs 5–9 and have been  shown  to
inhibit Th1 responses by interfering with cytokine production including  IL-
12 by murine DCs 1, 6–9.
IL-12 is produced by monocytic cells, DCs,  B  cells,  and  other  accessory
cells and plays a key role in the development of Th1 responses  10–14.  It
is a potent inducer of interferon-γ either alone or in  synergy  with  other
inducers and  causes  proliferation  of  T  and  NK  cells  and  lymphokine-
activated killer cells 11–14. IL-12  is  a  heterodimeric  70-kDa  protein
composed  of  a  disulfide  linked  40-kDa  and  35-kDa  subunits  15, 16.
Although the IL-12p40 subunit is secreted in excess over IL-12p70, only  the
heterodimer is biologically active. Many cell types,  including  those  that
are not known to produce IL-12,  express  mRNA-encoding  IL-12p35.  However,
mRNA-encoding IL-12p40 seems to be restricted  to  cells  that  produce  the
biologically  active  IL-12  15, 16.  Therefore,  IL-12p40  expression  is
generally considered as an indicator of IL-12p70 production.  Moreover,  the
IL-12p40 subunit is also shared by another Th1 cytokine, IL-23, which  makes
it a highly significant cytokine for determination  of  Th1  type  responses
17.
There  is  relatively  little  information  on  the  role  of  intracellular
signaling molecules that regulate  IL-12p40  synthesis  in  human  monocytic
cells  following  LPS  stimulation.  LPS-induced  cell  signaling   involves
activation of tyrosine and serine/threonine protein  kinases  including  PKC
and the mitogen-activated protein kinases 18–21. We  and  others  20, 22
have previously demonstrated  the  involvement  of  c-Jun-N-terminal  kinase
JNK in  IL-12p40  production  in  LPS-stimulated  human  monocytic  cells.
Multiple transcription factors including  interferon-γ  regulatory  factors,
NFκB,  Ets-2,  AP-1,  C/EBP,  and  PU.1  transcription  factors,  and  their
complexes have been suggested to regulate  IL-12p40  transcription  in  LPS-
stimulated murine and human monocytic cells 20, 23–28.
The molecular mechanism by which CyA and FK506 inhibit  IL-12p40  production
in monocytic cells remains unknown. Since both  FK506  and  CyA  are  potent
inhibitors of calcineurin 3, 4, we  hypothesized  that  these  agents  may
inhibit IL-12p40 production through  interference  with  calcium  signaling.
However, several studies have  reported  the  negative  regulatory  role  of
calcium and the phosphoinositide 3-kinase PI3K  signaling  in  LPS-induced
IL-12p40 production in murine macrophages  and  DCs  29–36.  Surprisingly,
and in contrast  to  the  regulation  of  murine  IL-12p40  production,  our
results  suggest  that  LPS-induced  IL-12p40   production   is   positively
regulated by  the  PI3K  as  well  as  the  calcium  signaling  pathway,  in
particular calmodulin CaM and the calmodulin-dependent  protein  kinase-II
CaMK-II. Furthermore, both CyA and FK506  inhibited  LPS-induced  IL-12p40
production by inhibiting  the  activation  of  CaM  and  CaMK-II  and  their
downstream NFκB and AP-1 transcription factors  without  affecting  the  JNK
pathway.



 Monocytes from PBMCs of healthy blood donors  were  isolated  as  described
previously  20.  Briefly,  PBMCs  were  isolated   by   density   gradient
centrifugation over Ficoll-Hypaque Amersham  Biosciences.  Monocytes  were
isolated by negative selection by depletion of T and B cells using  magnetic
polystyrene Dynabeads coated with Abs specific for CD2 T  cells  and  CD19
B cells Dynal Biotech, Oslo, Norway. The  monocytes  obtained  contained
<1 CD2+ T  cells  and  CD19+ B  cells  as  determined  by  flow  cytometric
analysis. The human promonocytic cell line, THP-1,  was  obtained  from  the
American Type Culture Collection Manassas,  VA.  Cells  were  cultured  in
IMDM  Sigma  supplemented  with  10  FBS   Invitrogen,   100   units/ml
penicillin, 100 μg/ml gentamicin, 10 mM HEPES, and 2 mM glutamine.
IL-12p40 ELISA—IL-12p40 was  measured  by  using  two  different  mAbs  that
recognize distinct epitopes as described previously 20. Briefly,  IL-12p40
was detected by using a primary anti-IL-12p40 mAb and a second  biotinylated
anti-IL-12p40 mAb 4  μg/ml  and  350  ng/ml,  respectively,  R&D  Systems.
Streptavidin-peroxidase Jackson ImmunoResearch  Laboratories,  West  Grove,
PA was used at a final dilution of  1/1000.  rIL-12p40  R&D  Systems  was
used as a standard. The sensitivity of the ELISA for IL-12p40 was 16 pg/ml.
RNA Isolation and RT-PCR—Total  RNA  was  extracted  as  described  using  a
monophasic  solution  containing  guanidine  thiocyanate  and  phenol   Tri
Reagent solution; Molecular Research Center, Cincinnati, OH  20.  RNA  1
μg was reverse transcribed by using Moloney murine leukemia  virus  reverse
transcriptase PerkinElmer Life Sciences. Equal aliquots  5  μl  of  cDNA
equivalent to 100 ng of RNA were subsequently amplified  for  IL-12p40  373
bp and β-actin 663 bp using their respective primers as  described  20.
Real-time RT-PCR was performed with Universal PCR Master Mix  using  β-actin
and IL-12p40 primers  Applied  Biosystems.  Data  were  analyzed  on  7500
System SDS software, version 1.3 Applied Biosystems.
 Changes in concentrations of intracellular free calcium  were  measured  by
flow cytometry as described previously  37.  Cells  were  pretreated  with
FK506 and CyA for 2 h followed by washing with Ca2+-free  phosphate-buffered
saline  and  were  resuspended  in  buffer  A  containing   1   mM Fluo-3/AM
Molecular Probes. The reaction was stopped by adding an  equal  volume  of
buffer B buffer A containing 5 FBS, pH 7.4.  After  washing,  cells  were
analyzed for Ca2+ levels in the  presence  and  the  absence  of  LPS  by  a
FACScan flow cytometer BD Biosciences equipped  with  CellQuest  software,
version 3.2.1 fl. Ca2+ ionophore A23187 20 mM and 5 mM EGTA  Sigma  were
used as controls.
 Briefly, phosphorylation of Akt, p85 PI3K, p110α PI3K,  JNK,  CaMK-II,  and
NFAT were determined using anti-phospho-Akt  New  England  Biolabs,  anti-
phospho-p85, anti-phospho-p110 PI3K, anti-phospho-JNK,  anti-phospho-CaMKII,
or anti-NFAT Cell  Signaling  antibodies,  respectively.  To  control  for
protein loading, the membranes were stripped and  reprobed  with  antibodies
directed  against   their   respective   unphosphorylated   proteins   Cell
Signaling. The immunoblots were visualized by ECL as described 20.
Plasmid Constructs—A series of  deletion  of  hIL-12p40  promoter  fragments
–880 to +108 were generated by PCR and subcloned into the NheI/NcoI site  of
the pGL3B luciferase reporter  plasmid  as  described  previously  20.  To
generate mutations in AP-1 nd NFκB binding sites on IL-12p40 promoter  –232
to –116, site-directed mutagenesis was performed  by  PCR  using  mutagenic
primers 20. All sequences were confirmed  by  the  Biotechnology  Research
Institute, University of Ottawa.
 DNA transfection was performed by FuGENE 6 reagent Roche  Diagnostics  as
described previously 20, 38. Briefly, 1 μg of the test plasmid and 0.5  μg
of the pSV-β-galactosidase Promega, Madison, WI were incubated for  5  min
in 50 μl of IMDM. The mixture was then incubated with 3 μl of  FuGENe  6  in
50 μl of IMDM to allow formation of DNA-liposome complexes. These  complexes
were added to the cell suspension in each  well.  After  24  h,  cells  were
stimulated with LPS for another 24 h followed by analysis of luciferase  and
β-galactosidase activity by using luciferase and β-galactosidase assay  kits
Promega.  Luciferase  activities   were   normalized   by   measuring   β-
galactosidase value. 
Transfection of p85 PI3K  siRNA—Cells  106  were  transfected  with  siRNA
SMARTpool PI3K p85α and  nonspecific  control  pool  siRNA  control  using
DharmaFECT™ 2 transfection reagent as per  the  manufacturer's  instructions
Dharmacon. Following transfection,  cells  were  stimulated  with  LPS  1
μg/ml for 8 h for real-time PCR and for 30 min  for  Western  blotting  for
p85 PI3K. The supernatants were collected after 24 h of stimulation for  IL-
12p40 ELISA.
 Briefly, cells 107 were harvested in  Tris-EDTA-saline  buffer,  pH  7.8,
and lysed for 10 min at 4 °C with buffer A containing  0.1  Non-idet  P-40.
The pellets containing the nuclei were suspended in buffer B at 4 °C for  15
min. Both buffers A and B contained  the  proteolytic  inhibitors  and  have
been described 20. Nuclear proteins 5 μg were mixed for 20 min  at  room
temperature with either 32P-labeled  AP-1  or  NFκB  oligonucleotide  probes
20, and the  complexes  were  subjected  to  non-denaturing  5  PAGE.  To
illustrate specificity of binding for NFκB and AP-1 probes,  the  wild  type
and mutant oligonucleotide sequences corresponding  to  the  NFκB  and  AP-1
binding sites in the IL-12p40 promoter were  used  as  cold  competitors  as
described earlier 20.  The  gel  was  dried  and  exposed  to  x-ray  film
Eastman Kodak Co
 Means  were  compared  by  two-tailed  Student's t test.  The  results  are
expressed as mean ± S.E.



 LPS stimulation of primary human monocytes and THP-1 cells induced  maximum
expression of IL-12p40 at 6 h  post-stimulation.  Treatment  of  cells  with
either CyA or FK506 for 2 h prior  to  stimulation  with  LPS  significantly
inhibited IL-12p40  expression  as  determined  by  semiquantitative  RT-PCR
Fig 1A and ELISA Fig 1B in both primary  monocytes  and  THP-1  cells.
Since FK506 and  CyA  exert  their  biological  effects  by  inhibiting  the
calcium-dependent signaling  pathway  3, 4,  we  determined  the  role  of
calcium  signaling  in  LPS-induced  IL-12p40  production.  Stimulation   of
monocytes and THP-1 cells with LPS increased  the  levels  of  intracellular
calcium at  8  and  12  min,  respectively,  that  was  inhibited  following
treatment of cells with EGTA Fig 2A. FK506 and CyA inhibited  LPS-induced
increases in intracellular calcium in both monocytes from  a  mean  channel
fluorescence of 128 to the basal levels of 32 and THP-1 cells from a  mean
channel fluorescence of 64 to the basal levels of 32  in  a  dose-dependent
manner Fig 2A. To confirm that FK506 and  CyA  are  biologically  active,
Jurkat cells were treated with these agents for 2  h  prior  to  stimulation
with PMA/ionomycin  for  another  5  min  followed  by  analysis  of  NFAT-4
expression by  Western  blotting.  NFAT-4  is  constitutively  expressed  in
resting Jurkat cells. PMA/ionomycin stimulation  induces  its  translocation
into the nuclei that is inhibited by prior treatment of  cells  with  either
CyA or FK506 PMA and ionomycin treatment caused disappearance of the  NFAT-4
band, which was restored by pretreatment of cells with FK506  or  CyA  Fig
2B. To determine a direct link for LPS-induced increases  in  intracellular
free calcium and IL-12p40 production, cells were incubated with  EGTA  prior
to LPS stimulation for 24 h. EGTA inhibited LPS-induced IL-12p40  production
in both cell types as measured by ELISA Fig 2C and  semiquantitative  RT-
PCR analysis Fig 2D.
  To  further  delineate  the   role   of   calcium   signaling,   we   used
pharmacological inhibitors specific for members  of  the  calcium  signaling
pathway. Increases in cytoplasmic calcium concentrations  occur  by  stimuli
that activate  voltage  or  ligand-gated  calcium  channels  in  the  plasma
membrane or following release of calcium present  in  intracellular  stores,
mainly in the endoplasmic reticulum ER 39, 40.  The  role  of  receptor-
mediated entry of extracellular Ca2+was studied by using SKF-96365 41.  To
determine  whether  calcium  release  from   the   ER   regulates   IL-12p40
expression, we employed 2-APB, an inhibitor  of  the  inositol  triphosphate
receptor which blocks calcium release  from  the  ER  by  blocking  inositol
triphosphate receptorgated channels 42.  Prior  treatment  of  cells  with
either SKF-96365  or  2-APB  significantly  inhibited  LPS-induced  IL-12p40
production in  both  cell  types  as  determined  by  ELISA  Fig  3A  and
semiquantitative RT-PCR analysis Fig 2D. It may be noted  that  monocytes
were more sensitive to the effects of EGTA, SKF-96365, and 2-APB  than  were
THP-1 cells. These results  suggest  that  receptor-mediated  Ca2+ entry  as
well as the Ca2+ release from the ER may  be  involved  in  LPS-induced  IL-
12p40 expression.
CaM, a major calcium receptor, is present in both  cytoplasmic  and  nuclear
compartments. The calcium/CaM complex regulates several  downstream  targets
including protein kinases and phosphatases 43, 44. To determine  the  role
of  CaM,  we  used  a  specific  calmodulin  inhibitor,  W-7  37, 45.  W-7
inhibited LPS-induced IL-12p40 production in both cell types  as  determined
by ELISA Fig 3B and  semiquantitative  RT-PCR  analysis  Fig  2D.  One
major family of CaM effectors is  the  calmodulin-dependent  protein  kinase
CaMK, which includes a multifunctional kinase, CaMK-II.  To  gain  further
insight into the role of calcium/CaM, we examined the  role  of  CaMK-II  by
employing   the   CaMK-II-specific   inhibitor,   KN93    46, 47,    which
significantly inhibited LPS-induced IL-12p40 production in both  cell  types
as determined by ELISA Fig 3B and semiquantitative RT-PCR analysis  Fig
2D.  We  also  confirmed  that  LPS  induced  CaMK-II  phosphorylation  was
inhibited by W-7 and KN93 in both cell types Ref 37 and Fig 3C.
 The PI3K pathway negatively regulates IL-12p40  expression  in  murine  DCs
and monocytic cells 32–36. Therefore, it  was  of  interest  to  determine
whether PI3K regulates human IL-12p40 production in a manner similar to  the
calcium  pathway.  Therefore,  we  first  demonstrated  that   LPS   induced
phosphorylation of Akt,  a  downstream  substrate  for  PI3K,  and  LY294002
inhibited this phosphorylation in both monocytes and THP-1 cells Fig  4A.
Interestingly,  Ly294002  significantly   inhibited   LPS-induced   IL-12p40
expression in both cell types Figs 2D and 4B.
Mammalian class-IA PI3K are heterodimers consisting of a regulatory  subunit
p85α, p85β, p55, or  other  splice  variants  and  a  p110  α,  β,  or  γ
isoforms catalytic unit 48–50. Herein, we investigated the  role  of  the
p110α isoform by  employing  THP-1  cells  stably  deficient  in  the  p110α
isoform  51.  Cells  were  stably  transduced  with  a  lentiviral  vector
expressing short hairpin RNA for the p110α subunit HR-p110α3. These  cells
exhibited complete inhibition of the PI3K p110α isoform  expression  without
affecting the expression of p85  Fig  4C  or  that  of  p110β  and  p110γ
isoforms Ref 51 and data not shown. Cells  transduced  with  the  control
lentiviral vector HRp110α1 exhibited normal  levels  of  the  p110α  isoform
Fig 4C, inset, and Ref 51. To determine  the  involvement  of  the  PI3K
p110α isoform, normal THP-1 cells and  THP-1  cells  stably  expressing  HR-
p110α3 and HR-p110α1 were stimulated with LPS for 24 h and analyzed for  IL-
12p40  expression.  The  results   show   that   IL-12p40   production   was
significantly reduced in THP-1 cells  transfected  with  HR-p110α3  compared
with the control Fig 4C suggesting a key role for p110α isoform  of  PI3K
in LPS-induced IL-12p40 production. The role of  p85  subunit  of  PI3K  was
confirmed by transfecting THP-1 cells with vectors  containing  p85-specific
or a control  siRNA.  Transfection  with  p85-specific  siRNA  significantly
reduced LPS-induced p85 phosphorylation Fig 5A  and  IL-12p40  expression
as determined by real-time PCR Fig 5B and ELISA Fig 5C.
 We have previously demonstrated a key role for JNK in LPS-induced  IL-12p40
production 20. Since PI3K is involved in TLR-4-mediated signaling and  Akt
has been shown to regulate  c-Raf  ERK,  p38,  and  JNK  mitogen-activated
protein kinases 52, 53, we examined whether activation of calmodulin/CaMK-
II,  PI3K,  and  JNK  by  LPS  constitute  distinct  pathways  in   IL-12p40
regulation. This was accomplished by pretreating cells for 2 h with  various
concentrations  of  inhibitors  specific  for  calcium-dependent   signaling
followed by LPS stimulation and analysis of  Akt  and  JNK  phosphorylation.
EGTA, 2-APB, SKF, KN93, W7, FK506, and  CyA  suppressed  phosphorylation  of
Akt without any effect on JNK phosphorylation in THP-1 cells  and  monocytes
Fig 6. These results suggest  that  LPS-induced  IL-12p40  production  is
regulated by two  distinct  pathways,  namely  PI3K  activated  upstream  by
CaM/CaMK-II and the JNK pathway, independent of the  latter.  Moreover,  CyA
and FK506 inhibited LPS-induced IL-12p40 production  through  the  selective
inhibition  of  pathways  involving  calcium  Fig  2A  and  PI3K  without
affecting the JNK pathway Fig 6, A and B.
 We have previously shown that LPS-induced IL-12p40 production is  regulated
by JNK through the activation of NFκB and AP-1 20.  To  determine  whether
LPS-induced IL-12p40 production is regulated by  CaM/CaMK-II-activated  NFκB
and/or AP-1, we confirmed our earlier results by transfecting cells  with  a
series of 5′-deletion constructs  containing  IL-12p40  promoter  –880/+118
bp sequences Fig 7A linked with the luciferase reporter plasmid,  pGL3B.
Subsequent   LPS-induced   luciferase   activity   analysis   showed    that
transfection with the –880 to +118 construct induced significant  luciferase
activity compared with  the  cells  transfecetd  with  the  control  vector,
whereas transfection with the  –116  vector  abrogated  luciferase  activity
Fig 7B. Transfection with vectors containing mutant NFκB in the  presence
of PU.1 binding  sequence  –128,  pIL-12p40.PU.1,  NFκBmut  abrogated  the
luciferase activity, while transfection with vectors  containing  wild  type
AP-1 and mutant NFκB sites  –232,  pIL-12p40.AP-1,  Ets-2,  PU.1,  NFκBmut
still exhibited an induction of luciferase activity after  LPS  stimulation.
However, when both AP-1 and NFκB sites  were  mutated  –232,  pIL-12p40.AP-
1mut,  Ets-2,  PU.1,   NFκBmut,   LPS-induced   luciferase   activity   was
significantly decreased Fig 7B, left panel. These  results  suggest  that
both AP-1 and NFκB regulate LPS-induced IL-12p40 production and that  either
one or the other are required to induce IL-12p40 transcription.
CaMK-II has been shown to act as a mediator  of  IκB  kinase  activation  in
response to CD3/T cell  receptor  stimulation  54.  To  determine  whether
CaM/CaMK-II  regulated  LPS-induced   IL-12p40   expression   through   NFκB
activation, cells transfected with the  full-length  IL-12p40  promoter  and
the  deletion  constructs  were   treated   with   various   pharmacological
inhibitors of the calcium and PI3K pathways EGTA, W7, 2-APB, SKF-96365, KN-
93,  CyA,  FK506,  and  Ly294002  prior  to  LPS  stimulation  followed  by
measurement  of  luciferase  activity.  All   these   agents   significantly
decreased the LPS-induced luciferase activity in cells transfected with  the
full  length  –880  to  +112,  the  –232  pIL-12p40.AP-1,  Ets-2,   PU.1,
NFκBmut, and –128 pIL-12p40. PU.1, NFκB promoter  constructs  Fig  7B.
These results suggest  that  CyA-  and  FK506-mediated  inhibition  of  LPS-
induced IL-12p40 transcription in human monocytic cells  involves  CaM/CaMK-
II- and PI3K-dependent activation of NFκB and/or AP-1.
 —We confirmed our earlier results showing that NFκB and AP-1 regulate  LPS-
induced IL-12p40 transcription 20.  LPS  stimulation  induced  significant
binding of NFκB and AP-1. The specificity  of  NFκB  and  AP-1  binding  was
demonstrated by competition with specific and  nonspecific  oligonucleotides
and by supershift analysis with anti-p50 and anti-p65 NFκB,  and  anti-c-Jun
and anti-c-Fos antibodies for AP-1 Fig 8, A and B. To  determine  whether
binding of NFκB and AP-1 to their respective binding sites in  the  IL-12p40
promoter was regulated by CaM/CaMK-II and/or PI3K, cells were  treated  with
inhibitors of the calcium and PI3K  signaling  pathways  before  stimulation
with LPS. As before, EGTA, W7, SKF-96365, KN-93, FK506,  CyA,  and  LY294002
inhibited the binding of NFκB and AP-1 to their respective  probes  in  LPS-
stimulated cells Fig 8, A and B. These results suggest  that  LPS-induced
IL-12p40 transcription is regulated by CaM/CAMK-II as well as  PI3K  through
NFκB and  AP-1  activation.  Moreover,  CyA  and  FK506  inhibited  IL-12p40
transcription by inhibiting AP-1 and NFκB activity.



CyA and FK506 are potent inhibitors of calcium-regulated signaling and
exert their biological effects through calcineurin 3, 4. PI3K and calcium
pathways were shown to negatively regulate LPS-induced IL-12p40 expression
in murine macrophages and DCs 32–36. In contrast, our results show that
calcium signaling through CaM and CaMK-II, as well as through PI3K
positively regulated LPS-induced IL-12p40 production in human monocytic
cells. Moreover, both CyA and FK506 inhibited LPS-induced IL-12p40
production by inhibiting NFκB and AP-1 activity through the upstream
CaM/CaMK-II-activated PI3K and independent of the JNK pathway.
Inhibition of calcium signaling has been shown to enhance IL-12p40
production in LPS-stimulated murine monocytic cells. Ligation of Fc
receptors on murine monocytic cells inhibited LPS-induced IL-12p40
production that was reversed by pretreatment with the calcium ion chelator,
EGTA 29–31. In contrast, our results show that inhibition of LPS-induced
intracellular free calcium by EGTA significantly reduced IL-12p40
production in human monocytic cells. A positive role for calcium was
further supported by employing specific inhibitors for CaM/CaMK-II. CaM is
a key signaling protein responsible for integrating Ca2+ signals to
transcription factors 44. Two important downstream targets of CaM are
calcineurin and CaMK-II 55, 56. As with other kinases, CaMK-II undergoes
autophosphorylation on a threonine residue contained in a phosphopeptide
common to its α and β subunits thereby converting it into a Ca2+/CaM-
independent enzyme 57. The results obtained by employing specific
inhibitors for CaM, calcineurin, and CaMK-II suggested that LPS-induced IL-
12p40 production is regulated by the CaM/CaMK-II pathway. The observation
that both FK506 and CyA inhibited LPS-induced increase in intracellular
free calcium, CaMK-II phosphorylation, and IL-12p40 production in monocytic
cells suggests that this may be a mechanism by which these agents inhibit
Th1 type responses in these cells. The observed enhanced sensitivity of
primary monocytes to the calcium inhibitors compared with the THP-1 cells
may be due to inherent differences between the primary versus the leukemic
cells.
Class Ia PI3K consist of a 85-kDa regulatory subunit α, β, and γ  isoforms
and a 110-kDa catalytic unit α, β, and δ isoforms. Interaction of the  p85
subunit with  phosphorylated  YxxM  motifs  in  transmembrane  receptors  or
adaptor molecules results in the recruitment  of  p85-p110  heterodimers  to
its substrate  phosphatidylinositol  4,5-bisphosphate  49, 50, 58–60  that
leads  to  the  release  of  phosphatidylinositol  3,4,5-trisphosphates  and
eventual phosphorylation of a number of substrates  including  Akt,  protein
kinase A, and protein kinase C  isoforms  48, 59.  There  is  considerable
evidence to suggest that PI3K plays a key  role  in  the  negative  feedback
regulation  of  IL-12p40  production  by  murine  DCs  and  monocytic  cells
32, 33. PI3K was shown  to  negatively  regulate  IL-12p40  production  by
human  monocytic  cells  stimulated  by   the   TLR-2   ligand,Porphyromonas
gingivalis LPS 34 and following ligation of complement receptors  C1q  and
C5a in LPS-stimulated human and murine monocytic cells and DCs 35, 61.  In
contrast, we show that LPS-induced  IL-12p40  production  by  primary  human
monocytes as well as  THP-1  cells  is  regulated  positively  through  PI3K
activation.
Understanding the role  and  functions  of  PI3K  family  members  has  been
difficult  because  of  their  resistance  to  genetic  manipulation,  broad
biological activity of PI3K inhibitors to  inhibit  nearly  all  classes  of
PI3K isoforms 62, and  embryonically  lethal  PI3K  p110α  or  p110β  gene
knockouts 63, 64. There is  evidence  to  suggest  that  the  p85  subunit
independent of the p110α  can  regulate  the  expression  of  several  genes
including  IL-2  65, 66.  Recently,  using   an   RNA   interference-based
approach, the p110α subunit of  PI3K  was  shown  to  regulate  vitamin  D3-
induced adherence and  upregulation  of  CD11b  in  monocytic  cells  51.
Interestingly, herein, we have demonstrated that THP-1  cells  deficient  in
the expression of the p110α catalytic unit of  PI3K  produced  significantly
reduced levels of IL-12p40 in response to LPS  without  any  effect  on  the
expression of IL-10, CD44, B7.1, B7.2, and CD11b  data  not  shown.  These
results suggest that p85/p110α PI3K selectively  regulates  LPS-induced  IL-
12p40 production.
The results of this study in concert with our earlier  observations  showing
the regulation of LPS-induced IL-12p40 production through  the  JNK  pathway
20 suggest that  LPS-induced  IL-12p40  production  is  regulated  by  two
distinct signaling pathways namely  the  CaM/CaMKII-activated  PI3K  pathway
and  the  JNK  pathway.  Interestingly,  both  pathways  regulated  IL-12p40
production through the activation of NFκB and  AP-1  transcription  factors.
It is not clear why the JNK pathway failed to compensate for LPS-induced IL-
12p40 production in the presence of  inhibitors  specific  for  the  calcium
pathway. In addition to signal transduction pathways,  molecular  mechanisms
controlling  gene  expression   involve   chromatin   remodeling   and   DNA
methylation 67–69. Understanding the status of  promoter  methylation  and
chromatin  remodeling  may   elucidate   the   mechanisms   underlying   the
involvement   of   distinct   signaling   pathways   controlling    IL-12p40
transcription.
Herein, we have shown that IL-12p40 production is regulated by NFkB and  AP-
1 through the activation of upstream calcium and PI3K pathways. NFκB and AP-
1 are known to be activated by the calcium/CaMKs  54,70,  although  it  is
not clear if Akt and CaM/CaMK-II can directly interact with the  members  of
the NFκB and AP-1 family members. It is also not clear that in the  presence
of an intact promoter  whether  both  of  these  transcription  factors  can
synergize to  induce  IL-12p40  transcription.  Although  deletion  mutation
studies conducted to define the role  of  individual  transcription  factors
did not reveal a synergistic effect at the  level  of  luciferase  activity,
the potential cooperation between these two transcription factors cannot  be
ruled out. It is possible  that  the  level  of  cooperation  between  these
transcription  factors  is  modulated  following  interaction   with   other
signaling  molecules  activated  by  different  external  stimuli.   Further
studies are needed to elucidate the precise cooperation between NFκB and AP-
1 and possibly other factors involved in IL-12p40 transcription.
The unique role of IL-12p40 in the regulation of IL-12 and IL-23 suggests
that it may be critically involved in the immunopathogenesis of Th1-
mediated inflammatory and autoimmune disorders. Our investigations
regarding the molecular mechanism underlying the inhibitory effects of CyA
and FK506 on IL-12p40 production have revealed for the first time a
positive role of calcium and PI3K and in particular a selective involvement
of p110α subunit of PI3K in regulating IL-12 production by human monocytes.
These findings raise the possibility of developing novel agents capable of
inhibiting calcium and PI3K pathways as potential therapeutics for
disorders related to excessive IL-12 production. The observations that
calcium and PI3K signaling pathways positively regulate human IL-12p40
production in contrast to their negative regulatory role in murine IL-12p40
production highlight the importance of understanding in detail the
mechanism involved in regulating the production of critical cytokines IL-12
and IL-23 in human settings.

