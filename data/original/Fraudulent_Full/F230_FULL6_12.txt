
All blood vessels are lined by endothelium and, except for the  capillaries,
surrounded by one or more layers of smooth muscle cells  SMCs.  These  two
cell populations are the major components in the normal blood  vessel  wall.
Ontogenetically,  blood  vessel  formation   in   the   embryo   starts   by
differentiation of endothelial cells ECs  from  the  splanchnic  mesoderm,
especially fetal liver kinase 1+ Flk1+ mesoderm cells.1 In general, it  is
thought that the subendothelial SMCs differentiate from  mesenchymal  cells,
mesoderm, neural crest or epicardial  cells  and  migrate  from  the  vessel
wall 2–5. Mironov et al 6 reported that the aorta of the  5-  to  12-week-old
embryos consisted of three  sublayers  differing  in  cellular  composition.
Although the majority of embryo aortic cells appeared like  SMCs,  cells  of
the  outer  sublayer  resembled   fibroblasts   or   poorly   differentiated
mesenchymal cells. As both ECs and SMCs may derive from the mesoderm, it  is
possible  that  they  have  a  common  progenitor.  However,  difficulty  in
preparing pure populations of these lineages has hampered dissection of  the
mechanisms underlying vascular formation in the development of  the  embryo.
Recently, a common vascular progenitor was proposed  by  Yamashita  et  al 7
They reported that  Flk1+  cells  derived  from  embryonic  stem  cells  can
differentiate into both ECs and mural cells and can reproduce  the  vascular
organization process.

Mesenchymal stem cells MSCs were  originally  isolated  from  bone  marrow
that has the potential to differentiate into  multiple  lineages,  including
bone, cartilage, adipose and muscle 8–10. In  recent  years,  others  and  we
have demonstrated that subpopulations of MSCs from bone  marrow  could  also
differentiate into ECs under specific conditions 11–13.  In  our  study,  the
typical phenotype of these progenitor cells  is  CD105+,  Flk1+  and  CD34−.
CD105,  or  endoglin,  is  a  homodimeric  membrane  glycoprotein  primarily
expressed on ECs. In association with  transforming  growth  factor  TGF-β
receptors I and II, it can  bind  TGF-β1  and  -β3  and  form  a  functional
receptor  complex 14.  Interestingly,   except   on   ECs   and   adventitial
fibroblasts, CD105 is also found on a minority  of  medial  SMCs  in  normal
coronary arteries 15.

In an effort to study the distribution of CD105+ cells in fetal tissues by
immunohistochemistry, we found that CD105+ cells also exist in a variety of
tissues, including arterial walls data not shown. The fact that CD105 is
reportedly expressed in both ECs and SMCs, and CD105+ cells from bone
marrow are also positive for Flk1, a marker for putative vascular
progenitor cells, 7 prompted us to isolate CD105+ cells from the arterial
walls and investigate their differentiation potential Here, we describe
for the first time the isolation and ex vivo expansion of
Flk1+CD105+CD31−αSMA− vascular progenitor cells from the human fetal aorta
wall that can differentiate at the single-cell level not only into ECs but
also into SMCs, adipocytes and osteocytes.



All human tissues used in this study were procured  under  approval  of  the
Institutional Review Board of Chinese Academy of Medical Sciences  &  Peking
Union Medical College and were in compliance with the Helsinki  Declaration.
Fetal aortas were obtained from 20 to 22 week gestation fetuses.  The  fetal
aorta walls were  excised,  washed  with  D-Hank's  solution  and  carefully
depleted of connective tissues, and ECs  were  scraped  off.  The  remaining
arterial wall was cut  and  digested  for  45  min  at  37°C  with  type  II
collagenase  0.2;  Worthington  Biochemical  Corp,  Lakewood,  NJ,   USA,
followed by  digestion  with  0.1  trypsin.  The  tissues  were  triturated
vigorously and passed through a 70-μm filter, and the cells  were  collected
by centrifugation. The  cells  were  resuspended  and  plated  in  expansion
medium.  Expansion  medium  consisted  of   58   D-MEM/F-12   Gibco   Life
Technologies, Paisley, UK, 40 MCDB-201 medium Sigma, St Louis, MO,  USA,
2  fetal  calf  serum  FCS,   Gibco   Life   Technologies,   10−8   mol/L
dexamethasone Sigma, 10−4 mol/L  ascorbic  acid  2-phosphate  Sigma,  10
ng/mL epidermal growth factor Sigma, 100 U/mL  penicillin  and  100  μg/mL
streptomycin Gibco at 37°C and a 5 CO2 humidified  atmosphere.  When  the
cells reached 75 confluency, they  were  harvested  by  trypsinization  and
CD105+ cells were sorted by means of micromagnetic beads  Miltenyi  Biotec,
Sunnyvale, CA, USA. Flow  cytometry  analysis  showed  that  99.5  of  the
eluted cells  were  CD105+.  Then  the  sorted  cells  were  cultured  at  a
concentration of one single cell per well in 96-well plates pre-coated  with
extracellular matrix ECM gel Sigma supplemented with  3  FCS,  5  ng/mL
interleukin-6 Sigma, 30 ng/mL stem cell factor Sigma and 30  ng/mL  bone
morphogenetic protein-4 BMP-4; Sigma. This is referred  to  as  the  BMP-4
culture system in this paper. After 10 days, single-cell clones were  picked
up and expanded in expansion media supplemented with 10 ng/mL  interleukin-6
and 1 lenolenic acid bovine serum albumin Gibco Culture-expanded  clonal
cells were used in the  following  experiments.  All  the  experiments  were
performed with the 20th passage and the 35th passage; the results  presented
in this article are all with the 35th passage.


For immunophenotype analysis  of  the  expanded  clonal  cells,  cells  were
detached and stained sequentially with primary  antibodies  and  appropriate
fluorescein isothiocyanate FITC-conjugated secondary  antibodies.  Working
concentrations for primary  antibodies  against  human  CD105  PharMingen,
CD31, CD34, CD45, Flk1 Santa Cruz  Biotechnology,  Inc,  CA,  USA,  HLA-DR
Mountain  View,  CA,  USA,  α-smooth  muscle  actin  αSMA;   Dako   Corp,
Carpenteria, CA, USA, smooth muscle myosin  heavy  chain  MHC;  Dako  and
calponin Dako  were  10–20  ng/mL.  For  the  detection  of  intracellular
protein Flk1,  cells  were  permeabilized  with  methanol/phosphate-buffered
saline PBS for two minutes at −20°C. We  used  same-species,  same-isotype
irrelevant antibody  as  negative  control.  After  three  washes  with  PBS
containing 0.5 bovine serum albumin, cells  were  resuspended  in  PBS  and
analyzed using a FACSCalibur  flow  cytometer  Becton  Dickinson,  Mountain
View, CA, USA.


The culture-expanded clonal cells were plated on fibronectin at 2 ×  104/cm2
in EGM-2 medium Clonetics Inc, San Diego, CA, USA with 10  ng/mL  vascular
endothelial growth factor VEGF;  Clonetics  Inc  and  cultured  for  15–20
days 16.  After  differentiation  induction,  cells  were   fixed   with   4
paraformaldehyde and subjected to immunofluorescence analysis for CD31,  Tek
Santa Cruz and  von  Willebrand  factor  vWF  by  confocal  fluorescence
microscopy. To  demonstrate  endothelial   differentiation,   fluorescence-
activated cell sorting FACS and Western blot methods were also  used.  For
Western  blotting,  protein  extracts  were  prepared  from  logarithmically
growing cells both  before  and  after  differentiation  and  analyzed  as
previously described 13.


A total of 2 × 104 cells/cm2 were plated in EGM-2 medium  supplemented  with
50 ng/mL platelet-derived growth factor-BB PDGF-BB, R&D Systems,  MN,  USA
with  medium  exchanges  every  three  days  for  about  15–20  days.  After
differentiation induction, cells were fixed  with  4  paraformaldehyde  and
subjected to immunofluorescence analysis for αSMA, MHC and  calponin.  Cells
were evaluated by confocal fluorescence  microscopy.  Results  were  further
confirmed by FACS analysis and Western blot.

For Western blotting, protein extracts were  prepared  from  logarithmically
growing cells both before and after differentiation  by  lysis  in  buffer
25 mmol/L Tris-HCl, pH 7.4, 0.15 mol/L NaCl,  1  Nonidet  P-40,  1  mmol/L
EDTA and 2 mmol/L EGTA. Then  the  protein  lysates  were  separated  on  a
sodium dodecyl sulfate 7.5 polyacrylamide gel Bio-Rad, Hercules, CA,  USA
and transferred onto nitrocellulose membrane.  After  blocking,  blots  were
incubated with monoclonal antibodies  to  αSMA,  human  smooth  muscle  MHC,
calponin and β-actin at 1:100 in the blocking solution for one hour at  room
temperature. The blots were washed and incubated  with  a  sheep  anti-mouse
horseradish peroxidase-conjugated IgG E I du Pont de Nemours, MA,  USA  at
1:5000 for one hour. Immunodetection using  the  enhanced  chemiluminescence
method ECL kit; Amersham was performed  according  to  the  manufacturer's
instructions.


The culture-expanded clonal cells at 2 ×  104/cm2  were  induced  for  three
weeks in Dulbecco's modified Eagle medium supplemented  with  1  FCS,  1  ×
10−7 mol/L dexamethasone and 1.0 × 10−9 mol/L insulin. At  the  end  of  the
culture, the cells were fixed in 10 formalin for 10 min  and  stained  with
fresh oil red-O solution Sigma to show lipid droplets  in  induced  cells.
Reverse transcriptase–polymerase chain reaction RT–PCR was also  performed
for the detection of adipocyte-specific gene lipoprotein lipase 13.


The culture-expanded clonal cells at 2 × 104/cm2 were induced in  osteogenic
medium 1.0 × 10−8 mol/L dexamethasone, 2.0 × 10−4 mol/L ascorbic acid,  7.0
× 10−3 mol/L β-glycerolphosphate; Sigma for two to three  weeks.  Then  the
cells were stained with Von  Kossa  to  reveal  osteogenic  differentiation.
RT–PCR was also performed for  the  detection  of  bone  cell-specific  gene
osteopontin 13.


We  used   an   established   wound-healing   model   to   investigate   the
differentiation of cells  into  vascular  beds  during  angiogenesis.17  All
animal handling and experimental procedures  were  approved  by  the  Animal
Care and Use Committee of Chinese  Academy  of  Medical  Sciences  &  Peking
Union Medical College. Female NOD/SCID mice 6-  to  8-week-old  were  bred
and maintained under defined conditions  in  individually  ventilated,  HEPA
filtered cages Techniplast, Milan, Italy. Mouse skin was cleaned with  70
alcohol and a full-thickness wound was made by  pinching  a  fold  of  flank
skin and punching through the two layers of skin  with  4-mm  punch  biopsy.
Four hours later, the culture-expanded cells  106  cells  per  mouse  were
injected via the tail vein.  Wound  tissues  were  harvested  together  with
normal skin tissue from the opposite flank 10 days later.  Incorporation  of
donor-derived cells into  wound  vasculature  was  analyzed  by  three-color
immunofluorescence with mAbs against mouse  vWF  Cy5-coupled,  human  αSMA
FITC-coupled and human vWF phycoerythrin PE-coupled.


Our previous studies provided evidence that extensive microvascular injury
is a critical lesion in the gastrointestinal tract syndrome after whole-
body radiation 18. Mice were sublethally irradiated 300 cGy with a cesium
source MDS Nordion; Gammacell, Ottawa, CA, USA prior to transplantation
of 106 cultured cells via the tail vein. Mice were killed two months later.
Incorporation of donor-derived cells into small intestine vasculature was
analyzed by the above three-color immunofluorescence method.



Previously we found that CD105+ cells could be detected beneath the
endothelium of the arterial arch, including the intima, media and
adventitia of the vessel, by immunochemistry. Here we isolated CD105+ cells
from the fetal aorta walls. To prove single-cell derivation of the
differentiated cell types, the sorted CD105+ cells were cultured at a
concentration of one cell per well in 96-well plates pre-coated with ECM
gel in the BMP-4 culture system. Wells with single adherent cells were
identified during the first 24 h. The appearance of cell colonies was
checked daily. Wells that appeared to contain more than one colony were
discarded. After 20 days, a total of 49 colonies were found, but only four
were successfully passaged. Cell phenotype analysis showed that 99 of the
cells expressed Flk1 and CD105, but did not express CD34, CD45, CD11a,
CD11b, CD31 and vWF. They also did not express vascular smooth muscle
marker αSMA, MHC and calponin Figure 1. The cells were fibroblast shaped
or polygonal


Until now, there has been no evidence that cells from human vascular walls
could differentiate into cells with endothelial characteristics. In the
present study, cell clones were harvested, expanded and divided into
portions for different cell differentiation induction Figures 2a and b.
When clonal cells were cultured in endothelium-specific induction media
EGM-2 media supplemented with VEGF for three weeks, cells adopted a
cobblestone-like morphology, typical of ECs Figure 2c. Figure 2d showed
the cells cultured in vascular smooth muscle-specific induction media see
below for details.

We then evaluated these progenies for markers of endothelial
differentiation. Undifferentiated cells did not express CD31, Tek and vWF.
Following treatment with VEGF for 21 days, about 70 of cells expressed
CD31, Tek and vWF Figure 3a. Parallel experiments with human umbilical
vascular ECs served as positive control. Results were further confirmed by
FACS analysis Figure 3b and Western blot Figure 3c.

When the clonal cells were cultured in vascular smooth muscle-specific
induction media EGM-2 media supplemented with PDGF-BB for three weeks,
they assumed a typical ‘hill and valley’ morphology Figure 2d.
Undifferentiated cells did not express α-SMA, MHC and calponin. Following
treatment with PDGF-BB for 21 days, about 80 of cells expressed αSMA, MHC
and calponin Figure 4a. Parallel experiments with human vascular SMCs
hVSMCs served as positive control. Results were further confirmed by FACS
analysis Figure 4b and Western blot Figure 4c. As these cells could
differentiate into ECs and SMCs, we term these cells multipotent vascular
progenitor cells MVPCs.

When clonal MVPCs were cultured in adipocyte-specific induction media for
two weeks, more than 90 of cells differentiated into lipid-laden cells
that stained by oil-red O Figures 5a–d. Results were further confirmed by
RT–PCR analysis for expression of adipocyte-specific gene lipoprotein
lipase Figure 5e.

When clonal MVPCs were cultured in osteogenic medium for two to three
weeks, more than 90 of cells could be induced into osteogenic lineage as
demonstrated by Von Kossa staining Figures 6a and b. Results were further
confirmed by RT–PCR analysis for expression of bone cell-specific gene
osteopontin Figure 6c.


To determine whether MVPCs could contribute to angiogenesis of the injured
gastrointestinal tract and in the skin wound model, triple-color
immunofluorescence was used to observe ECs and SMCs in the same tissue
sections. As expected, in mice that had received MVPC transplantation,
cells expressing human EC marker vWF were seen in the skin injury section.
Among them were cells positive for SMC marker αSMA Figures 7A and B. No
staining was seen in non-transplanted animals data not shown. In small
intestine tissue sections, cells stained by antibody against human vWF, but
not human αSMA, were seen Figures 7C and D.




To our knowledge, Sainz et  al 19  provided  the  first  evidence  that  the
normal arterial wall harbors side population cells with vascular  progenitor
properties in adult mice. We describe here for the first time the  isolation
and ex vivo culture of MVPCs from the human fetal vascular  wall  that  can,
at the single-cell level, generate mesenchymal osteoblasts, adipocytes  and
smooth muscle and  visceral  mesodermal  endothelial  cell  types.  After
transplantation, these MVPCs could  contribute  to  injury-induced  vascular
regeneration. These findings  may  have  many  implications  physically  and
pathologically.

Ontogenetically,  blood  vessel  formation   in   the   embryo   starts   by
differentiation of  ECs  from  the  splanchnic  mesoderm,  especially  Flk1+
mesoderm cells 1. Postnatal neoangiogenesis, in  response  to  tissue  injury
and  remodeling,  was  previously  thought   to   occur   solely   via   the
proliferation  and  migration  of  ECs  from  pre-existing  vasculature.  In
addition,  recent  studies  suggest  that  endothelial  progenitors  located
within  the  peripheral  blood  and  bone  marrow,  appear  to  function  in
neovascularization and angiogenesis, and perhaps also  re-endothelialization
of injured arteries, and significantly contribute to injury- and  pathology-
induced neovascularization 20–22.  In  the  present  study,  we  showed  that
Flk1+CD105+ MVPCs could differentiate into ECs under the induction of  VEGF.
To our knowledge, only Hu et al 23  mentioned  in  the  Discussion  part  of
their recent report that endothelial progenitor cells  may  also  reside  in
the vessel walls.

Similarly, the progenitor for vascular  SMCs  remains  a  controversy.  SMCs
have different embryonic origins, depending on the segment of  the  arterial
system involved. In some vertebrates, SMCs  in  the  upper  portion  of  the
thoracic aorta are derived from a neuroectodermal source, whereas  those  in
the abdominal  aorta  are  derived  from  a  mesenchymal  source 3.  Although
likely, this has  not  been  confirmed  in  humans.  The  SMCs  of  coronary
arteries appear to originate  from  a  third  precursor  population  in  the
intracardiac mesenchyme 2. SMCs within the media of  large  arteries  may  be
heterogeneous,   with   different   proliferative    and    matrix-producing
capabilities 4. Also, bone marrow cells were demonstrated to be a  source  of
SMCs.24

To complicate matters further,  transdifferentiation  of  ECs  to  SMCs  was
reported. DeRuiter et al 25  reported  that  vascular  ECs  can  delaminate,
migrate  into  the  subendothelium   and   transdifferentiate   into   SMCs.
Arciniegas et al 26 reported that adult bovine  ECs  can  transdifferentiate
into  SM-like  cells  in  vitro.  They  showed  TGF-β1  induced  α-SM  actin
expression in aortic ECs, whereas expression of factor VIII-related  antigen
was lost. While we were preparing this manuscript, a  new  study  by  Hu  et
al23 showed that Sca-1+ progenitor cells purified  from  the  mouse  aortic
root can migrate through an irradiated vein graft to the  neointima  of  the
vessel and transdifferentiate into SMC-like neointimal cells that  expressed
the  early  SMC  differentiation  marker  gene  SM22.  Thus  it  seems  that
progenitors for SMCs may be  more  heterogeneous.  Our  study  provides  new
insights into the origin of cells populating the developing neointima.

On the other hand, one of the most important findings of this study  is  the
differentiation  of  progenitor  cells  toward  adipocytes  and  osteocytes.
Atherosclerosis  is  a  chronic  fibroproliferative  inflammation   of   the
arterial wall brought about and exacerbated by the  dynamic  interaction  of
disordered  lipid  metabolism  and  other  insults  and  risk  factors  with
homeostatic mechanisms designed to protect against such injury 27,28.  Fatty-
streak formation is a typical pathological change in atherosclerosis.  Fatty
streaks initially consist of lipid-laden  monocytes  and  macrophages  foam
cells together with  T  lymphocytes.  Later  they  are  joined  by  various
numbers of SMCs, foam cells and platelets. As  MVPCs  could  be  induced  to
transdifferentiate  into  adipocytes  in  vitro,   it   is   reasonable   to
hypothesize  that  this  process   may   occur   in   the   development   of
atherosclerosis. In the development of atherosclerosis, deposition of  lipid
is a critical step.  The  recruitment  of  circulating  monocytes  into  the
vascular intima and their subsequent  transformation  into  macrophages/foam
cells are key elements of the initiation of atherosclerosis. In the  present
study, we found that MVPCs had the potential  to  differentiate  into  lipid
cells. Whether  this  happens  in  vivo  is  an  interesting  question  that
deserves further study.

In  arterial  plaque,  there  are  a  number  of  cell  types  among   them
mononuclear phagocytes, T cells, B cells,  natural  killer  cells  and  mast
cells, all thought to originate from circulating hematopoietic  and  immune
precursors. But two key cell  types,  ECs  and  SMCs,  are  presumed  to  be
resident arterial cells. According to  this  model,  proliferating  arterial
cells are derived from  the  clonal  expansion  of  rare  resident  arterial
progenitor cells in response to specific stimuli 29. These findings even  led
to the more extreme model that views plaque as a benign  SMC  tumor  in  the
arterial wall. Identification of putative  vascular  progenitor  cells  that
give rise to different cells may well fit this benign SMC  tumor  model.  To
explain the presence of progenitor cells in the vessel, one  possibility  is
that these cells are reserved  during  development  from  embryo  to  adult,
although there is a possibility that they may come from bone marrow.

Calcification is a  common  feature  of  advanced  atherosclerotic  lesions.
Atherosclerotic plaque calcification, especially in the  coronary  arteries,
decreases vessel elasticity,  augments  plaque  brittleness,  and  leads  to
increased clinical complications such  as  myocardial  infarction,  impaired
vascular  tone,  dissection  in  angioplasty,  poor  surgical  outcome   and
coronary insufficiency due  to  loss  of  aortic  recoil 30,31.  Despite  its
clinical  significance,  the  molecular   mechanisms   regulating   vascular
calcification are unclear.  Historically,  ectopic  calcification  has  been
considered  a  passive  process  involving  spontaneous  calcium   phosphate
mineral  precipitation  in  necrotic  tissue.  However,  several  lines   of
evidence  have  recently  emerged  supporting  the  concept   that   ectopic
calcification, like the mineralization  of  bones  and  teeth,  is  a  cell-
regulated  process.  Then  what  kinds  of  cells  contribute  to   vascular
calcification?  Recently,  an  SMC  phenotypic  transition  associated  with
vascular calcification has been  proposed  by  several  authors.  Steitz  et
al 32 showed that bovine aortic SMCs lost their lineage markers,  SM22α  and
smooth muscle α-actin, within 10  days  of  being  placed  under  calcifying
conditions.  Conversely,  the  cells  gained  an  osteogenic  phenotype   as
indicated by an increase in  expression  and  DNA-binding  activity  of  the
transcription factor,  core  binding  factor  α1  Cbfa1.  Moreover,  genes
containing the Cbfa1 binding site, OSE2, including osteopontin,  osteocalcin
and  alkaline  phosphatase,  were  elevated 32.  Shioi  et  al 33   described
increases in osteopontin transcripts and a reduction in SM  α-actin  protein
levels using a β-glycerophosphate-induced bovine vascular SMC  calcification
model. In vitro studies also suggest  that  vascular  pericytes  retain  the
capacity to differentiate into osteoblast-like cells in the artery wall 34.

These studies all indicate that pathological calcification of blood  vessels
shares features  with  normal  bone  formation.  Proteins  involved  in  the
regulation of skeletal bone formation were present in human  atherosclerotic
lesions.  These  proteins  include  osteoprotegerin  and  its  ligand,  bone
sialoprotein,  BMP  -2  and  BMP-4,  osteocalcin,  osteonectin,  matrix  Gla
protein  and  osteopontin.  Considering   the   similarities   between   the
calcification of arteries and osteogenesis, it is possible that  progenitors
of osteoblasts may also exist in the arterial wall. In  the  present  study,
we isolated vascular progenitor cells  that  could  be  induced  to  undergo
calcification similar to mineralization in skeletal  tissue,  including  the
expression  of  osteopontin.  Hence  these  progenitor  cells   share   many
phenotypic features with MSCs isolated from bone marrow.

In conclusion, it seems that there is at least a less studied cell
population in the vessel wall. What is the role of this cell population in
the physiology and pathology of blood vessels? Do ECs and SMCs convert
their phenotype during developmental or pathological processes and give
rise to an interim phenotype that is CD105+? Here, we describe for the
first time the isolation and ex vivo expansion of cells from the human
fetal aorta wall that can differentiate at the single-cell level not only
into osteoblasts, chondrocytes, SMCs and adipocytes, but also into cells of
endothelium in vitro. When transplanted into mice, they can engraft into
new vessels and adopt an endothelial fate. Thus we found a possible common
origin for aortic ECs and SMCs. An extension of this finding may imply that
vascular progenitors are able to differentiate into all types of
mesenchymal cells.
