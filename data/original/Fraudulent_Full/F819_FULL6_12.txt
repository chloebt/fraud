Malignant pleural mesothelioma MPM is an uncommon, locally
invasive, and rapidly fatal malignancy with an incidence that
continues to increasing in the Western world. It is linked to asbestos
exposure and genetic susceptibility 1.
Despite the best and most aggressive, often integrated, standard therapeutic approaches, overall
survival remains very poor 2.
The failure rate points clearly to the need
for the development of novel therapy for patients with MPM. In this
setting, one of the promising paths of experimentation is artificial
induction of apoptosis. To our knowledge, however,
the mechanism and pathway of induction of apoptosis
in MPM is known only partially 3,4. In this malignancy,
which is extremely rich in peculiarities, p53 mutations
are uncommon 3–5. This certainty may suggest that
alterations in the expression of this gene are unlikely
to be responsible for either the creation of an MPM
phenotype or MPM insensitivity to apoptotic induction itself. In simian virus 40 SV40-positive MPM,
this occurrence may be explained by an inactivation of
the p53 function coming from the viral TAG 6.
Homozygous deletions of the INK4a/ARF locus, however,
have been shown to be the predominant events that
occur at a frequency of  70 in this malignancy 7–9.
This deletion results in the loss of p14ARF, in the increase of MDM2, and in the functional inactivation of
p53, thereby diminishing the p53 response to genotoxic stress 10,11. It was reported previously that the
p14ARF transfection resulted in apoptotic cell death
through the induction of p14ARF overexpression 12.
Conversely, it is interesting to note that others found
that high BCL-XL expression is relatively uniform for
this tumor, in which BCL-2 expression usually is
rare 13,14. BCL-X, as a result of alternative splicing, encodes two death regulators that exhibit opposing activities. BCL-XL exerts a protective effect, whereas the
shorter form, BCL-XS, is proapoptotic 15. The National
Cancer Institute Anticancer Drug Screen NCI-ACDS
study16 reported a strong negative correlation between
basal BCL-XL protein and mRNA levels and drug sensitivity. Conversely, BAX and BCL2 showed no correlation at all with drug sensitivities. These observations
suggest that BCL-XL may play a unique role in general
resistance to cytotoxic agents 70,000 drugs were considered in the study by Amundson et al 16. The broad
nature of the association between endogenous BCLXL expression and protection from chemotoxicity indicates that this gene is an extremely important general determinant of cell death and suggests a p53-
independent mechanism of BCL-XL action as a major
component of chemoresistance in tumor cells.
The main pathway involved in inducible resistance is the activation of nuclear factor B NF-B
within tumors in response to chemotherapy or to tumor necrosis factor TNF.17–19 NF-B suppression of
apoptosis appears to be a transcriptional event, because it activates expression of TRAF-1 and TRAF-2
and of c-IAP1 and c-IAP2 to block caspase 8 activation 19,20. It is interesting to note that other antiapoptotic genes that are activated transcriptionally by
NF-B include BCL-XL 21.
Different studies have described the inducible antiapoptotic function of the transcription factor NF-B
as a principle mechanism by which cancer cells are
protected from undergoing programmed cell death
after exposure to TNF,  irradiation, and antineoplastic drugs, including SN38 19,22–25. Activation of the transcription factor NF-B by extracellular signals involves
its release from the NF-B inhibitor  protein IB-
in the cytoplasm and subsequent nuclear translocation 19. NF-B modulates the expression of many genes
that control cell survival19Although some of the target
gene products are protective and others induce cell
death, the primary role of NF-B is to promote cell
survival, because massive apoptosis of liver cells has
been observed in embryonic NF-B knockout mice 26.
Paradoxically, NF-B can be activated by anticancer
agents, including topoisomerase I and II inhibitors 19.
It is accepted generally that camptothecin CPT induces NF-B activation during the process of apoptosis in different mammalian cells, such as HeLa cells,
colon carcinoma cells, and lung carcinoma cells.
Huang et al27 have elucidated a series of nuclear
events induced by CPT that converge with cytoplasmic
signaling events responsible for the activation of NF-
B, which can provide an antiapoptotic function, and
showed that inhibition of NF-B augments CPT-induced apoptosis. Similar results were obtained by Cusack et al,23,24 who showed enhanced chemosensitivity to CPT inhibiting NF-B activation through
adenovirus-mediated transfer of the superrepressor
IB- or utilizing the proteasome inhibitor PS-341. To
understand better the response of tumor cells to CPT
and to identify potential targets for adjuvant therapy,
Carson et al 28 examined the global changes in mRNA
abundance in HeLa cells after CPT treatment using
Affymetrix U133A Gene Chips, which include all annotated human genes 22,283 probe sets. This pharmacogenomic approach led those authors to identify
two pathways that are CPT-induced: 1 the epidermal
growth factor receptor; and 2 NF-B-regulated antiapoptotic factors. In that study, the authors concluded
that inhibitors of these respective pathways enhanced
the cytotoxicity of CPT superadditively, suggesting
their potential as targets for adjuvant therapy with
CPT. We previously showed that TNF increased CPT
sensitivity through the inhibition of NF-B activation
shortening of its duration in a human ovarian carcinoma cell line 25.
We used this therapeutic approach shortening of
NF-B persistence in human MPM; thus, TNF was
administered in combination with SN38 the active
metabolite of irinotecan in 8 human MPM cell lines
MSTO-221H, IST-MES1, IST-MES2, MPP89, H28,
H513, H2052, and H290. The actions of TNF on 1
SN38-induced NF-B activation, 2 the level of apoptosis, 3 the transcription of BCL-XL, and 4 the activation of apoptotic effectors caspases were evaluated.
 
NCI-H28, H513, and H290 mesothelioma cells were a
kind gift from Dr J D Minna Hamon Center for
Therapeutic Oncology Research, Dallas, TX. H2052
CRL-5915 and MSTO-211H CRL-2081 mesothelioma cells; renal carcinoma A498, ACHN, and CAKI-1
cells; prostate carcinoma PC-3 cells; and lymphoid
HL60 and K562 cells were obtained from American
Type Culture Collection Manassas, VA. IST-MES1,
IST-MES2, and MPP89 mesothelioma cells were kindly
donated by Dr S Ferrini National Institute for Research on Cancer, Genoa, Italy. All cells were cultured
in RPMI-1640 complete medium containing 10 fetal
calf serum. Cell counts were determined using a
Coulter Counter with channelyzer attachment to
monitor cell size Coulter Electronics, Hialeah, FL.
Cell membrane integrity was determined with a Trypan blue-dye exclusion assay.
 
SN38, which is the active metabolite of the topoisomerase I Top I inhibitor irinotecan, was kindly
provided by Dr J Patrick McGovren Pharmacia Upjohn Inc, Kalamazoo, MI. Recombinant human TNF
rHuTNF was obtained from KNOLL-BASF Ludwigshafen, Germany. A stock solution of rHuTNF that
contained 0.1 mg/mL of protein was stored at 80 °C.
Specific activity was 8.74 106
U/mg protein 1000
U/mL  1.38 ng/mL; 48-hour L929 bioassay without
Actinomycin D, as determined in the KNOLL-BASF
laboratory. The caspase 3 inhibitor, z-DEVD-fmk, was
obtained from Calbiochem San Diego, CA. The proteasome inhibitor dipeptide boronic acid analog PS-
34123 was provided by Millennium Pharmaceuticals
Cambridge, MA.
 
Drug-induced cytotoxicity was determined using a
standard clonogenic assay drug exposure for 1 hour,
as described previously.25 Cell colonies were counted
after 10 days.
The 50 inhibitory concentration IC50 values
were extrapolated from the dose-response curves, as
estimated by performing a simple linear regression: Y
 log fa
/fu
 versus X  log drug concentration, in
which fa
is the fraction affected by the dose calculated
as a ratio treated cells to untreated cells, and fu
is the
unaffected fraction ie, 1-fa 29,30.

Morphologic assessment was determined either
by staining cells with 4,6-diamidino-2-phenylindole
DAPI or by transmission electron microscopy TEM
analysis. Briefly, 1 105
cells were seeded on a slide 24
hours before treatment; after treatment, slides were
washed twice in phosphate buffered saline PBS, fixed
for 5 minutes at room temperature in 95 ethanol, air
dried, and then dipped for 5 minutes in 0.1 g
DAPI/mL in a methanol solution. One thousand cells
were scored for each slide. 2 Untreated and treated
MSTO-211H cells attached and suspension cells, respectively were fixed for 2 hours at 4 °C in 2.5
glutaraldehyde and 0.5 tannic acid in 0.1 M cacodylate buffer, pH 7.4, and analyzed by JFE Enterprises
Brookeville, MD at a magnification of  2000.

Cytosolic extracts were prepared as described previously.31 In brief, MSTO-211H cells were harvested by
gently scraping and were incubated in a buffer containing 220 nM mannitol and 60 mM sucrose on ice for 30
minutes. Then, cells were broken in a Dounce homogenizer by 70 gentle strokes of a type B pestle. The homogenates were centrifuged at  16,000 g for 15 minutes, and the mitochondria-free supernatant fluids were frozen at 70 °C until further analysis. Both extracts of the
pellets and whole cell extracts were obtained by dissolving in lysis buffer, followed by repetitive vortexing, and
freeze thawing. After centrifugation at  16,000 g, the
supernatant fluids were stored at 70 °C. To control for
similar protein loading or contamination of cytosolic
extracts with mitochondria, the immunoblots see
above were stripped and reprobed with antiactin antibodies diluted 1:3,000 or anticytochrome c oxidase antibodies diluted 1:300; COX Vb, Boehringer, Mannheim, Germany.
 
Samples were washed twice with PBS, scraped off the
plates, and lysed in cell lysis buffer 50 mM Tris-HCl,
pH 7.5; 150 mM NaCl; 1 Nonidet P-40; 0.5 sodium
deoxycholate; and 0.1 sodium dodecyl sulphate
SDS. Whole cell lysates were boiled, and the protein
concentration was determined with the Bradford assay Bio-Rad Laboratories, Hercules, CA. Equal
amounts of protein 20 – 40 g were separated by
SDS-polyacrylamide gel electrophoresis under reducing conditions in 4 –20 linear gradient polyacrylamide gels Ready-Gel; Bio-Rad Laboratories. After
the proteins were transferred to an Immobilon-P
membrane Millipore Corp., Bedford, MA, the membranes were blocked with 5 nonfat milk powder and
0.2 Tween 20 in Tris-buffered saline TBS-T overnight at 4 °C and then incubated with the primary
antibody for 1 hour at room temperature. Membranes
were then washed in TBS-T for 3 5-minute periods.
Primary antibodies for p14ARF, p16INK4a, caspase 8,
caspase 9, Bcl-2, Bax, BCL-XL, IB-, and actin were
obtained from Santa Cruz Biotechnology Santa Cruz,
CA. Polyclonal antibody to caspase 3 and monoclonal
antibodies to polyADP-ribose polymerase PARP
and cytochrome c were obtained from PharMingen
San Diego, CA. Horseradish peroxidase-conjugated
goat antimouse or donkey antigoat antibodies were
used as secondary antibodies Santa Cruz Biotechnology. Proteins were visualized with chemiluminescent
luminol reagents Santa Cruz Biotechnology.
 
Cells were plated in log phase in T75 flaks 2700 cells/
cm2 in complete medium for 24 hours; treated for 1
hour with SN38 alone, TNF alone, or combined SN38
and TNF; incubated in drug-free medium for additional 24 hours; then counted before flow cytometric
analysis. Samples were prepared for flow cytometry
essentially as described previously 25. Briefly, cells were
washed with 1  PBS, pH 7.4, and then fixed with
ice-cold 70 ethanol. Samples were washed with 1
 PBS and stained with propidium iodide 60 g/mL
Sigma Chemical Company, St Louis, MO containing
RNase 2 g/mL Sigma Chemical Company for 30
minutes at 37 °C. Cell cycle analysis was performed
using a Becton Dickinson fluorescence-activated cell
analyzer and Cell Quest software version 1.2; Becton
Dickinson Immunocytometry Systems, Mansfield,
MA. For each sample, at least 15,000 cells were analyzed, and quantitation of the cell cycle distribution
was performed using Modfit LT software version
1.01; Verity Software House Inc, Topsham, ME.
 
Alkaline elution was performed to assess DNA damage by detecting DNA strand breaks as described
previously 25,32. Before alkaline elution and drug
treatments, cells were radiolabeled with 0.02
Ci/mL of 14C thymidine for 24 hours 2 doubling
times at 37 °C and then chased in nonradioactive
medium for 4 hours. After drug treatments 1 hour,
cells were scraped in Hanks balanced salt solution,
counted, and aliquots of cell suspensions were
placed in drug-containing ice-cold Hanks balanced
salt solution. After alkaline elution, filters were incubated at 65 °C with 1 N HCl for 45 minutes; then,
0.04 M NaCl were added for an additional 45 minutes. Radioactivity in all fractions was measured
with a liquid scintillation analyzer Packard Instruments, Meriden, CT.

DNA single-strand breaks SSB were assessed by alkaline elution under deproteinizing, DNA denaturing
conditions. Briefly, after treatment, radiolabeled cells
were harvested at 4 °C, loaded onto polycarbonate
filters 2-m pore size; Poretics, Livermore, CA, and
lysed with SDS buffer 0.1 M glycine; 0.025 M ethylenediamine tetraacetic acid EDTA; 2 weight/volume SDS; and 0.5 mg/mL proteinase K, pH 10.0. The
lysis solution was washed from filters with 0.02 M
EDTA, pH 10, and the DNA was eluted with tetrapropylammonium hydroxide-EDTA, pH 12.1, containing
0.1 SDS at a flow rate of 0.035 mL per minute into 5
fractions at 3-hour intervals.
 
Nucleic extracts were prepared according to the
method described by Vikhanskaya et al 31. Briefly, 5 105
cells were collected, washed in PBS, and pelleted. Pellets were resuspended in 400 mL of hypotonic buffer 20 mM N-2-hydroxyethyl piperazine-N-
2-ethane sulphonate HEPES, pH 7.9; 10 mM KCl; 0.1
mM EDTA; 0.1 mM ethylene guanine tetraacetic acid
EGTA; 1 mM dithiothreitol DTT; and 0.5 mM
phynel methyl sulfonyl fluoride PMSF. The cells
were allowed to swell on ice for 15 minutes; then, 25
L of an 18 solution of Nonidet NF-40 were added,
and the tubes was vortexed vigorously for 10 seconds.
The homogenate was centrifuged for 30 seconds in a
microfuge. The nuclear pellet was resuspended in 50
L ice-cold buffer 20 mM HEPES, pH 7.9; 0.4 M NaCl;
1 mM EDTA; 1 mM EGTA; 1 mM DTT; and 1 mM
PMSF, and the tubes were rocked vigorously at 4 °C
for 15 minutes. Nucleic extracts were centrifuged for 5
minutes in a microfuge at 4 °C, and the supernatant
fluid was frozen as aliquots at 70 °C. One milligram
to 3 mg of each cell treatment were incubated on ice
for 30 minutes in 15 mL of buffer containing 10 mM
Tris, pH 7.5; 50 mM NaCl; 1 mM EDTA; 1 mM DTT; 3
Mg poly dI-dC; 2 mL pab65 Santa Cruz Biotechnology or nonspecific antibodies; 1I ng of 32P endlabeled oligonucleotide; and part of the enhancer sequence from the human immunodeficiency virus LTR
region ENH7 from 115 to 81: GCTTGCTACAAGGGACTTTCCGCTGGGGACTTTCC was added for another 15 minutes at room temperature. DNA-protein
complexes were separated by electrophoresis through
5 native polyacrylamide gels, dried, and visualized.31
Internucleosomal DNA fragmentation was shown by the
harvesting of total cellular DNA, as described previously 33. Briefly, adherent cells and detached cells were harvested separately, washed, and lysed with 50 mmol/L
Tris, pH 7.5; 10 mmol/L EDTA; 0.5 Triton-X-100; and
0.5 mg/mL proteinase K for 2 hours at 50 °C. Samples
were then extracted twice with phenol/chloroform/
isoamyl alcohol and precipitated with ethanol. The pellet
was resuspended in Tris-EDTA and 10 g/mL ribonuclease A, and the DNA was separated on a 2 agarose
gel.

One microgram of total RNA was reversed transcribed
for 1 hour at 37 °C in 20-L reactions containing 50
mM Tris, pH 8.3; 75 mM KCl; 3 mM MgCl2
; 500 M
each deoxyadinosine triphosphate dATP, deoxyguanosine triphosphate dGTP, deoxycytidine
triphosphate dCTP, and deoxythymidine triphosphate dTTP; 7.5 g/mL random hexamers Promega
Corp., Madison, WI; 10 mM DTT; and 200 U Moloney
murine leukemia virus reverse transcriptase Life
Technologies, Inc, Bethesda, MD. For the detection
of BCL-XL mRNA, 2 L of the reverse-transcribed material was amplified by polymerase chain reaction
PCR in 50-L reactions containing 10 mM Tris, pH
8.3; 50 mM KCl; 1.5 mM MgCl2
; 200 M each dATP,
dGTP, dCTP, and dTTP; 0.2 M of each primer; and
0.25 U Amplitaq DNA polymerase Applied Biosystems
formerly Perkin-Elmer Applied Biosystems, Foster
City, CA for a total of 28 cycles consisting of 94 °C for
45 seconds, 55 °C for 60 seconds, and 72 °C for 60
seconds, with a final extension step at 72 °C for 10
minutes. Amplification products were analyzed by
electrophoresis in ethidium bromide-stained agarose
gels. Primer sequences were as follows: BCL-XL, 5-
CCCAGAAAGGATACAGCTGG-3forward and 5-
GCGATCCGACTCACCAATAC-3reverse; TNF-R1,
5-AGTGCTGTTGCCCCTGGTCAT-3sense, 5-
TTTTCAGGTGTCGATTTCCCA-3antisense; and
glyceraldehyde 3-phosphate dehydrogenase, 5-GGT
CAT CCC TGA GCT GAA CG-3and 5-TTC GTT GTC
ATA CCA CGA ATT G-3.
 
Both parametric statistics and nonparametric statistics the Student t test and the Mann–Whitney U test,
respectively; not significant P  0.05 were used. For
each drug combination, the combination index CI
CI  0.3, strong synergism; CI  1, additive; CI  1, antagonism and its 95 confidence limits were estimated.
 
MPM cells were exposed to rHuTNF or to SN38 for 1
hour and then plated for 10 additional days in drug-free
medium. The IC50 was reached only after exposure to
tremendously high concentrations of TNF  25,000
U/mL Table 1. TNF resistance was not correlated with
the absence of specific TNF receptors TNF-R1s; therefore, overall, human MPM cell lines MSTO-221H, ISTMES1, IST-MES2, MPP89, H28, H513, H2052, and H290
showed TNF-R1 mRNA fig 1A. Conversely, MPM cells
were very sensitive to SN38 Table 1.
All MPM cell lines were examined for p14ARF/
p16INK4a protein expression. In MSTO-211H, ISTMES1, H28, H513, H2052, and H290 MPM cell lines,
endogenous p14ARF and p16INK4A expression was absent the C33A cervical carcinoma cell line was used as
a positive control for p14ARF/p16INK4a expression.
Only IST-MES2 and MPP89 cells expressed the p14ARF
and p16INK4a proteins fig 1B. Data obtained in H28,
H513, H2052, and MSTO-211H cells were in agreement with previous reports 8.
Consequently, the sensitivity to SN38 or to TNF Table 1 was independent of
p14ARF/p16INK4a protein expression.
When MPM cell lines were incubated simultaneously with different concentrations of SN38 and
with a nontoxic concentration of TNF 1000 U/mL, a
strong synergism was observed. Table 1 shows the IC50
values obtained for SN38 or TNF as single agents and
for the combination SN38 and TNF. Table 1 reports
also the combination index and the confident limit at
95.
Different drug schedules were chosen for all of
the following experiments: 1 rHuTNF, 1000 U/mL
in all experiments; 2 SN38 at the concentrations
needed to reach the IC50 value as reported in Table
1; equiactive; 3 SN38 at the nontoxic concentration
of 1.0  103
nM the survival fraction was  95 in
overall cell lines; equimolar; and 4 SN38 plus
rHuTNF, 1000 U/mL. The exposure time was 1 hour.
After drug removal, cells were allowed to grow for an
additional 3–24 hours in drug-free medium.
 
TNF administered simultaneously, as reported by
different authors 33 and in our previous articles, 25,32
increased the level of DNA-SSB under-deproiteinizing condition induced by CPT. In MSTO-211H
cells, SN38 1-hour treatment at the IC50 induced
DNA breaks, as shown in figure 2. The combination
of SN38 and TNF, as expected, increased strongly
the fraction of eluted DNA from filters fig 2A. One
hour after drug removal, DNA eluted slower; and,
after 3 hours, its kinetics were superimposable with
the kinetics of the control fig 2B.

Table 2 shows that TNF alone induced a small but
statistically significant P 0.002 Student t test G2
-M
arrest; whereas SN38, at the IC50 value, induced a
strong S arrest and a small fraction of sub-G0
-G1
population. SN38 at nontoxic concentrations did not affect the composition of the cell cycle. The combination of SN38 1.0  103
nM plus TNF 1000 U/mL
Table 2 induced G2
-M arrest as well as TNF alone, a
depletion of the S-phase, and a significant fraction of
the sub-G0
-G1
cell population. This fraction was
higher than that induced by SN38 alone at the IC50
value.

Overall, MPM cell lines did not have constitutive, activated NF-B complexes figs 3, 4, similar to some
control cells renal carcinoma A498, ACHN, and
CAKI-1 cells and human lymphoid HL60 and K562
cells fig 3A. The human prostate carcinoma PC3
cell line fig 3A represented a positive control high
level of constitutive activated NF-B complexes 34.
The kinetics of formation and persistence of
NF-B complexes were studied initially by electrophoretic mobility-shift assay in MSTO-211H cells.
rHuTNF at 1000 U/mL, according to our previous
results,20,24 induced NF-B complexes after a short
treatment; they were evident after 30 minutes of
incubation fig 3B,C. SN38 at the IC50 value did
not induce NF-B complexes until 3 hours fig 3C.
The combination of SN38 and rHuTNF induced
NF-B complexes with a pattern of activity similar
to TNF that was reduced in SN38-treated cells
fig 3B.
The generation of NF-B complexes also was
analyzed in MSTO-211H cells that were treated for
24 hours. NF-B complexes remained for 12 hours
in TNF-treated cells fig 3D, whereas SN38-treated
cells IC50 value were present for 12 hours fig 3D
and disappeared after 24 hours fig 3D In SN38
plus TNF-treated cells fig 3D, NF-B complexes,
which were visible weakly after 3 hours, disappeared
quickly and became undetectable at 6 hours.
The persistence of NF-B complexes was evaluated in all MPM cell lines that were treated for 6
hours with SN-38 at the IC50 value, with TNF 1000
U/mL, or with combined SN38 and TNF fig 4A. In
all cell lines, the NF-B complexes induced by the
combination disappeared completely after 6 hours
fig 4A.
To assess the specific effects of the combination of
TNF and SN38 on shortening the persistence of NF-B
complexes, the activity of PS-341 was evaluated in
MSTO-211H cells fig 4B. It is known that PS-341
enhances SN38 chemosensitivity through the inhibition of NF-B activation induced by SN38.23 Treatment with PS-341 alone 1 M for 1 hour did not
result in activation of NF-B fig 4B. PS-341 was able
to inhibit the constitutive activation of NF-B in PC3
cells, as reported previously.35 In MSTO-211H cells,
pretreatment with PS-341 prior to drug administration
incompletely inhibited the activation of NF-B induced by exposure to SN38 or to TNF.
Normally, NF-B resides in the cytoplasm as an
inactivated form in a complex with IB-. IB-
modulates the function or activity of NF-B through
its proteolytic degradation in response to different
extracellular stimuli, such as TNF.19. This process
results in the translocation of NF-B to the nucleus.19 Overall, MPM cell lines showed IB- protein
degradation fig 5 after TNF treatment or SN38
treatment, whereas the combination treatment upregulated IB- protein expression 6 hours after
treatment fig 5.

Nuclei of treated and untreated cells were observed
for typical morphologic apoptotic changes after
DAPI staining Table 3. The combination of SN38
and TNF induced a severe induction of apoptosis,
whereas SN38 alone at the IC50 concentration was
moderately active in inducing apoptosis. rHuTNF 1000
U/mL and nontoxic concentrations of SN38 were completely unable to trigger apoptosis Table 3.
TEM examination definitely confirmed the dramatic induction of apoptosis by the appearance of
typical apoptotic bodies when the mesothelioma
cell line MSTO-211H was treated with SN38 in combination with rHuTNF  of positive cells, 48.0
 1.2; P 0.002; Student t test fig 6A. Apoptosis
was induced in a small population of control cells
1.5  0.5, cells treated with a nontoxic concentration of SN38 5.1  0.3; P  0.05 not statistically
significant, and cells that were exposed to 1000
U/mL rHuTNF  1; P  0.1; not statistically significant fig 6A. No necrotic cells were seen in any
sample.
Gel-ladder analysis confirmed the induction of
severe DNA fragmentation related to the induction of
apoptosis approximately 180 base pairs only in cells
that were treated with the combination of SN38 and
TNF both in MSTO-211H cells fig 6B and in all MPM
cells data not shown.

Overall, the MPM cell lines overexpressed BCL-XL
proteins and mRNA figs 7–9 relative to positive
controls K-562, A498, ACHN, CAKI-1, and PC3 cells
and negative controls HL-60 cells fig 7A.16 In all
lines, the combination treatment down-regulated
BCL-XL protein expression figs 7B, 8 and mRNA
expression fig 9 in MSTO-211H cells.
Time-course experiments showed that BCL-XL
proteins were present after 3 hours of treatment,
started to disappear 6 hours after treatment, and were
undetectable after 12 hours fig 8. Release of cytochrome c to cytoplasm was analyzed on Western
blot analysis in mitochondrial and cytoplasmic fractions of MSTO-211H cells. Only when cells were
treated with the combination of SN38 and TNF was
cytochrome c released from the mitochondria to the
cytoplasm fig 9A.

To determine the requirement of caspase activities
during combined SN38 and TNF-induced apoptosis,
caspase activity was evaluated in MSTO-211H cells
fig 10. The treatment of cells with the combination caused activation of caspase 8, as indicated by
a reduction in the intensity of the proenzyme. The
activation of caspase 9 by the combination was evidenced by the reduction in the intensity of the
proenzyme. Activated caspase 3 was detected after
combination treatment as a double band representing a p19 proteolytic fragment and the active subunit p17, respectively. Therefore, the combination
of SN38 and TNF caused a marked increase in the
cleavage products of PARP. Caspase activation during the course of combined SN38 and TNF-induced
apoptosis also was confirmed by using a caspase
3-specific inhibitor, z-DEVD-fmk. This caspase inhibitor was able to prevent the activation of
caspases and the induction of apoptosis  of apoptotic cells after DAPI staining, 8.7 1.2, as
expected. Therefore, these data suggest that the
combination of SN38 and TNF blocking NF-B activation induces dramatic induction of apoptosis,
and apoptosis is mediated by BCL-XL downregulation and consequential caspase 3 activation.
In the current study, we showed that TNF sensitizes
SN38-induced cytotoxicity of human mesothelioma
cancer cell lines MSTO-221H, IST-MES1, IST-MES2,
MPP89, H28, H513, H2052, and H290. In addition, we
demonstrated that the major induction of massive
apoptosis induced by TNF in combination with SN38
is related mainly to NF-B time shortening in mesothelioma cells; as a result, the expression of BCL-XL
is down-regulated, cytochrome c is released into the
cytoplasm, caspase 3 is activated, and apoptosis is
triggered. The partial inhibition of NF-B complexes
induced by SN38 with PS-341 in MSTO-211 cells was
not enough to down-regulate the constitutive BCL-XL
expression significantly fig 7C. Therefore, our observations indicate that a short persistence of NF-B
complexes and BCL-XL downregulation both are re
lated to the ability of the cell to undergo apoptosis. It
is interesting to note that elevated basal expression of
BCL-XL protein was not correlated with constitutive
activation of NF-B. This observation is true not only
for MPM cell lines but also for lymphoid and renal
carcinoma cell lines.
The critical event in the initiation of apoptosis is
mitochondrial dysfunction, which is regulated by
BCL-2 family proteins. BCL-2 family proteins consists
of 3 subfamilies: the proapoptotic and antiapoptotic
proteins, including Bax proapoptotic and BCL-2 antiapoptotic, and the BH-3 subfamily. The antiapoptotic BCL-2 subfamily includes BCL-XL. The switching
on and off of apoptosis is determined by the ratio of
proapoptotic protein to antiapoptotic protein 15.
In the NCI-ACDS study,16 a strong negative correlation was reported between basal BCL-XL protein and
mRNA levels and drug sensitivity, suggesting that
BCL-XL may play a unique role in general resistance to
cytotoxic agents 70,000 drugs considered. In view of
these data, it is not surprising that MPM would be
strongly resistant to drugs. Our observations that the
combination of SN38 and TNF down-regulated the
expression of both BCL-XL proteins and mRNA may
be crucial in opening new possibilities for MPM therapy. Moreover, ARF is not involved in drug sensitivity
in MPM.
The results of the current study demonstrated that
TNF administered simultaneously with SN38 reduces
the time of persistence of NF-B complexes, causing a
dramatic induction of apoptosis through the downregulation of BCL-XL; consequently, a strong, synergistic, cytotoxic effect is achieved. CPT derivatives
such as irinotecan and topotecan are approved by the
Food and Drug Administration in the U.S., and several
analogues currently are in various stages of clinical
evaluation ie, 9-aminocamptotecin and rubitecan 36. The North Central Cancer Treatment Group
designed a Phase II trial to assess the efficacy and
toxicity of topotecan in patients suffering from unresectable MPM. Topotecan administration was tolerated reasonably well; however, the response rate was
insufficient to warrant an additional study 37. Conversely, combination studies may be more promising.38The combination of irinotecan with cisplatin has
definite activity against MPM and is tolerated well 38.
The Top I inhibitor irinotecan must be converted into its considerably more active metabolite
SN-38 which has 1000 times the potency of the
parent compound by the endogenous enzyme carboxyl esterase, which is found in the plasma, the
intestinal mucosa, and the liver 39. The distribution
of irinotecan and its active metabolite SN-38 in the
pleural fluid, after intravenous administration, was
evaluated in patients with different histologic subtypes of MPM. Irinotecan appeared in the pleural
fluid 1 hour after intravenous infusion, and the level
peaked after 6 hours. SN-38 also was observed in the
pleural fluid 1 hour after infusion, and a concentration equivalent to that in plasma was achieved after
4 hours 37.
Most patients with earlier stage MPM present with
pleural effusions, which allow chemotherapeutic
agents to be administered intrapleurally. This therapeutic approach has the pharmacologic advantage of
exposing pleural tumors to greater concentrations of
the agents compared with the concentrations available using the intravenous route. It has been found
that TNF is tolerated better and is more active when it
is infused locally ie, intraperitoneally 40,41. Intrapleural administration of TNF in MPM patients is a
well tolerated regimen; however, regrettably, it also is
associated with an inconsistent and rather moderate
impact on the production of pleural fluid 42. In all
patients with advanced disease that obliterates the
pleural space, the intrapleural administration of drugs
can prove difficult. Moreover, MPM easily can metastasize despite its pronounced tendency to remain localized in the pleural cavity. Intrapleural therapy may
be limited to patients who have earlier stage disease
and to those patients with small-volume disease for
whom adequate direct diffusion into the tumor still is
possible.
We showed previously that weekly intraperitoneal
administration of mitoxantrone at a dose of 6 mg/m2
and TNF at a dose of 200 mg/m2 is a feasible regimen
with acceptable toxicity.41 Therefore, the regimen of
combined TNF and irinotecan SN38 is its active metabolite is expected to have acceptable toxicity. However, studies currently are ongoing in mice to evaluate the toxicity of this regimen.