
Inhibition of voltage-dependent calcium channels by seven-transmembrane
receptors 7TMR2 is one of the primary means of regulation of calcium-
dependent physiological processes such as synaptic transmission, muscle
contraction, and membrane excitability. In neurons, the Cav2.2 N-type
channel is a prominent target for G protein-mediated modulation 1, 2.
Inhibition of Cav2.2 channels can be voltage-dependent, and mediated by
direct interactions with G protein β-γ subunits 3, 4. In addition,
kinases such as protein kinase C and tyrosine kinases have been shown to
inhibit Cav2.2 channels in a voltage-independent manner 5, 6. Additional
mechanisms may exist by which Ca2+ influx is regulated. Dunlap and
Fischbach 7 have suggested that transmitter-mediated shortening of the
duration of the action potential could be due to a decrease in the number
of voltage-dependent calcium channels at the membrane. Recently we have
reported an additional mechanism by which 7TMRs can regulate neuronal
calcium levels that involves a rapid internalization of voltage-dependent
calcium channels into clathrin-coated vesicles upon receptor activation
8. Here we demonstrate that β-arrestin 1 is associated with Cav2.2
channels and that activation of 7TMRs results in the formation of an
arrestin-receptor-channel complex. This interaction is required for
internalization of calcium channels and plays a role in the modulation of
calcium current.



 The following primary antibodies were used in these studies:  rabbit  anti-
pan-α1 1:200,1.5 μg/ml Alomone Labs,  Jerusalem,  Israel,  anti-arrestin
1:500, BD Biosciences, and anti-GABAR1 1:200, Chemicon.  Anti-β-arrestin
1 and anti-β-arrestin 2 antibodies, and recombinant β-arrestin 1 and 2  29
were kindly provided by the Lefkowitz laboratory.  The  following  secondary
antibodies were used in our studies: Oregon Green 488-conjugated goat  anti-
rabbit IgG H+L 1:200,  10  μg/ml,  Cy3-conjugated  goat  anti-mouse  IgG
H+L 1:200, 7.5 μg/ml, and Cy5-goat anti-guinea  pig  IgG  H+L  1:200,
7.5 μg/ml Jackson Laboratories. Antibodies from the Lefkowitz  laboratory
were used in experiments shown in  Figs 2a, 5d,  and 6f.  Calcium  channels
were detected by indirect immunofluorescence using  an  anti-pan-α1 antibody
that recognizes 1382-1400 of the rat α1  subunit  from  skeletal  muscle,  a
region conserved  across  all  the  α1 subunits  of  high  voltage-activated
calcium channels; this has the advantage of not  binding  the  SNARE-binding
region.
 Sequences of the fluoresceinated 894-929 and 920-944 peptides used in  this
study were based on Cav2.2  α1 sequence  from  chick  dorsal  root  ganglion
DRG  neuron  CDB1,  GenBank™  AAD51815.  Peptides  were  synthesized  by
FastMoc chemistry at the Tufts University Core  Facility  Boston,  MA  and
purified by high performance  liquid  chromatography  with  >97  purity  as
determined by mass spectrometry. The N terminus  included  the  sequence  of
the penetratin domain of the Drosophila protein Antennapedia. Peptides  were
dissolved in 5 mM acetic acid at 1 mg/ml  and  diluted  into  HEPES-buffered
saline 0.01 M HEPES, pH 7.4, and 0.15 M NaCl for biochemical experiments.

The fluoresceinated peptides that show no significant  homology  with  other
proteins was detected as tested by BLAST search.  Control  experiments  were
performed and no differences were detected between  cells  loaded  with  the
peptide and unloaded  cells.  For  each  peptide  used  in  our  studies  we
performed time course and concentration-response  experiments  to  determine
optimal experimental conditions.  Pilot  studies  were  conducted  in  which
fluoresceinated peptides were used to assess peptide entry into the cells.

 Embryonic chick  sensory  neurons  were  grown  in  culture  as  previously
described 6.
 Agonist was prepared fresh in HBS Ca2+ external buffer 2.5 mM KCl at  100
mMconcentration ×1000  ±  baclofen  4-amino-3-4-chlorophenyl-butanoic
acid  Sigma.  Stock  solution  was  diluted  in   the   appropriate   HBS
Ca2+ external buffer immediately prior to  experiments.  Cells  were  washed
once  with  HBS  Ca2+ external  buffer  2.5  mM KCl  at  room  temperature
followed by the addition of 2 ml of HBS  Ca2+external  buffer  60  mM KCl,
with or without a final concentration of 100 μM baclofen for 20 s or  5  min
at room temperature.
Cultures  grown  on  poly-L-lysine   glass   coverslips   were   fixed   and
permeabilized in methanol at -20 °C for  15  min  followed  by  three  5-min
washes in PHEM buffer 60 mM PIPES, 25 mM HEPES, 10 mM EGTA, 2 mM MgCl2,  pH
6.9. Blocking was performed using 5 bovine serum albumin  in  PHEM  buffer
for 1-h at 4 °C, and incubation with primary antibody  in  1  bovine  serum
albumin and 1 normal goat sera in PHEM buffer was performed overnight at  4
°C.  After  washes  with  PHEM  buffer,  coverslips  were   incubated   with
fluorophore-conjugated secondary antibodies in 1 bovine serum albumin  PHEM
buffer for 1.5 h at room temperature in  the  dark.  Glass  coverslips  were
washed 4 times 5 min each in 1  bovine  serum  albumin  PHEM  buffer  and
mounted on glass slides with  one  drop  of  Vectashield  anti-fade  reagent
Vector Laboratories, Burlingame, CA and sealed.
 Confocal laser scanning microscopy was performed  at  the  MSSM  Microscopy
Shared Resource Facility, using a Zeiss  Meta510  UV  microscope  with  an
inverted Axiovert. Images of  fixed  cells  were  obtained  with  a  pinhole
setting of 1.0 using a UV ×63 1.4NA oil objective lens at  slow  acquisition
speed with ×4 frame averaging accumulation.  The  number  of  sections  were
calculated by the software  based  on  acquisition  of  sections  at  240-nm
intervals in the Z-plane. The confocal microscope  settings  were  kept  the
same for all scans. All morphometric measurements were done using  Metamorph
image analysis software Universal Imaging Corporation, West  Chester,  PA.
Neurons were selected and carefully manually traced  for  maximum  accuracy.
The average intensity of fluorescence signal  was  measured  in  the  traced
regions and background staining determined over neuron-free  areas  of  the
culture was subtracted. Intensity measurements are expressed  in  arbitrary
units of fluorescence per square area.

The integrated density of each optical slice  was  measured  and  the  total
surface and cytoplasmic intensity per pixel were  calculated.  Membrane  and
cytoplasmic  staining  were  assessed  by  integrated  density  morphometric
analysis using Metamorph. We used regions of interest and for every  optical
slice the whole area was defined as total fluorescence and the  interior  of
the cell as  the  cytosolic  fluorescence.  The  membrane  fluorescence  was
defined as the difference of total cytosolic.  The  integrated  values  were
determined by measuring the fluorescence values as a function of area.

The plasma membrane was stained with 1 μM FM4-64X, a form of the FM4-64  dye
that can be used in  fixed  cells  Invitrogen.  Line  scans  of  intensity
profiles  across  the  cells  were  generated  with   Metamorph   Universal
Imaging. We measured the fluorescence intensity over  a  distance  covering
the membrane and the cytosol. Three line  profiles,  avoiding  the  nucleus,
were performed to obtain an average profile of  fluorescence  intensity  for
each cell.

For  co-localization  of  two  different  proteins  of  interest,  pictures,
usually green in one case and red in the other were merged and  co-localized
puncta,  which  appear  yellow,  were  counted  for  each  cell.   For   the
measurement of the degree of co-localization,  the  correlation  coefficient
Pearson coefficient between  the  two  different  signals  was  calculated
using Metamorph. For each experiment random groups of cells were scored  for
individual puncta and overlapping puncta of  two  proteins  of  interest  in
matched pairs per cell with a minimum of 25 cells scored per experiment  and
conditions for manual counting and 10 cells per  experiment  and  conditions
for automated counting. Imaging analysis  was  performed  in  a  doubleblind
fashion. Cells were given a code and analyzed in a random manner.

Statistical analysis was  performed  using  Student's  t-tests,  or  one-way
analysis of variance as appropriate.  Statistical  differences  of p <  0.05
were considered significant.
 Biotinylated ω-conotoxin GVIA Bachem, Torrance,  CA  was  incubated  with
Quantum dot 655-conjugated streptavidin Invitrogen for 5 min, then 200  μl
of complete medium was added and the solution was incubated at 30 °C for  20
min. DRG neurons grown on glass  bottom  culture  dishes  were  preincubated
with 120 nM Quantum dot 655-labeled ω-conotoxin GVIA for 1 h in  DRG  medium
at 30 °C in a CO2 incubator  to  minimize  ligand-bound  channel  re-uptake.
Cells were washed twice with 1  mM Ca2+ external  buffer  1  mM CaCl2,  133
mM NaCl, 0.8  mM MgCl2,  10  mM tetraethylammonium-Cl-,  25  mM HEPES,  12.5
mMNaOH, 5mM dextrose, 0.3 mM tetrodotoxin Calbiochem,  La  Jolla,  CA  to
remove unbound conotoxin.  Specificity  of  biotinylated  ω-conotoxin  GVIA-
Quantum  dot-streptavidin  conjugate  labeling  of   Cav2.2   channels   was
determined by preincubating cells with unlabeled ω-conotoxin GVIA  750  nM
and biotinylated ω-conotoxin  GVIA-Quantum  dot  655-streptavidin  conjugate
120 nM for 3 h at 30 °C in a  CO2 incubator  prior  to  confocal  imaging.
Regions of interest were scanned at high acquisition speed at 2-s  intervals
in one X-Y focal plane with the appropriate stage head  configured  for  the
Bioptechs glass bottom culture dishes fitted with a perfusion pump.

For analysis, regions of  interest  were  selected  and  carefully  manually
traced for maximum accuracy. The average intensity  of  fluorescence  signal
in the traced regions was measured using  Physiology  version  3.2  software
Zeiss and background staining determined over neuron-free  areas  of  the
culture was subtracted. Intensity measurements are expressed  in  arbitrary
units of fluorescence per square area.

 Antibodies raised against β-arrestin 1 and β-arrestin 2 were  microinjected
into  the  cell  body  using  an  automated   injector   Eppendorf   5246.
Fluoresceinated dextran Molecular Probes, Eugene, OR was co-injected  with
the antibodies to  allow  subsequent  identification  using  epifluorescence
optics. Control experiments were performed by injecting  vehicle,  preimmune
serum or rabbit IgG-containing internal solution.  Confocal  microscopy  was
performed 30 min after injection.
Whole  cell  recordings  were  performed  as  described   in   Ref 6.   For
extracellular application, agents were diluted into  standard  extracellular
saline and applied via a widebore pipette. For the experiments presented  in
this report, calcium current has been corrected  for  rundown  by  measuring
calcium current as a function of time in control cells without  transmitter.
Cells used for experiments exhibited a rundown of the current of  less  than
1/min.
1 × 106 DRG cells were used for each condition. DRG neurons were exposed  to
control solution or  control  solution  containing  100  μM baclofen.  After
agonist treatment, DRG neurons were lysed with ice-cold  buffer  phosphate-
buffered saline, pH 7.4, containing  250  μM sodium  pervanadate,  1  v/v
Nonidet P-40, 1 mM Pefabloc, 1 mM EDTA, 1 mM EGTA, 10  μg/ml  pepstatin,  10
μg/ml leupeptin, and 100 μg/ml soybean trypsin inhibitor, 100 μg/ml  calpain
I, and 100 μg/ml calpain  II  inhibitors.  The  α1 subunit  of  the  Cav2.2
channel was immunoprecipitated as previously described 9.
2 mg  of  rat  brain  lysate  was  incubated  with  100  μg  of  His6-tagged
recombinant protein bound to nickel beads for 4  h  at  4°C.The  beads  were
spun down and washed 3 times. Beads were mixed with 25 μl of Laemmli  sample
buffer and boiled for 5 min. After spinning down the beads, the  supernatant
sample was resolved by 7.5 SDS-acrylamide gel. Immunodetection of  arrestin
was carried out using anti-arrestin Chemicon, 1:1000.
To test interactions of recombinant arrestin with the calcium channel, 2  μg
of His6-tagged recombinant  protein  containing  the  channel  sequence  was
mixed and incubated with 2 μg of arrestin for 30 min  at  room  temperature.
Nickel beads were used to pull down; immunodetection of β-arrestin  1  or  2
was carried out  using  antibodies  provided  by  the  Lefkowitz  laboratory
1:1000.



 Imaging experiments in live embryonic chick DRG neurons  have  shown  that,
within seconds of receptor activation, calcium  channels  are  cleared  from
the membrane and sequestered  in  clathrin-coated  vesicles  8.  The  fast
kinetics of internalization of  calcium  channels  raises  the  question  of
whether components of the exocytic machinery exist  in  close  proximity  or
pre-associated with the calcium channel. Whereas there is a high  degree  of
co-localization between calcium channels and the endosomal markers Rab5  and
clathrin heavy chain in cells exposed to agonist, there is a very low  level
of co-localization prior to receptor activation 8.
By analogy to 7TMR trafficking, we  sought  to  determine  whether  arrestin
associates with internalized calcium channels. Both β-arrestin 1 and  2  are
expressed in DRG neurons as determined in immunoblotting  experiments  Fig
1a. We next determined whether calcium channels were spatially  distributed
in close proximity to arrestin. In the  presence  of  saline,  most  of  the
calcium channels are localized in the plasma membrane as  the  channels  co-
localize with the plasma membrane marker FM4-64 Fig  1b.  Optical  slices
from saline-treated neurons show that both calcium channels and arrestin co-
localize as indicated by the  yellow  signal  indicating  overlap  of  green
fluorescent signal Cav2.2 channels and red fluorescent signal  arrestin.
Both proteins are associated with the top slices; in the middle  slices  the
fluorescence  signal  forms  a  ring  around  the  periphery  of  the   cell
suggesting   association   with   the   membrane   Fig   1, c and e,   and
supplementary materials Fig S1. Upon exposure to agonist  for  20  s,  the
fluorescence signal  becomes  more  intense  in  the  middle  slices  and  a
decrease is observed in  the  levels  of  membrane-associated  arrestin  and
Cav2.2 channel Fig 1d and e, and supplementary  materials  Fig  S2.  The
Pearson correlation coefficient between calcium  channels  and  arrestin  is
0.73 ± 0.09 in saline-treated cells  and  0.8  ±  0.06  in  baclofen-treated
cells n = 25. Together these results  suggest  that  arrestin  and  Cav2.2
channels are preassociated in the cell surface  and  are  internalized  upon
7TMR activation.
Arrestin co-precipitates with Cav2.2 channel protein from chick DRG  neurons
treated with saline or baclofen Fig 2a  giving  further  support  to  the
observation that arrestin is preassociated with the calcium  channel  in  an
agonist-independent fashion. We have  previously  used  anti-pan-α1 antibody
to  precipitate  Cav2.2  channels  from  DRG   neurons   9, 25.   Arrestin
immunoprecipitates from cultured embryonic chick DRG neurons and  rat  brain
lysates contained Cav2.2 channel protein Fig 2b. Moreover, the  detection
of arrestin/Cav2.2 channel association in rat brain  lysates  suggests  that
this interaction takes place in central nervous system neurons.
Association of arrestin  with  calcium  channels  might  occur  proximal  to
release sites as suggested by the high degree  of  correlation  between  the
localization of arrestin and that of synapsin I, a synaptic vesicle  protein
Fig 2, c and d. 83  of  the  arrestin  co-localize  with  synapsin  n =
20, Fig 2d.
 Because arrestin is known to interact with phosphorylated  7TMRs  10, 11,
we determined whether arrestin and the Cav2.2 channel are part of a  complex
that contains  the  receptor.  Indirect  immunofluorescence  using  confocal
laser  microscopy  was  used  to  visualize  the  GABABR1  subunit  of   the
GABAB receptor blue signal, Cav2.2 channel green  signal,  and  arrestin
red signal X-Y optical slices were taken from the top to  bottom  of  DRG
neurons treated with  saline  or  baclofen.  Very  low  co-localization  was
detected between the receptor and arrestin  or  Cav2.2  channel  in  saline-
treated neurons Fig 3a and supplementary materials Fig  S3.  The  Cav2.2
channel-arrestin correlation coefficient is 0.70; the other values  for  co-
localization are below 0.10 n= 20, Fig 3e.
Upon a 20-s exposure to baclofen, DRG neurons exhibited an increase in the
degree of co-localization between Cav2.2 channels, arrestin, and receptors
as indicated by the white fluorescence signal Fig 3, b ande, and
supplementary materials Fig S4. Most of the co-localization takes place
in the middle slices suggesting that the signal is cytoplasmic and that
these proteins are internalized together. The co-localization of the Cav2.2
channel-arrestin-receptor is transient, as the analysis of neurons exposed
to agonist for 1 or 5 min revealed a very low degree of co-localization
Fig 3, c, d, and e. Our results show that co-localization of the
receptor with arrestin and the calcium channel is highest at 20 s, a time
point in which inhibition of calcium channels by 7TMRs is still at its
maximum 12. The degree of co-localization between the proteins is lower
at 1 and 5 min, which parallels the desensitization of transmitter-mediated
inhibition of calcium current.
Activation of GABAB receptors increases the  amount  of  receptor  that  co-
precipitates with Cav2.2 channel Fig 4. When the presence of the  GABABR2
was probed by immunoblotting of calcium channel precipitates a band  in  the
130-kDa  region  was  observed  in  agreement  to  published  reports  13.
Exposure of neurons to agonist resulted in a 4-fold increase in  the  amount
of receptor detected in the  immunoprecipitates  Fig  4.  When  membranes
were probed for arrestin, no change in the  amount  of  arrestin  associated
with the channel was observed, which is in agreement with the results  shown
in Fig 2a. The biochemical and imaging data suggest that receptor-arrestin-
channel complexes are formed upon 7TMR activation and that  these  complexes
are internalized.
 The Cav2.2 channel/arrestin interaction was further  supported  by  results
from  experiments  in  which  His6-tagged  recombinant  proteins  containing
sequences from the cytoplasmic regions of the α1 subunit of Cav2.2  channels
were used in arrestin pulldown assays from  rat  brain  lysate.  Recombinant
protein containing the SNARE-binding or synprint region from this  loop  II-
III region amino acids aa 726-984 bound to arrestin Fig  5a,  whereas
no binding was detected in samples incubated with the remainder of loop  II-
III or the C terminus. Truncation of  the  synprint  domain  protein  showed
that aa 894-944 are required for binding to arrestin. 
Recombinant β-arrestin 1 binds to recombinant protein containing the
sequence of the synprint region of the Cav2.2 channel suggesting a direct
interaction between the two proteins Fig 5c. This interaction is
selective for β-arrestin 1, as no binding to the synprint region of the
calcium channels was detected when recombinant β-arrestin 2 was used in the
experiments.
We tested whether arrestin plays a role in agonist-induced internalization
of voltage-dependent calcium channels. Calcium channels at the cell surface
were labeled using biotinylated ω-conotoxin GVIA bound to streptavidin-
conjugated quantum dots. Chick DRG neurons express only one type of calcium
channel, Cav2.2 channel N type 14, which is located both at the
terminals and the soma. Channels at both locations are coupled to the
exocytic machinery 15, 16. Cav2.2 channels exhibited a punctate
distribution in the top plane of the membrane Fig 6, a and b.
Preincubation of DRG neurons with unlabeled toxin prevents the binding of
Qdot 655-streptavidin-biotinylated ω-conotoxin GVIA demonstrating that the
probe binds selectively supplementary materials Fig S5. Incubation of
DRG neurons with the Qdot 655-conjugated streptavidin alone did not result
in significant labeling of the surface of DRG neurons supplementary
materials Fig S5. These results are not qualitatively different from
those that we have previously obtained using rhodamine-conjugated ω-
conotoxin GVIA.
In live cell imaging experiments, exposure of DRG  neurons  to  baclofen,  a
GABAB receptor agonist 100 μM and  well  established  modulator  of  these
channels, produced a decrease in fluorescence signal in the top  surface  of
the cell within 2  s  Ref 8, Fig  6a  concurrent  with  an  increase  in
fluorescence in the middle optical slices Fig  6, a and d.  The  decrease
in fluorescence in the top surface was transient as  the  signal  reappeared
at the cell surface within several minutes  of  continued  agonist  presence
Fig 6a. After channels reappeared, the agonist was washed away and  cells
were exposed to saline for 1 min. A second application of agonist failed  to
elicit any change suggesting that the response had desensitized  Fig  6c.
No change in fluorescence was observed in  cells  exposed  to  saline  Fig
6b.
We designed cell-permeant peptides based on the sequence  of  the  arrestin-
binding site in the Cav2.2 channels  Gallus  gallus CDB1,  aa  894-944  to
interfere with arrestin/channel interaction in  DRG  neurons.  Two  peptides
were designed: one containing aa 894-929 and a second one containing aa 920-
944. The N termini of the peptides included the sequence of  the  penetratin
domain of the Drosophila protein Antennapedia and the  C  termini  contained
fluoresceinated amino acids to visualize the peptides. Entry into the  cells
of the cell-permeant peptides was monitored by fluorescence and 80  of  the
cells contained peptide following a 5-min incubation.  The  894-929  peptide
prevented agonist-induced Cav2.2 channel  internalization  without  altering
their basal distribution Fig 6e. The scrambled peptide data  not  shown
and 920-944 peptide were without effect Fig 6e.
To determine whether there is selectivity in which  β-arrestin  is  involved
in channel  trafficking,  we  injected  DRG  neurons  with  antibodies  that
preferentially recognize β-arrestin 1 or β-arrestin 2. The  antibodies  were
introduced by microinjection into the cell body and fluorescein-dextran  was
included in the solution to visualize the injected cells.  Calcium  channels
were visualized with  Qdot  655-streptavidin  and  biotinylated  ω-conotoxin
GVIA. Neurons injected with anti-β-arrestin 1 showed a significant  decrease
in baclofen-induced internalization of Cav2.2 channels; changes observed  in
the presence of anti-β-arrestin 2 were  not  significant  Fig  6f.  Taken
together, these results suggest that β-arrestin 1 is required  for  agonist-
induced internalization of calcium channels.  Cells  injected  with  anti-β-
arrestin  1  showed  a  decrease  in  agonist-mediated   voltage-independent
inhibition.



We have shown that, in both embryonic chick DRG neurons and  rat  brain,  β-
arrestin  1  is  bound  to  Cav2.2  channels  in  the  absence  of  receptor
activation. This is in contrast  to  previous  reports  of  arrestin-binding
proteins  that  showed  a  requirement  for  receptor  activation  for   the
nucleation of signaling complexes. Unlike the interaction of  arrestin  with
receptors 17 and NHE5 18,  the  interaction  of  calcium  channels  with
arrestin does not require phosphorylation,  as  channel  phosphorylation  is
undetectable prior to activation of  GABABreceptors  9.  Furthermore,  the
arrestin-binding site in the calcium channels  does  not  contain  consensus
sites for phosphorylation.
The arrestin-binding site in Cav2.2 channels is located within  loop  II-III
in the SNARE-binding region, a region important for the regulation  of  both
channel activity and secretion 19. Binding of  syntaxin  to  this  channel
region plays a role  in  voltage-dependent  inhibition  by  stabilizing  the
binding of G protein β-γ subunits to the channel  20.  Synaptotagmin  also
binds to the channel in this  region  upon  depolarization,  in  a  calcium-
dependent manner 21. Deletion of the SNARE-binding  region  rat  726-984
from loop II-III results in mistargetting of Cav2.1  channels  22;  future
studies should address whether  this  is  due  to  long-term  disruption  of
arrestin/calcium channel interaction. It should be noted that the  arrestin-
binding region,  aa  894-929,  is  highly  conserved  63  homology  among
voltage-dependent calcium channels further suggesting that  arrestin/channel
interaction will be found in  different  regions  of  the  brain  throughout
different species.
Arrestin-channel-receptor complexes were detected at early time  points  20
s in  the  time  course  of  the  agonist-induced  response.  Our  previous
electrophysiological studies of calcium channel modulation have shown  that,
at  this  time  point,   calcium   channel   inhibition   is   maximal   and
desensitization is not observable  12.  Channel  internalization  was  not
observed in experimental paradigms  that  favored  desensitization  such  as
long-term  exposure  to  agonist  >40  s, Fig  6, a and d  or  repetitive
exposure to agonist Fig 6c. Furthermore, in experiments in which  neurons
were exposed to agonist for 1 min, a time point in  which  calcium  channels
return to the surface and desensitization of  7TMR-induced  calcium  channel
inhibition have been observed, there is no significant receptor/arrestin co-
localization and receptors have returned to the surface.  The  formation  of
these complexes seems to  be  important  for  the  suppression  of  neuronal
excitability through the inhibition  of  calcium  influx  into  the  neurons
rather than termination or desensitization of the response.
Recent studies from Zamponi and  colleagues  23  have  shown  that  Cav2.2
channels are internalized with ORL nociceptin receptors upon  a  prolonged
30-min exposure of tsA-201 cells and primary  rat  DRG  neurons  to  ligand.
Whereas this internalization of calcium channels is a  response  to  chronic
exposure to an agonist, our observations on the spatiotemporal  distribution
of calcium channels have focused on early events after  receptor  activation
<1 min 8. Agonist-induced desensitization of calcium current  inhibition
and return of the calcium channels to the surface occur in  the  time  scale
of 1 min 8, 12. In contrast to the nociceptin receptor studies  23,  our
biochemical and imaging experiments do not  show  a  significant  degree  of
GABABreceptor/Cav2.2 channel association prior to receptor activation.
In contrast to the cell surface abundance of NHE5 that can be altered by  β-
arrestin 1 or β-arrestin 2 18, agonist-induced channel internalization  is
selectively regulated by β-arrestin 1. The selectivity of  these  two  forms
of arrestin is largely unexplored in intact primary cells or tissue. Both β-
arrestin 1 and  β-arrestin  2  play  a  role  in  receptor  endocytosis  and
activation of the mitogen-activated protein  kinase  pathway  24.  Whereas
GABAB receptors increase mitogen-activated protein kinase activity, mitogen-
activated protein kinases do not play a role in the  modulation  of  calcium
current 25.
Various reports suggest that the selectivity in the  actions  of  β-arrestin
arises at the level of the  receptor.  For  example,  whereas  β-arrestin  1
plays a role in AT1a 26 and NK1 27 receptor-mediated activation of  ERK,
this pathway is activated through β-arrestin 2 in the case of CCR7  receptor
28.
Both β-arrestin 1 and β-arrestin 2  are  widely  expressed  in  the  nervous
system 29, 30. β-Arrestin  2  knockout  mice  exhibit  several  phenotypes
associated with neuronal signaling such as enhanced sensitivity to  morphine
in pain assays 31, enhanced reward response to opioids 32, and  decrease
in dopamine-mediated locomotor  responses  33.  Through  interaction  with
phosphodiesterase 4,  β-arrestin  2  regulates  opioid-mediated  presynaptic
inhibition by regulating synaptic release probability 34. Recently it  has
been shown that β-arrestin 2 associates with Akt  and  phosphatase  PP2A  to
mediate dopaminergic responses 33.
β-Arrestin 1 is  the  predominant  form  of  arrestin  found  in  the  small
diameter DRG neurons 35. As these neurons  are  involved  in  nociception,
one would expect alterations  in  the  regulation  of  painful  stimuli.  In
conclusion, our studies have shown that β-arrestin 1 associates with  Cav2.2
channels and it is required for agonist-mediated internalization of  calcium
channels. Accurate calcium  signaling  requires  the  proper  spatiotemporal
organization of voltage-gated  calcium  channels  and  signaling  molecules.
Arrestin could work both as a scaffold for calcium  channels  to  facilitate
internalization and as a switch for recovery from presynaptic inhibition  as
it plays a role in the recycling of calcium channels. We cannot exclude  the
possibility that other components of the active zone are  being  sequestered
along with the calcium channels, raising the possibility that this  is  part
of a more extensive remodeling of the active zone. Given  the  abundance  of
arrestin in the nervous system and its spatial correlation  with  components
of the  release  machinery,  this  new  function  for  arrestin  could  have
important implications for the modulation of synaptic function.

