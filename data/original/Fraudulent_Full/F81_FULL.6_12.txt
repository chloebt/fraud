
Endothelial cells ECs control the infiltration of solutions, blood
proteins, and circulating cells, such as leucocytes and lymphocytes, into
the vessel. Vascular endothelial cadherin VE-cadherin is the most
important adhesive component of endothelial adherens junctions involved in
barrier function 1,2. VE-cadherin associates with α-catenin, β-catenin, and
p120 catenin via its cytoplasmic tails. The cadherin-mediated endothelial
integrity is disrupted by phosphorylation of tyrosine residues of VE-
cadherin and its components, which results in decoupling of the p120 and β-
catenin complex from VE-cadherin and subsequent internalization of VE-
cadherin 1,2. The barrier function of VE-cadherin is known to be regulated
by a variety of extracellular stimuli, including lysophosphatidic acid
LPA, in physiological and pathological conditions 1,3. However, the
effects of LPA are controversial LPA is known to be accumulated in mildly
oxidized low-density lipoprotein LDL and to increase permeability and
induce vascular dysfunction in ECs 4–6. Thus, LPA might be a component
involved in the formation of atherosclerotic lesions stimulated by oxidized
LDL 4,7. In contrast, LPA has also been reported to stabilize endothelial
barrier function and decrease EC permeability in vitro, 8,9 and to stimulate
blood vessel formation during development and stabilize vessel assembly in
vivo 10–12. However, the precise mechanism by which LPA regulates the
opposite actions on EC permeability or integrity of adherens junction
remains elusive. p2y5 has recently been identified as one of the candidate
genes of autosomal recessive hypotrichosis, characterized by sparse hair on
the scalp, 13,14 and now proposed as LPA6, with the ability to couple to
G12/13 proteins, resulting in the morphological change and contraction of
the cells 15. In human umbilical vein endothelial cells HUVECs, both LPA1
and p2y5/LPA6 henceforth termed LPA6 are expressed, and LPA induced
contraction of the cells through LPA6 15. However, the signaling mechanism
and physiological role of LPA6 have not been elucidated.
Ki1642516 and Ki16198 characterization of the pharmacological specificity
will be described elsewhere were provided by Kyowa Hakko Kirin Co, Ltd
Tokyo, Japan. The cDNAs coding G12VRap1 and Rap1GAPII were generously
provided by Professor Naoki Mochizuki National Cardiovascular Center
Research Institute, Osaka, Japan. For other materials, see expanded
Methods in the Supplementary material online.

HUVECs and human aortic endothelial cells HAECs; both at passage 3 were
purchased from Kurabo Industries Ltd Osaka, Japan and Takara Bio Inc
Otsu, Shiga, Japan, respectively, and cultured in RPMI 1640 medium
supplemented with 20 v/v fetal bovine serum and several growth
factors 17. In our previous studies, the plastic culture dishes or plates
BD Falcon™, Becton, Dickinson and Company, Tokyo, Japan were usually
coated with collagen as an additional extracellular matrix. We examined the
effects of extracellular matrices, including laminin, fibronectin, and
collagen, on the LPA actions on the cell–cell contact and VE-cadherin
integrity, and found that the culture plates or dishes without additional
matrix are better for the detection of LPA effects on endothelial barrier
function. In the present study, therefore, Falcon culture dishes or plates
were used without further coating with any matrix, unless otherwise stated.
The cells were treated or infected for appropriate times with small
interfering RNAs siRNAs or adenoviruses to modulate the activity of the
cellular target proteins. Where indicated, the cells were treated with
pertussis toxin PTX, 100 ng/mL or its vehicle phosphate-buffered saline;
PBS for 16 h, and with Ki16425 5 µmol/L, SP600125 10 µmol/L, SB203580
10 µmol/L, PP2 10 µmol/L, or their vehicle 0.1 dimethyl sulfoxide
for 20 min before experiments with LPA. Note that the cells were maintained
in culture medium containing fetal bovine serum and other supplements
immediately before the experiments with LPA. The cells were then washed
twice with HEPES-buffered medium, followed by incubation with the indicated
concentrations of LPA for 10 min at 37°C in the same HEPES-buffered medium.
Further details can be found in the Supplementary material online.

The cells were photographed under a phase contrast microscope. The
cell–cell dissociation activity was manually assessed as a gap area, which
is defined as a percentage of the area without cells over the total area.

Membrane and cytosolic fractions were prepared as previously described18
and processed for western blotting with anti-VE-cadherin antibody. For
other procedures, see expanded Methods in the Supplementary material
online.

The recombinant adenovirus for JNK-binding domain of JNK-interacting
protein-1 JIP1-JBD was constructed as described previously 19. A dominant-
negative form of human Rac1 T17NRac1 from Upstate Charlottesville, VA,
USA was used for recombinant adenovirus construction. To construct
recombinant adenovirus for human Rap1GAPII, the entire coding region of
Rap1GAPII20 was subcloned into pShuttle-CMV according to the instruction
manual of AdEasy XL Adenoviral Vector System. The cDNA coding G12VRap1, a
constitutively active form of Rap1, containing FLAG, FLAG peptide epitope,
in Adeno-X Expression System, 20 was used for recombinant adenovirus
construction. The recombinant adenovirus for T19NRhoA a dominant-negative
form of RhoA and G14VRhoA a constitutively active form of RhoA were
constructed as described previously 21. The adenovirus expressing green
fluorescent protein was used for evaluation of the expression by
visualization. The cells were infected as described previously 22. Further
details can be found in the Supplementary material online.

As for the transfection of siRNAs, HUVECs were seeded at a density of 5.0 ×
104 cells/cm2. Sixteen hours later, 100 nmol/L of non-silencing siRNA and
siRNAs specific to Gα12, Gα13, Src, LPA1, LPA6, and C3G were transfected
using RNAiFect reagent according to the manufacturer's instructions, as
described previously 23. After 48 h transfection, knockdown of targets was
assessed by RT-PCR or western blotting.

The total RNA was isolated by RNAiso Plus Takara Bio Inc, Otsu, Shiga,
Japan according to the instruction manual The isolated total RNA was
treated with DNase I to remove genomic DNA contaminating the RNA
preparations, and subjected to quantitative RT-PCR using TaqMan technology
as described previously 24. The sources of probes are shown in the
Supplementary material online. The expression level of the target mRNA was
normalized to the relative ratio of the expression of GAPDH mRNA.

The incubation with LPA and test agents was terminated by washing twice
with ice-cold PBS and adding 0.8 mL of a lysis buffer according to the
instruction manual of Active GTPase Pull-down and Detection Kits Thermo
Fisher Scientific Inc, Rockford, IL, USA. Rap1 and Rho activity was then
assessed by the pull-down assay of the active form of Rap1 and Rho with GST-
RalGDS RBD and GST-Rhotekin RBD according to the manufacturer's
instructions, respectively.

For analyzes of cellular proteins, the cells were washed with ice-cold PBS
and harvested by adding a lysis buffer composed of PBS, 1 Nonidet P-40,
0.5 sodium deoxycholate, 0.1 SDS, 1 µmol/L EDTA, and 1 proteinase
inhibitor cocktail. The recovered lysate was centrifuged at 14 000 × g for
20 min. The supernatant was resolved by 12.5 SDS–PAGE for Rap1 or 10
SDS–PAGE for other proteins, and the protein bands were detected by the
alkaline phosphatase method, as described previously 25.

This was performed by using the Endothelial Tube Formation Assay Kit Cell
Biolabs, Inc, San Diego, CA, USA. HUVECs were treated with siLPA1,
siLPA6, or non-silencing RNA, and then suspended in RPMI 1640 medium
containing 10 fetal bovine serum at 2 × 105 cells/mL. Cell suspension 150
µL, with or without LPA, was placed onto the solidified extracellular
matrix gel, which was prepared from Engelbreth-Holm-Swarm EHS tumor
cells, on 96-well dishes and incubated for 16 h before taking photographs,
according to the manufacturer's instructions.

Wistar rats were fed rodent chow and housed in micro-isolator cages in a
pathogen-free facility. This study conforms to the Guide for the Care and
Use of Laboratory Animals NIH publication number 85-23, revised 1996, and
all animal procedures were performed in accordance with the guidelines of
the Animal Care and Experimentation Committee of Gunma University. A rat
model of peripheral circulation insufficiency was made as described by
Ishida et al26 Briefly, male rats 13 weeks old were anaesthetized with
sodium pentobarbitone intraperitoneally at 40 mg/kg, and the level of
anesthesia was controlled by inhalation of 1–2 isoflurane Dainippon
Sumitomo Pharma Co Ltd, Osaka, Japan through a Small Animal Anesthetizer
and Anesthetic gas scavenging system Muromachi Kikai Co Ltd, Tokyo,
Japan to maintain spontaneous breathing throughout the procedures. The
adequacy of anesthesia was confirmed by loss of the paw pinch reflex
retraction of a hindpaw after pinching between the toes and absence of
visual movement on incision. The left femoral artery was exposed by
surgical incision. A 5 lactic acid–saline solution 100 µL was injected
into the left femoral artery. Ki16198 60 mg/kg or vehicle 0.5 sodium
carboxymethylcellulose was administered orally 3 h before surgery and
twice a day on the first to fourteenth days after surgery. Macroscopic
observation of the treated hindlimbs was carried out on the third, seventh,
and fourteenth day after the lactate injection. The lesions were graded 0–4
for gangrene, mummification or loss of digits as follows: grade 0, normal
appearance; grade 1, the region was limited to the nails; grade 2, the
region was limited to the tips of the digits; grade 3, gangrene of the
digits; and grade 4, loss of the digits. The lesion point score was taken
as the sum of the five digits, adding five points if gangrene extended to
the lower leg. The rats were killed with an injection of sodium
pentobarbitone intraperitoneally at 250 mg/kg of body weight.

Cellular activity or response was determined in duplicate or triplicate.
Results of multiple observations are presented as the mean ± SEM gap area
reflecting cell–cell dissociation activity and mRNA measurement or as
representative results images of morphological change of the cells and
western blotting data from three or four different batches of cells,
unless otherwise stated. For in vivo experiments, statistical significance
was assessed by ANOVA; values were considered significant at P < 0.05 *.
We first searched for an in vitro system suitable for the characterization
of the roles and signaling mechanisms of LPA in endothelial integrity.
When confluent HUVECs see Supplementary material online, Figure S1A and B
were cultured on plastic plates without additional extracellular matrix and
were exposed to LPA, we found that organized cell–cell interaction was
disrupted, and intercellular gaps appeared depending on the concentrations
of LPA employed; the clear gap was observed at 0.1∼1 µmol/L LPA but not at
10 µmol/L LPA. A similar morphological change induced by LPA was observed
in HAECs see Supplementary material online, Figure S1C. The change in
cell–cell interaction was observed in the culture plates coated with
laminin; however, when HUVECs or HAECs were cultured on fibronectin- or
collagen-coated plates, no appreciable morphological change was detected
with LPA, regardless of its concentration see Supplementary material
online, Figure S1D and E. We therefore cultured HUVECs on the plastic
plates without additional extracellular matrix for further characterization
of the actions of LPA, unless otherwise specified.
The LPA-induced gap formation was completely reversed by Ki16425, an LPA1
and LPA3 antagonist, and PTX, a Gi protein inhibitor, whereas these
inhibitors hardly affected the apparent cell–cell contact in the absence
basal and presence of a high concentration of LPA Figure 1A and B. The
cell–cell dissociation was associated with internalization or translocation
of VE-cadherin from the membrane to the cytosolic fraction by 1 µmol/L LPA,
which was susceptible to Ki16425 and PTX Figure 1C. As expected, a high
concentration of LPA 10 µmol/L did not change VE-cadherin translocation,
irrespective of the treatment of the cells with the LPA antagonist and PTX
Figure 1C. Thus, a low concentration of LPA disrupts VE-cadherin
integrity, whereas a high concentration of LPA masks the disruptive
activity and thereby protects endothelial barrier function. Henceforth, 1
and 10 µmol/L LPA were used as low and high concentrations, respectively,
unless otherwise stated.


JNK and p38MAPK have been shown to negatively regulate endothelial barrier
function 27–29. A low concentration of LPA induced phosphorylation of JNK
and p38MAPK, reflecting their activation, which was inhibited by Ki16245
and PTX Figure 1D. Unexpectedly, however, a high concentration of LPA
also stimulated the enzyme phosphorylation, which was insensitive to
Ki16245 and PTX treatment Figure 1D, suggesting that the activation
mechanisms of MAPKs may be distinct between low and high concentrations of
LPA. The roles of MAPKs, including JNK and p38MAPK, and the upstream Rac1
were confirmed by their selective or specific inhibitors, a dominant-
negative mutant for Rac1 T17NRac1 or DN-Rac, and the JIP1-JBD; a
specific inhibitor of the JNK pathway; see expanded Results and Figure S2
and S3 in Supplementary material online. These results suggest that Rac1
is located upstream of MAPKs in the LPA signaling, regardless of its
concentration.

HUVECs substantially express LPA1 and LPA6 mRNAs Figure 2A 15. Small
interfering RNAs for LPA1 and LPA6 specifically inhibited the expression of
the respective LPA receptor subtype mRNA Figure 2B and protein
Figure 2C. In these conditions, LPA1 siRNA remarkably attenuated
cell–cell dissociation Figure 2D, gap formation Figure 2E, and VE-
cadherin translocation Figure 2F induced by 1 µmol/L LPA, whereas LPA6
siRNA hardly affected these disruptive parameters induced by 1 µmol/L LPA.
Interestingly, however, knockdown of LPA6 reversed the protective effect on
VE-cadherin integrity induced by 10 µmol/L LPA and thereby unmasked LPA-
induced disruptive responses Figure 2D, E and F. The combination of both
LPA1 siRNA and LPA6 siRNA induced an effect similar to treatment with LPA1
siRNA alone Figure 2D, E and F, further suggesting that the disruptive
activity of LPA, regardless of its concentration, is mediated by LPA1.
Thus, LPA1 and LPA6 may mediate LPA-induced disruption and protection of VE-
cadherin integrity, respectively. Although HAECs express LPA4 and LPA5 in
addition to LPA1 and LPA6, knockdown study revealed similar regulation by
LPA1 and LPA6 of VE-cadherin integrity in HAECs Supplementary material
online, Figure S4.

The role of LPA receptor subtypes in the activation of MAPKs was examined
Figure 2G. As expected, phosphorylation induced by 1 µmol/L LPA was
inhibited by the knockdown of LPA1. In contrast, phosphorylation induced by
10 µmol/L LPA was inhibited by neither LPA1 siRNA nor LPA6 siRNA, and the
inhibition was observed only when both LPA1 and LPA6 were knocked down
Figure 2G. These results suggest that a low concentration of LPA
stimulates LPA1, whereas a high concentration of LPA has the ability to
stimulate both LPA1 and LPA6, both of which are coupled to the signaling
pathways to stimulate MAPKs. Thus, although LPA6 stimulates MAPKs, LPA6 may
be coupled to the additional signaling pathway which overcomes the
negative signals of MAPKs against VE-cadherin integrity.


Experiments with either Gα12 siRNA or Gα13 siRNA suggested that both G12
and G13 proteins are required for the LPA6-mediated protective effect on VE-
cadherin integrity see Supplementary material online, Figure S5. Rho
signaling is known to be regulated by G12/13 proteins. When HUVECs were
sparsely cultured on collagen-coated plates, the cells were contracted or
rounded by 10 µmol/L LPA in a manner sensitive to Y-27632, a Rho kinase
inhibitor see Supplementary material online, Figure S6, confirming the
previous observation 15. Even in the confluent culture conditions without
additional matrix employed in the present study, LPA activated RhoA through
LPA6 Supplementary material online, Figure S7A. However, RhoA does not
appear to be involved in the regulation of either a disruptive or a
protective action by LPA in our culture conditions; a change in RhoA
activity induced by a dominant-negative RhoA T19NRhoA or DN-Rho or a
constitutively active mutant for RhoA G14VRhoA or CA-Rho; Supplementary
material online, Figure S7B did not appreciably affect LPA-induced
disruption and protection of cell–cell interaction, and VE-cadherin
integrity Supplementary material online, Figure S7C and D. Moreover, Y-
27632, a Rho kinase inhibitor, failed to affect cell–cell dissociation and
VE-cadherin translocation Supplementary material online, Figure S7E and
F.
We searched for another signaling molecule working downstream of G12/13
proteins. We speculated the involvement of Src 30–32. As shown in Figure 3A,
phosphorylation of Src at Tyr416, reflecting activation of Src, was
induced by a high concentration, but not by a low concentration, of LPA.
The LPA-induced Src phosphorylation was abrogated by knockdown of LPA6, but
not by LPA1 Figure 3A. Either Gα12 siRNA or Gα13 siRNA also inhibited the
LPA-induced Src phosphorylation Figure 3B, whereas a change in RhoA
activity failed to alter Src phosphorylation Supplementary material
online, Figure S7B. Either inhibition of Src by a Src specific inhibitor,
PP2, or knockdown of Src by its specific siRNA Figure 3E and H reversed
the protective effects of a high concentration of LPA on gap formation
Figure 3C and F and VE-cadherin translocation Figure 3D and G without
appreciable effects by the low concentration of LPA. These results strongly
suggest that Src is a critical signaling molecule to link LPA6/G12/13
protein and the downstream signaling pathway for the protection of VE-
cadherin integrity.


Rap1 is known to play a critical role in the stabilization of VE-cadherin
assembly 1,33. As shown in Figure 4A, 10 µmol/L LPA activated Rap1, which
was sensitive to LPA6 siRNA but not LPA1 siRNA, suggesting mediation by
LPA6. The role of Src in Rap1 activation was investigated by PP2 and Src
siRNA in Figure 4B. In this experiment, the effect of 8-CPT-2′-O-Me-cAMP,
which specifically activates Epac, a Rap1 guanine nucleotide exchange
factor GEF, was also examined. Consistent with the previous result, 20 8-
CPT-2′-O-Me-cAMP activated Rap1 in a similar manner to a high concentration
of LPA. The treatment with PP2 and Src siRNA almost completely inhibited
LPA-induced Rap1 activation, whereas abrogation of Src activity hardly
affected 8-CPT-2′-O-Me-cAMP-induced Rap1 activation Figure 4B, suggesting
that Rap1 is located downstream of Src.

Abrogation of Rap1 activity by forced expression of Rap1GAPII, which
inactivates Rap1 by stimulating hydrolysis of GTP in the active form of
Rapl Supplementary material online, Figure S8A, reversed the LPA6-
mediated protection of VE-cadherin integrity; 10 µmol/L LPA induced
dissociation of the cell–cell interaction Figure 4C and VE-cadherin
translocation Figure 4D to an extent similar to that induced by 1 µmol/L
LPA. In contrast, the expression of a constitutively active Rap1, G12VRap1
Supplementary material online, Figure S8B, attenuated 1 µmol/L LPA-
induced or LPA1-mediated disruption of barrier integrity Figure 4C and D.
However, the modulation of the Rap1 activity hardly affected LPA-induced
Src phosphorylation and activation of MAPKs Supplementary material online,
Figure S8C and D. These results suggest again that Rap1 is located
downstream of Src in the LPA6 signaling pathway and that its activation
may interrupt the downstream signaling of MAPKs.

There are several types of RaplGEFs, including Epac, C3G, PDZ-GEFs, and
dedicator of cytokinesis 4 33. As described above, an Epac–Rap1 system is
working Figure 4B in association with stabilization of VE-cadherin in
HUVECs 20. However, we failed to detect cAMP accumulation with LPA data not
shown. When the expression of C3G mRNA was knocked down more than 80
Figure 5A, the inhibitory effect induced by 10 µmol/L LPA on cell–cell
dissociation Figure 5B and VE-cadherin translocation Figure 5C was
reversed, in association with an inhibition of Rap1 activation Figure 5D.
These results suggest that C3G is involved in LPA6-mediated Rap1
activation.


To further confirm the role of LPA receptor subtypes in endothelial barrier
function, we evaluated whether or not LPA regulates angiogenesis, in which
new capillaries sprout from the existing vessels in association with EC
migration, proliferation, and stabilization of barrier integrity 12. The
high concentration of LPA 10 µmol/L accelerated clear tube formation,
whereas the low concentration of LPA 1 µmol/L induced aggregation but not
tube formation Figure 6. It is currently unknown whether or not the
aggregation of ECs at 1 µmol/L LPA is related to the disruption of VE-
cadherin. However, the aggregation of ECs was reversed by the loss of LPA1.
In contrast, LPA-induced tube formation was completely blocked by LPA6
knockdown.


Finally, we examined the effect of Ki16198, a methylester derivative of
Ki16425 suitable for oral administration, on lactate-induced limb lesions
due to peripheral artery occlusion in vivo26 Supplementary material
online, Figure S9. The pharmacological properties of Ki16198, eg
specificity and potency, are indistinguishable from those of Ki16425, an
LPA1 and LPA3 antagonist. As shown in Supplementary material online, Figure
S9, Ki16198 significantly inhibited lactate-induced limb lesions. In this
model, it is supposed that dysfunction or injury of ECs may lead to
adhesion of platelets to subendothelial collagen and initiate their
aggregation. Thus, ECs and platelets are major cell types involved in this
injury. However, considering a recent study,34 in which platelet activation
by LPA was mediated by Ki16425-insensitive LPA5 receptors, the results of
Supplementary material online, Figure S9 suggest that LPA may be involved
in the dysfunction or injury of ECs through the Ki16425 derivative-
sensitive LPA receptors, probably LPA1.

Our lactate limb-lesion experiments suggested that LPA may be involved in
the dysfunction or injury of ECs, possibly through LPA1, in vivo. In
contrast, studies with mice deficient in autotaxin, a major enzyme for LPA
synthesis, showed profound vascular defects in the yolk sac and
embryo, 10,11 resembling the Gα13 knockout phenotype, 35 suggesting a
critical role of LPA signaling through G13 proteins in vascular
stabilization. Thus, in addition to the in vitro experiments described
above in the Introduction, 4–6,8,9 in vivo results suggest that LPA has
positive and negative effects on vascular integrity. In the present study,
for the first time, we showed that the opposite actions of LPA are mediated
by different subtypes of LPA receptors; LPA1 disrupts, whereas a recently
identified LPA6 protects, barrier function of ECs in the specific culture
conditions. The role of LPA6 was confirmed by in vitro tube formation
assay, reflecting stabilization of vascular integrity, on the specific
extracellular matrix gel, excluding the possible occurrence of protective
effects through LPA6 only in the unusual culture conditions without
additional extracellular matrix.

The LPA6-mediated protection of barrier function required a high
concentration of LPA ∼10 µmol/L C18:1 LPA, whereas LPA1-mediated VE-
cadherin internalization and subsequent endothelial cell–cell dissociation
occur at a low concentration ∼1 µmol/L C18:1 LPA. Much lower affinity of
LPA6 roughly 100 times than that of LPA1 may account for this
observation 15. Although the LPA concentration in plasma is maintained in
the low nanomolar range in normal conditions, LPA can be greatly increased
in microenvironments of platelet clotting and pathological lesions, such as
atherosclerosis4 and cancer, 36 where LPA may play a critical role to
regulate endothelial barrier functions.

It is unlikely that intracellular Ca2+ and cAMP, important intracellular
messengers in diverse LPA-induced actions in a variety of cells, play an
appreciable role in the regulation of VE-cadherin integrity by LPA in ECs,
as assessed by the failure of a significant change in both intracellular
signaling molecules data not shown 6. Our results indicated that LPA1
disrupts VE-cadherin-mediated barrier integrity through the Gi
protein–Rac1–JNK/p38MAPK pathway, whereas LPA6 protects it through the
G12/13 protein–Src–Rap1 pathway. The proposed scheme of signaling
mechanisms of regulation of VE-cadherin integrity through LPA1 and LPA6 is
shown in the Supplementary material online, Figure S10.

LPA-induced stimulation of disruption of cell–cell contact has been
attributed, in some cases, to the contraction of ECs through RhoA/Rho
kinase. 6 In addition to RhoA, however, Rac1 has also been suggested for the
disruption of the endothelial barrier without contraction of cells by
vascular endothelial growth factor 37,38 and platelet-activating factor 39.
Our results support the role of Rac1 in the disruption of cadherin-mediated
endothelial integrity. Although LPA6 appeared to mediate contraction
through Rho kinase when HUVECs were cultured sparsely on collagen-coated
plates Supplementary material online, Figure S6,15 RhoA activation may
not be involved in the regulation of barrier integrity by LPA in the
confluent culture conditions employed in the present study Supplementary
material online, Figure S7. In addition to the difference in cell density,
extracellular matrix might affect the regulatory roles of RhoA and Rac1 in
cell contraction and cell–cell association Supplementary material online,
Figure S1. JNK and p38MAPK may be working downstream of Rac1 and involved
in the disruption of VE-cadherin integrity, 23,27–29, which may in part
account for the distinct role of Rac140 in the regulation of barrier
function. However, how Rac1 regulates MAPKs and downstream signaling
awaits further characterization.

In addition to the disruptive action of LPA, the lipid protects the
endothelial barrier function through LPA6, with intracellular signaling
pathways different from those of LPA1. G12/13 protein seems to mediate LPA6-
induced protection of the barrier function. However, it is unlikely that
RhoA and Rho kinase may play a role either in the LPA1-mediated disruption
as described above or in the LPA6-mediated protection of VE-cadherin
Supplementary material online, Figure S7. Similar linkage of G12/13
protein and Src independent of RhoA has been implicated in integrin-
mediated cell adhesion to the extracellular matrix in platelets, 32 thrombin-
induced Akt phosphorylation in platelets, 30 and aldosterone-induced
elastogenesis in cardiac fibroblasts 31.

The present study demonstrated that Rap1 plays a key role in LPA6-mediated
protection of barrier function. The protective role of Rap1 in VE-cadherin
assembly is widely recognized 1,33. Inhibition of Rap1 activity by Rap1GAPII
and forced expression of constitutively active Rap1 hardly affected LPA-
induced JNK and p38MAPK activation, suggesting that Rap1 modulates the
downstream signaling pathway of JNK and p38MAPK, thereby protecting VE-
cadherin integrity. The regulatory mechanism by Rap1, however, remains
uncharacterized. Further experiments are required to investigate the
molecular mechanism involved in the downstream signaling of Rap1. Src
seems to play a protective role in VE-cadherin integrity in the presence of
a high concentration of LPA in our system. The Rap1 activation in
association with Src activation may compensate for or overcome the tyrosine
kinase-induced disruption of VE-cadherin integrity, which has been reported
with vascular endothelial growth factor and cytokines 1,3. Consistently with
previous studies, 41,42 knockdown experiments with C3G siRNA suggested that
C3G, a Rap1GEF, may function in the downstream pathway of Src signaling
for the activation of Rap1.

In conclusion, the balance of small GTPases, including RhoA, Rac1, and
Rap1, may determine the degree of VE-cadherin integrity, depending on the
LPA concentration, LPA receptor subtypes, cell density, and extracellular
matrix. While a low concentration of LPA induced VE-cadherin
internalization and cell–cell dissociation through LPA1–Gi
protein–Rac1–JNK/p38MAPK, a high concentration of LPA protected the
endothelial barrier integrity through LPA6–G12/13 protein–Src–Rap1.
