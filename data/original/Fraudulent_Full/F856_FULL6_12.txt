Nonsteroidal anti-inflammatory drugs NSAIDs are
prescribed widely for patients with painful osteoarthritis,
many of whom subsequently require total
joint arthroplasty. Because of the increased risk of
perioperative bleeding, NSAIDs frequently are discontinued
7 to 10 days before elective surgery 1.
Continuing NSAIDs before total hip arthroplasty
has been associated with a twofold increase in the
incidence of perioperative bleeding 2,3. A reduction
in perioperative blood loss is a desirable goal to
facilitate the operative procedure and to reduce the
risks of blood transfusion as well as wound hematoma
or hemarthrosis, which could necessitate further
surgery. Discontinuing NSAIDs before surgery
can result, however, in an arthritic flare, not only in
the operative joint, but also in other arthritic joints,
leading to increased preoperative pain. Severe postoperative
pain may result because the intensity of
preoperative pain has been shown to correlate directly
with the severity of pain and amount of
opioid analgesics required in the postoperative period
after total joint arthroplasty 4. A flare-up of
arthritis in other joints may interfere with postoperative
physical therapy and rehabilitation. Patients
undergoing total knee arthroplasty TKA receive
warfarin for deep vein thrombosis prophylaxis.
The concomitant administration of warfarin and
NSAIDs results in an increase in the incidence of
bleeding complications 5.
Rofecoxib, a selective cyclooxygenase 2 COX-2
inhibitor, shows similar analgesic efficacy to ibupro-
fen and naproxen in the management of postsurgical
dental pain 6–8. The perioperative administration
of rofecoxib has shown a significant opioidsparing
effect after major orthopaedic surgery 9.
Selective COX-2 inhibitors do not interfere with the
coagulation system and may provide effective postoperative
analgesia without an increased incidence
of bleeding when used during the perioperative
period 9. The primary purpose of this study was to
determine whether rofecoxib can be administered
safely in the perioperative period to patients with
osteoarthritis scheduled to undergo TKA. A secondary
endpoint of this study was to determine
whether rofecoxib is an effective analgesic in the
management of osteoathritic pain before surgery.
A total of 100 patients scheduled to undergo
elective cemented TKA were included in this Institutional
Review Board–approved, randomized,
double-blind study. Patients were eligible for this
study if they had osteoarthritis requiring NSAID
therapy, were American Society of Anesthesiologists’
physical status I–III, aged 40 to 85 years, and
weighed 40 to 110 kg. Patients were excluded if
they had a history of peptic ulcer disease, renal
insufficiency serum creatinine, _ 1.5 mg/dL, or
blood urea nitrogen, _ 22 mg/dL, asthma in association
with nasal polyps, or bleeding diathesis or if
preoperative coagulation tests platelet count, activated
partial thromboplastin time, prothrombin
time, and international normalized ratio INR
were abnormal.
One month before scheduled surgery, 87 n _
 87 of the enrolled patients predeposited 1 to 2
units of autologous blood. All patients started oral
iron until the day before surgery. Autologous donation
was per surgical routine and patient preference
and was not controlled for study purposes. All
NSAIDs were discontinued 10 days before surgery,
and the patients were assigned randomly in a double-
blind manner to 1 of 2 groups: The placebo
group n _  50 received a placebo pill every day
starting 3 days before surgery. The rofecoxib group
n _  50 received rofecoxib, 25 mg, every day
starting 3 days before surgery. All patients were
asked to rate their pain score on a visual analog
scale VAS, with 0 representing no pain and 10
representing the worst imaginable pain. Pain scores
were obtained at rest and with movement walking
a distance of 100 feet while off NSAID therapy for
7 days and while on their study medications for 3
days. Patients were asked to rate their overall VAS
pain score immediately before surgery, in the preoperative
holding area.
Intraoperatively, all patients received the same
general anesthetic technique thiopental sodium
Pentothal, 3–5 mg/kg; fentanyl, 5 _ g/kg; and
isoflurane. A lumbar epidural catheter was placed
before surgery. After a test dose 3 mL of 1.5
lidocaine with epinephrine 1:200,000, 8 mL of
0.125 bupivacaine was injected. No other epidural
analgesics were administered during surgery.
Mean arterial blood pressure was maintained
within 20 of baseline values normotensive anesthesia,
and all patients were kept normothermic
36.5 _  0.5°C. With the patient under general
anesthesia, the operative extremity was exsanguinated
with an Esmarch bandage, and a pneumatic
tourniquet was inflated to 350 mm Hg. The knee
joint was exposed through a midline incision of the
skin and a medial parapatellar capsular incision,
and all components were cemented. Before cementing
and at the end of the operation, the tourniquet
was deflated, and hemostasis was achieved.
The wound was closed in layers over 2 large 1/8-
inch suction drains, which were connected to a
salvage system. Salvaged blood loss was reinfused
for the first 6 hours postoperatively, then the system
was placed to gravity overnight. All drains were
removed the following day. Intraoperative blood
loss was estimated by combining changes in sponge
weights assuming a density of 1 g/mL with blood
volume collected in the suction canister. Postoperative
blood loss was determined from drain output
for the 24 hours after surgery.
Patients were connected to a patient-controlled
epidural analgesia PCEA pump Abbott PCA Plus,
Abbott Park, Chicago, IL on arrival in the postanesthesia
care unit. The PCEA solution contained
0.1 bupivacaine and 5 _ g/mL of fentanyl. Initial
settings were as follows: basal rate, 6 mL/h; incremental
dose, 3 mL; lockout interval, 20 minutes;
and 4-hour limit, 60 mL. Patients continued their
prearranged study medications for the 48 hours
after surgery placebo or rofecoxib, 25 mg. In addition,
patients received warfarin, 5 mg orally, every
day starting at 10:00 PM  on the day of surgery.
Seven days before surgery, patients had baseline
preoperative laboratory evaluation, including serum
hemoglobin, hematocrit, prothrombin time,
platelet count, and INR. These studies were obtained
each morning postoperatively on a daily
basis in addition to regular testing of stool for guaiac.
Demographic data, operative and tourniquet
times, hemoglobin, hematocrit, INR, and blood loss
were analyzed with analysis of variance. Pain scores
were analyzed by using the Kruskal-Wallis test. If a
significant result was obtained, Wilcoxon testing
was performed to determine between which groups
there was a significance; a Bonferroni adjustment
was made for multiple comparisons. Significance
was determined at the P _  .05 level.
A power analysis was performed with the following
assumptions: _  of .05; power of 90; average
intraoperative blood loss of 250 _  100 mL, and
24-hour postoperative blood loss of 1,000 _  300
mL. We assumed that a blood loss of 250 mL is
statistically significant based on previous data from
blood loss in TKA studies 10–12. Based on these
assumptions, a sample size of 26 patients per treatment
group would be required to determine a significance
in 24-hour blood loss.
There were no differences in demographic variables, operative times, or tourniquet times between
the 2 groups Table 1. There was no difference in
the total volume of epidural analgesia infused during the 48 hours postoperatively between the placebo 366  39 mL or the rofecoxib 302  23 mL
group. Preoperative VAS pain scores while off
NSAID therapy at rest and with movement were
similar for both groups Table 2. VAS pain scores
while on study medication for 3 days were significantly lower at rest and with movement for the
rofecoxib group compared with placebo P  .0001
Table 2. VAS pain scores immediately before surgery were significantly lower for the rofecoxib
group compared with placebo P  .0001 Table
2. There were no differences in intraoperative
blood loss 272  32 mL vs 262  37 mL, blood
loss in the postanesthesia care unit 425  33 mL vs
402  46 mL, blood loss on the surgical ward
423  37 mL vs 420  38 mL, or 24-hour total
blood loss 1,124  63 mL vs 1,084  89 mL for
the control versus the rofecoxib group. Similarly,
there were no differences in the number of patients
requiring postoperative blood transfusions 27 of 50
54 vs 31 of 50 62 or blood transfused in the
first 72 postoperative hours 640  37 mL vs 610 
23 mL in the placebo versus rofecoxib group. There
were no differences in hemoglobin, hematocrit, or
INR on postoperative days 1, 2, or 3 between the 2
groups Table 3.
There were no other postoperative complications
observed during the study period, including wound
hematoma, gastroduodenal perforation, ulcers, and
bleeds. No patient had guaiac-positive stool during
the study period. There were no clinically significant alterations in renal function serum creatinine,
1.5 mg/dL; blood urea nitrogen increase of at least
200 from baseline or absolute value, 22 mg/dL;
serum potassium, 5.5 mmol/L; or serum sodium,
120 mmol/L observed during the 5-day study
period.
In this study, we have shown that rofecoxib can
be administered safely in the perioperative period to
patients undergoing cemented TKA. The administration
of rofecoxib starting 3 days before surgery
and continued 2 days postoperatively resulted in no
significant increase in the incidence of perioperative
bleeding or wound problems. The use of rofecoxib
with concomitant warfarin therapy resulted in no
significant increase in the INR compared with placebo.
A major concern with the perioperative administration
of NSAIDs is their inhibitory effect on platelet
aggregation. This concern has led to the recommendation
that NSAIDs be discontinued before
patients undergo elective surgery 1–3. Continuing
conventional NSAIDs before THA has been associated
with a twofold increase in the incidence of
perioperative bleeding, resulting in higher transfusion
requirements 2. The perioperative use of
ketorolac, a parenteral NSAID, has been associated
with an increased risk of bleeding in patients undergoing
TKA 13. The use of NSAIDs has been
associated with other postoperative complications,
including wound hematoma, upper gastrointestinal
tract bleeding, and hypotension 1. The likelihood
of developing these complications was found to be
5.8 times greater for patients using NSAIDs 24
hours before surgery than without such usage 1.
In contrast to conventional NSAIDs, the use of
COX-2 specific inhibitors may represent a significant
therapeutic advance in the perioperative management
of pain. Preliminary data from animal
studies have revealed that selective inhibition of
COX-2 produces analgesia with substantial safety
advantages over existing NSAIDs 13. Conventional
NSAIDs nonspecifically inhibit the COX-1
and the COX-2 isoforms 14. It is believed that the
therapeutic activity of NSAIDs is primarily the inhibition
of COX-2, whereas the toxicity results from
inhibition of COX-1 15. Because platelets express
only COX-1 and are incapable of expressing COX-
2, 16 selective COX-2 inhibitors do not inhibit
platelet function. Rofecoxib has been shown to
have no effect on platelet function measured by
serum thromboxane production and ex vivo  platelet
aggregation 17. Rofecoxib in doses of 1,000 mg/d
had no effect on platelet aggregation or bleeding
time 8. A previous study showed that a single
dose of either selective COX-2 inhibitor, rofecoxib,
50 mg, or celecoxib, 200 mg, administered before
surgery resulted in no significant increase in the
incidence of intraoperative bleeding during major
spine surgery 9. Our present study revealed that
the preoperative and continued postoperative administration
of rofecoxib had no effect on the incidence
of intraoperative and postoperative bleeding
after TKA. There were no other postoperative complications
observed during the study period, including
wound hematoma, gastroduodenal perforation,
ulcers, and bleeds. No patient had guaiac-positive
stool during the study period.
Another risk factor for postoperative bleeding
with NSAID use is the concomitant administration
of warfarin. Antithrombotic agents such as warfarin
are administered routinely as prophylaxis against
thromboembolism after many orthopaedic procedures
18, and their interaction with NSAIDs
has been associated with an increased risk of bleeding
complications 5. Conventional NSAIDs enhance
the hypoprothrombinemic effect of warfarin
through a pharmacokinetic action by inhibiting the
hepatic metabolism of this drug 5. Conventional
NSAIDs can alter protein binding of warfarin and
result in an increased incidence of bleeding 5. It is
recommended that the concomitant administration
of NSAIDs with warfarin be avoided 5. Rofecoxib
offers an advantage, however, to patients being
administered warfarin. Previous studies have
shown that patients taking warfarin doses of 2 to
8.5 mg daily in conjunction with rofecoxib, 25 mg
daily, had mean increases in INR of approximately
8 19. In our study, the concurrent administration
of rofecoxib, 25 mg, and warfarin, 5 mg, daily
resulted in no significant increase in either the
prothombin time or INR when compared with placebo.
The secondary endpoint of this study was to
determine whether rofecoxib can manage osteoarthritic
pain effectively before anticipated surgery.
After discontinuing NSAIDs, most patients in each
study group reported moderate-to-severe pain with
movement VAS _ 7. When the study group patients
started rofecoxib for 3 days before surgery,
they noted a significant reduction in pain scores at
rest and with movement compared with placebo.
Undertreatment of preoperative pain can result in
increased patient dissatisfaction as well as increased
pain intensity and opioid use in the postoperative
period 4. Increased pain in the postoperative period
after TKA interferes with the patient’s ability to
participate in a rehabilitation program and to regain
an optimal level of function. Early ambulation is
impeded, resulting in delayed discharge from the
hospital and higher medical costs and potentially
influencing long-term recovery 20. In the present
study, we used PCEA as the primary analgesic technique
in the immediate postoperative period. We
previously showed that PCEA is an effective modality
in the management of pain after TKA, with most
patients reporting VAS pain scores _ 3 21. To
show any further significant reduction in pain
scores with the additional use of rofecoxib, a considerably
larger sample size would be required.
Clinical studies are being performed to determine
the analgesic efficacy of administering rofecoxib in
conjunction with patient-controlled analgesia opioids
after total joint arthroplasty.
The results of this study show that rofecoxib can
be administered safely in the perioperative period
to patients undergoing TKA. The administration of
rofecoxib in conjunction with warfarin resulted in
no significant increase in the incidence of perioperative
bleeding or INR compared with placebo. The
administration of rofecoxib before surgery resulted
in improved preoperative pain scores at rest and
with movement. Based on our results, we recommend
that, in contrast to conventional NSAIDs,
rofecoxib does not need to be discontinued before
elective cemented TKA.