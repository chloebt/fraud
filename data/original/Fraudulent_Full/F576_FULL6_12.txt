Human T-cell leukemia virus type I HTLV-I is the
causative agent of adult T-cell leukemia ATL, a
malignant condition of mature CD4+
T-cells 1–3. HTLV-I is
also linked to the development of several chronic inflammatory
diseases such as HTLV-I-associated myelopathy/tropical spastic
paraparesis 4,5. Presently, there is no accepted curative therapy
for ATL and patients often progress to death, with a median
survival time of 13 months for those with aggressive ATL 6.
Conventional therapies do not appear to prolong the life of
patients with ATL and hence the establishment of new therapeutic
strategies for ATL is necessary.
Akt is a serine/threonine protein kinase that functions as
a critical regulator of cell survival and proliferation. A variety
of growth factors and other extracellular stimuli can activate
phosphatidylinositol 3-kinase PI3K through activation of their
cognate receptors. The activated PI3K converts the plasma
membrane lipid second messenger phosphatidylinositol-
4, 5-bisphosphate to phosphatidylinositlo-3,4,5-triphosphate
PIP3. Subsequently, PIP3
recruits downstream molecules,
particularly Akt and phosphoinositide-dependent kinase
PDK-1, via binding to their pleckstrinhomology domains 7.
At the membrane, Akt is activated through phosphorylation
by PDK-1 8,9. Activated Akt in turn regulates a wide range of
target proteins, such as the FOXO transcription factors, Bad,
caspase-9, glycogen synthase kinase GSK-3β and the tuberin/
hamartin complex, which regulate cell survival, proliferation
and growth.
The Akt signaling pathway is not regulated in numerous
tumors 10. Manipulation of Akt activity resulted in altering the
response of tumor cells to chemotherapy and irradiation 11,12.
Previous experiments showed that activation of PI3K-Akt
signaling is involved in fibroblast Rat-1 transformation by the
HTLV-I Tax protein 13. These findings suggest that the Akt
signaling pathway may be related to the cell transformation
of HTLV-I-infected T-cells, and inhibitors of this pathway
may be effective in the treatment of ATL.
Selective inhibitors of the Akt signaling pathway are not
only potent tools for investigating the biological roles of this
important signaling pathway,14 but also potential therapeutic
candidates for the treatment of Akt signaling-dependent
tumors 11,12. Although there is tremendous interest in the regulation of the Akt signaling pathway for the benefit of killing
cancer cells, 15,16 not many inhibitors of this pathway have
been identified since the first synthetic PI3K inhibitor LY294002
was reported 14.
Curcumin diferuloylmethane, the major yellow pigment
in turmeric, which is commonly used as a flavoring and
coloring agent in foods, drinks and cosmetics, is obtained
from rhizomes of the plant Curcuma longa Linn. Several
recent studies have shown that curcumin has anti-inflammatory,
antioxidant and anticarcinogenic properties 17,18. Curcumin
suppresses tumor initiation and promotion in animal models
and is a potent inhibitor of key molecules in oncogenic signaling pathways, including cycloxygenase-2,19 lipoxygenase,
ornithine decarboxylase, 20,21 c-Jun/AP-1,22 NF-κB,23 c-Jun
N-terminal kinase 24 and protein kinase C 25. Recently, we
demonstrated that curcumin suppresses cell growth and
survival of HTLV-I-infected T-cell lines and primary ATL
cells by inhibiting the NF-κB and AP-1 signaling pathways 26,27.
However, little is known about the effect of curcumin on the
Akt signaling pathway.
In the present study, we hypothesized that Akt signaling
is necessary for malignant cell growth and survival of ATL
cells, that curcumin could be effective against ATL, and that
the anti-ATL effect of curcumin is mediated through the
inhibition of Akt activity. To test our hypothesis, we evaluated Akt activity in HTLV-I-infected T-cell lines and primary
ATL cells, and examined the effects of curcumin on Akt
activity of these cells.
 
LY294002 was obtained from Calbiochem La Jolla, CA, USA.
Curcumin was purchased from Merck kgaA Darmstadt,
Germany.
 
Anti-Akt, antiphospho-Akt Ser473, anti-PDK1, antiphosphoPDK1 Ser241 and antiphospho-GSK-3β Ser9 antibodies
were purchased from Cell Signaling Technology Beverly,
MA, USA. Anti-GSK-3β antibody was purchased from BD
Transduction Laboratories San Jose, CA, USA. Anti-actin
and anti-c-Myc antibodies were obtained from NeoMarkers
Fremont, CA, USA. Anti-cyclin D1 antibody was purchased
from Medical and Biological Laboratories Nagoya, Japan.
Horseradish-peroxidase-conjugated antimouse and antirabbit
IgG antibodies for western blotting were purchased from
Amersham Biosciences Arlington Heights, IL, USA.
 
The HTLV-I-infected T-cell lines, MT-2,28 C5/MJ,29 SLB-130
and HUT-1022 were maintained in RPMI 1640 medium
supplemented with 10 heat-inactivated fetal bovine serum,
50 U/mL penicillin and 50 µg/mL streptomycin Sigma-Aldrich,
St Louis, MO, USA at 37°C in 5 CO2. MT-2, C5/MJ and
SLB-1 are HTLV-I-transformed T-cell lines established by an
in vitro coculture protocol. The clonal origin of HUT-102 had
not been determined.
 
Peripheral blood mononuclear cells PBMCs were obtained
from two healthy volunteers or two patients with ATL. The
diagnosis of ATL was based on clinical features, characteristic
hematological findings, presence of serum antibodies to ATL-associated antigens, and presence of HTLV-I proviral genome
in DNA from leukemic cells. PBMCs were isolated by ficollHypaque Pharmacia LKB, Piscataway, NJ, USA with density
gradient centrifugation. Each patient had more than 90
leukemic cells in the blood at the time of analysis. The study
protocol was approved by the Human Ethics Review Committee
of University of the Ryukyus, and a signed consent form was
obtained from each patient.
 
Western blot analysis was performed as described previously 31.
In brief, whole cell lysates were subjected to sodium dodecyl
sulfate-polyacrylamide gel electrophoresis and electroblotted
onto polyvinylidene difluoride membranes Millipore Billerica,
MA, USA, and then analyzed for immunoreactivity with
the appropriate primary and secondary antibodies, as indicated
in the figures. Reaction products were visualized using Enhanced
Chemiluminescence reagent, according to the manufacturer’s
instructions Amersham Pharmacia, Uppsala, Sweden.
 
The Akt kinase assay was performed using the Akt kinase
assay kit Cell Signaling Technology according to the protocol
recommended by the manufacturer. Briefly, the cells were
washed with phosphate-buffered saline and 200 µL lysis buffer
20 mM Tris-HCl pH 7.5, 150 mM NaCl, 1 mM EDTA,
1 mM EGTA, 1 Triton X-100, 2.5 mM sodium PPi, 1 mM
β-glycerol phosphate, 1 mM Na3
VO4, 1 mM phenylmethylsulfonyl fluoride and 1 mM leupeptin was added to the cells
for 10 min. Lysates were immunoprecipitated for 2 h at 4°C
with anti-Akt antibody. The immunoprecipitates were washed
with lysis buffer and kinase buffer. Kinase reaction was
performed for 30 min at 30°C in kinase buffer in mM, 25
Tris-HCl pH 7.5, 5 β-glycerol phosphate, 2 dithiothreitol,
0.1 Na3 VO4 and 10 MgCl2 supplemented with 200 mM
adenosine triphosphate ATP and 1 µg GSK-3α/β fusion
protein. Samples were loaded into a 12 acrylamide gel.
Phosphorylation of GSK-3α/β was measured by western
blotting with antiphospho-GSK-3α/β Ser21/9 antibody.
 
The antiproliferative effects of LY294002 against different
HTLV-I-infected T-cell lines and normal PBMCs from a
healthy volunteer were measured by the WST-8 method
Cell Counting Kit-8; Wako Chemical, Osaka, Japan based
on the MTT assay, as described previously.32 Briefly, 5 × 103
cells cell lines or 1 × 105
cells PBMCs were incubated
in triplicate in a 96-well microculture plate in the presence
of different concentrations of LY294002 0–50 µM in a final
volume of 0.1 mL for 48 h at 37°C. Subsequently, 5 µL Cell
Counting Kit-8 solution 5 mM WST-8, 0.2 mM 1-methoxy
5-methylphenazinium methylsulfate and 150 mM NaCl was
added and the cells were further incubated for 4 h. The number
of surviving cells was determined by a 96-well multiscanner
autoreader at an optical density of 450 nm. Cell viability was
determined as a percentage of the control without LY294002.
To determine the status of Akt activation in HTLV-I-infected
T-cell lines, phosphorylation of Akt was assessed by immunoblotting with phospho-specific antibody against phosphorylated Akt at Ser473.
Phosphorylated Akt protein was detected in
all HTLV-I-infected T-cell lines. Although Akt protein was
expressed in HTLV-I-negative PBMCs of healthy volunteers
Normal #1 and #2, the expression level of phosphorylated
Akt in these cells was weaker than that in MT-2 cells fig 1a,
top panels. PDK1, an upstream kinase that is an activator of
Akt, was highly expressed and phosphorylated in all HTLVI-infected T-cell lines fig 1a, third and fourth panels. One
of the downstream substrates of Akt is GSK-3β whose
activity is repressed through Akt-mediated phosphorylation
at Ser9 33. It is therefore consistent that GSK-3β was
phosphorylated in all HTLV-I-infected T-cell lines fig 1a, fifth
and sixth panels. In contrast, the expression of phosphorylated
PDK1 and GSK-3β was weaker in HTLV-I-negative PBMCs of
healthy volunteers than in MT-2 cells fig 1a, third-sixth panels.
To demonstrate that phosphorylated Akt is enzymatically active,
we performed in vitro Akt kinase assays with GSK-3 fusion
protein as a substrate. The cell lysates were immunoprecipitated
with anti-Akt antibody and kinase reaction was performed
with GSK-3α/β fusion protein. Phosphorylation of GSK-3α/
β was measured by western blotting using antiphospho-GSK-
3α/β Ser21/9 antibody. Akt kinase activity of HTLV-Iinfected T-cell lines was higher than that of HTLV-I-negative
PBMCs of healthy volunteers, suggesting that the
phosphorylation status of Akt was consistent with Akt kinase
activity in HTLV-I-infected T-cell lines fig 1b.
PI3K-Akt inhibitor LY294002 decreases Akt activity and 
suppresses cell growth of HTLV-I-infected T-cell lines
Akt phosphorylation was PI3K-dependent because it was
reduced in HTLV-I-infected T-cell lines treated with the PI3K
inhibitor LY294002 fig 2a, top panel. However, LY294002
had no effect on the total level of Akt fig 2a, middle panel.
These results indicate that the PI3K-Akt signaling pathway
is constitutively activated in HTLV-I-infected T-cell lines. To
evaluate the role of constitutively activated Akt in cell growth
of HTLV-I-infected T-cell lines, we treated HTLV-I-infected
T-cell lines and HTLV-I-negative PBMCs of a healthy volunteer
with LY294002, and then the cell number was counted using
the WST-8 method. The results of these studies demonstrated
that inhibition of the PI3K-Akt signaling pathway by LY294002
suppresses cell growth of HTLV-I-infected T-cell lines but not
that of HTLV-I-negative PBMCs of a healthy volunteer fig 2b.
Curcumin blocks constitutive Akt activity in HTLV-Iinfected T-cell lines and primary ATL cells
To investigate the effect of curcumin on Akt activity in
HTLV-I-infected T-cell lines, we examined the regulation of
Akt phosphorylation using curcumin. Curcumin inhibited the
constitutive phosphorylation of Akt in HTLV-I-infected T-cell
lines in a time- fig 3a, top panel and dose-dependent fig 3b,
top panel manner, but had no effect on the total level of Akt
fig 3a,b, middle panels. Furthermore, Akt was constitutively
phosphorylated in primary ATL cells and curcumin inhibited
constitutive phosphorylation of Akt in primary ATL cells
fig 3c. To evaluate the effects of curcumin on the molecule
upstream of Akt, we examined the activity of PDK1 by
measuring the phosphorylation status of this protein. PDK1
is an activator of Akt by phosphorylating Akt. Curcumin
reduced the expression of phosphorylated PDK1 in a time and dose-dependent manner without affecting the total amount
of PDK1 fig 3d,e, top and second panels. Curcumin also
inhibited phosphorylation of GSK-3β in a time- and dose-dependent manner, but had no effect on the GSK-3β level
in HTLV-I-infected T-cell lines fig 3d,e, third and fourth
panels. Thus, the reduction of phosphorylated PDK1 correlates
with those of phosphorylated Akt and GSK-3β. To examine if
the inhibitory effect of curcumin on phosphorylation of Akt
is associated with Akt kinase activity, we performed in vitro
  Curcumin suppressed Akt kinase activity
in HTLV-I-infected T-cell lines in a time-dependent manner
fig 3f. These results suggest that curcumin inhibits
constitutive Akt activity by inhibiting phosphorylation of
PDK1, resulting in dephosphorylation of GSK-3β in HTLV-Iinfected T-cell lines.
Expression of downstream target genes of GSK-3β
in curcumin-treated HTLV-I-infected T-cell lines
The activity of GSK-3β is inhibited by Akt through
phosphorylation at Ser9. Therefore, curcumin stimulates
GSK-3β activity. We next examined whether or not curcumin
alters the expression of downstream target proteins, cyclin
D1 and c-Myc, which are phosphorylated and degraded
by activated GSK-3β 34,35. Cyclin D1 was overexpressed in
HTLV-I-infected T-cell lines, and curcumin markedly reduced
the expression of cyclin D1 following 5 h exposure or longer
fig 4, upper panels. Curcumin also reduced the expression
of c-Myc in a time-dependent manner fig 4, middle panels.
The major findings of our study are: i the Akt signaling
pathway is constitutively activated in HTLV-I-infected T-cell
lines and primary ATL cells, but not in HTLV-I-negative
PBMCs from healthy volunteers. ii Inhibition of the Akt
signaling pathway by LY294002 leads to suppression of cell
growth of HTLV-I-infected T-cell lines, but not that of HTLVI-negative PBMCs from a healthy volunteer. iii Curcumin
reduces phosphorylation of PDK1 and inhibits activation of
the Akt signaling pathway. Because curcumin suppresses cell
growth of HTLV-I-infected T-cell lines and primary ATL
cells, 26,27 curcumin may suppress cell growth, at least in part,
by inhibiting the Akt signaling pathway. Our results indicate
that the constitutive active Akt signaling pathway may play a
critical role in ATL cell survival
We showed that Akt was constitutively activated in HTLVI-infected T-cell lines and primary ATL cells. Several reports
have suggested that Akt can be activated in a PI3K-independent manner. However, Akt activation is PI3K-dependent in
HTLV-I-infected T-cell lines, because phosphorylation of Akt
at Ser473 is sharply reduced in cells treated with the PI3K
inhibitor LY294002 fig 2a. These results suggest that HTLVI-infection can induce constitutive Akt activation through the
PI3K signaling pathway. PDK1 was also constitutively phosphorylated in HTLV-I-infected T-cell lines. Activation of PI3K leads
to colocalization of Akt with PDK1 at the plasma membrane
and PDK1 can phosphorylate Akt at Thr308. Although
phosphorylation at Thr308 partially activates Akt,36 full activation of Akt requires phosphorylation on Ser473. However, the
mechanism mediating Ser473 phosphorylation remains controversial Because Ser473 phosphorylation is dependent on
PI3K, in addition to Thr308, PDK1 is assumed to be the kinase
for Ser473 phosphorylation 37. Therefore, constitutively phosphorylated PDK1 may enhance Akt kinase activity in HTLVI-infected T-cell lines. Blocking Akt activity by LY294002
induced cell growth arrest in HTLV-I-infected T-cell lines
fig 2b, suggests that constitutive activated Akt plays a role
in growth and survival signaling of HTLV-I-infected T-cells.
Previous studies showed that HTLV-I transforming protein
Tax induces PI3K signaling pathway activation and is associated with cell transformation of fibroblast cell line stably
expressing the Tax protein 13. During preparation of this article, Jeong et al showed that the Akt signaling pathway is
activated in HTLV-I-transformed cells, and Tax can activate
this signaling pathway by inducing Akt phosphorylation 38.
These observations indicate that the Tax protein plays a critical role in constitutive activation of Akt in HTLV-I-infected
T-cell lines. Although we confirmed the activation of Akt in
HTLV-I-infected T-cell lines, phosphorylation of Akt was
also observed in some primary ATL cells fig 3c, in which
expression of the Tax protein was not detected data not shown.
These results suggest that Tax-independent mechanisms that
can activate the Akt signaling pathway may exist. We are currently investigating the mechanisms of Tax-independent Akt
activation in ATL cells.
It has been shown that curcumin and its derivatives inhibit
Akt in human renal and prostate cancer cell lines 39–41. Consistent with these observations, curcumin suppressed Akt
activity of HTLV-I-infected T-cell lines and primary ATL cells
fig 3a,c. Curcumin also reduced constitutive phosphorylation
of PDK1, suggesting that curcumin inhibits Akt activity by
reducing phosphorylation of PDK1.
Although components of the PI3K-Akt signaling pathway
present promising targets for therapeutic intervention,
non-specific drug toxicity is one of the major problems in
anticancer drug development. Because the PI3K-Akt pathway
is involved in the survival, growth and proliferation of normal
cells, a reasonable therapeutic index would depend on tumors
being more sensitive to inhibitors of this pathway than normal
tissues. Curcumin hardly inhibited the survival of normal
PBMCs and systemic treatment of curcumin caused no
obvious toxicity in mice 26. Moreover, phase I clinical trials
indicate that curcumin is tolerated in extremely large oral
doses without apparent toxicity in humans.42 These results
indicate that curcumin is a pharmacologically safe agent.
Therefore, curcumin is a potentially useful anti-ATL drug.
Multiple oncogenic signals such as NF-κB,43 AP-1, 44
Jak-Stat45 and Akt pathways may be involved in leukemogenesis of ATL. Curcumin has been reported to regulate these
signaling pathways 22,23,39–41,46,47. Recently, we demonstrated
that curcumin suppresses cell growth and survival of HTLVI-infected T-cell lines and primary ATL cells by inhibiting
the NF-κB and AP-1 signaling pathways 26,27. Because
curcumin inhibits multiple signaling pathways which are
associated with leukemogenesis of ATL, we propose that
curcumin is a potentially promising compound for the
treatment of ATL.
In summary, we demonstrated that Akt signaling is constitutively activated in HTLV-I-infected T-cell lines and primary
ATL cells, and activation of Akt is linked to cell growth of
HTLV-I-infected T-cell lines. Curcumin, which is known to
suppress cell growth and survival of HTLV-I-infected T-cell
lines and primary ATL cells, inhibited activation of the Akt
pathway and reduced the expression of cyclin D1 and c-Myc
proteins, downstream targets of the Akt signaling pathway.
Our results highlight the importance of the PI3K-Akt signaling
pathway as a new target for the development of therapeutic
strategies against ATL. We propose that curcumin is a potentially
promising compound for the treatment of ATL.