
LIDOCAINE was widely used for spinal anesthesia
on outpatients for lower limb procedures
due to its fast onset and short duration. Unfortunately,
spinal lidocaine is often associated with
transient neurologic symptoms TNS 1, 2. This
disturbing syndrome has led to interest in
finding out alternative agents for spinal route.
Small doses of long-acting local anaesthetics of
different baricities with or without additives have
been used for that purpose 3–5. However, the use
of these low doses has resulted to an increased
incidence of block failure and delayed recovery
6, 7. Recently, articaine, clinically similar agent to
lidocaine, was also suggested for day-case spinal
anesthesia 8.
2-chloroprocaine 2-CP has been reintroduced
for spinal anesthesia and a preservative-free solution
of CP is available for off-label use 9, 10. 2-CP is
an amino-ester local anaesthetic with a very short
half-life and this shows a favorable profile for short procedures.
Therefore, it might be an interesting
agent for ambulatory spinal anesthesia. There
is not yet much information available comparing
different doses of 2-CP in surgical ambulatory
patients. Casati and colleagues have used, in two
studies, an additive-free preparation of 2-CP,
10mg/ml 11, 12. We conducted this prospective,
randomized, double-blind study to compare the
clinical profile and side effects of 35, 40, 45 and
50mg of 2-CP, 10mg/ml for short-lasting outpatient
spinal anesthesia.


After Local Ethical Committee approval and written
informed consent had been obtained, 64
patients ASA physical status I–III; age range, 18–
80 years; 28 males and 36 females scheduled for
lower limb surgery under ambulatory settings of single-shot spinal
anesthesia were included in
this prospective, randomized, double-blind study.
The exclusion criteria included pregnancy, morbid
obesity body mass index, 435 kg/m2, diabetic
and other neuropathies, skin infection at the site
of injection, allergy to 2-CP and other common
contraindications for spinal anesthesia. The
patients were randomly allocated to one of four
possible groups to receive 35, 40, 45 and 50mg of
preservative-free isobaric 2-CP Sintetica S.A.,
Mendrisio, Switzerland, 10 mg/ml for single-shot
spinal anesthesia, with the use of sealed envelopes
indicating the dose group of each patient. The CP
solution a density of 1.00064 mg/ml at 37 1C; the
density was measured in the Anton Paar DMA
Density Meter; Anton Paar Ltd, Hertford, UK
used can be considered to be isobaric with the
cerebrospinal fluid CSF. The doses were chosen
on the basis of our previous clinical experience. To
ensure that the procedure was adequately blind,
one anaesthetist was responsible for patient randomization
and performing of the spinal block, and a
second anaesthetist, unaware of the patient group
allocation, was responsible for intra-operative and
post-operative assessments and data collection. All
patients were unaware of the group allocation.

A 27-gauge pencil point spinal needle Pencan B. Braun, Melsungen, Germany
with the needle
bevel cranially oriented through the introducer in
the midline of the L3–L4 interspace identified by
the line joining the iliac crests was used. During
the spinal puncture the patients were in the left
lateral position. After the skin infiltration with
lidocaine 0.5 ml of 10 mg/ml the volume of 3.5–
5.0 ml of 2-CP was administered manually with an
injection speed of approximately 1ml in 10 s. Ten
minutes after 2-CP administration the following
were assessed: i response to pinprick at the T12
dermatome level on the non-operated side; ii
motor function on the side of surgery assessed by
modified Bromage scale 0, no motor block; 1,
inability to raise extended legs; 2, inability to flex
knees; 3, inability to flex ankle joints. Anaesthesia
was considered to be successful when there was
loss of sensation to pinprick at _ T12 and modified
Bromage scale at _ 2. Further assessments of
sensory and motor block were performed every
10 min until fulfilment of home discharge criteria.
Times from the spinal injection to complete regression
of sensory and motor block, ability to urinate
and unassisted ambulation were also recorded.
Criteria for the home discharge were follows: stable
vital signs, able to tolerate liquids by mouth, able to walk without
assistance, able to micturate and no
presence of wound pain.
Patient monitoring during operation included
electrocardiogram ECG, non-invasive measurement
of blood pressure at 3-min intervals and
recording of peripheral oxygen saturation. Any
decrease in systolic blood pressure below
90mmHg or more than 25 of the pre-operative
value was treated by an attending anaesthetist with
a 5mg ephedrine bolus, which was repeated until
hypotension was corrected. Bradycardia defined
as decrease in heart rate below 40 b.p.m. was
treated with 0.5mg atropine intravenously.
Post-operative analgesia consisted of diclofenac
100 mg/os every 12 h starting from the day of
surgery. The rescue analgesia with oral tramadol
50mg was provided if requested.
All patients were clinically assessed after operation
until home discharge and once on the third
post-operative day with the use of a telephone call
to determine signs of TNS, neurotoxicity and postdural
puncture headache.


The primary outcome parameter was the difference
in the sensory block recovery. The sample size
calculation was based on detecting at least a
9min difference between the groups in this issue
11. Considering an effect size to standard deviation
ratio ranging between 1.1 and 1.2, 16 patients
per group were required to detect the designed
difference in regression of spinal block, accepting a
two-tailed a error of 0.05 and a b error of 0.8 13.
The results are expressed as the mean standard
deviation, SD unless mentioned otherwise. An
integer was assigned to each dermatomal level
ie, T10511, T11510, T1259, L158, etc. for
statistical analysis of dermatomal height. The
normality of distribution was assessed using the
Kolmogorov–Smirnov test. The Levene’s test was
used to assess the homogeneity of the groups.
Continuous variables were compared using
one-way ANOVA with Tukey’s post hoc test for
parametric data and the Kruskal–Wallis test
with a non-parametric multiple comparison procedure
if the data were not normally distributed.
P-value o0.05 was considered statistically significant.
Analysis was performed using the statistical
package SAS 9.1 for Windows SAS Institute Inc,
Cary, NC.



The patient characteristics are presented in Table 1.
There were no differences between the groups in
terms of age, weight, height, gender, ASA physical
status and duration of surgery. Spinal anesthesia
was successful in all patients and no supplemental
analgesics were required. No differences in the
median maximum level of sensory block between
the 35, 40, 45 and 50mg dose groups were reported
P50.66, Fig 1. It was T9 in all four groups. The
recovery profile of sensory block was faster in 35
and 40mg group compared with 50mg group
P50.005, Table 2. No differences in the resolution
of motor block were observed among four study
groups. The ability to walk without assistance and
urinate was faster in 35 and 40mg group compared
with 50mg group P50.001 and 0.009, respectively.
In these matters the 45mg group did not
differ from the other groups.
Nine patients required ephedrine for correction
of hypotension cumulative dose, 1.3 3.6 mg and
four patients required atropine for correction of
bradycardia cumulative dose, 0.05 0.2 mg during
surgery.
In the perioperative period no major adverse
events occurred in any of the patients and none
of the patients had signs of TNS or post-dural
puncture headache.


This prospective, randomized, double-blind study
was designed to compare clinical profile and side
effects of different doses of 2-CP, 10 mg/ml, for
outpatient spinal anesthesia. Results of the present
study suggest that all four doses 35, 40, 45
and 50 mg of 2-CP, 10 mg/ml result in sufficient
spinal anesthesia with no need for supplemental analgesics. In our study
no differences in the
maximum level of sensory block between the 35,
40, 45 and 50mg dose groups were reported. Our
results in the maximum level of sensory block are
in accordance with recent data from studies in
ambulatory patients with the use of 2-CP, 10 mg/
ml in dose range between 30 and 50mg 11. Also
several studies in volunteers have shown that the
doses from 30 to 60mg of 2-CP, 20 mg/ml, are
suitable for outpatient spinal anesthesia 14. In
this study of 2-CP, 10 mg/ml off-label solution with
a density of 1.00064 mg/ml at 37 1C was used. This
solution used can be considered to be isobaric with
the CSF. The density of a local anaesthetic is an
important determinant of its spread in the intrathecal
space. In volunteer studies 2-CP, 15–30 mg/ml,
solutions were used 15–17. They are slightly
hyperbaric relative to CSF at 37 1C 18. Therefore
slightly different results in the spread of anesthesia
compared with our study can be obtained. In
one volunteer study, where hyperbaric solution
20–30 mg/ml, some diluted with dextrose was
used peak block height increased with increasing
dose from 10 to 60mg 19.
      In our study, the ability to walk without assistance
and urinate increased with augmenting the
dose from 35 and 40 to 50mg means 123–165 min.
With the use of 30, 40 and 50mg of 2-CP, 10 mg/ml
in patients no differences were observed in time to
void spontaneously and home discharge home
discharge in 182–203 min 11. Kopacz reported
complete sensory block regression within 103, 114
and 132 min after 30, 40 and 60mg of 2-CP, respectively.
The recovery profiles on patients of our
study are consistent with those reported in volunteer
studies 19.
In volunteer studies CP has produced anesthesia
of shorter duration than with equal dose of
lidocaine and discharge criterias were achieved
more rapidly with CP than with procaine, lidocaine
or small dose of bupivacaine 15–17. In comparison comparison
with equal doses 50 mg and concentrations
10 mg/ml of 2-CP and lidocaine in ambulatory
patients, CP produced a faster onset of spinal
block, with quicker recovery of sensory/motor
function and unassisted ambulation, and lower
incidence of TNS 12.
Some clinicians have advocated use of small
dose of hyperbaric bupivacaine instead of lidocaine,
as the incidence of TNS is significantly less
20. Unlike small-dose bupivacaine, spinal 2-CP
produces profound lower extremity motor block,
and has significantly faster resolution of block and
return to ambulation 17. When producing a unilateral
block with hypo- or hyperbaric local anaesthetics
the time required to achieve readiness
to surgery may cause pre-operative delay up to
15–30 min. The median sensory block height in our
study was T10 after 10 min, which could result
in an earlier start of operation. Therefore it seems
to be reasonable to use for fast onset a spinal
anaesthetic.
      In the early 1980s case reports of severe neurologic
injury associated with the use of epidural
CP raised concern its toxicity 21, 22. Toxicological
studies implicated sodium bisulphite at low pH
as the likely cause of neurologic deficits 23, 24.
2-CP without sodium bisulfite has not produced
these symptoms 17. In the present study we did
not find any case of TNS or signs of neurotoxicity.
The off-label 2-CP, 10 mg/ml solution which we
used is a preservative and antioxidant-free solution.
This finding is in agreement with findings
reported in studies on patients and volunteers 11,
12, 15–17. The small sample size of our study
prevents us to conclude the safety profile of this
drug for spinal route and larger studies are needed
to evaluate the incidence of TNS and signs of
neurotoxicity.
In conclusion, spinal 2-CP, 10mg/ml 35, 40, 45
and 50mg provide reliable sensory and motor
block for ambulatory surgery, while reducing the dose of 2-CP to 35 and
40mg resulted in a spinal
block of faster ambulation.
