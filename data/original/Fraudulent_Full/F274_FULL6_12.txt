
Midbrain dopaminergic mDA neurons are located in the ventral
tegmental area and substantia nigra, and have been studied
extensively owing to their involvement in Parkinson’s disease, as
well as in psychiatric and affective disorders. During development,
mDA neurons are generated in the embryonic ventral midbrain,
and numerous efforts have been made to characterize the
molecular program that controls the specification, differentiation,
and survival of mDA neurons in development. This line of
research is likely to facilitate the engineering of mDA neurons
from stem cells that, in turn, may be critical for development of
stem cell-based therapies for Parkinson’s disease.
Molecular mechanisms underlying the development of mDA
neurons are still poorly understood. The development of mDA
neurons can be divided into three distinct stages: regional specification
and early and late differentiation Ang, 2006. The first
postmitotic DA neurons are detected in the ventral midbrain at
approximately embryonic day E 10.5 in mouse, and the induction
of mDA depends on sonic hedgehog emanated from the
floor plate Hynes et al, 1995a,b; Hynes and Rosenthal, 1999
and fibroblast growth factor family member 8 FGF8 secreted
from mid/hindbrain boundary MHB ie, isthmus Ye et al,
1998; Lee et al, 2000. Secreted protein WNT1 in the midbrain is
crucial for mDA development by the prevention of expansion of
Nkx2.2-expression domain in the ventral midbrain Prakash et
al, 2006. In addition to these secreted proteins, several transcription
factors are also required for proper development of
mDA neurons Simon et al, 2004; Simeone, 2005; Prakash and
Wurst, 2006, such as Lmx1a and Msx1 Andersson et al, 2006a;
Ono et al, 2007, Foxa1 and Foxa2 Ferri et al, 2007, Nurr1
Saucedo-Cardenas et al, 1998; Sakurada et al, 1999, Pitx3
Smidt et al, 2004; Maxwell et al, 2005; Martinat et al, 2006,
Ngn2 and Mash1 Andersson et al, 2006b; Kele et al, 2006, En genes
Simon et al, 2001, 2004; Albe«ri et al, 2004, and Otx2
Puelles et al, 2004.
Lmx1a and Lmx1b are in the same LIM class homeobox gene
family, and Lmx1b is first identified as the key gene responsible
for nail–patella syndrome, a dominant inherited disease in human
Chen et al, 1998; Dreyer et al, 1998. Lmx1b is widely
expressed in developing CNS Asbreuk et al, 2002; Dai et al,
2008, and it plays a key role in the development of serotonergic
neurons in the raphe nuclei Cheng et al, 2003; Ding et al, 2003
and spinal dorsal horn Ding et al, 2004. On the other hand,
isthmic organizer IsO is the local signaling center for patterning
and growth of the mid/hindbrain region Joyner et al, 2000;
Simeone, 2000; Liu and Joyner, 2001; Wurst and Bally-Cuif,
2001; Nakamura et al, 2005, and Lmx1b expression at MHB is
essential for the development of the tectum and cerebellum by
controlling the initiation of Fgf8, the key protein for the IsO
inductive activity, and the maintenance of expression of other
IsO-related genes in mice Guo et al, 2007.
A previous study has reported the loss of mDA neurons in
Lmx1b_/_ embryo, and it was claimed that Lmx1b was essential
for the differentiation of mDA neurons Smidt et al, 2000; Burbach
and Smidt, 2006. In the present study, we revisited this
issue by selective deletion of Lmx1b in postmitotic mDA neurons
as well as in their progenitors after the formation of IsO, and
found that Lmx1b expression inmDAneurons is not required for
their differentiation or maintenance. Our data also demonstrate
that the loss of DA neurons in Lmx1b_/_ embryos is due to the
loss of IsO inductive activity during early embryonic
development.


Lmx1b gene deletion mutant Chen et al, 1998, Lmx1bflox/flox Guo et al,
2007, Nestin-Cre Isaka et
al, 1999, TH-Cre Gelman et al, 2003, and
Datcre/_ Zhuang et al, 2005 mice were generated
and genotyped as previously described. For
inactivation of Lmx1b expression in postmitotic
mDA neurons, we crossed heterozygous
Lmx1b mice with TH-Cre or DatCre/_ mice, and
their TH-Cre;Lmx1b_/_ and DatCre/_;
Lmx1b_/_ offspring were mated with
Lmx1bflox/flox mice. The desired TH-Cre;
Lmx1bflox/_ hereafter referred to as Lmx1bTH
CKO and DatCre/_;Lmx1bflox/_ Lmx1bDat
CKO progeny were obtained. To delete Lmx1b
in mDA precursors, we crossed Lmx1bflox/flox
mice with Nestin-Cre mice, and Nestin-Cre;
Lmx1bflox/flox Lmx1bNestin CKO mice were obtained.
The age of embryos was determined according
to the plug date considered to be
E0.5. Because the mating time among the mice
may differ, the actual developmental stage of
the embryo was further ascertained according
to the criteria described previously Kaufman,
1998. In each set of experiments, at least three
mutant embryos and equal number of control
embryos were used. Animal experiments were
reviewed and approved by the Animal Studies
Committee at the Institute of Neuroscience,
Chinese Academy of Sciences.
Generation of Wnt1-Lmx1b transgene. We
generated Wnt1-Lmx1b transgene according to
a reported method Danielian and McMahon,
1996. We prepared the transgenic construct by
blunt ligating a 1119 bp fragment containing
the coding region of the mouse Lmx1b complementary
DNA into the EcoRV site of the
pWexp2 construct. The construct was released from the plasmid by SalI
digestion and used for pronucleus microinjection in FVB fertilized eggs.
Transgenic embryos and mice were genotyped using PCR with primers
specific for the full length of Lmx1b. To obtain Wnt1Lmx1b;Lmx1b_/_
mice, Wnt1-Lmx1b transgenic mice were mated with Lmx1b_/_ mice.
BrdU labeling. Pregnant mice were given a single injection of BrdU 60
_g/g body weight intraperitoneally at 10.5 and 11.5 d after coitus, and
were allowed to survive for 2 h. Sections were processed for double
immunostaining
of Lmx1b and BrdU see below to localize Lmx1b expression
in the ventral midbrain.
Embryos
were fixed overnight at 4¡C in 4 paraformaldehyde in 0.1 M
phosphate buffer PB; pH 7.4 and either stored in methanol at _20¡C
for whole-mount in situ hybridization only or cryoprotected with 20
sucrose in 0.01 M PBS pH 7.4. Postnatal mice were perfused with normal
saline followed by the fixative. Brains were cryosectioned on a cryostat
and mounted onto polylysine-coated glass slides. In situ hybridization
was performed essentially the same as described previously Guo et
al, 2007. The following mouse antisense RNA probes were used: Fgf8
Wassarman et al, 1997, Wnt1 Danielian and McMahon, 1996, and
Aldhla1 and Lmx1b Ding et al, 2004. For immunostaining, the following
primary antibodies were used: rabbit and mouse anti-TH 1:2000;
Sigma, rabbit anti-Lmx1b 1:2000 Dai et al, 2008, rabbit antiserotonin
1:5000; Sigma, rabbit anti-Nurr1 1:500; Santa Cruz, rat
anti-Dat 1:1000; Chemicon, rabbit anti-Vmat2 Chemicon, goat anti-
Foxa2 1:500; Santa Cruz, and rabbit anti-Pitx3 gift from J. Peter H.
Burbach, Rudolph Magnus Institute of Neuroscience, University Medical
Center Utrecht, Utrecht, Netherlands. Sections were incubated overnight
at 4¡C with the appropriate primary antibody diluted in 0.1
Triton X-100 and 10 normal donkey serum in PBS. After being washed
in PBS, the sections were incubated 2 h at room temperature with the
corresponding biotinylated secondary antibody and then with avidin-
Cy3 Jackson ImmunoResearch Laboratories or Cy2 Jackson ImmunoResearch
Laboratories for 1 h. For double staining of TH with Lmx1b
or Nurr1, and Lmx1b with BrdU, Cy3-labeled donkey anti-mouse IgG Jackson
ImmunoResearch Laboratories and Cy2-labeled donkey antirabbit
IgG Jackson ImmunoResearch Laboratories were used. Nissl
staining was performed to examine gross brain structure as described
before Guo et al, 2007.
For analysis of mDA neuron development, we
counted numbers of TH- and Pitx3-positive neurons at E12.5, postnatal
day 0 P0, and adulthood 3 months in the mice we examined. For
embryos at E12.5, every third section 16 _m was collected; for P0 and
for adult brains, every fifth section 30 _m was collected. Numbers of
positive cells obtained from one brain were pooled and divided by numbers
of sections counted. Comparisons were performed using an unpaired
Student’s t test, and for each set of comparison at least three
mutant mice and wild-type littermate controls were included. The
statistical
significance level was set at p _ 0.05. Data are expressed as mean _
SEM.


We first examined Lmx1b expression in the ventral midbrain at
different embryonic stages and in postnatal ventral midbrain. At
E10.5, an initial stage for mDA progenitors, Lmx1b was expressed
in the ventricular zone of the ventral midbrain where mDA neurons
are generated, as shown by colocalization of Lmx1b with
BrdU injected 2 h before mice were killed Fig 1A. At E11.5, a
stage when DA neurons are actively generated,
Lmx1b expression in the ventral ventricular
zone was extinguished, and it was
only observed in the mantle layer Fig
1B. At E12.5, TH/Lmx1b-coexpressing
neurons were found in the ventral midbrain,
and its number was increased at
E14.5 Fig 1C,D. The generation of mDA
neurons from a neural progenitor cell can
be divided into three distinct phases based
on the expression of molecular markers
and the state of differentiation: regional
and neuronal specification, early differentiation,
and later differentiation Ang,
2006; Ferri et al, 2007. As shown in Figure
1C, TH/Lmx1b-coexpressing cells were located
ventral and ventrolateral to those expressing
Lmx1b only, showing Lmx1b expression
in early differentiation stage of
mDA neurons. In addition, coexpression
of TH and Lmx1b was observed at later
embryonic stages, and it persisted from
early postnatal stage to adulthood data
not shown. Thus, we confirmed previous
results Asbreuk et al, 2002; Andersson et
al, 2006; Ono et al, 2007; Dai et al, 2008
that Lmx1b is expressed in mDA neurons
throughout all developmental stages as
well as in matured mDA neurons.
Expression of Lmx1b in later differentiating
mDA neurons is not
required for their differentiation
To study whether Lmx1b is required for the
differentiation of mDA neuron, we first selectively
deleted Lmx1b in later differentiating
mDA neurons by crossing
TH-Cre;Lmx1b_/_ mice with Lmx1bflox/flox
mice. Lmx1bTH CKO mice survived postnatally
with no obvious defect in the brain.
Indeed, Lmx1b expression in Lmx1bTH
CKO embryos was largely inactivated in the ventral midbrain at
E12.5 and completely at E13.5 Fig 2B, data not shown. On the
other hand, its expression at MHB and other brain regions such
as the raphe nuclei and dorsal spinal cord was remained unchanged
data not shown, indicating a specific deletion of
Lmx1b in mDA neurons. Surprisingly, mDA neurons revealed by
expressions of TH, Pitx3, and Nurr1 were observed in Lmx1bTH
CKO embryos at E12.5 with no obvious difference from wildtype
controls in terms of cell number TH-positive cells/section:
wild type 236 _ 49, CKO 246 _ 54; Pitx3-positive cells/section:
wild type 251 _ 8, CKO 241 _ 28; n _ 3 for each; p _ 0.05, wild
type vs CKO and location Fig 2C–H. To study whether this
deletion causes defective mDA neuron development at later embryonic
stage, we examined mDA neurons in Lmx1bTH CKO at
birth. mDA neurons were present in the ventral tegmental area
and substantia nigra in Lmx1bTH CKO mice at P0, and their
distribution pattern and cell number were essentially similar to
those in wild-type controls Fig 3A–F,K. These results were
confirmed by observation on Lmx1bDat CKO mice in which
Lmx1b was specifically deleted in later-differentiating mDA neurons
as well Figs 3H, 4B, showing no difference between
Lmx1bDat CKO and wild-type controls Fig 3I–K. In addition, because Lmx1b
expression is persistent in postnatal mDA neurons,
mDA neurons in adult brain were also examined with mDA
makers TH, Pitx3, Dat, and Vmat2 in Lmx1bTH CKO and
Lmx1bDat CKO mice. Again, expression of these proteins in the
ventral tegmental area and substantia nigra of these two CKO
mice was similar to that of wild-type control Fig 4C,E,G,I and
data not shown. Furthermore, expression of TH in the striatum,
the major targeting area of mDA neurons, in these CKO mice
showed no detectable difference compared with wild-type control
Fig 4D,F,H, suggesting normal axonal innervation of
mDAneurons in the absence of Lmx1b expression. Together, our
results demonstrate that Lmx1b is not required for later differentiation
or survival of mDA neurons.
The above evidence clearly shows that Lmx1b expression in
laterdifferentiating
mDA neurons is not necessary for their development.
We reasoned that the loss of mDA neurons in Lmx1b_/_
embryos is due, at least partly, to the loss of the IsO inductive
activity Guo et al, 2007. To testify this hypothesis, we generated
Wnt1-Lmx1b transgene by which Lmx1b expression can be reactivated
in Lmx1b_/_ embryos. Wnt1 expression domain overlapped
with Lmx1b at MHB from early somite stages to E11.5
Fig 5A,B Guo et al, 2007. Wnt1-Lmx1b transgenic mice were
viable, fertile, and apparently normal, with no detectable defect in
the CNS. Lmx1b expression at MHB was stronger in Wnt1-Lmx1b
embryos compared with wild-type controls Fig 5A,C, arrowheads,
and it was ectopically expressed in the neural crest where
endogenous Wnt1 is expressed Fig 5A–C, arrows. As expected,
Lmx1b expression was observed atMHBin Wnt1Lmx1b;Lmx1b_/_
embryos, and more importantly expression of Fgf8 and Wnt1 was
rescued at MHB Fig 5D–I. Furthermore, in Wnt1Lmx1b;
Lmx1b_/_ mice, the dorsal midbrain ie, tectum and cerebellum
showed apparently normal appearance compared with wild
type, whereas their development are severely defective in
Lmx1b_/_ mice Fig 5J–L. Given this evidence, we concluded
that Lmx1b expression at MHB is not only required but also
sufficient for the formation of IsO inductive activity in early
embryogenesis.
mDAneurons develop normally in Wnt1Lmx1b;Lmx1b_/_ mice
mDAneurons are lost in Lmx1b_/_ mice Smidt et al, 2000; Guo
et al, 2007. However, our present data obtained from selective
inactivation of Lmx1b in later-differentiating mDA neurons indicate
that Lmx1b is not required for their later differentiation or
survival As mentioned above, on the basis of our present and
previous results, we hypothesized that the loss of mDA neurons
in Lmx1b_/_ mice is due to the loss of the IsO inductive activity
in the absence of Lmx1b at MHB. Therefore, mDA neurons were
examined in Wnt1Lmx1b;Lmx1b_/_ mice with normal IsO inductive
activity restored by Wnt1-Lmx1b transgene Fig 5. We
found that mDA neurons indicated by TH, Pitx3, and Nurr1
expression were observed in Wnt1Lmx1b;Lmx1b_/_ mice, and its
number and distribution pattern were similar to wild-type controls
at E12.5 Fig 6D–F,P,Q, E14.5 Fig 6G–O, and P0 Fig
7. Based on both loss- and gain-of-function data, we concluded
that IsO-inductive activity is essential for the development of
mDA neurons in mice.
Lmx1b is not only expressed in differentiating mDA neurons, but
also in their precursors Fig 1. With the advantage TH-Cre- and
Dat-Cre-mediated specific inactivation of Lmx1b, we demonstrate
that Lmx1b expression in later-differentiating mDA neurons
is not required for their differentiation or maintenance. We
next asked whether Lmx1b expression in mDA precursors and
early differentiating mDA neurons plays a role in their development,
and therefore contribute to the loss of mDA neurons in
Lmx1b_/_ mice. In addition, in Wnt1Lmx1b;Lmx1b_/_ embryos,
weak Lmx1b expression was transiently observed in the ventricular
zone of the ventral midbrain at E12.5 Fig 6C, raising the
possibility that mDA neurons were rescued in Lmx1b_/_ embryos
by the transient expression of Lmx1b
in mDA progenitors rather than by the recovered
IsO activity. To address these possibilities,
we generated Lmx1bNestin CKO
mice. The cre gene is driven by the Nestin
promoter and enhancer, and Cre is expressed
in the precursor cells of developing
CNS Isaka et al, 1999. The onset of
Nestin-Cre activity occurs around E10.5,
as indicated by X-gal staining in Nestincre;
Rosa26 mice data not shown, which is
consistent with previous results Vernay et
al, 2005. In Lmx1bNestin CKO embryos,
Lmx1b transcripts were not detectable in
the ventral midbrain at E10.5 Fig 8B, a
stage of Lmx1b initiation in mDA precursors,
but its expression in the roof plate
and MHB were not affected data not
shown. After E11.5, Lmx1b expression
was not found in the whole midbrain or
other regions of the CNS in Lmx1bNestin
CKO mice Fig 8D, data not shown.
Lmx1b is essential for the serotonergic
neuron development in the raphe nuclei of
the brainstem Cheng et al, 2003; Ding et
al, 2003 and spinal dorsal horn Ding et
al, 2004. Thus, to confirm the effects of
Nestin-Cre-mediated Lmx1b deletion in the
brain, we examined serotonin expression in
the brainstem and found no serotonin-expressing
neuron in Lmx1bNestin CKO embryos
at E12.5 Fig 8T, which is consistent
with the previous results Cheng et al, 2003;
Ding et al, 2003; the spinal dorsal horn also
showed similar phenotype as what found in
Lmx1b_/_ embryos Ding et al, 2004.
Thus, Lmx1bNestin CKO embryos allow us to
study the functions of Lmx1b in mDA precursors
during neurogenesis.
As the Nestin-Cre activity turns on in
the neural tube at E10.5, a stage after the
formation of the IsO, and therefore
Nestin-Cre-mediated gene inactivation is
a novel mouse model to study a role of
genes in the mid/hindbrain development,
independently of its involvement in the
IsO inductive activity Vernay et al, 2005.
Accordingly, in Lmx1bNestin CKO embryos,
we found that IsO activity was indeed
remained intact as evidenced by the normal expression of
Fgf8 and Wnt1 atMHBFig 8I–L and normal appearance of the
tectum and cerebellum Fig 8U,V. Therefore, Lmx1b expression
at MHB is essential for the formation of IsO inductive activity
by E10.5 and therefore important for the development of the
tectum and cerebellum.
We found that mDA neurons were present in the ventral midbrain
of Lmx1bNestin CKO embryos at E12.5 Fig 8M–R, and
ventral tegmental area and substantia nigra at P0, with no difference
from wild-type controls TH-positive cells/section: wild
type, 1406_156; CKO, 1666_256; wild type, n_3; CKO, n_
4; p_0.05,WTvs CKO Fig 9. Lmx1a expression in the ventral
midbrain is critical for the specification of mDA neurons
Andersson et al, 2006a; Ono et al, 2007. To study whether
normal mDA neurons in Lmx1bNestin CKO
mice is due to the compensation from
Lmx1a, we examined the expression of
Lmx1a. It showed that Lmx1a expression
in Lmx1bNestin CKO embryos is similar to
that in wild-type embryos with no obvious
upregulation at E10.5 and E12.5 Fig 8E–
H. In addition, expression of Foxa2,
Nurr1, and Aldh1a1 Raldh1, which are
also molecular markers for mDA progenitors
in the ventral midbrain Walle«n et al,
1999; Andersson et al, 2006b; Ferri et al,
2007, remained unchanged in the ventral
midbrain of Lmx1bNestin CKO embryos at
E10.5 see supplemental Fig 1, available at
www.jneurosci.org as supplemental material
Thus, our data indicated that Lmx1b
expression in the mDA precursors is dispensable
for their differentiation and
maintenance.


Consistent with a previous report Smidt
et al, 2000, our previous and present data
also showed the loss of mDA neurons in
Lmx1b_/_ mice. However, the present
study further demonstrates that the loss of
mDA neurons in Lmx1b_/_ mice is secondary
to the disruption of the IsO inductive
activity in the absence of Lmx1b at
MHB. We also provided evidence for the
conclusion that Lmx1b expression inmDA
neurons is dispensable for their differentiation
and maintenance. The present data
also argue for a notion that the IsO is not
only essential for the morphogenesis of the
dorsal midbrain and cerebellum but also
for the development of mDA neuron in
the ventral midbrain.
To study the role of Lmx1b in mDA neuron
development, we selectively inactivated
Lmx1b expression in later differentiating
mDA neurons by Dat-Cre
and TH-Cre as well as in mDA precursors
by Nestin-Cre. To our surprise, mDA neurons
were present in the ventral midbrain
at embryonic stages and in the ventral tegmental
area and substantia nigra in P0 and
adult brains Figs 2–4, 8, 9. Thus, the
present findings indicate that Lmx1b expression in mDA neurons
is not required for their differentiation or maintenance.
Lmx1b is also expressed in DA progenitors at E9.5, and it
remains unclear whether Lmx1b is involved in the initial specification
of mDA neuron progenitors before E10.5. mDA neurons
are first present in the ventral midbrain in mouse at E10.5 Bayer
et al, 1995, and TH-positive mDA neurons in Lmx1b_/_ embryo
show no difference from wild-type embryo in terms of cell
numbers and distribution at this stage see supplemental Fig 2,
available at www.jneurosci.org as supplemental material;
their reduction in Lmx1b_/_ embryo occurs by E11.5. mDA
neurons develop normally in Lmx1b Nestin CKO embryo, in
which Lmx1b expression in the ventral midbrain is inactivated
at E10.5 Figs 8, 9. Thus, further studies are needed to study
whether Lmx1b plays an essential role in the early specification
of mDA progenitors.
Lmx1a, a paralogue of Lmx1b, is a key instructor in the specification
of mDA neurons, based on the data from loss- and gain of-function experiments in chick embryos and from in vitro differentiation
of mouse ES cells Andersson et al, 2006a, and a recent study Ono et
al, 2007 shows that mDA neurons is reduced
in drher mutant mice, which carry a mutation in the Lmx1a
locus Millonig et al, 2000, as a consequence of the reduced
neurogenesis in mDA progenitors and misspecification of mDA
neurons in some of mDA neurons. However, the phenotype is
not as severe as that found by siRNA for Lmx1a in chick embryos
Andersson et al, 2006a, and most mDA neurons differentiated
normally in drher mice, suggesting residual
Lmx1a activity in drher mutants or the
existence of compensating factors in the
mice Ono et al, 2007. Because Lmx1a
and Lmx1b are homolog genes and coexpressed
in mDA neurons, it is possible that
Lmx1a and Lmx1b control the mDA neuron
development in a combinational manner.
This may account for the normal development
of most mDA neurons in drher
mice and mDA neurons in the three CKO
mice and Wnt1Lmx1b;Lmx1b_/_ mice.
Nevertheless, Lmx1b alone is not crucial
for mDA neuron differentiation or maintenance
in mice.
The present data indicate that the loss of
mDA neurons in Lmx1b_/_ embryo is
due to the loss of IsO inductive activity in
the absence of Lmx1b expression at MHB.
IsO is the local signaling center for the
morphogenesis of the mid/hindbrain region
Joyner et al, 2000; Simeone, 2000;
Liu and Joyner, 2001; Wurst and Bally-
Cuif, 2001; Nakamura et al, 2005. The
position of the IsO controlled by Gbx2 and
Otx2 determines the location and size of
mDA neurons and hindbrain serotonergic
cell population during development
Brodski et al, 2003. Our previous study
shows that Lmx1b expression is required
for the IsO inductive activity by control of
initiation of Fgf8 and maintenance of expression
of Wnt1 and other IsO-related
transcription factors Guo et al, 2007,
and the present study further indicates
that Lmx1b at MHB is sufficient for the
formation of IsO activity, because transgenic
expression of Lmx1b at MHB in
Lmx1b_/_ embryos restored its inductive
activity, as indicated by the recovered expression
of Fgf8 and Wnt1 and normal appearance
of the tectum and cerebellum
Fig 5. Importantly, mDA neurons were recovered
in WntLmx1b;Lmx1b_/_ mice Figs
6, 7. Furthermore, when Lmx1b was inactivated
in the whole brain by E10.5 by Nestin-
Cre, IsO remained intact in the CKO mice
and mDA neurons differentiated normally,
providing additional evidence for the requirement
of IsO in mDA neuron development
Figs 8, 9. Therefore, IsO is not only
essential for the morphogenesis of the dorsal
midbrain but also for the development of
mDA neurons in the ventral midbrain. In fact, FGF8 andWNT1,the
major secreted proteins for the IsO activity, are known to be involved
in the induction and differentiation of mDA neurons Hynes and
Rosenthal, 1999; Simeone, 2005; Burbach and Smidt, 2006; Prakash
and Wurst, 2006. Therefore, it is possible that the loss of mDA
neurons in Lmx1b_/_ mice is due to, at least partly, the loss of Fgf8
and Wnt1 expression in the absence of Lmx1b. In summary, we provided
evidence in
vivo that Lmx1b expression in mDA neurons
is not required for their difference or
maintenance in mice. The loss of mDA
neurons in Lmx1b mutant mice is due to
the disruption of the IsO activity in the
absence of Lmx1b expression at MHB. Our
data also suggest that a special caution
should be paid in studying the role of a
gene in mDA neuron development when it
probably participates or has been known
to be involved in the regulation of IsO inductive
activity.
