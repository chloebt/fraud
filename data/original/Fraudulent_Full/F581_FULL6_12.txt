
Adult T-cell leukemia ATL is an aggressive and usually fatal
hematological malignancy that is etiologically linked to infection with
human T-cell leukemia virus type 1 HTLV-1 14, 33, 43. Conventional
chemotherapeutic drugs used for the treatment of patients with ATL have
yielded only a limited improvement in prognosis 41. Currently, the
molecular mechanism of malignant transformation by HTLV-1 remains
undefined. Expression of the virally encoded Tax protein appears to be a
critical event during the leukemogenesis of ATL. Tax is a transcriptional
activator that modulates the expression of HTLV-1 long terminal repeats
LTRs and the transcription of many cellular genes. Tax can immortalize
primary human T cells derived from peripheral blood or cord blood 11, 12
and induce tumors in transgenic mice 29, probably by activating a variety
of proteins, including transcription factors such as cyclic AMP response
element-binding protein CREB, serum-responsive factor, and NF-κB 10.
β-Catenin is a multifunctional protein that participates in both cell-cell
adhesion and the transcription of T-cell transcription factor
Tcf/lymphoid enhancer binding factor Lef target genes.β -Catenin levels
are regulated posttranslationally by the Wnt signaling pathway. In the
absence of secreted Wnt glycoprotein ligands, the modular axin protein
provides a scaffold for the binding of glycogen synthase kinase 3β GSK-
3β, adenomatous polyposis coli APC protein, and β-catenin. This
facilitates the phosphorylation of β-catenin by GSK-3β, allowing
phosphorylated β-catenin to be ubiquitinated for rapid degradation by the
proteasome 34. Mutations in APC,β -catenin, or axin increase the steady-
state level ofβ -catenin, and many cancers, including colorectal cancer,
result from hyperactivity of the Wnt/β-catenin signaling pathway due to the
constitutive β-catenin-mediated transactivation of Tcf-dependent genes
34. Thus, aberrant activation of β-catenin has major oncogenic effects
9, 31. Accordingly, disruption of this signaling pathway holds promise
for the development of new anticancer drugs.
β-Catenin is highly expressed in several leukemic cell lines 4. In B-cell
chronic lymphocytic leukemia, Wnt3, Wnt5b, Wnt6, Wnt10a, Wnt14, and Wnt16,
as well as the Wnt receptor Frizzled 3, are highly expressed, resulting in
the activation of β-catenin-mediated transcription and an enhanced survival
of chronic lymphocytic leukemia lymphocytes 22. These results suggested
that β-catenin is associated not only with epithelial cancer but also with
leukemia and lymphoma. However, neither the expression of β-catenin in HTLV-
1-infected T cells nor the function of β-catenin in leukemogenesis induced
by HTLV-1 has been elucidated. The goals of this study were to determine
the status of β-catenin signaling in HTLV-1-infected T cells and to
elucidate the molecular activation of this signaling pathway.



 The proteasome inhibitor N-acetyl-Leu-Leu-norleucinal LLnL and the
phosphatidylinositol 3-kinase PI3K inhibitor LY294002 were purchased from
Calbiochem San Diego, CA. Cycloheximide and lithium chloride were
obtained from Sigma-Aldrich St Louis, MO.
 Anti-β-catenin and anti-GSK-3β antibodies were purchased from BD
Transduction Laboratories San Jose, CA. An anti-actin antibody was
obtained from NeoMarkers Fremont, CA. An antibody to Tax Lt-4 was
described previously 40. Anti-Akt, anti-phosphorylated Akt Ser473, and
anti-phospho-GSK-3β Ser9 antibodies were purchased from Cell Signaling
Technology Beverly, MA. Anti-nucleolin and anti-IκBα antibodies were
purchased from Santa Cruz Biotechnology Santa Cruz, CA. Horseradish
peroxidase-conjugated anti-mouse and anti-rabbit immunoglobulin G
antibodies for Western blotting were purchased from Amersham Biosciences
Piscataway, NJ.
 The HTLV-1-infected T-cell lines MT-2 27, SLB-1 20, HUT-102 33, MT-1
26, TL-OmI 39, and ED-40515− 23 were maintained in RPMI 1640 medium
supplemented with 10 heat-inactivated fetal bovine serum, 50 U/ml
penicillin, and 50μ g/ml streptomycin Sigma-Aldrich at 37°C in 5 CO2. MT-
2 and SLB-1 are HTLV-1-transformed T-cell lines which were established by
an in vitro coculture protocol. MT-1, TL-OmI, and ED-40515− are leukemic
T-cell lines derived from patients with ATL. HUT-102 was established from a
patient with ATL, but its clonal origin is unclear. HeLa human cervix
adenocarcinoma cell line cells were maintained in Dulbecco's modified
Eagle's medium supplemented with 10 heat-inactivated fetal bovine serum,
50 U/ml penicillin, and 50 μg/ml streptomycin at 37°C in 5 CO2.
 A β-catenin expression plasmid pCGN/β-catenin and a human Tcf-4
expression plasmid pEF-BOS HA/Tcf-4E were described previously 17, 18.
The pGL3-OT and pGL3-OF reporter plasmids 37 were kindly provided by B.
Vogelstein The Sidney Kimmel Comprehensive Cancer Center, Johns Hopkins
University School of Medicine, Baltimore, MD. pGL3-OT and pGL3-OF contain
three copies of the Tcf site 5′-AGATCAAAGG-3′ and a mutant sequence 5′-
AGGCCAAAGG-3′, respectively, upstream of the c-fospromoter and the
luciferase open reading frame. An HTLV-1 LTR luciferase reporter plasmid
LTR-Luc, which contains the HindIII fragment from the HTLV-1 LTR, was
kindly supplied by I. Futsuki Nagasaki University School of Medicine,
Nagasaki, Japan. An NF-κB reporter plasmid κB-Luc containing five tandem
repeats of an NF-κB binding site from the interleukin-2 receptor α chain
gene was kindly provided by J. Fujisawa Kansai Medical University, Osaka,
Japan. A series of expression vectors for Tax Tax WT and mutants thereof
Tax M22, Tax 703, and Tax K88A was described previously 13, 24. An
expression plasmid for dominant-negative CREB pCMV-KCREB was purchased
from BD Biosciences Clontech Mountain View, CA. A dominant-negative Akt
expression plasmid pCMV5-K169A, T308A, S473A-Akt has Lys-169-, Thr-308-,
and Ser-473-to-Ala mutations and was kindly provided by D. Alessi
University of Dundee, Dundee, United Kingdom.
 Cells were lysed in sodium dodecyl sulfate SDS sample buffer containing
62.5 mM Tris-HCl pH 6.8, 2 wt/vol SDS, 10 glycerol, 6 2-
mercaptoethanol, and 0.01 bromophenol blue. Nuclear and cytoplasmic
extracts were prepared by using a nuclear extraction kit Active Motif,
Carlsbad, CA according to the manufacturer's instructions. The lysates
were resolved by electrophoresis on polyacrylamide gels and then
electroblotted onto polyvinylidene difluoride membranes Millipore,
Billerica, MA. Membranes were incubated with the appropriate primary
antibody, as indicated, overnight at 4°C. After being washed, the blots
were exposed to the appropriate secondary antibody conjugated with
horseradish peroxidase for 1 h at room temperature. The reaction products
were visualized using enhanced chemiluminescence reagent Amersham
Biosciences according to the manufacturer's instructions.
 HeLa cells were transfected using Lipofectamine reagent Invitrogen,
Carlsbad, CA according to the manufacturer's protocols. HTLV-1-infected T-
cell lines were transfected using a previously described DEAE-dextran
method 30. In brief, 1× 107 cells were incubated for 30 min at room
temperature with 2.2 ml of transfection solution containing plasmid DNA and
100 μg of DEAE-dextran Amersham Biosciences in RPMI 1640 serum-free
medium. Cells were then rinsed with 5 U/ml heparin Wako, Osaka, Japan in
RPMI 1640 and incubated in complete medium for 48 h. Cells were transiently
transfected with the indicated effecter plasmids and luciferase reporter
constructs. In all cases, the reference plasmid phRL-TK, which contains
the Renilla luciferase gene under the control of the herpes simplex virus
thymidine kinase promoter, was cotransfected to correct for transfection
efficiency. Luciferase assays were performed by using a dual-luciferase
reporter system Promega, Madison, WI in which relative luciferase
activities are calculated by normalizing transfection efficiencies
according to the Renilla luciferase activity.
 To repress β-catenin, a predesigned double-stranded small interfering RNA
siRNA siGENOME SMARTpool CTNNB1; Dharmacon, Inc, Lafayette, CO was
used. A siCONTROL non-targeting siRNA pool Dharmacon, Inc was used as a
negative control. siRNAs were transfected into HUT-102 cells with a
Nucleofector device program T-20 and a Cell Line Nucleofector kit V
Amaxa, Inc, Cologne, Germany. Transfected cells were incubated for 12 h,
seeded into 24-well plates at 5 × 104 viable cells per well, and incubated
for the indicated times. The number of viable cells was determined every 24
h by counting trypan blue-excluding cells in a hemocytometer.
 Total cellular RNA was extracted from cells by the use of TRIzol reagent
Invitrogen as described by the supplier. First-strand cDNAs were
synthesized using an RNA-PCR kit Takara Bio, Shiga, Japan with random
primers. Thereafter, cDNAs were amplified for β-catenin, Tax, andβ -actin.
The oligonucleotide primers used were as follows: forβ -catenin, 5′-
TGATGGAGTTGGACATGGCCATGG-3′sense and 5′-CAGACACCATCTGAGGAGAACGCA-
3′antisense; for Tax, 5′-CCCACTTCCCAGGGTTTGGACAGA-3′sense and 5′-
CTGTAGAGCTGAGCCGATAACGCG-3′antisense; and for β-actin, 5′-
GTGGGGCGCCCCAGGCACCA-3′sense and 5′-CTCCTTAATGTCACGCACGATTTC-
3′antisense. Product sizes were 570 bp for β-catenin, 203 bp for Tax, and
548 bp for β-actin. The amplification programs were as follows: for β-
catenin, 30 cycles of denaturing at 94°C for 1 min, an annealing step at
60°C for 40 s, and an extension step at 72°C for 50 s; for Tax, 30 cycles
of denaturing at 94°C for 30 s, an annealing step at 60°C for 30 s, and an
extension step at 72°C for 90 s; and for β-actin, 28 cycles of denaturing
at 94°C for 30 s, an annealing step at 60°C for 30 s, and an extension step
at 72°C for 90 s. The PCR products were fractionated in 2 agarose gels and
visualized by ethidium bromide staining.
 The Akt kinase assay was performed using an Akt kinase assay kit Cell
Signaling Technology according to the protocol recommended by the
manufacturer. Briefly, the cells were washed with phosphate-buffered
saline, and 200 μl of lysis buffer 20 mM Tris-HCl pH 7.5, 150 mM NaCl, 1
mM EDTA, 1 mM EGTA, 1 Triton X-100, 2.5 mM sodium PPi, 1 mMβ -glycerol
phosphate, 1 mM Na3VO4, 1 mM phenylmethylsulfonyl fluoride, and 1 mM
leupeptin was added to the cells for 10 min. Lysates were
immunoprecipitated for 2 h at 4°C with anti-Akt antibody. The
immunoprecipitates were washed with lysis buffer and kinase buffer 25 mM
Tris-HCl pH 7.5, 5 mMβ -glycerol phosphate, 2 mM dithiothreitol, 0.1 mM
Na3VO4, and 10 mM MgCl2. Kinase reactions were performed for 30 min at
30°C in kinase buffer supplemented with 200 μM ATP and 1 μg GSK-3α/β fusion
protein. The samples were loaded into a 12 acrylamide gel. Phosphorylation
of GSK-3α/β was measured by Western blotting with anti-phospho-GSK-3α/β
Ser21/9 antibody.



 We first analyzedβ -catenin expression in six HTLV-1-infected T-cell
lines. Theβ -catenin protein was highly expressed in three of the lines,
namely, MT-2, SLB-1, and HUT-102, whereas only weak expression was detected
in the three ATL-derived T-cell lines, ie, MT-1, TL-OmI, and ED-40515−
Fig 1A, top panel. Although the MT-2 cell lysates showed two Tax-
immunoreactive bands, consistent with the 40-kDa Tax protein and a 69-kDa
fusion between the envelope and the Tax coding sequence reported previously
15, Tax protein levels were similar in the three HTLV-1-infected T-cell
lines expressing high levels of β-catenin protein. In contrast, Tax was
hardly detectable in the three ATL-derived T-cell lines, which expressed
low levels of β-catenin Fig 1A, middle panel. Noβ -catenin protein was
detected in a sample of normal peripheral blood mononuclear cells data not
shown. To determine the cellular distribution of β-catenin, nuclear and
cytoplasmic cell fractions from all cell lines were analyzed by Western
blotting.β -Catenin was most abundant in the nuclear fractions of the Tax-
positive HTLV-1-infected T-cell lines, while the Tax-negative cells showed
relatively smaller amounts of nuclear β-catenin protein than of the
cytoplasmic pool Fig 1B.
To investigate whether the nuclear accumulation of β-catenin in Tax-positive
HTLV-1-infected T-cell lines resulted in transcriptional activation of theβ
-catenin/Tcf complex, both Tax-positive MT-2, SLB-1, and HUT-102 and Tax-
negative MT-1 and ED-40515− T-cell lines were transfected with Tcf
reporter plasmids containing three copies of the wild-type pGL3-OT or
mutant pGL3-OF Tcf site. The Tax-positive cells showing increased β-
catenin protein in the nucleus showed a higher level of transcriptional
activation of pGL3-OT than did the Tax-negative HTLV-1-infected T-cell
lines Fig 1C. Activation of pGL3-OF was not observed in any cell lines.
These results suggested that the accumulation of β-catenin in the nucleus
enhances the transcriptional activity of the β-catenin/Tcf complex.
 To determine whether the low level ofβ -catenin protein in the Tax-
negative HTLV-1-infected T-cell lines was due to proteasomal degradation,
we treated the cells with LLnL, a potent proteasome inhibitor, and then
again analyzed the expression levels of β-catenin protein by Western blot
analysis Fig 1D. In a Tax-negative T-cell line, ED-40515−, significant
accumulation of the β-catenin protein was detected in the presence of LLnL,
in a time-dependent manner. A similar result was obtained for another Tax-
negative cell line, MT-1 data not shown. In contrast,β -catenin levels
were not further increased by LLnL treatment in the Tax-positive HUT-102 or
MT-2 cells data not shown. LLnL treatment did not change the expression
level of Tax protein. Therefore, the difference in β-catenin levels between
Tax-positive and Tax-negative HTLV-1-infected T-cell lines is attributable
to differential degradation but not to differential expression. These
results tempted us to investigate the role of the Tax protein in the
activation of β-catenin signaling.
 To examine the role ofβ -catenin in HTLV-1-infected T-cell growth, HUT-102
cells were transfected with β-catenin siRNA. Cell growth was inhibited in
HUT-102 cells transfected with β-catenin siRNA compared with cells
transfected with nontargeting siRNA Fig 2A. The suppression of β-catenin
mRNA expression byβ -catenin siRNA was confirmed by RT-PCR Fig 2B. These
results indicated the specific role of β-catenin in the growth of Tax-
positive HTLV-1-infected T cells.
 To determine whether Tax affects endogenous β-catenin levels, HeLa cells
were transfected with increasing amounts of a wild-type Tax Tax WT
expression plasmid. Total cell extracts and total RNA from transfected HeLa
cells were analyzed for β-catenin expression by Western blot and RT-PCR
analyzes, respectively. Increased levels ofβ -catenin protein were detected
in the presence of Tax, in a dose-dependent manner Fig 3A. In contrast,
β-catenin mRNA levels were not changed by Tax Fig 3B. Next, to determine
the signaling pathway responsible for the Tax-inducedβ -catenin protein
accumulation, we used three Tax mutant expression plasmids, which have been
described previously 13, 24. Tax M22, which has amino acid substitutions
at residues 130 and 131, from Thr-Leu to Ser-Ala, effectively activates the
cyclic AMP response element CRE, which mediates the Tax-dependent
activation of the HTLV-1 LTR but not of the NF-κB element. Tax 703 has
amino acid substitutions at residues 319 and 320, from Leu-Leu to Arg-Ser,
which make it equivalent to mutant M47, and Tax K88A carries a single amino
acid substitution at position 88, from Lys to Ala. Tax 703 and Tax K88A
activate the NF-κB element but do not affect the CRE. In the current
experiments, Tax M22, but not Tax 703 or Tax K88A, increased theβ -catenin
protein level Fig 3A. In contrast,β -catenin mRNA levels were not
altered by any of the Tax mutants Fig3B. The expression levels of Tax
protein and mRNA were increased independently of the amount of plasmid used
Fig 3A and B. These results suggested that Tax inducesβ -catenin
expression at the posttranscriptional level by activation of the CREB
signaling pathway.
 To elucidate the mechanisms by which Tax induces β-catenin protein
accumulation, we examined the effects of Tax on β-catenin turnover. HeLa
cells were transfected with either empty vector Tax −, wild-type Tax
Tax WT, or mutant Tax M22, 703, or K88A expression plasmids. To
evaluate the degradation of β-catenin proteins, we also transfected β-
catenin expression plasmids together with Tax. Cycloheximide 12.5 μg/ml
was added to the cell culture medium 24 h after transfection to block new
protein synthesis, and protein extracts were prepared 0, 2, 4, and 6 h
after the addition of cycloheximide. Western blotting showed thatβ -catenin
was rapidly degraded in HeLa cells transfected with empty vector Tax −,
whereas β-catenin levels remained stable in the presence of Tax WT
throughout the 6-h chase period Fig 4. The increased β-catenin levels
induced by Tax are therefore a consequence of protein stabilization.
Expression of the mutant protein Tax M22 also stabilized β-catenin protein
levels for 6 h, whereas cells transfected with the 703 and K88A mutants
showed reduced levels, as in the controls. These results suggested that the
Tax-induced activation of the CREB signaling pathway stabilizes the
cellular levels of β-catenin.
 We next asked whether the stabilization of β-catenin protein by Tax
affected β-catenin/Tcf transcriptional activity Fig 5A. In a transient
transfection assay using pGL3-OT as a reporter, the expression of Tax WT or
β-catenin alone in HeLa cells activated the reporter. The combination of β-
catenin and Tax WT had an even greater effect. No significant responses
were seen with the mutant reporter pGL3-OF data not shown. These
results suggested thatβ -catenin and Tax activate Tcf synergistically. We
also tested the effects of Tax mutants on β-catenin/Tcf transcriptional
activity. As with β-catenin protein stabilization, Tax M22, but not the 703
or K88A mutant, activated the pGL3-OT reporter and acted synergistically
with β-catenin. None of the Tax mutants affected the pGL3-OF reporter
activity data not shown. Together, these sets of experiments suggest that
the stabilization ofβ -catenin protein by Tax enhances the transcriptional
activity of β-catenin/Tcf and that this effect of Tax is mediated via the
CREB signaling pathway.
 To further ascertain the biological role of CREB in transcriptional
activation of the β-catenin/Tcf complex by Tax, HeLa cells were transfected
with KCREB, a dominant-negative version of CREB. The pCMV-KCREB construct
encodes a CREB protein with Arg 287 mutated to Leu in the DNA-binding
domain. KCREB acts as a dominant-negative repressor of wild-type CREB,
blocking its binding to CRE. HeLa cells were transfected with β-catenin,
Tcf-4, and the pGL3-OT or pGL3-OF reporter plasmid in the presence or
absence of Tax WT and increasing amounts of KCREB Fig 5B. KCREB
inhibited the Tax-induced activation of pGL3-OT luciferase activity in a
dose-dependent manner, whereas no significant responses were seen with the
pGL3-OF reporter plasmid. An HTLV-1 LTR luciferase reporter LTR-Luc
containing three unique CRE-containing 21-bp repeats was used to confirm
the effects of KCREB on the action of Tax. HeLa cells were transfected with
LTR-Luc or a κB-Luc reporter plasmid in the presence or absence of Tax WT
and increasing amounts of KCREB. Tax could activate gene expression from
both the HTLV-1 LTR and NF-κB reporters. However, KCREB inhibited only HTLV-
1 LTR transcriptional activity, not NF-κB transcriptional activity
Fig 5C, indicating that KCREB blocks Tax-induced activation of the CREB,
but not NF-κB, pathway. These results indicated that Tax activates the β-
catenin/Tcf transcriptional activity through the activation of CREB
signaling.
 The activation of Akt signaling has been associated with an accumulation
ofβ -catenin 36. Akt activation can be mediated by the activation of
PI3K, and Tax activates the PI3K pathway 21. In addition, Jeong and
colleagues recently showed that Akt is activated in HTLV-1-transformed T-
cell lines and that Tax activates Akt in these cells 16. To determine
whether Akt activation is associated with the stabilization ofβ -catenin in
the HTLV-1-infected T-cell lines, we examined the phosphorylation status of
Akt and the in vitro Akt kinase activity relative to β-catenin protein
expression Fig 6A. Phosphorylated Akt was strongly detected in the three
Tax-positive HTLV-1-infected T-cell lines MT-2, SLB-1, and HUT-102,
whereas only weak expression of this phosphorylated protein was detected in
the Tax-negative T-cell lines MT-1, TL-OmI, and ED-40515−. Total Akt
protein was similarly expressed in all cell lines. The in vitro Akt kinase
activity was also higher in Tax-positive T-cell lines than in those that
were Tax negative. Next, to examine how Tax regulates Akt signaling
activity, Tax WT or a Tax mutant M22, 703, or K88A was expressed in HeLa
cells together with Akt. The activation of Akt was assessed by Western blot
analysis of phosphorylated Akt and GSK-3β, which is a downstream target of
Akt kinase. Tax WT and Tax M22 increased the phosphorylation of Akt and GSK-
3β, while Tax 703 and Tax K88A induced no phosphorylation of either
protein. Total Akt and GSK-3β protein levels were not affected by any of
the expressed plasmids. These results indicated that Tax also activates Akt
signaling via activation of the CREB pathway. The expression ofβ -catenin
protein was also increased by Tax WT and Tax M22, but not by the 703 and
K88A mutants Fig 3, suggesting that the accumulation of β-catenin
protein might be mediated by activation of the Akt signaling pathway,
resulting in inactivation of GSK-3β.
 To examine the role of Akt signaling in the accumulation of β-catenin in
HTLV-1-infected T cells, Tax-positive MT-2 cells were treated with the PI3K
inhibitor LY294002 Fig 6C. In the presence of 20 μM LY294002, β-catenin
expression was significantly reduced, in a time-dependent manner.
Phosphorylation of both Akt and GSK-3β was inhibited by LY294002, whereas
the total levels of both proteins were unaffected. The expression of Tax
was not changed by LY294002 treatment. These data suggested that
overexpression of β-catenin in MT-2 cells is mediated by constitutive
activation of PI3K/Akt signaling. Next, to elucidate the role of GSK-3β
activity in regulating β-catenin expression, we used lithium chloride,
which acts as a noncompetitive inhibitor of GSK-3β 19 Fig 6D, and
observed an induction of β-catenin protein expression in Tax-negative MT-1
cells, whose GSK-3β proteins are active dephosphorylated at Ser9. Similar
results were obtained with another Tax-negative cell line ED-40515−
data not shown. In contrast, β-catenin levels were not increased after
lithium chloride treatment in the Tax-positive MT-2 cells Fig 6D or HUT-
102 cells data not shown, whose GSK-3β proteins are already inactive
phosphorylated at Ser9. The expression of Tax itself was not changed by
lithium chloride treatment. These results indicated that LiCl increases β-
catenin levels in cell lines which have activated GSK-3β but not in those
which have inactive GSK-3β, suggesting that the difference in β-catenin
levels between Tax-positive and Tax-negative HTLV-1-infected T-cell lines
is attributable to the differential activity of GSK-3β.
 To further study the enhancive effect of Tax and Akt activation onβ
-catenin/Tcf-mediated transcription, we used wild-type Akt-WT and
dominant-negative mutant Akt-DN Akt expression plasmids to directly
examine the involvement of Akt in Tax-inducedβ -catenin/Tcf transcription
Fig 6E. Akt-WT enhanced pGL3-OT activity induced by Tax, while Akt-DN
suppressed the Tax-induced pGL3-OT activity. Both the wild-type and
dominant-negative mutant Akt plasmids did not affect pGL3-OF activity.
These data demonstrated that β-catenin/Tcf transcriptional activation by
Tax is mediated via the Akt signaling pathway.




In this study, we observed an accumulation of nuclear β-catenin protein and
an enhanced transcriptional activity of β-catenin/Tcf in Tax-positive HTLV-
1-infected T-cell lines but not in those that were Tax negative. Proteasome
inhibition restored β-catenin protein expression in Tax-negative, but not
Tax-positive, T-cell lines, suggesting that Tax might stabilize the β-
catenin protein by inhibiting protein degradation. Transfection with β-
catenin siRNA inhibited cell growth of HUT-102 cells, a Tax-positive HTLV-1-
infected T-cell line. We further demonstrated that HTLV-1 Tax activates β-
catenin/Tcf-dependent transcription by stabilizing the β-catenin protein
through the CREB signaling pathway in Tax-transfected HeLa cells.
Furthermore, transient expression of Tax in HeLa cells led to the CREB-
dependent phosphorylation and activation of Akt and the subsequent
phosphorylation and inactivation of the Akt target, GSK-3β. In turn,
dominant-negative Akt inhibited the Tax-inducedβ -catenin/Tcf
transcriptional activity, and upon inactivation of the negative Wnt
signaling regulator, GSK-3β,β -catenin was stabilized to therefore
activateβ -catenin/Tcf-dependent transcription Fig 7. Treatment of Tax-
positive T-cell lines with a PI3K inhibitor and inactivation of GSK-3β in
Tax-negative T-cell lines implicated GSK-3β inactivation in the process ofβ
-catenin accumulation in HTLV-1-infected T cells.
The Akt signaling pathway is important for the survival and growth of
numerous types of cancer cells. Previous studies showed that Tax induces
PI3K signaling activation and that this activation is associated with
transformation of Rat-1 fibroblast cells stably expressing Tax 21. More
recently, Jeong and colleagues demonstrated that Akt signaling is activated
in HTLV-1-transformed cells and that Tax can activate this signaling
pathway by inducing Akt phosphorylation 16. These observations indicated
that Tax-induced Akt signaling activation plays an important role in the
transformation of HTLV-1-infected cells. Consistent with these findings, we
found here that phosphorylation and activation of Akt were associated with
Tax expression in HTLV-1-infected T-cell lines and that Tax induced Akt
activation in transfected HeLa cells. Moreover, we demonstrated, for the
first time, an association between the activation of CREB signaling by Tax
and the Tax-induced activation of Akt. Recently, silencing of CREB gene
expression by RNA interference decreased the phosphorylation of Akt induced
by forskolin stimulation 25. However, it remains unclear exactly how Tax
activates Akt through the CREB signaling pathway, and future studies need
to address this issue. During preparation of this article, a study showing
Tax-mediated Akt activation was published by Kuan-Teh Jeang's lab 32.
They concluded that Tax-mediated Akt activation depended on the ability of
Tax to interact with the p85α subunit of PI3K but not on the CREB-
activating activity of Tax. Moreover, they found that a Tax mutant, Tax
M22, could not activate Akt in mouse embryonic fibroblasts MEFs. In the
present study, we demonstrated that Tax M22 could activate Akt in HeLa
cells. However, they reported that a Tax mutant with disrupted NF-κB
activation, Tax S258A, could activate Akt in MEFs. The precise reason for
these differences is not clear, but we cannot exclude the possibility that
these differences could be attributable to the differences in the cell
lines which were used for the Akt assay. We used HeLa cells, which are
highly transformed, but primary cells such as MEFs were used in Jeang's
study. Further analysis is needed to elucidate whether the differences
between the two studies are due to the differences in the cells which were
used for the experiments.
Recently, Yang et al demonstrated that the APC gene promoter region was
methylated in some cases of acute or chronic ATL 42. Epigenetic
modifications can affect gene expression and contribute to the pathogenesis
of tumor formation and growth. Methylation of CpG islands within tumor
suppressor genes is an important oncogenic mechanism in certain cancers,
including hematological malignancies. Yang's results suggested that a loss
of APC gene expression by methylation of its promoter might lead toβ
-catenin stabilization. However, we observed normal expression of the APC
protein in all HTLV-1-infected T-cell lines tested here data not shown.
Therefore, diminished APC function by hypermethylation may not be a common
mechanism for inducingβ -catenin activation in HTLV-1-infected T cells.
In this study, we demonstrated that the CREB-activating activity of Tax is
important for Akt andβ -catenin activation, which enhances cell growth and
survival Previous studies reported that the CREB pathway is required for
the clonal expansion of CD4+ and CD8+ T cells 1, and a Tax mutant which
is active for CREB but deficient in NF-κB signaling can immortalize human
primary T lymphocytes 35. Consisting with our findings, these results of
previous studies indicated that the Tax-activated CREB pathway plays an
important role in the permanent growth and immortalization of human T
lymphocytes.
What is the specific role of β-catenin in Tax-mediated biology? To answer
this question, we demonstrated that transfection with β-catenin siRNA
inhibited the growth of the HTLV-1-infected T-cell line HUT-102, which
expresses high levels of β-catenin protein. Our results are consistent with
a previous study showing that the growth of HUT-102 cells was inhibited by
overexpression of a dominant-negativeβ -catenin or dominant-negative Tcf
expression plasmid 4. These results implicated β-catenin in the cell
growth of HTLV-1-infected T cells. Enhanced expression of the β-catenin-
regulating genes, such as cyclin D1 and c-myc, which regulate cell cycle
progression and apoptosis, has been observed in Tax-positive HTLV-1-
infected T-cell lines data not shown. It could thus be proposed that
overexpression of these proteins might result in malignant cell growth of
HTLV-1-infected T cells. Importantly,β -catenin expression was not
increased in peripheral blood mononuclear cells from ATL patients data not
shown. Because the expression of Tax was not detected in these ATL cells,
overexpression of β-catenin may depend on Tax expression and may not be
necessary to maintain the malignant phenotype in the late Tax-independent
stage of ATL.
The Wnt/β-catenin signaling pathway has been identified as a common target
for perturbation by viruses, as demonstrated by the following examples from
the literature. Similar to the effect of Tax onβ -catenin signaling,
Epstein-Barr virus encodes latent membrane protein 2A, which activates PI3K
and Akt, resulting in GSK-3β inactivation and β-catenin stabilization 28.
The latency-associated nuclear antigen of Kaposi's sarcoma-associated
herpesvirus binds to GSK-3β and sequesters it in the nucleus, preventing β-
catenin phosphorylation 6, 7. The large T antigen of JC virus, a human
polyomavirus, interacts with β-catenin, stabilizing it and promoting its
nuclear accumulation as well as activating a c-myc promoter 5, 8. The Vpu
protein of human immunodeficiency virus type 1 binds to the β-transducin
repeat-containing protein and blocks the ubiquitination and proteasomal
degradation of β-catenin 2. Hepatitis B virus X protein achieves β-
catenin stabilization by suppressing GSK-3β activity in an Src kinase-
dependent manner 3. Finally, hepatitis C virus NS5A activates PI3K,
resulting in stabilization ofβ -catenin 38. Therefore, the Wnt pathway is
targeted by many different oncogenic viral proteins via distinct
mechanisms, indicating the importance of this pathway in the genesis of
virus-associated tumors.

In summary, the data presented here show that β-catenin signaling is
activated in Tax-positive HTLV-1-infected T-cell lines. Activation of the
Akt pathway by Tax induced the inactivation of GSK-3β, leading to
stabilization of the β-catenin protein. The increased β-catenin expression
induced by Tax was followed by an upregulation of β-catenin-induced
transcriptional activity, and the activation of β-catenin through the Akt
signaling pathway was mediated by the activation of CREB signaling via Tax
activation. Together, our results implicate an important role for Tax in
the activation of the β-catenin signaling pathway and thereby in the
transformation of T lymphocytes by HTLV-1 infection.


