
There is a chronic shortage of suitable organ donors for islet
transplantation in human type

1 diabetes 1. It is usually necessary to harvest multiple pancreata to
obtain sufficient

 islets for transplantation 2, 3. This has prompted the search for more
efficient techniques of islet transplantation and engraftment and is the
driving force behind the present study using

vascularized tissue engineering methodology to promote organ function and
insulin production.

The current clinical method of islet transplantation is injection into the
portal vein 3, 4.

Experimentally, islets are injected into the portal vein, into the omental
pouch 5, or under the

renal capsule 6, or microencapsulated prior to their injection into one
of these sites 7. Of the

major factors affecting the long-term viability of transplanted  islets,
hypoxia is a significant

problem because islets are particularly susceptible to ischemia-reperfusion
injury in the first 48 h

after transplantation 8,9. In clinical trials, vascular thrombosis 10
and irreversible metabolic

changes such as liver steatosis 11 have been identified as long-term
complications of repeated

portal vein injection of islets. With microencapsulation, the functional
longevity of encapsulated

islets in vivo may be eventually compromised by cellular overgrowth,
despite ongoing attempts

to maintain long-term membrane porosity 12.

An isolated but readily accessible chamber with an independent blood supply
could improve in

vivo culture of islets and extend our understanding of the signals required
for expansion of

insulin-producing tissue. Our laboratory has developed several three-
dimensional in vivo models

that can sustain transplanted cells. In the  rat, an arteriovenous AV
loop created by

anastomosing the divided ends of the femoral artery and vein was sandwiched
between layers of

dermal collagen and the construct contained within a subcutaneously
implanted polycarbonate

chamber in the groin 13. Spontaneous outgrowth from the loop resulted in
new fibrovascular

tissue, which invaded the collagenous scaffold and filled the chamber. A
similar model in the

mouse, in which a flow-through AV pedicle was enclosed in a subcutaneously
inserted

cylindrical silicone chamber, also resulted  in angiogenesis and
spontaneous generation of

connective tissue 14. Vascularized tissue growth occurred even in the
absence of added

extracellular matrix ECM or cells, substantially filling the chamber
within 2–4 weeks 15. By

4–12 weeks, we found further expansion and maturation of this tissue, with
progressively greater

collagen fibril alignment 16. The addition of various ECM scaffolds
within the chamber

increased the rate of new tissue growth 16. Interestingly, the addition
to the mouse chamber of

specific components, such as Matrigel® and fibroblast growth factor-2 FGF-
2, promoted

differentiation of adipose tissue 14. In vascularized rat chambers we
have also observed the

growth and differentiation of myoblasts after 2–6 weeks into myotubes and
myofibers in the new

tissue, staining positively for desmin and dystrophin, respectively 17.
Theoretically, any

combination of ECM, cells, and specific growth or differentiation factors
may be added to these

chambers to engineer new tissue. Furthermore tissue can be retrieved easily
or further

components added at any stage.

Initial experiments with an in vivo vascularized rat chamber seeded with
syngeneic islets

revealed maintenance of immunohistochemical insulin and glucagon expression
after 3 weeks

18. Similarly, adult mouse islets and fetal mouse pancreata have been
shown to survive in

vascularized chambers in the groin of non-diabetic mice for up to 10 weeks
Cronin, K.J. et al,

unpublished data. In this study, we used mice with streptozotocin-induced
diabetes to evaluate

islet grafts grown in chambers maintained around the epigastric artery and
vein groin chambers

or around the splenic artery and vein adjacent to the pancreas splenic
chambers, compared with

the “benchmark” renal subcapsular site. Our aim was to demonstrate that the
chamber model

allows the development of an  insulin-producing graft containing  viable
islets, suitable for the

treatment of diabetes.




Experiments performed on adult male C57BL/6j mice 22–28 g Animal
Resource Centre,

Perth, Western Australia were conducted under NHMRC Australia guidelines
and as approved

by the Animal Ethics Committee, St Vincent’s Hospital Melbourne. Mice were
housed in an

approved facility, on a 12 h day/night cycle, and given food and water ad
libitum.

Streptozotocin-induced diabetes

Freshly weighed streptozotocin STZ; Sigma Chemical Co, St Louis, MO was
dissolved in

0.01 mol/l tri-sodium citrate buffer, pH 4.5, at 20 mg/ml and administered
within 5 min via tail

vein injection at an initial dose of 0.16 mg/g. Over the next 3 days, mice
were checked for excess

urination and water consumption. At day 5, a saphenous vein bleed was
performed under 2

flurothane anesthesia and blood glucose BG  level measured with a glucose
oxidase-based

monitor MediSense Precision Plus, Abbott Laboratories, Doncaster,
Victoria, Australia. Only

mice with non-fasting BG>17 mmol/l and polyuria were defined as diabetic
and included in the

study. Mice that did not initially develop diabetes ~50 of all mice were
re-injected with half

the initial dose of STZ 0.08 mg/g and BG measured again at day 5. Using
this protocol, ~70

of STZ-injected mice became diabetic.



Islets were isolated from C57BL/6 mice as described previously 19. The
common bile duct was

cannulated, and the pancreas was distended with 3 ml RPMI medium Life
Technologies,

Gaithersburg, MD containing 1.3 U/ml of collagenase P Roche Molecular
Biochemicals, IN.

Pooled pancreata were digested at 37°C for 20 min then disrupted with
shaking. Islets purified

on a Histopaque-1077 density gradient Sigma were washed and cultured at
37°C in 5 CO2 in

CMRL medium-1066 Life Technologies containing 10 FCS antibiotics, and
glutamine for no

more than two days prior to transplantation. The yield was usually 150–250
islets per mouse, and

islets of variable size were noted. The islets maintained their
morphological integrity and

viability during culture in complete CMRL medium prior to transplantation.

Immediately prior to surgery, 400  hand-picked islets were collected into a
sterile Erlenmeyer

tube, washed three times with PBS, and centrifuged to remove FCS. The
islets were finally resuspended in 50  μl Matrigel® BD Biosciences,
Bedford, MA at 4°C for seeding into one

chamber or injecting into the  right kidney. On average, ~45  μl of the
islet suspension,

corresponding to ~350 islets, was injected into the chambers or the kidney.






Mice were anaesthetized with  chloral hydrate  4 mg/g body weight, i.p.,
and surgery was

performed under aseptic conditions. Cylindrical silicone chambers made from
laboratory tubing

Dow-Corning Corporation, Midland, MI were cut into segments 5.5 mm in
length, 3.35 mm

internal diameter, volume approx 50  μl each having a lateral slit. The
abdominal hair was

removed using depilatory cream, and the skin was decontaminated with
alcohol and chlorhexidine. A midline abdominal incision exposed the
abdominal contents. The branch of

splenic artery, where it exited the pancreas, was identified, and adherent
adipose tissue was

dissected from the vessels to create a 1 cm length of splenic pedicle. The
chamber was inserted

around this pedicle. The distal end and the lateral split were sealed with
Ethicon Bone Wax

TM

Johnson and Johnson International, European Logistics Centre, Brussels,
Belgium. The islets in

Matrigel® 50 μl in a cooled syringe were injected into the open
proximal end of the chamber

then sealed with bone wax, taking  care not to damage the pedicle Fig
1A. The splenic

chambers were kept in position by the pressure exerted from surrounding
organs and suture

stabilization was not therefore required. The  abdomen was closed in
layers, with 6/0 nylon

sutures for the abdominal wall and stainless steel clips for the skin.



This model has been described previously in detail 14. In outline, using
mice anesthetized with

chloral hydrate as described above, hair on the groin and upper legs of
mice was removed using a

depilatory cream, and the skin was decontaminated using alcohol and
chlorhexidine. An incision

was made above the groin fat pad, from the groin crease to the knee.
Superficial epigastric

vessels were dissected free of surrounding fat from their origin at the
femoral vessels for a

distance of ~1 cm to their point of entry into the groin fat pad. The
entire fat pad was mobilized

free of skin so that the epigastric vessels free of fat could be embodied
into the cylindrical

chamber via a lateral split. The whole chamber was anchored to underlying
muscle using 10/0

nylon microsutures. A small amount of the fat pad was used to  plug the
distal end of the

chamber. The lateral slit was sealed with bone wax and the chamber filled
with ~50 μl of islets

suspended in Matrigel® from a pre-cooled syringe with a 25-gauge needle.
The proximal end of

chamber was sealed with bone wax Fig 1B, and the skin was closed with
stainless steel clips.



Using mice anesthetized as described above, we made a midline abdominal
incision, the right

kidney was identified and islets suspended in 50 μl of Matrigel® were
injected slowly through a

26-gauge needle, with the needle tip close to the surface of the capsule.
During injection, care

was taken so that the needle passed only through the renal capsule without
penetrating the body.

The needle was removed without apparent fluid loss. The abdomen was closed
as per the splenic

chamber operation.



All procedures were performed as above, except that Matrigel® minus islets
was used to fill the

chamber.

Daily care of diabetic mice, weekly blood glucose monitoring

Mice in the study groups had daily Clinistix™ urine glucose tests Bayer.
One unit of long-acting insulin Monotard; diluted 1 in 10, 0.1 ml was
injected subcutaneously if urine glucose

was positive. The injection sites were rotated between the back of neck and
two lumbar regions.

After operation, non-fasting BG levels were determined weekly on saphenous
vein samples. In

weeks 6, 8, and 10, glucose tolerance tests, chamber harvesting and fasting
BG, respectively,

were performed. Glucose tolerance test and fasting blood glucose
determination

An intraperitoneal i.p. glucose tolerance test GTT was performed 6
weeks after surgery. Mice

were fasted for 6–8 h, with only water provided ad libitum. BG was measured
by right saphenous

vein sample immediately prior to i.p. injection of 0.2 ml of a 250 mg/ml
solution of D-glucose

dissolved in saline 2 mg/g body weight. BG was again measured 30, 60, 90,
120, and 180 min

after glucose injection. The left saphenous vein was sampled if there was
any difficulty obtaining

a drop of blood from the right vein. “Glucose tolerance” was defined as the
insulin-dependent

glucose uptake in 1 h 20. In the context of the GTT, the BG level at
90/120 min minus BG

level at time 0 should be ≤50 of the maximal BG level at 30/60 min minus
BG level at time 0,

to be regarded as an “effective” treatment 20.

Ten weeks after surgery, once splenic and groin chambers had been
harvested, fasting BG levels

were determined for these two groups only.



Mice were anesthetized as described earlier, and the chambers or kidneys
were removed via a

minimal access incision, 8 weeks after the initial operation. At the time
of harvest, the size of

tissues and patency of the pedicle were assessed qualitatively. Mice from
the splenic and groin

chamber groups were allowed to recover for the two weeks prior to the
fasting BG

determination, before being killed by an i.p.  injection of Lethobarb  0.3
ml, pentobarbitone

sodium; Virbac, Sydney, NSW, Australia at  week 10. In the renal
subcapsular group, both

kidneys were removed for histology and the mice were killed at week 8.



Tissue was fixed overnight in 4 formaldehyde, before being processed and
embedded into

paraffin. Serial sections 5 μm were placed on slides coated with 3-
aminopropyltriethoxysilane

Sigma, dried overnight in a 37°C incubator, and stained with hematoxylin
and eosin H&E.

Immunohistochemical staining to identify  α glucagon,  β insulin, and
delta somatostatin

cells in islets was performed on a commercial autostainer DAKO,
Carpinteria, CA. Slides were

washed with PBS, incubated for 5 min with 3 hydrogen peroxide, then for 30
min with primary

antibodies: guinea pig anti-insulin antibody diluted 1:10, rabbit anti-
glucagon antibody diluted

1:200 or rabbit anti-somatostatin antibody diluted 1:100 DAKO. The
components of the rabbit

EnVision plus detection system DAKO were then applied for 30 min,
followed by

diaminobenzaldehyde reagent for 5 min. The slides were counterstained with
hematoxylin.

Rabbit non-immune serum was substituted for negative controls, and normal
pancreas sections

were tested as positive controls.



Sections were selected every 100  μm throughout each tissue graft, assuming
an average islet

diameter of 100–130 μm. Specimens harvested from splenic or groin chambers
were stained with

H&E, whereas those from the kidney were immunostained with insulin
antibody. With the

observer blinded to the origin of the specimen, each slide was observed at
×2 and then  ×20

magnification. Islets were quantified and their relative positions in the
specimen were recorded, taking care not to duplicate counting of the same
islet in consecutive sections. The total islet

count for each specimen was compiled.

To assess the degree of vascularization of the surviving islets in the
grafts, the distance between

each islet and the nearest blood vessel of >20  μm diameter was
determined using the

AxioVision, version 4.2, image processing and analysis system based on the
Zeiss Axioscope 2

microscope Carl Zeiss Vision, Munich, Germany.

Data are reported as mean ± standard deviation SD for all observations,
where  n equals the

number of mice. Statistical comparisons between groups, using ANOVA and a
post-hoc Tukey’s

multiple comparison test, were considered significant at P ≤ 0.05 GraphPad
Prism software, San

Diego, CA. The relationship between the “glycemic control index” and total
islet counts per

specimen was determined by least-squares regression analysis using SPSS for
Windows version

12.01.




At the time of harvest, the vascular pedicle was patent in all chambers.
After 6 weeks’ growth,

tissue occupied ~50 of the chamber volume for splenic chambers and ~90 of
the chamber

volume for groin chambers, most of the latter appearing to be vascularized
fat.

Weekly BG levels

The mean weekly BG levels for the control and groups over 10 weeks are
shown in Table 1. The

mean BG for non-diabetic mice was in the range 7–10 mmol/l and for STZ-
induced diabetic

mice 18–22 mmol/l. BG levels for sham-operated mice were indistinguishable
from nonoperated counterparts. BG in splenic chamber, groin chamber, and
renal subcapsule groups

exhibited a gradual fall in the first 4 weeks, stabilizing between weeks 4
to 7, and reaching

minimal levels of 10.9, 14.1, and 12.1 mmol/l, respectively, by week 7.
Compared with untreated

diabetic controls, these decreases in BG of 8.1, 8.2, and 7.2 mmol/l were
significant in all three

groups P<0.001. However, only the splenic chamber group reached the
threshold BG level of

11.1 mmol/l, regarded as a significant reversal  of hyperglycemia 21. The
decrease in BG

achieved by the splenic chamber group was significantly better than that of
the groin chamber

group P<0.01. However, no significant differences were found in BG
between the splenic

chamber and renal subcapsule groups, nor between the groin chamber and
renal subcapsule

groups. Once the islet-containing chambers had been harvested, the mean BG
returned to near

pre-operative levels in both splenic and groin chamber groups P<0.05 in BG
at week 9 vs. BG

at week 7.



An i.p. GTT was performed 6 weeks after islet grafting Fig 2. Fasting BG
in normal or sham operated mice was around 7.0 mmol/l. Following i.p.
glucose BG typically peaked at 12 mmol/l

after 30–60 min and returned to near-normal fasting levels of 7.7 mmol/l by
120 min mean

hourly glucose uptake being 87 of the i.p.-injected glucose bolus. By
contrast, diabetic control mice had a fasting BG of 13.8 mmol/l, which
peaked at 22.2 mmol/l at 30–60 min after i.p.

glucose, and was still significantly elevated at 20.3 mmol/l after 120 min
mean hourly glucose

uptake being 22 of the i.p.-injected glucose. The splenic chamber, groin
chamber, and renal

subcapsule groups had mean hourly uptakes of  40, 27, and 28 of the i.p.-
injected glucose,

respectively, which is less than the 50 hourly glucose uptake regarded by
Abel et al 20 as

“effective” treatment of hyperglycemia. All three groups had diminished
capacity for glucose

tolerance compared with normal/no-operation control mice P<0.01.

Morphological evidence of islet survival, hormone production, and
vascularization

After 8 weeks’ growth H&E staining revealed isolated surviving islets in
the chamber tissue.

Each cross section studied had between 0 and 7 islets, an average of 1
islet in every 2 sections

examined. Patent pedicles and well-dispersed small blood vessels were
observed in tissue from

both splenic and groin chambers. Fibrous responses were seen at the edge of
tissues. They were

generally free of inflammatory infiltrates Figs 3A and  4A. At higher
magnification, we

observed that islets had maintained their rounded morphology in the chamber
tissue.

Vascularized adipocytes were seen in the middle of the chamber tissue
Figs 3C and 4B–E, and

more numerous small blood vessels were seen in the fibrous zone Figs 3C
and 4A, D. Islets

were commonly located in vascularized connective tissue or adipose tissue
at the periphery of the

graft Fig 4B–D and were readily identified by their immunochemical
staining for insulin,

glucagon, and somatostatin Figs 3B–D and 4C–E. Islets injected under the
renal capsule were

more difficult to identify due to the similar histological appearance of
the glomeruli Fig 5A.

One islet, identified by insulin immunostaining, had a flattened morphology
Fig 5B, but others

had a more rounded in appearance Fig 5C.

Islets surviving after 8 weeks’ growth were commonly located adjacent to a
large artery or vein.

Morphological analysis of islets in graft tissue harvested from splenic
chambers and groin

chambers revealed that, on average, islets were 41.7 ± 27.7  μm n=29 and
47.7 ± 34.8  μm

n=40, respectively, from a major blood vessel. Interestingly some islet
clusters had capillaries

passing directly through them Fig 4B. Compared with the rich vasculature
in the chamber

tissue, blood vessels were difficult to identify in the kidney specimens,
so this parameter was not

determined for islets in the renal subcapsule group.

Quantification of islet survival in grafts after 8 weeks and correlation
with BG

The mean numbers of islets in grafts were 70 ± 22 range 4–120 in the
splenic chamber group,

23 ± 5 range 7–42 in the groin chamber group, and 19 ± 11 range 0–66 in
the renal

subcapsule group. The splenic chamber group had significantly more
surviving islets than the

other two groups P<0.05. Given that on average 350 islets were seeded,
the mean islet survival

was 22, 7, and 6, respectively range 0 to 37.

Glycemic control during islet transplantation in vascularized chambers was
estimated by plotting

the minimal BG in weeks 1–8 against total islet counts in the graft
specimen harvested at week 8.

Regression analysis showed a strong correlation between these two
parameters r=–0.68,

P<0.01


In this study, we have shown that islets seeded into vascularized chambers
reduce hyperglycemia

in STZ-induced diabetic mice just as effectively as islets injected under
the renal capsule, the

“benchmark” procedure 6.

Properties of vascularized chambers that promote islet survival

Several factors influence the viability of transplanted islets. Hypoxia is
likely to be one of the

major causes of islet loss soon after transplantation. Folkman and co-
workers determined that,

ideally, cells need to be within 150–200  μm of a blood vessel in order to
receive adequate

oxygen and nutrition for survival 22. Our  morphological study on  tissue
at 8 weeks

demonstrated that surviving islets were, on average, 45 μm from a major
blood vessel range 6–

118  μm, well within the survival limits defined  by Folkman. Islets
injected under the renal

capsule also become hypoxic, because normoxia is limited to a few cell
layers before critical

ischemia occurs 23, 24. During islet isolation, islet-basement membrane
interactions are

disrupted by collagenase digestion. In vitro studies have shown that
laminin-1 promotes survival

and differentiation of β cells 25. The use of laminin-rich Matrigel® as a
carrier for islets in the

vascularized chambers is designed to restore islet-laminin interactions and
improve islet survival

Once seeded into the chamber, islets should  benefit from both the
angiogenic and adipogenic

properties of Matrigel® 26. However, the time spent at 4°C necessary for
keeping Matrigel®

in its liquid form prior to islet seeding may be critical, as this
temperature is considered too low

for optimal β cell survival 27.

The lag-time before the decline of BG after islets are seeded into
vascularized chambers or

injected under the renal capsule may reflect the time taken for islet
engraftment within a new

vascular bed. Patency of the vascular pedicle was maintained in all
chambers and, histologically,

marked angiogenesis was observed throughout the entire tissue. Quantitative
morphological

studies after 8 weeks showed that glycemic control was improved with as few
as 50 ~14 of

the 350 islets surviving in chambers or under the renal capsule.
Importantly, a strong correlation

was also found between the minimal BG achieved by islet transplantation in
chambers and the

number of islets found in the specimen by morphological analysis P<0.01.
Endocrine function

was confirmed by the increase in glycemia after  the chambers were
surgically removed at 8

weeks, and by immunohistochemical staining for insulin, glucagon and
somatostatin.

Optimal site of chamber for islet transplantation

The lower mean BG over time, lower fasting glucose levels, improved glucose
uptake during the

GTT, and the higher islet counts per graft demonstrate that the splenic
chamber group had better

glycemic control than the groin chamber group. Splenic chambers inserted in
the pancreatic bed

use the same portal drainage as native islets,  whereas chambers inserted
in the groin have

systemic drainage. Portal drainage has been shown to provide more effective
and physiological

function of islets 28. Groin chambers, however, may have an advantage of
maintaining longterm patency of the pedicle because superficial epigastric
vessels, branching from femoral

vessels, form anastomoses with ilio-inguinal vessels. This may result in
arterial input and venous

drainage from both sides 13. We would expect the angiogenic capacity of
the two locations to

be similar. Relatively more tissue formed in groin chambers compared with
splenic chambers,

and qualitatively a greater percentage of adipose tissue was found in the
groin chambers. Islets were located in both the connective tissue and the
adipose tissue, with no apparent preference. In

all cases the islets were close to blood vessels, indicating that
vascularization is crucial for their

survival and function. Of particular interest was the finding of multiple
capillaries in some

revascularized islets, a typical example is shown in Fig 4B. This intimate
association between

islet and the bloodstream is vital for the islet’s ability to sense
hyperglycemia, to respond by

producing insulin then to secrete the hormone efficiently into the
bloodstream.

The renal subcapsule group had BG levels intermediate between the splenic
and groin chamber

groups but were not statistically different from either. These BG levels
were significantly better

than untreated diabetic controls P<0.001. The renal subcapsular site has
been shown previously

to maintain effective islet function compared with intrahepatic and other
sites in rats and mice

23, 24 due, in part, to the ready access of a blood supply 28, 29 and
rapid revascularization

rate 24. Most parameters suggest that vascularized chambers are at least
comparable with this

“benchmark” islet transplantation procedure.  Furthermore, data from the
islet counts per

specimen demonstrated the survival of significantly more islets on average
in vascularized

splenic chambers compared with either the renal subcapsule or the
vascularized groin chamber

groups P<0.05.


The clinical method of choice for islet transplantation is portal vein
injection, although

thrombosis after repeated injections and long-term metabolic disturbances
in the liver such as

steatosis are significant complications of this procedure 10, 11. Whole
pancreas transplantation

is another alternative but, as with islet transplantation, suffers from a
chronic lack of pancreata

2, 3.

The chamber model is a useful experimental tool, but would need to undergo
revision before it

could become the clinical method of choice. The surgery involved requires
considerable

expertise and may be too complex to be performed routinely by a general
surgeon. Surgical

insertion of chambers is an invasive procedure and the introduction of
infection is a further

complication. In its present form, the chamber technique cannot effectively
protect islets from

allorejection or recurrence of autoimmunity, but it could be combined with
cell protection

techniques such as islet microencapsulation 12, 30 to overcome this
problem and improve the

outcome. The islets most distant from the pedicle are subject to hypoxia in
the early days after

engraftment 8, 9, 31. It may be advantageous in the future to attempt
seeding islets directly

onto the pedicle in a smaller volume of carrier solution eg, 10–20 μl
to minimize islet loss.

The strategy of delaying the introduction of islets into chambers by 1 to 2
weeks, the time taken

to establish an adequate angiogenic response in the mouse model 14, could
ultimately improve

islet engraftment and viability. Nevertheless,  vascularized chambers have
some distinct

advantages. Chambers can be located on blood vessels  of choice, such as
the splenic vessels,

offering portal vein drainage of the islet-containing graft. Furthermore,
the contents of the

chamber could theoretically be replenished with fresh islets if the
original islets lose their ability

over time to produce insulin. Only time and further refinement of the
technique will prove

whether it has a future as a viable clinical method of islet
transplantation.

The use of the tissue-engineered, insulin-producing “organoid,” with its
intrinsic vascular supply,

the use of autologous cells and biomaterials where possible, and the
ability to retrieve or even

replenish seeded islets, could overcome several deficiencies in the
currently available methods of islet transplantation. In this novel model
the graft environment can be altered via ECM

substitution, cellular co-culture, and concurrent growth factor or drug
administration.

Furthermore, the transplanted tissues are recoverable without animal
sacrifice and are

microsurgically transferable within and between individuals. Due to its
independent vascular

supply, manipulation, analysis, and evaluation are simplified; for example,
blood sampling from

the afferent and efferent pedicle to assess insulin production by the
transplanted islets is

theoretically feasible. The model provides an in vivo culture system  for
the study of islet

transplantation and lends itself to the introduction of pancreatic stem
cells and  islet precursor

cells.
