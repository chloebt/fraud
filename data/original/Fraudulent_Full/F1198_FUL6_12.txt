
Churg-Strauss syndrome CSS is a rare disorder that is characterized by
asthma, hypereosinophilia, and evidence of vasculitis with massive
infiltration of eosinophils affecting a number of organs.1 The prevalence
is of the order of 1.3/100,000 in the general population, compared with
3.3/100,000 for polyarteritis nodosa and 5.3/100,000 for Wegener
granulomatosis 23. Successful treatment of this rare syndrome needs prompt
differentiation of CSS from asthma alone 45; however, there are few
serologic markers to distinguish CSS from asthma.
Vascular endothelial growth factor VEGF, a homodimeric, heparin-binding
glycoprotein of 34 to 42 kd, is one of the major mediators of angiogenesis
and vascular permeability.67 There have been several reports describing the
association of VEGF with systemic vasculitis, such as Wegener
granulomatosis, 8 giant-cell arteritis, 9 and Kawasaki disease 10. However, to
our knowledge there are no reports describing serum VEGF levels in patients
with CSS. In this study, we investigated 18 patients with CSS and found
increased serum levels of VEGF compared with asthma. We suggest a possible
association of VEGF with the pathogenesis of CSS.


This study was reviewed and approved by the Kagoshima University Faculty of
Medicine Committee on Human Research. We investigated 18 patients with CSS
who were admitted to the Division of Respiratory Medicine, Respiratory and
Stress Care Center, Kagoshima University Hospital from 1995 to 2005. There
were 8 men and 10 women mean ± SD age, 58.2 ± 18.2 years. For comparison,
we also investigated 19 patients with asthma 8 men and 11 women; mean age,
58.9 ± 15.3 years and 12 patients with acute bronchitis 5 men and 7
women; mean age, 58.9 ± 13.1 years. The diagnosis of CSS was made
according to the 1990 edition of CSS published by American College of
Rheumatology.11 All patients with CSS fulfilled more than five criteria. We
excluded patients with rheumatoid arthritis, diabetes mellitus, acute or
chronic liver disease, and immunologic abnormalities that predispose to
opportunistic infection. Peripheral eosinophil counts, platelet counts, C-
reactive protein, erythrocyte sedimentation rate ESR, smoking index
number of cigarettes smoked per day times smoking years, and Pao2 of all
patients were also determined.

In patients with CSS, asthma, and acute bronchitis, we measured serum
levels of VEGF before the patients underwent therapy. In eight patients
with CSS, serum VEGF levels were determined before therapy, 1 month after
the start of therapy, and 6 months after the start of therapy. All
participants gave written consent to participate in this study.

VEGF concentrations in sera were measured in duplicate for each sample
using a commercial enzyme-linked immunosorbent assay kit R&D Systems;
Minneapolis, MN that recognizes the soluble isoforms VEGF121 and
VEGF165. This assay is sensitive to 9 pg/mL 0.2 pmol/L of VEGF and does
not cross-react with platelet-derived growth factor or other homologous
cytokines. Optical density at 450 nm was measured Titertek Multiskan MC
plate reader; Flow Laboratories; Helsinki, Finland, and VEGF concentration
was determined by linear regression from a standard curve GraphPad
software; GraphPad; San Diego, CA for analysis.

Immunohistochemical staining for VEGF was performed using a rabbit
polyclonal antibody Santa Cruz Biotechnology, Santa Cruz, CA employing
the DAB method using the biopsy specimens of five CSS patients as described
previously 12. Briefly, 4-μm-thick sections were dewaxed and rehydrated. For
optimal antigen retrieval, sections were pressure cooked in 0.01 mol/L
citrate buffer pH 6.0 for 90 s. Endogenous peroxidase activity was
blocked using a 3 hydrogen peroxide solution in methanol for 10 min. After
washing, sections were incubated with primary antibody solution for 2 h at
room temperature using a 1:150 concentration working dilution of the
antibody. Negative control slides were incubated with rabbit polyclonal
antibody Super Sensitive Rabbit; Biogenex; San Ramon, CA. After washing,
secondary biotynated anti-Ig antibody Biogenex was added, and the mixture
was incubated for 30 min at room temperature. The sections were again
washed, and streptavidin conjugated to horseradish peroxidase Biogenex
was incubated for 30 min and then rinsed off with deionized water.
Diaminobenzidine tetrahydrochloride substrate solution was then added, and
the mixture was incubated for 10 min. A brown color reaction represented a
positive result.

We used one-way factorial analysis of variance with the Bonferroni-Dunn
test and Pearson correlation coefficient; p < 0.05 was considered
significant. Most values were expressed as mean ± SD.




The serum VEGF levels in patients with CSS were significantly higher than
in patients with asthma or acute bronchitis CSS, 753.2 ± 226.8 pg/mL;
asthma, 248.8 ± 188.2 pg/mL; acute bronchitis, 188.3 ± 108.4 pg/mL; Fig
1 . The sensitivity of VEGF levels to distinguish CSS from asthma was
93.3, and specificity was 81.8 cutoff value, 600 pg/mL. The serum VEGF
levels of eight patients with CSS remained high level 1,069.1 ± 225.7
pg/mL 1 month after the start of therapy and decreased 6 months after the
start of therapy Fig 2 . The serum VEGF level at 6 months after the
beginning of therapy 393.1 ± 122.4 pg/mL was significantly lower than
that at the start of therapy 809.3 ± 255.1 pg/mL; Bonferroni-Dunn test
with one-way factorial analysis of variance, p < 0.001. The clinical
symptoms of CSS patients improved with the use of corticosteroids. Serum
VEGF levels before the start of therapy showed a significant positive
correlation with peripheral eosinophil counts Fig 3 ; r = 0.779, p <
0.0001. Serum VEGF levels did not show a significant correlation with
platelet counts r = 0.311, p = 0.08, C-reactive protein level r = 0.231,
p = 0.12, ESR r = 0.309, p = 0.09, smoking index r = 0.199, p = 0.3,
and Pao2 level r = − 0.229, p = 0.11. There was no significant difference
in smoking index and Pao2 levels among these groups. Anti-neutrophil
cytoplasmic antibody was positive in 10 CSS patients. In
immunohistochemical analysis, infiltrating eosinophils in the lesions
stained intensely positive for VEGF Fig 4.



Our results showed significantly higher levels of serum VEGF in CSS
patients than in asthma patients. In addition, serum VEGF levels in CSS
patients before the start of therapy showed a significant positive
correlation with peripheral eosinophil counts, and tissue infiltrating
eosinophils stained intensely positive for VEGF. Human eosinophils
constitutively express messenger RNA encoding VEGF and also store VEGF,
probably in cytoplasmic granules; moreover, eosinophils release VEGF
following stimulation with granulocyte macrophage colony-stimulating factor
or interleukin-5 13. VEGF resides in the α-granules of platelets and is
released during blood clotting,14 and a direct correlation between platelet
count and serum VEGF level has been described 15. Therefore, it is suggested
to correct VEGF serum levels for variations in platelet count 16. In our
study, serum VEGF levels of CSS patients before the start of therapy did
not show a significant correlation with platelet counts or other
inflammation markers. Heavy smoking 17 and systemic hypoxia18 affect serum
VEGF levels. In this study, there were no significant correlation between
serum VEGF levels and smoking index or Pao2 levels, and there were no
significant differences in the smoking index and Pao2 levels among the
three groups of patients. Taken together, we think that eosinophils are the
main cellular source of serum VEGF in CSS. It is possible that VEGF
released from eosinophils might contribute to the recruitment of
inflammatory cells including T-cells and eosinophils by increasing vascular
permeability in the development of CSS. Interestingly, serum VEGF levels
remained high level 1 month after the beginning of therapy. In CSS,
prolonged survival of eosinophils due to inhibition of CD95-mediated
apoptosis by soluble CD95 seems to contribute to eosinophilia 19. Although
the mechanisms involved in eosinophil activation in CSS have not been
elucidated, data suggest a possible role of T-lymphocytes secreting
eosinophil-activating cytokines 20. Glucocorticoid, which was used for the
treatment of CSS in this study, is known to induce apoptosis of
eosinophils, 21 and eosinophil counts are very high in CSS. Therefore, we
think that the massive VEGF release of the apoptotic eosinophils might
explain the high VEGF levels 1 month after the start of therapy in CSS.
Administration of neutralizing monoclonal antibody against VEGF, such as
bevacizumab, 22 might have therapeutic effect against CSS.
The sensitivity and specificity of VEGF levels at 600 pg/mL to distinguish
CSS from asthma were 93.3 and 81.8, respectively. Abnormal laboratory
findings in patients with CSS include increased peripheral blood eosinophil
counts and a raised ESR, 23,24 but it is sometimes difficult to distinguish
CSS from asthma using these markers for the following reasons: 1 rarely,
eosinophilia is not present, and wide-ranging and rapid changes in
eosinophil counts happen in CSS25; 2 use of corticosteroids to treat
asthma may result in failure to detect eosinophilia in patients with
undiagnosed CSS; and d3 increase in ESR occurs in other disorders such as
infection. Another serologic marker, anti-neutrophil cytoplasmic
antibodies, is present in 44 to 66 of CSS patients, with the most common
pattern being perinuclear 26,27,28. In addition, anti-neutrophil cytoplasmic
antibody positivity needs to be confirmed by demonstration of
myeloperoxidase in serum 29. In our study, serum VEGF levels of CSS patients
remained high for 1 month, even after the start of therapy. Therefore, we
propose that measurement of serum VEGF may be a useful screening marker to
distinguish CSS from asthma, as a negative result greatly reduces the
likelihood of CSS. However, a positive result requires confirmation, as the
specificity is relatively low. Our study is too small and short to draw a
definitive conclusion. We propose that larger and longer studies addressing
this point are necessary to judge its diagnostic value.

