
Voltage-dependent Ca2+ channels are well known targets for inhibition  by  G
protein-coupled receptors, and multiple pathways for  inhibition  have  been
described 1, 2. Inhibition of Ca2+ channels can be  voltage-dependent  and
is mediated by G protein βγ subunits  1.  In  addition  kinases,  such  as
protein  kinase  C  and  tyrosine  kinases,  have  been  shown  to   inhibit
Ca2+channels 2. As these multiple pathways  converge  in  modulating  Ca2+
voltage-dependent channels, the question has arisen as to how these  signals
are integrated.

Activation  of  tyrosine  kinase  by  GABAB1  receptors  mediates   voltage-
independent inhibition of Cav2.2  N-type  current  in  chick  dorsal  root
ganglion DRG neurons 3, and  the  α1  subunit  of  the  channel  becomes
tyrosine-phosphorylated 4. The tyrosine-phosphorylated  channel  binds  to
RGS12, a “regulator of G protein signaling” or RGS protein, that contains  a
phosphotyrosine binding domain 4.  These  findings  raised  the  issue  of
whether tyrosine phosphorylation of the channel facilitates the  recruitment
of  other  signaling  molecules  that  could  then  alter   the   functional
characteristics of the channel and  what  role  such  interactions  have  in
signal integration.



We hypothesized that the Ca2+ channel itself serves as an integrator by
interacting with signaling molecules that regulate channel activity and by
modulating downstream signals. To test this hypothesis, we have
characterized interactions of the α subunit of the Cav2.2 N-type, α1B
channel with signaling molecules, and we show how these interactions are
regulated in an agonist and time-dependent manner and the consequences of
such regulation on channel activity. Our results demonstrate that dynamic
interactions at the level of the Ca2+channel can have short term effects
such as the modulation of channel activity and long term effects mediated
through the activation of MAP kinase.


 The α1 subunit of the channel was precipitated  using  an  antibody  anti-
α1B  raised  against  loop  II-III   of   the   Cav2.2   channel   Alomone
Laboratories, Jerusalem,  Israel;  we  demonstrated  previously  that  this
antibody immunoprecipitates the α1B  subunit  of  the  Cav2.2.  Anti-Pan  α1
subunit  antibody   Alomone   Laboratories,   Jerusalem,   Israel,   which
recognizes amino acids  1382-1400  of  the  rat  α1  subunit  from  skeletal
muscle, a region conserved across all  the  α1  subunits  of  high  voltage-
activated Ca2+ channels, was used in our experiments in confirm that the  α1
subunit of the calcium channel was precipitated by  the  anti-α1B  antibody.
Specific antibodies raised against amino acids 403-421  of  members  of  the
Src-related   kinase   family,   anti-TyrP-416   Src,   anti-SOS,    anti-
phosphotyrosine  4G10,  anti-PYK-2,  and  anti-Pan  Ras   antibodies,   were
purchased from Upstate Biotechnologies, Inc; anti-GST  was  purchased  from
Sigma, and anti-RasGRP was obtained  from  Santa  Cruz  Biotechnologies.  ω-
Conotoxin GVIA was purchased from Bachem.

 Embryonic  chick  sensory  neurons  were  grown  in  culture  as  described
previously 3. Dorsal root ganglia were dissected from  11-  to  12-day-old
embryos. Cells used for  electrophysiology  were  plated  at  a  density  of
∼50,000 cells per collagen-coated 35-mm  tissue  culture  dish  and  studied
between 1 and 3 days in vitro. Hippocampal cultures were prepared  from  rat
E18 embryos according to the protocol of Goslin et al  5.  Cultures  were
used between days 14 and 18 after  plating.  Hippocampus  pyramidal  neurons
were identified by morphology for electrophysiological experiments.

 Hippocampal slice preparation and maintenance were  modified  from  Blitzer
et al 6. Briefly,  male  Sprague-Dawley  rats  125-200  g  were  deeply
anesthetized with halothane and decapitated. The brain was  rapidly  removed
and placed in ice-cold artificial cerebrospinal  fluid  containing  in  mM
NaCl 118, KCl 3.5, MgSO4 1.3,  CaCl2  3.5,  NaH2PO4  1.25,  NaHCO3
24, and glucose 15, bubbled with 95 O2, 5  CO2.  The  hippocampus  was
then rapidly dissected out, and transverse slices of 400 μm  thickness  made
on a  tissue  chopper.  Slices  were  maintained  in  an  interface  chamber
artificial cerebrospinal fluid/humidified 95 O2,  5  CO2  atmosphere  at
room temperature for at  least  1  h  before  transfer  to  a  recirculating
submersion chamber, at which time drugs were applied.

 Whole-cell recordings were performed as we have described  previously  3.
For  extracellular  application,   agents   were   diluted   into   standard
extracellular saline 133  mM  NaCl,  1  mM  CaCl2,  0.8  mM  MgCl2,  10  mM
tetraethylammonium chloride, 25 mM HEPES, 12.5 mM NaOH, 5  mM  glucose,  and
0.3 μM tetrodotoxin and applied via a  wide-bore  pipette.  For  recordings
from hippocampal neurons, whole-cell recordings were performed at  days  14-
16 after plating, in the presence of 10 μM nifedipine and 200 nM  ω-agatoxin
IVA to isolate the Cav2.2 current from Cav1  and  Cav2.1  currents.  In  all
experiments employing GABA treatment, 100 μM  bicuculline  was  included  to
block  GABAA  receptors.  Ca2+  currents  were  corrected  for  rundown   by
measuring Ca2+ current as a  function  of  time  in  control  cells  without
neurotransmitter  application.  Cells  used  for  experiments  exhibited   a
current  rundown  of  less  than  1/min.  The  pipette  internal   solution
contained  150  mM  CsCl,  10  mM  HEPES,  5  mM  MgATP,  and  5  mM  biso-
aminophenoxyethanetetraacetic acid. Pipette resistances  prior  to  forming
high resistance seals ranged from 1 to 2 megohms.

 Data were filtered at 3 kHz, acquired at  10-20  kHz,  and  analyzed  using
PulseFit HEKA and  IgorPro  WaveMetrics  on  a  Macintosh  G3  computer.
Strong depolarizing conditioning pulses  to  +80  mV  that  preceded  test
pulses -80 to 0  mV  reversed  GABA-induced  voltage-dependent  inhibition
without affecting voltage-independent inhibition. A protocol  with  +80  mV,
20-ms prepulse, and a 5-ms interval prior to the  test  pulse  was  used  to
measure  the  voltage-independent  component   of   the   inhibition.   Such
conditioning pulses had no  effect  on  control  currents  recorded  in  the
absence of GABA. During  GABA  application,  test  pulse  currents  measured
before and after  the  conditioning  pulse  were  subtracted  to  yield  the
voltage-dependent   component.   Test   pulses   measured   following    the
conditioning pulse were subtracted from control currents  measured  in  the
absence of GABA to yield the voltage-independent component.

 Antibodies were microinjected into the cell  body  by  using  an  automated
injector  Eppendorf  5246.  Fluoresceinated  dextran  Molecular   Probes,
Eugene,  OR  was  co-injected  with  the  antibodies  to  allow  subsequent
identification by using epifluorescence  optics.  Control  experiments  were
performed by injecting internal solution, pre-immune serum, or  rabbit  IgG-
containing internal solution. Ca2+ current as a function  of  time  and  the
voltage-current relationship were measured prior to transmitter  application
to determine agonist-independent effects  of  the  antibodies  on  the  Ca2+
current. Electrophysiological recordings were obtained 1 h after  injection.
Cells were exposed to GABA 100 μM in the presence of  100  μM  bicuculline
to  block  GABAA  receptors,  and  the  magnitude   of   voltage-independent
inhibition was measured. Inward Ca2+ current was  evoked  by  stepping  from
-80 to 0 mV for 10 ms.

 1 × 106 DRG cells, 20 mg  of  rat  hippocampal  neurons,  or  4  adult  rat
hippocampal slices 1.6 mg, 400 μm thickness, kindly provided by Dr  Robert
Blitzer were used for each condition. DRG neurons  or  hippocampal  neurons
were exposed to control solution containing 100 μM  bicuculline  or  100  μM
GABA in the presence of 100 μM bicuculline. Hippocampal slices  were  placed
in oxygenated multiwelled submersion chambers with artificial  cerebrospinal
fluid at 25 °C. Agonists were added to the bath solution for  5  min.  After
agonist treatment, DRG neurons,  rat  hippocampal  neurons,  or  hippocampal
slices were lysed with ice-cold buffer phosphate-buffered saline,  pH  7.4,
containing 250 μM sodium pervanadate, 1 v/v Nonidet P-40, 1 mM  Pefabloc,
1 mM EDTA, 1 mM EGTA, 10 μg/ml pepstatin,  10  μg/ml  leupeptin,  100  μg/ml
soybean trypsin inhibitor, 100 μg/ml calpain I, and  100  μg/ml  calpain  II
inhibitors. The α1 subunit of the Cav2.2 channel was immunoprecipitated  as
described  previously  4.  Anti-α1B   Ca2+   channel   antibody   Alomone
Laboratories was used to precipitate the Ca2+ channel,  and  immunoblotting
was performed by using antip60 Src kinase antibody  1:1000,  anti-phospho-
Src kinase p-Y146 antibody 1:1000, anti-Ras 1:1000 antibody,  anti-SOS
antibody 1:  1000,  and  anti-RasGRP  1:1000.  For  detection  of  ShcC,
immunoblotting was performed using anti-ShcC  1:2000,  from  the  O'Bryan's
laboratory antibody. Antibodies against ShcC were raised  against  the  SH2
domain. The anti-ShcC immunoblot was subsequently stripped and  probed  with
anti-phosphotyrosine 4G10 antibody 1:1000.

In sequential immunoprecipitation  experiments,  precipitates  were  treated
with   lysis   buffer   containing   l   SDS   and   boiled.   The   second
immunoprecipitation    using    anti-PYK-2    antibody    1:300,    Upstate
Biotechnology, Inc for precipitation was performed by 10× dilution of  the
supernatant with wash buffer.

For control  experiments,  1  mg  of  DRG  neuron  membranes  was  used  for
precipitation.  Control  samples  were  exposed  to  IgG  1:500   in   the
precipitation  step,  and  immunodetection  was  performed  to  detect   the
protein.  Precipitated  proteins  were   resolved   by   7   SDS-PAGE   and
electrophoretically  transferred  onto  nitrocellulose.  Immunoblotting  was
performed to detect the α1 subunits of Cav2.2 and Cav1.2.

 Membrane-enriched fractions were  prepared  as  described  previously  4.
Briefly, cell  homogenates  were  centrifuged  at  1000  ×  g  for  10  min.
Supernatants were recentrifuged at 100,000 × g for 1 h, and the  pellet  was
resuspended  in  ice-cold  buffer  phosphate-buffered   saline,   pH   7.4,
containing 250 μM sodium pervanadate, 1 v/v Nonidet P-40, 1 mM  Pefabloc,
1 mM EDTA, 1 mM EGTA, 10 μg/ml pepstatin,  10  μg/ml  leupeptin,  100  μg/ml
soybean trypsin inhibitor, 100 μg/ml calpain I, and  100  μg/ml  calpain  II
inhibitors.

 Recombinant proteins of cytoplasmic  regions  of  the  α1  subunit  of  the
Cav2.2  channel  were  expressed  as  N-terminal  His6-and  S-epitope-tagged
proteins. The His6 tag facilitated protein  purification  by  using  nickel-
nitrilotriacetic acid chromatography, and the S-epitope  tag  was  used  for
precipitation experiments. In vitro phosphorylation  assays  were  performed
using  the  His6-S  tag  recombinant  proteins  containing  the  cytoplasmic
sequences of the α1  subunit  of  the  Cav2.2  channel  and  a  recombinant,
activated form of Src kinase Upstate Biotechnology, Inc. 10-20  units  of
recombinant Src kinase per assay were used in a reaction buffer  of  100  mM
Tris-HCl, pH 7.2, 125 mM MgCl2, 25 mM MnCl2,  2  mM  EGTA,  0.25  mM  sodium
orthovanadate, 2 mM dithiothreitol, and 100 μM ATP. Reactions were  run  for
40 min at 30 °C and terminated by the addition  of  Laemmli  sample  buffer.
Samples were resolved in a 12 SDS-PAGE, and  tyrosine  phosphorylation  was
detected using immunoblotting with the anti-phosphotyrosine  antibody  4G10.
Samples were prepared in duplicate.

 Samples were  resolved  by  SDS-PAGE  and  transferred  to  nitrocellulose.
Membranes were incubated with 1 μg/ml GST-constitutive active Ras  GST-V12H
Ras, GST-wild-type Ras, or GST alone.  Detection  of  protein  binding  was
performed using anti-GST 1:2000.

 For quantitation of immunoblots visualized using chemiluminescence, images
were obtained by using a Molecular Dynamics Personal Densitometer SI
Amersham Biosciences and analyzed using ImageQuant 1.11 for Macintosh.
Values were pooled for all the experiments, and mean values ± S.E. were
plotted. Representative examples are shown for the biochemical experiments.
For Figs 1, 2, 3, data from three independent experiments were pooled, and
for Fig 6 data from four independent experiments were pooled. For
electrophysiological experiments, cells from three independent platings
were used.



 Neurotransmitter-induced  inhibition  of  voltage-dependent  Ca2+  channels
exhibits distinct biophysical properties and can be discriminated  by  their
voltage  dependence.  In  the  presence  of   neurotransmitter,   a   strong
depolarizing pulse prior to the test  pulse  can  reverse  neurotransmitter-
induced  voltage-dependent  inhibition,  leaving   the   voltage-independent
inhibition unaffected. For routine  quantitation  of  the  voltage-dependent
and -independent inhibition, we use a pulse protocol  based  on  Luebke  and
Dunlap 7 +80 mV, 20-ms prepulse and a 5-ms interval  that  reverses,  on
average, 85 of the  voltage-dependent  inhibition,  recognizing  that  100
separation  is  not  feasible  using   electrophysiological   methods.   The
modulated Ca2+ current measured in the absence of a prepulse  is  subtracted
from that in the presence of  a  prepulse;  the  difference  represents  the
voltage-dependent inhibition. We have shown previously that introduction  of
an  Src  kinase  inhibitory  peptide   prevented   GABAB-mediated   voltage-
independent inhibition of Cav2.2  current  while  leaving  voltage-dependent
inhibition unaltered 3. To determine whether Src kinase  activity  induces
voltage-independent inhibition, we introduced an active form of  Src  kinase
through the recording pipette into DRG neurons Fig 1a. The  magnitude  of
Src kinase-induced and voltage-independent inhibition was 55 ± 7 n =  12.
A  prepulse  to  positive  potentials  did  not  reverse   this   inhibition
indicating that the Src kinase-mediated inhibition is voltage-independent.

To identify the endogenous  Src  kinase  that  mediates  voltage-independent
inhibition  of  Ca2+  current  in  DRG  neurons,  specific  antibodies  that
recognize individual members of the Src-related kinase family were  injected
into the cell bodies 1 h prior to performing whole-cell recordings. IgG  was
injected as a  control.  All  the  antibodies  were  injected  at  the  same
concentration 100 ng/ml. To determine agonist-independent effects  of  the
antibodies on Ca2+ current, peak current as  a  function  of  time  and  the
current-voltage relationship were determined prior to  GABA  application.  A
Pan-Src kinase  family  antibody  blocked  GABA-induced  voltage-independent
inhibition, from 37.5 ± 4 in control  cells  to  20.7  ±  3  in  antibody-
injected cells Fig 1b versus 39.5 ± 5 n = 9  in  control  IgG-injected
cells.  An   anti-pp60-Src   monoclonal   antibody   reduced   GABA-mediated
inhibition to 11.3 ± 8. Other properties of modulation such  as  the  rates
of onset and termination were unaltered  by  the  introduction  of  the  Src
antibody Supplemental Material Fig 1. Antibodies specific for  PYK-2  and
YES, two additional non-receptor tyrosine kinases, did not  have  an  effect
on the magnitude of inhibition Fig 1b.

Levitan and co-workers 8 have shown that K+ channels  associate  with  Src
kinase even in the absence of an agonist. To determine  whether  Src  kinase
associates with the Ca2+ channel and whether this  association  is  dynamic,
primary cultures of DRG neurons were stimulated with 100 μM GABA for  20  or
60 s and lysed Fig 1c. The α1 subunit of the channel was precipitated  by
using an antibody raised against loop  II-III  of  the  Cav2.2  channel;  we
demonstrated  previously  that  this  antibody  immunoprecipitates  the  α1B
subunit of the Cav2.2 channel from DRG  lysates  4.  Anti-Pan  α1  subunit
antibody that recognizes amino acids 1382-1400 of the rat  α1  subunit  from
skeletal muscle, a region conserved across  all  the  α1  subunits  of  high
voltage activated Ca2+  channels,  precipitated  a  protein  with  the  same
apparent size and pattern when immunoblotting  with  anti-α1B  antibody  was
performed  Fig  1d.  When  the  anti-Pan  α1  antibody  precipitate   was
immunoblotted with anti-α1C, no band was detected. Chick DRG neurons do  not
express Cav1 channels. Control experiments using IgG failed  to  precipitate
the channel Fig 1, d and e, and Supplemental Material Fig 1.  The  anti-
α1B precipitates  were  probed  in  immunoblots  using  anti-Src  monoclonal
antibodies that are specifically raised against c-Src and do  not  recognize
other members of the family. Src kinase is associated with  the  channel  in
both presence and absence of transmitter Fig  1c.  Similar  results  were
obtained in immunoprecipitation experiments in which anti-Pan α1  antibodies
were used.

We next determined the activation state  of  pp60-Src  associated  with  the
Ca2+  channel  using  an  antibody  that  specifically  recognizes  pp60-Src
phosphorylated on Tyr-416. Phosphorylation of this tyrosine is linked  to  a
local conformation switch that increases catalytic activity  of  the  kinase
9. The amount of Src kinase in the  Tyr-416-phosphorylated  form  detected
in the channel complex increases in the presence of GABA  Fig  1c,  middle
panel, phospho-Src. Application  of  100  μM  baclofen,  a  GABAB  receptor
agonist, for 20 s also increased the amount of  active  pp60-Src  associated
with the channel Fig 1f. A blocker of  the  Cav2.2  channel,  ω-conotoxin
GVIA 10 μM, did not alter the activation of Src  kinase  upon  application
of baclofen Fig 1f. These results indicate that  pp60-Src  is  associated
with the Cav2.2 channel prior to GABAB  receptor  activation  and  that  Src
kinase activity is increased as a consequence of receptor activation.

 To examine whether Src kinase also plays  a  role  in  modulating  of  Ca2+
current in central nervous system neurons, we tested the effect of the anti-
pp60-Src  monoclonal  antibody  on  the  modulation  of  Cav2.2  current  by
baclofen in rat hippocampal pyramidal neurons. The specific calcium  channel
inhibitors nifedipine 10 μM and ω-agatoxin  IVA  200  nM  were  used  to
block Cav1 and Cav2.1  currents  and  thus  isolate  the  Cav2.2.  To  avoid
problems with poor space voltage clamp,  rat  hippocampal  neurons  used  in
this study had tail currents with smooth and fast deactivation kinetics  ≤10
ms. Application of 100 μM baclofen reduced the Cav2.2 current by  45  ±  15
n = 7 in control neurons, whereas the Cav2.2 current was reduced  by  only
25 ± 13 n = 5 in neurons injected with anti-pp60-Src antibody Fig  2a.
Baclofen-mediated modulation of Cav2.2 current  is  voltage-independent,  as
it was not reversed by a prepulse to positive potential

We tested next for the  association  of  pp60-Src  with  the  Ca2+  channel-
containing complex  in  rat  hippocampal  slices.  Hippocampal  slices  were
prepared according to the method of Blitzer et al  6  and  placed  in  an
oxygenated multiwelled  submersion  chamber  with  artificial  cerebrospinal
fluid at 25 °C. The amount of Src kinase in the Tyr-416-phosphorylated  form
detected in the complex  increased  in  the  presence  of  GABA  Fig  2b.
Quantitation showed a 9-fold increase in phospho-Src kinase associated  with
the channel Fig 2b. Similar results were obtained in primary cultures  of
dissociated rat hippocampal neurons that were exposed to baclofen for  20  s
Fig 2c. These results from both chick DRG  neurons  and  rat  hippocampal
neurons indicate that pp60-Src associates with  Cav2.2  channels,  and  this
interaction appears to exist even in the absence of receptor activation.

 The functional consequence of pp60-Src associated with the  Cav2.2  channel
was further studied in chick DRG neurons. G protein-mediated  activation  of
pp60-Src can result in downstream activation of  the  monomeric  GTPase  Ras
10, 11. Ras was  detected  in  the  channel  immunoprecipitates  from  DRG
neurons treated with GABA or the GABAB receptor agonist baclofen  for  20  s
Fig 3, a and b,  and  Supplemental  Material  Fig  3a.  The  association
decreased by 60 s and was not observed in channel immunoprecipitates at 0  s
Fig 2a. Recruitment of Ras to the Ca2+ channel complex was also  observed
in dissociated rat hippocampal cultures Supplemental Material Fig 3b.

An inhibitor of tyrosine kinases, genistein, decreased  the  amount  of  Ras
that co-precipitated with the Ca2+ channel complex from  GABA-treated  chick
DRG neurons Fig 3c. This suggests  that  tyrosine  kinase  activation  is
required for the recruitment of Ras. Pretreatment of DRG  neurons  with  the
Cav2.2 channel blocker ω-conotoxin GVIA 10 μM decreased the amount of  Ras
recruited to the channel complex upon activation of  GABAB  receptors  Fig
3d. These results suggest that both  activation  of  Src  kinase  and  Ca2+
influx are needed for the recruitment of Ras to the channel complex.

 Our previous experiments demonstrated that ω-conotoxin GVIA did not block
Src kinase activation Fig 1. We thus hypothesized that the Ca2+
dependence on Ras engagement could reflect recruitment of a Ras-specific,
Ca2+-dependent guanine nucleotide exchange factor or recruitment of
signaling intermediary bridging pp60-Src and Ras. We identified SOS son of
sevenless as one nucleotide exchange factor potentially involved in the
activation of Ras in this pathway. Immunoblotting calcium channel complexes
with anti-SOS antibody indicated increasing amounts of SOS protein co-
precipitating with the channel in DRG neurons in which GABAB receptors were
activated by baclofen Fig 4a. Whereas RasGRP, another Ras exchange
factor, is expressed in DRG neurons, it does not co-precipitate with the
channel precipitated from neurons treated with baclofen Fig 4a, lower
panel.

We next tested whether a signaling molecule  downstream  from  pp60-Src  but
upstream  from  SOS  could  confer  Ca2+  dependence  to  the   GABA-induced
recruitment of Ras. PYK-2, a member  of  the  non-receptor  tyrosine  kinase
family, was considered a likely  candidate  because  it  is  Ca2+-dependent,
expressed in neurons, and known to activate Ras signaling cascades. To  test
whether PYK-2 is involved in the channel complex,  Cav2.2  channel  was  re-
precipitated anti-PYK-2 antibody. PYK-2 was detected  by  immunoblotting  in
baclofen-treated DRG neuron immunoprecipitates but not  detected  in  control
samples Fig 4b suggesting that PYK-2 is  recruited  upon  GABAB  receptor
activation. Immunoblotting for  activated  TyrP-416  pp60-Src  indicates
that activated Src is associated with PYK-2 in a  baclofen-dependent  manner
Fig 4b. Immunoblotting with anti-phosphotyrosine antibodies and anti-PYK-
2 antibodies suggests that PYK-2 is tyrosine-phosphorylated suggesting  that
this kinase is also active. Blockade of Ca2+ influx through the  channel  by
ω-conotoxin GVIA decreased the amount of  PYK-2  recruited  to  the  channel
complex by GABA, both in DRG  neurons  Fig  4c  and  hippocampal  neurons
Supplemental Material Fig 3c. Because PYK-2 is a  Ca2+-dependent  kinase,
it could act to confer calcium dependence to the recruitment of Ras  to  the
channel complex.

 We tested if Shc, one of the adapter proteins frequently associated with
Ras-mediated signaling, was present in the channel complex. We found that
ShcC, a neuronal isoform of Shc, also co-precipitates with the channel
following GABA treatment Fig 5a. The temporal profile of Ras and ShcC
association with the channel is very similar. If ShcC is upstream from Ras,
the active tyrosine-phosphorylated form of the protein should be present in
the channel complex. We therefore tested whether ShcC was tyrosine-
phosphorylated upon GABAB receptor activation. Tyrosine phosphorylation of
ShcC was detected in samples exposed to GABA for 20 s Fig 5a,lower
panel. Genistein 100 μM decreased the recruitment of ShcC to the complex
Fig 5b.
To determine whether PYK-2, Ras, and/or ShcC play a direct  role  in  GABAB-
mediated  inhibition  of  Cav2.2  channel,  cells  were  microinjected  with
antibodies against PYK-2, Ras, or ShcC. The effects of these  antibodies  on
the  magnitude  and  time  course  of   GABA-mediated,   voltage-independent
inhibition were measured. In cells injected with  the  anti-PYK-2  antibody,
the onset of GABAB-induced inhibition of Cav2.2  current  was  slower  by  a
factor of 2.4 n = 5, whereas anti-Ras  antibody  slowed  the  onset  by  a
factor of 2.5 n = 7 Fig 5c, left  and  middle  panels.  With  anti-ShcC
antibody, the onset of inhibition was slowed down by a factor of 2 n  =  9
Fig 5c,  right  panel.  Other  properties  of  modulation,  such  as  the
magnitude of inhibition and rate of desensitization  of  voltage-independent
inhibition,   remained   unaltered   Supplemental   Material   Fig    3d.
Desensitization was measured as percentage of inhibition as  a  function  of
time following maximal inhibition.  No  change  was  observed  in  the  peak
values of the basal current, 1.3 ± 0.4 nA in control cells n =  15  versus
1.1 ± 0.5 nA for anti-ShcC-injected cells n = 9. From these  observations,
we conclude that PYK-2, ShcC, and Ras participate directly  in  establishing
the rate of onset of GABAB-mediated voltage-independent inhibition.

 To determine whether Ras can  bind  directly  to  the  Cav2.2  channel  and
whether tyrosine phosphorylation can regulate the interaction, we  performed
overlay assays using recombinant proteins of the cytoplasmic regions of  the
Cav2.2 α1 subunit Fig 6a. First,  we  determined  which  regions  of  the
channel are tyrosine-phosphorylated. In vitro  phosphorylation  assays  were
performed by using a recombinant active form of pp60-Src. The first half  of
loop II-III, the synprint  region  to  which  the  SNARE-SNAP  soluble  NSF
attachment protein-synaptosome-associated protein receptors bind 12,  was
tyrosine-phosphorylated  in  vitro.  This  sequence  contains   2   tyrosine
residues. The C terminus sequence, which contains 14 tyrosine residues,  was
also phosphorylated Fig 6b. Overlay  assays  with  constitutively  active
G12V Ras indicated that Ras could bind to loop  II-III  synprint  region,
amino acids 726-984 both in the phosphorylated and non-phosphorylated  form
Fig 6c. Although the C terminus was heavily  phosphorylated,  no  binding
of  Ras  was  detected  to  this  portion  of  the   channel.   Densitometry
quantitation of the overlay blots indicated that 4-fold more  Ras  bound  to
the tyrosine-phosphorylated form  of  the  synprint  region  than  the  non-
phosphorylated form Fig 6d.

In total, these results suggest that GABAB receptor activation promotes  the
recruitment of Ras pathway components to  the  Cav2.2  channel  complex.  We
propose that  activation  of  Src  kinase  takes  place  upstream  from  the
activation and recruitment of PYK-2 to the  channel  complex  as  antibodies
against Src block the magnitude of the response.

 One possibility considered was that the effect of Ras on the rate of onset
of Ca2+ channel modulation was mediated by MAP kinase activation downstream
from Ras. Hence, we tested if activation of MEK was required for GABAB-
induced inhibition of Cav2.2 channel activity. Pharmacological inhibitors
of MEK PD98059 and p38 kinase SB203580 did not alter the magnitude or
time course of GABAB-mediated voltage-independent inhibition of Cav2.2
channels Fig 7, a-c.
We also tested for GABA-mediated activation of MAP kinases in primary DRG
neuron cultures. An increase in the levels of phosphorylated MAP kinase 2
p42 MAP kinase was detected within 20 s of exposure to GABA Fig 6a. No
increase in the levels of phospho-MAP kinase1/2 was detected in neurons
exposed to 100 μM norepinephrine for 20 s Fig 8a, suggesting selectivity
in the actions of GABAB receptors. Pretreatment with genistein or chelation
of extracellular calcium with EGTA blocked the increase in phospho-MAP
kinase induced by GABA Fig 8b. These results suggest that Ca2+ plays a
role in the GABA-induced increase of phospho-MAP kinase 2. When the
exposure to GABA was under depolarizing conditions 60 mM K+, an increase
in the levels of both phospho-MAP kinase 1 p44 MAP kinase and phospho-MAP
kinase 2 was observed Fig 8c. Potassium-induced depolarization by itself
did not cause a change in the levels of phosphorylated p42/44 MAP kinase
Fig 8, c and d. Selective blockade of Cav2.2 current with ω-conotoxin
GVIA prevented the increase in phospho-MAP kinase induced by GABA Fig 8,
c, 6th lane, and d. ω-Conotoxin GVIA 4 nM blocked the phosphorylation of
phospho-MAP kinase 2, which was induced by GABA even in the absence of
depolarization by high K+ Fig 8c. Genistein decreased the amount of
phospho-MAP kinase detected in DRG neurons treated with 100 μM baclofen,
suggesting that tyrosine kinase activation is needed for the GABAB-mediated
increase in MAP kinase activity Supplemental Material Fig 3. Whereas MEK
and MAP kinase inhibitors do not alter GABAB-mediated inhibition, results
from biochemical experiments suggest that activation of GABAB receptors in
turn activates the MAP kinase pathway.


Our results suggest that Cav2.2 channel acts as a dynamic scaffold by
interacting with signaling proteins in a transmitter- and time-dependent
manner. Src kinase is pre-associated with the channel, and its activation
is regulated by GABAB receptor signaling; this type of interaction alters
Ca2+ channel activity Fig 9. Src seems to act as an “on” and “off”
switch as its inhibition results in changes in the magnitude of inhibition
of Ca2+ current. The pre-association of tyrosine kinases and tyrosine
phosphatases2 with calcium channels may facilitate the rapid onset and
termination of the responses observed in ion channel modulation. Several
ion channels, including potassium and Ca2+ channels, are modulated by
tyrosine kinases. For example, L-type voltage-dependent Ca2+ channels are
modulated by insulin-like growth factor 13 and tyrosine kinase inhibitors
14. It has been shown that P/Q Ca2+ channels are modulated by Src kinase
15. In the case of the N-type Ca2+channel, Src-mediated modulation is
voltage-independent and differs from the Gβγ-mediated voltage-dependent
inhibition 1. However, both G protein α and βγ subunits are known to
activate Src kinase 15, and it is possible that GABAB-mediated signals
involve both downstream components.
In contrast to the pre-association of tyrosine kinases such as  pp60-Src,  a
second type of interaction elucidated here involves dynamic  recruitment  to
the Ca2+ channel of components of the Ras signaling pathway Fig 9;  these
interactions are rapidly induced by receptor activation. These  interactions
do not regulate the magnitude of the inhibitory response but  determine  the
rate of onset. Changes in the kinetics of the onset of  transmitter-mediated
inhibition of the Cav2.2 channel can result in alterations in the levels  of
Ca2+  influx  into  the  cell  and  its  temporal  resolution.  It  is  well
established that Ca2+-dependent processes are  sensitive  not  only  to  the
total amount of Ca2+ but also to its tempo-spatial distribution,  and  hence
such rate effects might have substantial cellular effects 16.

Complexes between ion channels and signaling  proteins  are  emerging  as  a
general feature of neuronal cells. Complexes  of  signaling  molecules  with
the NMDA receptor have been identified  in  the  postsynaptic  density  17,
18. Recently, it has also been reported that the L-type Ca2+ channel  forms
a complex with the β-adrenergic receptor and adenylyl cyclase 19. Here  we
provide evidence for both steady-state and dynamic Cav2.2 channel  complexes
with signaling molecules resulting in the  modulation  of  specific  channel
functions. In addition, our data suggest that  such  complexes  may  play  a
role in the activation of MAP kinase by GABAB  receptors.  Many  G  protein-
coupled  receptors  activate  the  Ras-MAP  kinase  pathway.   MAP   kinases
represent a major locus of signal integration.  Our  studies  indicate  that
Ca2+ influx through the Cav2.2 channel in addition  to  receptor  activation
is important for the GABAB-mediated activation of  downstream  effectors  of
the Ras pathway  such  as  MAP  kinase  and  presumably  in  transcriptional
regulation. In  cortical  neurons,  Ca2+  influx  through  the  L-type  Ca2+
channel produces the binding of calmodulin to the C terminus of the  channel
and results in an increase of phospho-cAMP-response element-binding  protein
and transcription 20. Ca2+ influx through the NMDA receptor is also  known
to produce an increase in  transcription  in  the  hippocampus.  This  NMDA-
mediated signal is also known to activate MAP kinase activity.

Thus, a number of channels are closely linked to transcriptional  regulation
through the MAP kinase pathway.

The activation of MAP kinase could represent a  bridge  between  short  term
and  long  term  changes  in  synaptic  plasticity.  Studies  of  long  term
potentiation in the hippocampus have shown the  importance  of  Ca2+  influx
for sustained MAP kinase activation 21. The prevailing view has been  that
Ca2+ influx through an L-type Ca2+ channel links membrane depolarization  to
Ca2+-dependent gene expression, whereas other  high  voltage-activated  Ca2+
channels N, P/Q, and R are involved only in exocytosis.  A  recent  report
22 has shown that in  rat  sensory  neurons  Ca2+  influx  through  Cav2.2
channels can  link  electrical  stimulation  to  changes  in  expression  of
tyrosine hydroxylase. The  effects  of  receptor-mediated  events  were  not
determined in these studies. Taken together these data suggest  that  N-type
Ca2+ channels may also regulate gene expression.

In conclusion, our studies show that interaction between N-type Ca2+
channel and signaling molecules can have both long and short term
consequences. Acutely, the timing of the channel-signaling molecule
interactions can determine the time course of Ca2+ channel modulation. Over
the long term, such interactions could also affect neuronal function by
regulating gene expression. Thus, the integration of signals by channel-
dependent scaffolding could offer the framework to produce a variety of
presynaptic responses that underlie the long and short term changes in
synaptic function in the nervous system. Given that synaptic efficacy is in
part determined by Ca2+ influx at the presynaptic sites, identification of
the properties of systems that determine the timing and magnitude of Ca2+
influx is critical for the understanding of synaptic physiology.
