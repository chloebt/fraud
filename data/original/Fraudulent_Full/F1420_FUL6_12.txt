
Factor  XI   FXI1 is   a   160,000-Dalton   disulfide-linked   homodimeric
coagulation protein that exists in plasma in a complex with  high  molecular
weight  kininogen  HK  1–7.  In  the  presence  of  HK  and  Zn2+   or
prothrombin and Ca2+, FXI can bind specifically  and  reversibly  to  high
affinity sites on the surface  of  stimulated  platelets  8, 9.  Activated
platelets can thereby promote FXI activation by thrombin  8, 10.  We  have
previously demonstrated that the Apple 3 A3 domain  of  FXI  mediates  the
binding of FXI to platelets 11, 12. FXI binds to the glycoprotein  Ib-IX-V
GPIb-IX-V  complex  since  1  Bernard-Soulier  platelets  lack  the  GPIb
complex and are deficient in binding FXI; 2 a monoclonal  antibody  against
GPIbα SZ-2 inhibits the binding of FXI to the platelet surface; 3  bovine
von Willebrand protein, which binds GPIbα, also inhibits the binding of  FXI
to activated platelets; and  4  FXI  binds  to  glycocalicin  the  soluble
extracellular region of GPIbα in the presence of  Zn2+ ions  13.  It  has
recently  been  determined  by  analyzing  fractions  from  sucrose  density
gradients of Triton X-100 platelet lysates that a  significant  fraction  of
the  GPIb-IX-V  complex  is  localized  within  membrane  rafts  in  resting
platelets and that this fraction  was  increased  upon  platelet  activation
14. The precise physiological role of lipid  rafts  is  unclear,  although
they have been postulated to be involved in localizing membrane ligands  and
in cellular signaling.
Lipid  rafts  also  called  detergent-insoluble/resistant   membranes   and
glycolipid-enriched membranes are microdomains of the  cell  membrane  that
contain saturated phospholipids, glycosphingolipid-containing assemblies  of
cholesterol that are highly ordered in array when compared with the rest  of
the cell membrane 15, 16. It is  well  documented  that  lipid  rafts  are
enriched in proteins such as the Src family kinases  17,  the  active  Lyn
protein tyrosine kinase 18,  and  the  glycosylphosphatidylinositol-linked
outer membrane protein 19. These microdomains can be  isolated  from  many
hematopoietic cells and  are  thought  to  be  involved  in  cell  signaling
15, 20, 21. It has been  suggested  that  clustering  of  raft  components
results in a coalescence of lipid rafts, thus connecting raft  proteins  and
accessory molecules into a larger domain 22. However, the  role  of  lipid
rafts in receptor signaling in non-immune cells such  as  platelets  is  not
known.
Since a significant component  of  the  GPIb  complex  is  localized  within
membrane rafts 14 and FXI binds to the GPIb complex 13,  we  have  asked
whether FXI binds to membrane rafts in activated and resting platelets.  Our
studies show that platelet-bound FXI is associated with membrane  rafts  and
that FXI  is  bound  to  the  GPIb-IX-V  complex  in  membrane  rafts.  This
interaction  may  have  a  physiological  role  as  a   platform   for   the
colocalization and activation of FXI by thrombin on the platelet surface.



 All reagents were obtained from Sigma unless stated otherwise. Human FXI,
human prothrombin, human FXIa, and human HK were purchased from Hematologic
Technologies Inc Essex Junction, VT. Human α-thrombin 2,800 NIH
units/mg was purchased from Enzyme Research Laboratories South Bend, IN.
Methyl silicon oil 1 DC-200 and Hi phenyl silicon oil 125 DC-550 were
purchased from William F. Nye Inc Fairhaven, MA. Carrier-free Na125I was
from Amersham Biosciences. The chromogenic substrate for measurement of
FXIa activity S2366 was obtained from Chromogenix Mölndal, Sweden. The
thrombin receptor agonist peptide TRAP, SFLLRN-amide, was synthesized at
the Protein Chemistry Facility of the University of Pennsylvania on the
Applied Biosystems 430A Synthesizer, and reverse-phase high performance
liquid chromatography was used to purify it to >99 homogeneity. A
monoclonal antibody SZ-2, which recognizes the NH2-terminal extracellular
globular domain of GPIbα, blocks thrombin binding to platelets at low
concentration, and inhibits thrombin-induced platelet aggregation, was used
in experiments examining FXI binding to platelets. An isotype-specific
mouse IgG2A κ chain control antibody was purchased from Sigma.
Purified FXI was radiolabeled with 125I by a minor modification 8 of the
Iodogen method to a specific activity of 5 × 106 cpm/μg. The radiolabeled
FXI retained >98 of its biological activity.
 Platelets were prepared from normal donors as described in Refs 9, 11,
and 12. Platelet-rich plasma obtained from citrated human blood was
centrifuged, and the platelets were resuspended in calcium-free Hepes-
Tyrode's buffer 126 mM NaCl, 2.7 mM KCl, 1 mM MgCl2, 0.38 mMNaH2PO4, 5.6
mM dextrose, 6.2 mM sodium Hepes, 8.8 mM Hepes free acid, 0.1 bovine serum
albumin, pH 6.5, and gel-filtered on a column of Sepharose 2B equilibrated
in calcium-free Hepes-Tyrode's buffer, pH 7.2. Platelets were counted
electronically Coulter Electronics, Hialeah, FL.
 Platelets were prewarmed to 37 °C and incubated at a concentration of 1 ×
108/ml in calcium-free Hepes-Tyrode's buffer, pH 7.3, in a 1.5-ml Eppendorf
plastic centrifuge tube with a mixture of radiolabeled FXI, divalent
cations, a thrombin receptor PAR-1 activation peptide SFLLRN-amide as a
platelet agonist 8, 11, and HK or antibodies or other proteins as
designated in the figure legends. All incubations were performed at 37 °C
without stirring after an initial mixing of the reaction mixture. At
various added FXI concentrations, aliquots were removed 100 μl and
centrifuged through a mixture of silicone oils as described in Refs 8–11.
 Gel-filtered platelets 1–2 × 108/ml, containing125I-FXI and cofactors
were washed and pelleted in 80 sucrose in MES-buffered saline MBS: 25
mM MES, pH 6.5, 150 mM NaCl. The washed and pelleted platelets were lysed
with 2 ml of ice-cold Triton X-100 containing 10 mM benzamidine, 1
mM phenylmethylsulfonyl fluoride, 20 μg/ml aprotinin, 20 μg/ml leupeptin,
100 μg/ml soybean trypsin inhibitor. All subsequent steps were performed at
4 °C. The lysate was adjusted to 40 w/v sucrose by addition of an equal
volume of 80 w/v sucrose in MBS. Four milliliters of 30 sucrose
followed by 2 ml of 5 sucrose were gently layered over the 40 sucrose
fraction in the ultracentrifugation tube. The samples were then centrifuged
at 200,000 × g at 4 °C for 18 h in a SW40 Rotor Beckman Instruments.
Eleven equal fractions were collected from the top of the gradient. Protein
concentrations were determined using the Micro BCA Protein Assay Pierce
according to the manufacturer's instructions. Dot blots were performed
using a Bio-Dot Microfiltration Apparatus Bio-Rad according to the
manufacturer's instructions and probed for the ganglioside GM1 with
horseradish peroxidase-conjugated cholera toxin B-subunit 10 μg/ml.
 To specifically deplete the platelet membrane of cholesterol, platelet-
rich plasma was incubated with methyl-β-cyclodextrin MβCD at a final
concentration of 20 mM for 30 min at 37 °C. Cells were repleted with
cholesterol by incubating them in the presence of a cholesterol/MβCD
mixture for 1 h at 37 °C. A stock solution of 0.4 mg/ml cholesterol and 10
cyclodextrin was prepared by vortexing at ∼40 °C in 10 ml of 10 MβCD with
200 μl of cholesterol 20 mg/ml in ethanol solution.
Activation of FXI 60 nM by thrombin 1.25 nM was measured by chromogenic
assay as described previously 8, 10. Briefly, incubations were carried
out at 37 °C in 200 μl of Tris-buffered saline 50 mM Tris, 150 mM NaCl, pH
7.3 and 1 bovine serum albumin. Gel-filtered platelets were activated by
incubation at 37 °C for 1 min with thrombin receptor activation peptide
SFLLRN-amide, 25 μM. After dilution to a final volume of 1 ml with Tris-
buffered saline with 1 bovine serum albumin containing 600 μM S2366 EPR-
para-nitroanilide, Chromogenix, the amount of free para-nitroanilide was
determined by measuring the changes in absorbance at 405 nm A405. The
amount of FXIa generated was assayed by reference to a standard curve
constructed using purified FXIa.





 Platelet rafts have been isolated previously 18, 23 at low temperatures.
They are insoluble in non-ionic detergents and are isolated by continuous
gradient centrifugation based on their low density buoyant characteristics.
TRAP-activated platelets were lysed in an ice-cold solution of Triton X-100
at various concentrations, 0.025–1, and the lysate was centrifuged in a
sucrose gradient for 18 h at 4 °C. The results shown in Fig 1 were those
obtained when TRAP-activated platelets were lysed in 0.25 Triton X-100
since this concentration of detergent resulted in maximal raft association
of platelet-bound FXI as shown in Fig 2 and discussed below. Eleven
fractions 900 μl each were collected from the top of the gradient. In
agreement with previous studies on platelet rafts 18, 23, the low density
raft-containing fractions 1–4 contained less than 5 of total cellular
protein with the higher density fractions 7–11 containing the majority of
the protein Fig 1A. The raft marker ganglioside GM1 determined the
position of lipid rafts within the sucrose gradient. This was identified by
ligand blotting with cholera toxin B-subunit, which specifically binds this
lipid 24. Dot blot analysis revealed that lipid rafts localize to
fractions 1–4 within the gradient. Dot blot staining was evident also in
fractions 9, 10, and 11 of the sucrose gradient. This might represent lipid
rafts that are solubilized by detergent or unable to float as a result of
linkage to the cytoskeleton. In all subsequent experiments below, GM1
localization was determined to be unaffected by platelet activation, by the
concentration of Triton X-100 utilized, by protein HK or prothrombin or
metal ion Zn2+ or Ca2+ cofactors for FXI binding, or by the presence of
antibody SZ-2 in the recombinant A3 domain data not shown.
 Since a significant proportion of the GPIb-IX-V  complex  is  contained  in
membrane rafts and FXI binds to GPIb, we designed experiments  to  determine
whether labeled FXI is also incorporated into membrane rafts. Since  it  has
been reported 25, 26 that certain ligand-membrane  raft  associations  may
be disrupted by concentrations of Triton X-100 >0.5, we initially aimed  to
optimize the detergent concentration required for optimal FXI-membrane  raft
association. We therefore examined the distribution of 125I-FXI  in  sucrose
density fractions of lysates using Triton X-100 concentration of  0.025–1
from TRAP-activated platelets in the presence of  cofactors  that  optimize
FXI binding to platelets. Fig 2 indicates that  at  higher  concentrations
of Triton X-100 0.5–1 little  or  no  FXI  is  associated  with  platelet
membrane rafts, while at concentrations of Triton  X-100  ≤0.25,  ∼5–8  of
the bound FXI is associated  with  the  membrane  raft  fractions  1–4.  The
majority of bound FXI ∼87–90 is localized within fractions  6–11  of  the
sucrose gradient, and ∼5 of bound  FXI  was  recovered  in  the  pellet  of
Triton X-100  lysates.  These  data  indicate  that  platelet-bound  FXI  is
associated with the membrane raft fraction, and its association  with  rafts
is dependent upon the concentration of Triton X-100 used to  solubilize  the
platelets.
 We have previously determined the optimal  cofactor  requirements  for  FXI
binding to the platelet surface. It was shown that HK, Ca2+,  and  Zn2+ ions
or prothrombin and Ca2+ ions are required as cofactors for  optimal  binding
of FXI to the activated platelet  surface  8, 27. Fig  3, A and B,  shows
the distribution of 125I-FXI in sucrose density fractions  of  lysates  from
both stimulated and unactivated platelets, respectively, using  the  various
cofactors necessary to bind FXI to the platelet surface. The data show  that
the presence of prothrombin 1.2  μM  and  CaCl2 2  mM  or  HK  42  nM,
ZnCl2 25 μM, and CaCl2 2 mM are required not only for binding of FXI  to
the activated platelets but also for FXI association with the membrane  raft
fraction. The amount of FXI associated with the  raft  fraction  5–8  was
the same for both cofactors. Platelets that were not activated did not  bind
FXI, and thus FXI was not associated with membrane rafts.  A  lesser  amount
of FXI was bound to membrane rafts 3–4 of total binding in  the  presence
of ZnCl2 25 μM without added HK, but no FXI binding was  observed  in  the
presence of CaCl2 2  mM  without  added  prothrombin.  Thus,  the  optimal
conditions for binding FXI to the activated platelets are also  optimal  for
FXI association with membrane rafts.
 Membrane cholesterol depletion, using the cholesterol-binding agent MβCD,
is a well established method for disrupting lipid rafts 21. It has been
demonstrated in platelets that MβCD effectively removes cholesterol from
platelets as a function of time 14. MβCD did not induce microparticle
formation nor did it alter platelet count. In platelets treated with MβCD
for 30 min, FXI was completely removed from the lipid raft fractions, and
this removal was reversed by cholesterol repletion Fig 4. GM1
localization was also monitored by ligand blotting with the cholera toxin B-
subunit and demonstrated complete elimination of raft formation by MβCD
treatment ie only fractions 8–11 were positive for GM1, whereas
treatment with MβCD in the presence of cholesterol had no effect on GM1
localization data not shown. To exclude the possibility that the loss of
FXI binding to lipid rafts was due to a loss of receptors for FXI on the
platelet surface, we assessed whether MβCD affected total binding of FXI to
the activated platelet.Fig 5 demonstrates that even after treatment of
platelets with concentrations of MβCD as high as 40 mM there was no effect
on the amount of FXI bound to the activated platelet.
 FXI binds activated platelets specifically and reversibly, and this
interaction is mediated by amino acids within the A3 domain of FXI
11, 12, 27. Fig 6A demonstrates that the recombinant A3 domain abolishes
the binding of FXI to membrane rafts as well as the non-raft fractions in
the sucrose gradient. In a study demonstrating that the platelet receptor
for FXI is the GPIb-IX-V complex, an antibody SZ-2 that recognizes the
NH2-terminal extracellular globular domain was shown to block FXI binding
to activated platelets 13. Fig 6B demonstrates that the antibody SZ-2
also abolishes binding of FXI to lipid rafts as well as non-raft fractions
in the sucrose gradient. These experiments are consistent with the
conclusion that FXI binds through its A3 domain to GPIbα and that the FXI-
GPIb-IX-V complex is compartmentalized within membrane raft structures.
 Activated gel-filtered platelets as well as glycocalicin promote FXI
activation by thrombin in the presence of HK or prothrombin at optimal
rates, thereby initiating intrinsic coagulation independent of contact
proteins 13. Initial rates of FXI activation by thrombin were
significantly decreased compared with normal platelets when activated
Bernard-Soulier platelets deficient in GPIb complex were used as a
surface 13. These observations suggest that the GPIb-IX-V complex serves
as a receptor for facilitating thrombin-catalyzed FXI activation. To
investigate the physiological relevance of FXI-GPIb-IX-V complex-raft
association, we disrupted the structural integrity of lipid rafts by
cholesterol depletion with MβCD. Initial rates of FXI activation by
thrombin on TRAP-activated platelets were inhibited >85 from ∼17 to 2.4
nM/min by cholesterol depletion Fig 7. These experiments suggest that
the localization of FXI to lipid rafts containing the GPIb complex is
important for the activation of FXI by thrombin on the platelet surface.



Utilizing a sequence  of  amino  acids  Ser248–Val271  in  the  A3  domain
11, 12, 27, FXI binds reversibly and specifically to high  affinity  sites
on the activated  platelets  in  the  presence  of  HK  and  Zn2+ ions  or
prothrombin and Ca2+ ions 8, 9 as a consequence of  which  the  rate  of
FXI  activation  by  thrombin  is  accelerated  >5,000-fold   8, 10.   FXI
interacts with the GPIb-IX-V complex  on  the  platelet  surface,  and  this
interaction promotes thrombin-catalyzed FXI activation 13.  The  GPIb-IX-V
complex is a large  plasma  membrane  complex  comprising  four  polypeptide
chains, GPIbα, GPIbβ, GPIX, and GPV, arranged in  stoichiometry  of  2:2:2:1
28.  Approximately  25,000  copies  of  the  first  three   peptides   are
constitutively expressed on the  unactivated  platelet  surface  along  with
half as many copies of GPV 29–31. The complex also binds thrombin and  von
Willebrand factor with high  affinity  32.  However,  platelet  activation
results in a ∼65 decrease of GPIb from the  platelet  surface  33  and  a
redistribution to the surface canalicular system 34. Therefore, how do  we
account for the fact that platelet activation is required  for  the  binding
of FXI to  platelet  GPIb-IX-V  complex  and  its  activation  by  thrombin?
Recently it has been reported that specialized membrane microdomains  lipid
rafts are involved in GPIb-IX-V-mediated signaling 14.  Lipid  rafts  are
dynamic assemblies of cholesterol and sphingolipids that  are  more  ordered
in structure than the rest of the membrane 15, 21. They  are  believed  to
act as “platforms”  for  signal  transduction  and  ligand  localization  by
selectively attracting certain  proteins  while  excluding  others.  It  was
found that a significant fraction of the  GPIb-IX-V  complex  exists  within
rafts in resting platelets 8, which increased to 26 upon von  Willebrand
factor-induced activation of platelets 14. Since FXI binds to 1,500  sites
on the platelet surface, ie to only a  small  fraction  of  the  GPIb-IX-V
molecules,  it  is  possible  that  lipid  rafts  may  compartmentalize  and
colocalize FXI with thrombin  in  the  GPIb-X-V  complex  on  the  activated
platelet surface.
The present studies support the conclusion that FXI can interact with  lipid
rafts on the platelet surface and that this interaction is mediated  through
the GPIb-IX-V complex. The structural integrity of lipid rafts is  important
for promoting optimal rates of FXI activation by thrombin  on  the  platelet
surface. The evidence that supports this conclusion is as follows. 1  ∼5–8
of bound FXI is associated with membrane rafts in  TRAP-activated  platelets
Figs 2 and 3, A and B;  2  FXI-raft   association   was   disrupted   by
cholesterol depletion Fig 4; 3 FXI binds to lipid rafts through  the  A3
domain Fig 6A; 4 the SZ-2 antibody blocks the binding of  FXI  to  lipid
rafts suggesting FXI interacts with GPIb in lipid rafts Fig  6B;  and  5
the initial rate of FXI activation by thrombin on  activated  platelets  was
inhibited >85 by cholesterol depletion leading  to  raft  disruption  Fig
7.
There have  been  contradictory  results  using  detergent  insolubility  to
determine whether various signaling proteins are  associated  with  membrane
rafts 35, 36.  This  was  probably  because  proteins  that  are  strongly
associated with membrane rafts are Triton  X-100-insoluble,  whereas  weakly
associated proteins are sensitive to detergent extraction. An example  of  a
ligand weakly associated with rafts is GPVI as evidenced by  the  fact  that
its recovery with the low  density  fraction  is  only  achieved  using  low
concentrations of Triton X-100  25.  Therefore,  we  first  optimized  the
experimental conditions for FXI association with  platelet  membrane  rafts.
At high concentrations of Triton X-100  0.5–1,  FXI  was  not  associated
with membrane rafts, whereas at concentrations ≤0.25, ∼5–8  of  bound  FXI
is associated with membrane rafts. Since the detergents  Brij58  and  Brij46
gave the same  pattern  of  distribution  of  the  FXI  within  the  sucrose
gradient, apparently the presence of FXI in the lipid  fraction  is  not  an
artifact of Triton X-100 solubilization data not shown.
Three  different  conditions  were  utilized  in  the   present   study   to
characterize FXI interactions with membrane rafts on platelets: ie in  the
presence of HK and Zn2+ ions, prothrombin and Ca2+ ions, or Zn2+ alone.  The
interaction of FXI with activated platelets is optimal in  the  presence  of
either prothrombin and Ca2+ or HK and Zn2+ with  1,500  sites  per  platelet
and Kdapp ∼ 10–15 nM 8, 9. However, in the presence of  Zn2+ ions  alone
25 μM, FXI interacts with half the number of platelet  sites  ∼800  sites
per platelet and Kdapp ∼ 12 nM 13. We determined that ∼8 of FXI  bound
to activated platelets appears in lipid rafts in  the  presence  of  HK  and
Zn2+ ions or prothrombin and Ca2+ ions Fig  3, A and B  and  ∼4  of  FXI
bound to activated platelets in the presence of Zn2+ alone appears in  lipid
rafts. Thus FXI binding to membrane rafts  appears  to  occur  even  in  the
absence of  added  protein  cofactors  provided  Zn2+ ions  are  present.  A
possible mechanism to account for this binding in the absence  of  added  HK
or prothrombin is  the  presence  of  HK  in  platelet  α-granules  and  its
secretion and surface membrane binding after platelet activation 37.
Several reports have implicated platelet membrane lipid rafts  in  signaling
events mediated by von Willebrand factor binding to GPIb-IX-V and  convulxin
binding to  GPVI  14, 18, 23, 25.  We  examined  both  TRAP-activated  and
resting platelets and determined  that  association  of  FXI  with  membrane
rafts is activation-dependent. Although the  GPIb-IX-V  complex  is  present
∼8 of total in lipid rafts of resting platelets, FXI is not able to  bind
to resting  platelets  or  to  associate  with  the  raft  membrane  of  the
unstimulated platelet. The mechanisms  accounting  for  activation-dependent
FXI binding to platelets and its association with GPIb-IX-V in  lipid  rafts
are currently unclear. It is possible that the conformation of GPIb-IX-V  is
altered when platelets are activated, allowing it to bind FXI and  associate
with lipid microdomains. Alternatively the character of the lipid  raft  may
change with platelet activation eg lipid  distribution  across  different
membrane  leaflets  may  be  drastically  different   when   platelets   are
activated. Also it is possible that an unknown cofactor eg platelet  HK
is exposed  or  secreted  upon  platelet  activation  that  permits  optimal
binding of FXI to GPIb-IX-V and/or  the  association  of  the  FXI-GPIb-IX-V
complex with lipid rafts.
To corroborate the finding that FXI is recruited to  the  GPIb-IX-V  complex
within  lipid  raft  fractions  upon  TRAP  activation,  we  determined  FXI
distribution by sucrose density centrifugation  after  incubating  platelets
with the SZ-2 antibody directed against the  NH2-terminal  globular  domain
of GPIbα or the recombinant A3 domain, which contains the platelet  binding
site for FXI. We previously determined that the  SZ-2  antibody  blocks  the
binding of FXI  to  activated  platelets  13  and  that  the  radiolabeled
recombinant A3 domain binds to activated  platelets  with  the  same Kd ∼10
nM and number of sites ∼1,500 as FXI, suggesting it is the  only  binding
site on FXI for the platelet surface 11. Both of these probes blocked  the
binding of FXI to  membrane  rafts  as  well  as  the  binding  to  non-raft
fractions, suggesting that FXI can also interact with GPIb-IX-V in the  non-
raft fractions. Thus, FXI binds  the  GPIb-IX-V  complex  both  in  membrane
rafts and in non-raft membranes, and the A3  domain  of  FXI  mediates  this
interaction.  Whether  some  biochemical  modification  of  FXI  may   occur
iepalmitoylation to  direct  it  to  the  lipid  rafts  is  unknown  but
possible since enzymatic acylation and deacylation of the GPIb-IX-V  complex
may regulate its association with rafts 14.
To investigate the physiological significance of  the  partitioning  of  FXI
into lipid rafts, we  selectively  depleted  platelet  membrane  cholesterol
using the cholesterol-binding agent MβCD. It has  been  determined  that  10
mMMβCD  efficiently   removes   cholesterol   from   3Hcholesterol-labeled
platelets as a function  of  time  with  ∼75  of  incorporated  cholesterol
released  in  30  min  14.  We  therefore  utilized  20  mM MβCD  in   our
experiments.  Cell  viability  was  not  affected  by  this  treatment.   In
platelets treated with MβCD for 30 min, FXI was completely removed from  the
lipid  raft  fractions,  and  replenishment  of  cholesterol   to   depleted
platelets restored the binding of FXI to the lipid raft fractions Fig  4.
To exclude the possibility that the  loss  of  FXI  interaction  with  rafts
following  cholesterol  depletion  was  due  to  a   failure   to   activate
cholesterol-depleted platelets, we assessed receptor levels in  FXI  binding
studies Fig 5. FXI binding to the activated platelets remained  unchanged
following a 30-min  treatment  of  up  to  40  mM MβCD  when  compared  with
untreated platelets. Thus, it is important  to  emphasize  that  cholesterol
depletion does not prevent the exposure of FXI  receptors  by  TRAP-mediated
PAR-1 activation but does prevent the association of  these  receptors  with
membrane rafts.
We also carried out experiments to determine whether the activation  of  FXI
by thrombin may occur within platelet  membrane  microdomains.  Cholesterol-
depleted, MβCD-treated platelets were activated with TRAP and  examined  for
their capacity to support FXI activation by thrombin. Initial rates  of  FXI
activation by thrombin were inhibited >85 by  cholesterol  depletion  Fig
7. That this inhibition by MβCD of the capacity of activated  platelets  to
facilitate FXI activation was not a consequence of  inhibition  of  platelet
activation is evidenced by the fact that MβCD-treated platelets  respond  to
TRAP normally by exposing FXI binding sites and by aggregation responses  to
TRAP data not shown. Thus, FXI  association  with  the  GPIb-IX-V  complex
within membrane microdomains is important for the  initiation  of  intrinsic
coagulation independent of contact proteins.
The stoichiometry of interaction between GPIb-IX-V and FXI  within  membrane
lipid rafts remains to be determined. Thus, of the ∼25,000 copies  of  GPIb-
IX-V per platelet, 8 ∼2,000  copies  per  platelet  is  partitioned  into
lipid rafts in resting platelets, and 26 ∼6,500 copies  per  platelet  is
partitioned into lipid rafts in von  Willebrand  factor-activated  platelets
14. In contrast, of the 1,500–2,000 FXI molecules bound per platelet,  ∼8
∼120–160 molecules  per  platelet  is  partitioned  into  rafts  of  TRAP-
activated platelets.  This  suggests  that  the  GPIb-IX-V  complex  is  not
saturated with FXI, which may  occupy  only  ∼10  of  the  available  raft-
associated GPIb-IX-V complexes. Another ligand known to bind to  and  signal
through  the  GPIb-IX-V  complex  is  thrombin  32.  The  present  studies
strongly suggest the possibility that the FXI-thrombin-GPIb-IX-V complex  is
partitioned within  membrane  lipid  microdomains  for  efficient  thrombin-
catalyzed  FXI  activation.  Consistent  with  this   possibility   is   our
observation that disruption of lipid rafts  by  cholesterol  depletion  with
MβCD does  not  prevent  platelet  activation  leading  to  FXI  binding  to
platelets but does prevent FXI  localization  within  lipid  rafts  and  FXI
activation by thrombin.

