
Garlic has been used for centuries for culinary purposes, and its health
benefits have been known since at least 1500 B.C. when ancient Chinese and
Indians used it as a blood-thinning agent 1. Hippocrates, the father of
modern medicine, used garlic to treat cervical cancer 2. In China, garlic
was shown to reduce the risk of esophageal and stomach cancers
by pic703. Garlic is also effective against breast and prostate
cancers due to the presence of S-allyl mercaptocysteine 4. Documented
scientific investigations on garlic were initiated by Louis Pasteur who
first reported its antibacterial and antifungal properties 5, 6. Albert
Schweitzer used this concept and treated dysentery in Africa with
garlic 7.

Subsequent studies found efficacy of garlic as a cardioprotectant. Numerous
studies documented the hypoglycemic, antiatherogenic and
antiatherosclerotic properties of garlic 8-10. Garlic was also found to
be beneficial against ischemic heart disease 11. A significant number of
clinical trials found garlic to lower total as well as LDL
cholesterol 12, although negative findings also exist 13. A recent
study showed garlic to be useful for lowering high blood pressure 14.

Many of the physiological effects of garlic are attributed to the volatile
sulfur compounds like thiosulfinates, which are also responsible for its
pungent aroma. Many pharmacological properties of garlic are also derived
from the organo-sulfur compounds like allicin and diallyl disulfide 9.
Most recently, the additional cardioprotective ability of garlic was
attributed to S-allylcysteine 15. Raw fresh garlic contains alkenyl
cysteine sulfoxides and γ-glutamyl alkenyl cysteine, which upon
activation is converted into S-allylcysteine deoxyallin due to the
deactivation of the enzyme allinase. Allicin is then formed from S-allyl-l-
cysteine, which is readily broken down in volatile sulfur compounds
including hydrogen sulfide H2S Figure 1. For that reason, when crushed,
raw garlic generates H2S 16. Processed and cooked garlic loses their
abilities to generate H2S, although they still retain their antioxidant
actions, thereby suggesting that raw garlic possesses more health benefits
than processed garlic. The purpose of the present study was to compare the
cardioprotective ability of freshly crushed garlic vis-à-vis processed
garlic. We fed a group of rats raw fresh garlic while another group was
given the processed one. After 30 days, isolated hearts were subjected to
30 min of ischemia followed by 2 h of reperfusion. Cardiac functional
parameters like heart rate, LVDP left ventricular develop pressure,
LVdp/dt first derivative of LVDP, coronary flow and aortic flow were
measured. To check the cardioprotective ability of both types of garlic
preparations, some related protein Bax, Bcl2, Akt, ERK1/2, JNK, PPAR,
Glut4 etc levels were also measured. The results of this study clearly
demonstrated superior cardioprotective effects of raw garlic compared to
its processed counterpart.





Garlic was purchased from a local supermarket. Fresh garlic was crushed and
made a slurry-like preparation with water 60 g of garlic in 400 mL of
water. For preparation of processed garlic, garlic was similarly crushed
and then dried in air for two days to allow all of the H2S to escape.
Again, this crushed and air-dried garlic was made into a slurry-like
preparation with water same concentration.



All animals used in this study received humane care in compliance with the
Animal Welfare Act and other federal statutes and regulations relating to
animals and experiments involving animals and adheres to principles stated
in the Guide for the Care and Use of Laboratory Animals, NRC Publication,
1996 edition. Sprague−Dawley male rats weighing between 250 and 300 g were
used for the experiment. The rats were randomly assigned to one of three
groups: 1 control, 2 fresh garlic, and 3 processed garlic Figure 2.
The rats were fed ad libitum regular rat chow with free access to water.
The fresh garlic/processed garlic treated rats were gavaged with 1 mL of
garlic extract 150 mg/kg body wt/day for 30 days while the control group
of rats were gavaged 1 mL of water for the same period of time. The animals
were gavaged with fresh garlic within 5 to 10 min of preparation while the
processed garlic was gavaged after two days of preparation.


At the end of 30 days, the rats were anesthetized with sodium pentobarbital
80 mg/kg, b.w., i.p., Abbott Laboratories, North Chicago, IL and
anticoagulated with heparin sodium 500 IU/kg b.w., i.v. Elkins-Sinn
Inc, Cherry Hill, NJ injection. After ensuring sufficient depth of
anesthesia, thoracotomy was performed, and the hearts were perfused in the
retrograde Langendorff mode at 37 °C at a constant perfusion pressure of
100 cm of water 10 kPa for a 5 min washout period 17. The perfusion
buffer used in this study consisted of a modified Krebs−Henseleit
bicarbonate buffer KHB in mM: sodium chloride 118, potassium chloride
4.7, calcium chloride 1.7, sodium bicarbonate 25, potassium biphosphate
0.36, magnesium sulfate 1.2, and glucose 10. The Langendorff preparation
was switched to the working mode following the washout period with a left
atrial filling pressure of 17 cm water. At the end of 10 min, after the
attainment of steady state cardiac function, baseline functional parameters
were recorded. Then the hearts were subjected to 30 min of global ischemia
followed by 2 h of reperfusion. The first 10 min of reperfusion was in the
retrograde mode to allow for post-ischemic stabilization and there after,
in the antegrade working mode to allow for assessment of functional
parameters, which were recorded at 30, 60, and 120 min of reperfusion.



Aortic pressure was measured using a Gould P23XL pressure transducer Gould
Instrument Systems Inc, Valley View, OH connected to a side arm of the
aortic cannula, and the signal was amplified using a Gould 6600 series
signal conditioner and monitored on a CORDAT II real-time data acquisition
and analysis system Triton Technologies, San Diego, CA 17. Heart rate
HR, left ventricular developed pressure LVDP defined as the difference
of the maximum systolic and diastolic aortic pressures, and the first
derivative of developed pressure dp/dt were all derived or calculated
from the continuously obtained pressure signal Aortic flow AF was
measured using a calibrated flow-meter Gilmont Instrument Inc,
Barrington, IL, and coronary flow CF was measured by timed collection of
the coronary effluent dripping from the heart.



At the end of reperfusion, a 1 w/v solution of triphenyl tetrazolium
chloride in phosphate buffer was infused into aortic cannula for 20 min at
37 °C 18. The hearts were excised, and the sections 0.8 mm of the heart
were fixed in 2 paraformaldehyde, placed between two coverslips and
digitally imaged using a Microtek ScanMaker 600z. To quantitate the areas
of interest in pixels, NIH image 5.1 a public-domain software package was
used. The infarct size was quantified and expressed in pixels.



Immunohistochemical detection of apoptotic cells was carried out using the
TUNEL method 18 Promega, Madison, WI. The heart tissues were
immediately put in 10 formalin and fixed in an automatic tissue-fixing
machine. The tissues were carefully embedded in the molten paraffin in
metallic blocks, covered with flexible plastic molds and kept under
freezing plates to allow the paraffin to solidify. The metallic containers
were removed, and tissues became embedded in paraffin on the plastic molds.
Prior to analyzing tissues for apoptosis, tissue sections were
deparaffinized with xylene and washed in succession with different
concentrations of ethanol absolute, 95, 85, 70, 50. Then the TUNNEL
staining was performed according to the manufacturer’s instructions. The
fluorescence staining was viewed with a fluorescence microscope AXIOPLAN2
IMAGING Carl Zeiss Microimaging, Inc NY at 520 nm for green
fluorescence of fluorescein and at 620 nm for red fluorescence of propidium
iodide. The number of apoptotic cells was counted throughout the slides and
expressed as a percent of total myocyte population.



Tissues frozen in liquid nitrogen and stored at −80 °C were homogenized
in 1 mL buffer A 25 mM Tris-HCl, pH 8, 25 mM NaCl, 1 mM Na-Orthovanadate,
10 mM NaF, 10 mM Na-Pyrophosphate, 10 nM Okadaic acid, 0.5 mM EDTA, 1 mM
PMSF, and 1× Protease inhibitor cocktail in a Polytron-homogenizer.
Homogenates were centrifuged at 2000 rpm at 4 °C for 10 min, and the
nuclear pellet was resuspended in 500 μL of Buffer A with 0.1 Triton X-100
and 500 mM NaCl. Supernatant from the above centrifugation was further
centrifuged at 10 000 rpm at 4 °C for 20 min, and the resultant supernatant
was used as cytosolic extract. The nuclear pellets were lysed by incubation
for 1 h on ice with intermittent tapping. Homogenates were then centrifuged
at 14 000 rpm at 4 °C for 10 min, and the supernatant was used as nuclear
lysate. Cytosolic, nuclear extracts were aliquoted, snap frozen and stored
at −80 °C until use. Total protein concentration in cytosolic and nuclear
extract was determined using BCA Protein Assay Kit Pierce, Rockford, IL.



Either cytosolic or nuclear proteins were separated in SDS-PAGE and
transferred to nitrocellulose filters. Filters were blocked in 5 nonfat
dry milk and probed with primary antibody for overnight 19. Primary
antibodies such as Nrf2, Akt, Phospho-Akt, Glut-4, p-65, histone, and
glyceraldehyde-6-phosphate dehydrogenase GAPDH were obtained from Santa
Cruz Biotechnology, Santa Cruz, CA, whereas Bax, Bcl-2, FoxO1, Phospho-
FoxO1, Jnk, Phospho-Jnk, ERK, Phospho-ERK were obtained from Cell Signaling
Technology, Beverly, MA. PPARα and PPARδ were obtained from Abcam Inc
Cambridge, MA. All primary antibodies were used at the dilution of 1:1000.
Protein bands were identified with horseradish peroxidase conjugated
secondary antibody 1:2000 dilution and Western blotting Luminol Reagent
Santa Cruz Biotechnology, Santa Cruz, CA. GAPDH and histone were used as
cytosolic and nuclear loading control, respectively. The resulting blots
were digitized, subjected to densitometric scanning using a standard NIH
image program, and normalized against loading control.



The values for myocardial functional parameters, total and infarct volumes
and infarct sizes and cardiomyocyte apoptosis are all expressed as the mean
± standard error of mean SEM. Analysis of variance test followed by
Bonferroni’s correction was first carried out to test for any differences
between the mean values of all groups. If differences between groups were
established, the values of the treated groups were compared with those of
the control group by a modified t test. The results were considered
significant if p < 0.05.






We compared the efficacy of freshly crushed garlic vs processed garlic as a
cardioprotectant. Although rats fed with either freshly crushed garlic or
processed garlic had superior ventricular performance compared to control
during the reperfusion phase Figure 3, the freshly crushed garlic group
displayed significantly greater recovery of aortic flow AF, left
ventricular developed pressure LVDP and the maximum first derivative of
developed pressure LVmax dp/dt compared to that achieved from processed
garlic. The heart rate and coronary flow did not vary between the groups.

Myocardial infarct size determined by TTC staining was about 37 ± 1.2
normalized to area of risk Figure 4A. There was no infarction when the
hearts were perfused with the KHB buffer without subjecting to ischemia and
reperfusion protocol control. While both of the experimental groups
showed lower infarct size compared to ischemia/reperfusion control, the
freshly crushed garlic group displayed smaller infarct size 20 ± 1.7 as
compared to that from processed garlic 23 ± 2.01.
The hearts subjected to ischemia/reperfusion showed about 33 apoptotic
cardio-myocytes compared to the control 5 Figure 4B. Garlic treated
hearts demonstrated substantial reduction in the number of apoptotic cells
and it was more prevalent in freshly crushed garlic group 15 ± 1.75 than
that in processed garlic group 21 ± 1.82.



It is well-known that ischemia/reperfusion causes cellular injury by
generating a death signal 20. Such death signal is produced from
defective MAP kinase/tyrosine kinase signaling and increased proapoptotic
Bax/Bcl-2 ratio 21. We, therefore, determined these pro- and anti-
apoptotic parameters in the hearts with and without garlic treatment. As
shown in Figure 5A, both fresh and processed garlic had the ability to
enhance the phosphorylation of ERK1/2 several-fold, but freshly crushed
garlic had a higher potential than processed garlic. Opposite
phosphorylation patterns for proapoptotic JNK and p38MAPK were obtained
with garlic treatment, wherein fresh garlic was superior over processed
garlic.

Ischemia/reperfusion increases proapoptotic protein Bax and reduces
antiapoptotic protein Bcl-2, as expected Figure 5B. While garlic
increased the Bcl-2/Bax ratio, a greater degree of enhancement was noticed
from the freshly crushed garlic group. Taken together, these results
indicate that although garlic generates an overall survival signal, freshly
crushed garlic has better efficacy than processed garlic in suppressing
proapoptotic factors and boosting the antiapoptotic ones.



Akt promotes cell survival by phosphorylating its downstream target FoxO,
which then exit the nucleus, resulting in the suppression of proapoptotic
genes 22. We thus examined if garlic could activate the Akt-FoxO axis. As
shown in Figure 6, garlic treatment caused extensive phosphorylation of Akt
and FoxO1 even at the baseline level. Further, while processed garlic only
moderately enhanced the phosphorylation of FoxO1, freshly crushed garlic
was more potent and ischemia/reperfusion I/R showed no effect. The
results thus indicate that while both garlic preparations activate Akt,
only the freshly crushed garlic has the ability to stimulate its downstream
target, that is, FoxO1.


Since antioxidants present in garlic have been attributed to its
cardioprotective effects, we compared the effects of freshly crushed and
processed garlic on induction Nrf2 and NFκB, two known mediators of redox
signaling Figure 7. We presumed that both garlic preparations would
contain comparable amounts of antioxidants while H2S would be absent from
the processed garlic. However, to our surprise, only freshly crushed garlic
could activate Nrf2 and the p65 subunit of NFκB. In fact, in processed
garlic group p65 reduced after ischemia/ reperfusion.


Since several reports link garlic with diabetes and obesity, the two well-
known cardiovascular risk factors, we examined the effects of garlic on the
expression of Glut-4 and PPARs, markers for diabetes/obesity. As shown in
Figure 8, at the baseline level, the amount of Glut-4 did not vary between
the groups. However, upon ischemia/reperfusion, significant reduction in
Glut-4 and PPAR were observed. In contrast, at the baseline level, PPARα
and PPAR δ were higher for the garlic group. Further, in the
ischemia/reperfusion group, Glut-4 increased significantly in the case of
the freshly crushed garlic group while it was reduced for the processed
garlic group. PPARδ also followed a similar pattern. In contrast, PPARα
significantly increased for both the freshly crushed garlic and the
processed garlic group.




The results of the present study showed for the first time that freshly
crushed garlic possesses superior and diverse cardioprotective abilities
compared to processed garlic as evidenced by its ability i to produce
greater postischemic ventricular recovery, lower myocardial infarction and
reduced cardiomyocyte apoptosis compared to processed garlic; ii to
generate greater degree of survival signal by boosting antiapoptotic ERK1/2
and Bcl-2/Bax ratio and by suppressing the death signal by decreasing the
phosphorylation proapoptotic JNK and p38MAPK; iii to stimulate Akt-FoxO
survival network signaling; iv to generate redox signaling by activating
Nrf2 and p65 subunit of NFκB; and v to reduce cardiovascular risk factors
associated with diabetes and obesity through the upregulation of GLUT-4,
PPARα and PPARδ. The superiority of freshly crushed garlic over processed
garlic might be due to H2S, which is absent in processed garlic.

The long-held belief that most of the health benefits of garlic are derived
from its antioxidant constituents has recently been challenged by Chuah et
al, providing evidence that S-allylcysteine provides protection from
myocardial infarction via a H2S-dependent pathway 20. Almost
simultaneously, Benavides et al also demonstrated that the vasoactivity of
garlic is derived from H2S 21. Taken together, these two studies thus
provided novel evidence that the cardioprotective function of garlic is in
part through the generation of H2S.

To understand why freshly crushed garlic can produce H2S while processed
garlic is devoid of this key gaseous molecule, it is necessary to know the
chemistry of H2S generation by garlic. Raw garlic contains alkenyl
cysteine sulfoxides and γ-glutamyl alkenyl cysteines, which upon crushing
yields S-allylcysteine deoxyallin due to the activation of the enzyme
allinase. The final product allicin is then formed from S-allyl-l-cysteine,
which is readily broken down into several volatile sulfur compounds
including H2S Figure 1. H2S being volatile then escapes. Processed garlic
contains only the oxidized products of allicin, presumably over 75 sulfur-
containing compounds including S-allyl-cysteine and ajoene methyl allyl
sulfide, with cholesterol lowering properties 22. In the heart,
cystathione-γ-lyase appears to be involved in the endogenous generation of
H2S 23.

As mentioned above, two classes of organosulfur compounds are present in
garlic: gamma glutamylcysteines and cysteine sulfoxide, about 80 of this
later compound being allylcysteine sulfoxide or allin. The enzyme allinase
catalyzes the formation of sulfenic acids, which spontaneously react with
each other to form thiosulfinate including allicin, which possesses
cardioprotective properties 24. Allicin and allicin-derived compounds are
metabolized to allyl methyl sulfide. Gamma gutamylcysteines, on the other
hand, are hydrolyzed to S-aylcysteine and S-1-propenylcysteine. Most of
these organosulfur compounds are present in the processed garlic, and some
of them might contribute to cardioprotection.

H2S has long been considered as a harmful chemical, responsible for diverse
health problems including neurotoxicity 25. Only recently, H2S was shown
to protect ischemic myocardium through preconditioning effect 26. It has
thus been proposed that H2S is the “third” endogenous signaling
gasotransmitter after NO and CO capable of providing
cardioprotection 27. Several recent studies demonstrated the ability of
H2S to preserve myocardial contractility, activate ATP-dependent potassium
channel and exert vasodilation, all in tune with its potential
preconditioning function 28-30.

Except for H2S, most of the bioactive compounds including antioxidants like
tocopherols, vitamin A, vitamin C and riboflavin are preserved in processed
garlic 31. A recent study also showed the presence of cycloalliin, a
stable organosulfur compound, in garlic 32. Thus, it is not surprising
that garlic retains many of its cardioprotective functions even after
processing. Nevertheless, in spite of the presence of numerous chemicals
including antioxidants, differences in the protective abilities between raw
and processed garlic have also been documented. As an example, while raw
garlic is highly effective in reducing serum cholesterol and triglyceride,
boiled garlic has minimal effects 33. According to a recent study, oven-
heating of garlic at 200 °C or immersing it in boiling water for 3 min or
less does not affect its ability to inhibit platelet aggregation, whereas
heating crushed garlic for 6 min completely suppressed IVAA 34.
Increasing concentration of crushed garlic extract has a dose dependent
positive IVAA in the aggregation reaction while uncrushed, microwaved
garlic samples are ineffective. It has also been shown that processed
garlic loses its antiplatelet activities due to the loss of allicin and
thiosulfinates 35. Fresh garlic also has higher antioxidant activities as
compared to processed garlic 36.

The present study was specifically designed to determine cardioprotective
abilities of freshly crushed garlic vs processed garlic using
ischemic/reperfused rat heart model. As mentioned earlier, both the garlic
preparations reduced ischemia reperfusion injury, but freshly crushed
garlic revealed superior cardioprotective ability. For example, compared to
control ischemia/reperfusion, freshly crushed garlic increased the
ventricular function including left ventricular developed pressure and
aortic flow from 66.5 ± 1.8 to 99.3 ± 2.6 and from 9.3 ± 1.1 to 26.8 ± 1.3,
respectively, while the processed garlic enhanced these functions from 66.5
± 1.8 to 88.7 ± 2.1 and from 9.3 ± 1.1 to 17 ± 1.5, respectively. The
differences in the improvement of ventricular function with freshly crushed
garlic and processed garlic were significant p < 0.05. Consistent with
the results of functional recovery, freshly crushed garlic revealed smaller
infract size 20 ± 1.7 compared to processed garlic 23 ± 2.01 and both
of these values were significantly lower than control 37 ± 1.2. In
concert, the number of apoptotic cardiomyocytes was significantly lower in
the freshly crushed garlic group and processed garlic group. In the case of
the control I/R group, the amount of apoptotic cardiomyocytes was 33; the
fresh garlic reduced it to 15 ± 1.75, whereas in the case of the processed
garlic, the amount were higher than fresh garlic but lower than the control
I/R group 21 ± 1.82.

Our results demonstrated that both the garlic preparations increased ERK1/2
phosphorylation and decreased p38MAPK and JNK activations several-fold
compared to control. Similar to the results of cardiac function and infarct
size, freshly crushed garlic consistently showed superior abilities vs
processed garlic to activate ERK1/2 and/or to down-regulate p38MAPK and
JNK. P38MAPK and JNK are known pro-apoptotic kinases while ERK1/2 is a well-
known antiapoptotic factor, which contributes to survival signals 37.
Consistent with these results, freshly crushed garlic reduced the Bax/Bcl-2
ratio more than processed garlic while both the garlic preparations reduced
the Bax/Bcl-2 ratio significantly than control. Our results also showed
that both the garlic preparations could phosphorylate Akt while only
freshly crushed garlic, and not the processed garlic could upregulate
FoxO1. PI-3-kinase-Akt signaling is known to regulate the survival signal
while FoxO1, a downstream target of Akt also regulate cell survival 38.
There are also reports indicating that FoxO1 could trigger activation of
Akt suggesting a feedback regulation of Akt by FoxO1 39. In concert,
freshly crushed garlic, and not the processed garlic, potentiated redox
signaling by activating NFκB and Nrf2. It is well-known that PI3K-Akt
pathway plays a crucial role in regulating Nrf2 mediated redox signaling,
which in turn regulates the survival signaling 40.

Finally, to our surprise, garlic could modulate the activities of Glut-4
and PPARs, which are linked with diabetes and obesity-related cardiac
disorders. To the best of our knowledge, there are only very limited
observations on the role of garlic in obesity. One such report showed that
garlic could reduce body weight via decrease in serum lipid and increase in
UCP-1 and UCP-2 mRNA expressions. Interestingly enough, while PPARα and
PPARδ were higher for both of the garlic preparations, PPARδ was reduced
after I/R only for the processed garlic group whereas it was increased for
freshly prepared garlic. PPARδ was increased for both of the garlic
preparations. GLUT-4 followed the same pattern as PPARδ. The reason for
differential regulation of PPARs and GLUT4 is not clear, and further study
is necessary to unreveal the mechanisms of PPARs and GLUT4 regulation with
garlic.

In summary, the results of the present study clearly demonstrated for the
first time that fresh garlic can provide superior cardioprotection as
compared to processed garlic. Fresh garlic appears to generate a potent
survival signal leading to the activation of antiapoptotic and anti-death
proteins that is presumably due to the presence of H2S. These results are
potentially important, as there is a growing interest among the heart
patients to use natural and complementary medicine. Current evidence
indicates that one in three American adults use some form of alternative
medicine. The results of the present study strongly suggest that using
fresh garlic would provide maximal and added benefits to the cardiovascular
patients.


