
The adipocyte secretes a number of peptides named adipocytokines or simply
adipokines. The most

abundantly secreted adipokine is adiponectin. Until now, two different cell
surface receptors for

adiponectin have been identified 1. Adiponectin is involved in numerous
physiological processes

due to its anti-atherogenic, anti-inflammatory and insulin-sensitizing
properties  1-3. The

adiponectin monomer is subjected to post-translatory modifications, which
are essential for the

formation of multimeric complexes that circulate in one of three forms:
high-molecular-weight

HMW-, middle-molecular-weight MMW-, or low-molecular-weight LMW-
adiponectin  4.

Previous studies suggest that the HMW-subform is the major active subform,
being responsible for

the insulin-sensitizing actions of adiponectin 5, 6.

It is well-known that levels of total adiponectin are down-regulated in
obesity-related

conditions like coronary artery disease or type 2 diabetes 7-10. These
conditions are all associated

with insulin resistance and demonstrate the role of adiponectin as an
endogenous insulin sensitizer

7-9. Studies in type 2 diabetic patients as well as in obese individuals
have revealed that it is

primarily the HMW-subform which is down-regulated, in part caused by the
inhibitory effects of

hyperinsulinemia on adiponectin production  10-12. By now, a negative
association between

weight and total adiponectin has been well-substantiated, whereas data on
changes in the molecular

distribution of adiponectin following weight loss have been inconsistent
13-18.

We hypothesized that the upregulated levels of adiponectin after weight
loss was primarily

due to an increase in the HMW-subform and that the HMW-subform was
correlated to the

improvement in insulin sensitivity observed after a significant weight
reduction 16, 17. In order to

explore this hypothesis in extreme obesity, we measured serum levels of
total adiponectin and its

major subforms in a group of men and women with an average BMI of 60 kg/m

2

before and 6

months after gastric banding GB.




The study was based on paired observations in 23 extremely obese subjects
12 females before and

6.6±0.2 months after insertion of an adjustable gastric band, using a
laparoscopic procedure.

However, weight changes up to 2 years after GB have been included. The
patients were 23-58 years

old with a mean of 43±2 years. The majority of patients suffered from co-
morbidities such as

diabetes mellitus n=6, hypertension n=12, depression n=3, T4-
substituted myxoedema n=2

and dyslipidemia n=1. However, all patients were optimally treated for
their co-morbidities at the

time of inclusion as well as during the study period. During both visits,
patients were hospitalized

and fasting from the night before the blood sample was taken. Collected
blood was clotted,

centrifuged at 5 °C, and stored at -80 °C until analyzes. Integrated values
were based on samples

collected at -10, 0, 30 and 60 min after a standardized meal bread
stimulation and both times it

was the same meal, which was ingested within 10 minutes. The rationale for
performing a meal

tolerance test rather than a glucose tolerance test was that the meal test
better resembles the normal

physiological conditions. Insulin sensitivity HOMA-S and  pic-cell
function HOMA-B were

estimated using the HOMA-index 19. The study was performed in accordance
with the Helsinki

Declaration with amendments, after approval by the local ethics committee,
and all patients gave

their informed consent.



Serum total adiponectin was determined by a validated in-house time-
resolved immunoflourometric

assay TR-IFMA as previously described 20. A high, medium and low
control were included on

each plate, and only assay runs with controls within given values were
accepted. The intra- and

inter-assay coefficient of variation CV for the adiponectin assay was <5
and <10, respectively.

The three major subforms of adiponectin were isolated by  a validated in-
house FPLC method as

previously described 21. The intra-assay CVs of the HMW-, MMW-, and LMW-
measurements

were less than 4, 6 and 3, respectively, and the corresponding inter-assay
CVs were less than 6,

12 and 7, respectively 21. Other measurements

Serum ghrelin was measured by an in-house radioimmunoassay RIA performed
as previously

described 22 The RIA measures acetylated as well as non-acetylated
ghrelin, and therefore, the

obtained levels are equivalent to total ghrelin. Serum leptin was measured
by a novel in-house TRIFMA based on commercial reagents catalogue # DY398
from R & D Systems Abington, UK.

The assay was performed essentially as described for the adiponectin assay,
using europium as

tracer 20. Intra-assay and inter-assay CVs were below 5 and 10,
respectively. Serum glucose

was determined by the glucose-oxidase method, serum insulin by commercial
TR-IFMA from

PerkinElmer LifeSciences, Turku, Finland.

We used SigmaStat version 3.5 Systat Software Inc for the statistical
analyzes. SigmaStat tests all

data for normality using the Kolmogorov-Smirnov test and for equal
variance, and based on these

results, data were compared by either parametric paired or unpaired t-
tests or non-parametric tests

Mann-Whitney’s or Wilcoxon’s rang sum tests. Repetitive measures of body
weight before GB

vs. 6 months, 1 and 2 year after GB were compared by Kruskal-Wallis
repeated measures ANOVA

on ranks followed by Dunnett’s test. Statistical associations between the
measured variables were

assessed by simple, unadjusted linear regression analysis, which was
performed on cross sectional

data values before and after GB, respectively as well as on delta values
calculated as levels after

GB minus levels before GB. During the meal stimulation, integrated values
were calculated as the

sum of the four observations, which were obtained at standardized time
intervals -10, 0, 30 and 60

min. Data are given as mean ± SEM expect for HOMA-values, which due to a
skewed distribution

are given as mean 25 - 75th

 percentiles. P–values < 0.05 were considered statistically significant.


Clinical characteristics and laboratory values of the study population
before and after GB are listed in Table 1. Before GB, the patients were
extremely obese BMI=59.3±1.8 kg/m,

weight=159.2±29.6 kg, and accordingly, the participants had elevated
fasting glucose and insulin

levels. Before GB, males were heavier than females 176.2±8.1 vs. 143.6±7.3
kg; P<0.01, whereas 2

their BMI values were similar 61.9±1.9 vs. 57.0±2.8 kg/m ; P=0.17. No
other gender-specific differences were observed pre-GB.

Six months after GB, the patients had experienced a weight loss of 20.5±2.1
kg P<0.001 or 12.9±3.3 of their preoperative body weight and their
fasting plasma glucose became close to normal, whereas plasma insulin
remained unchanged. However, no significant changes in insulin sensitivity
or _-cell function were observed Table 1. At the time of writing, we had
access to weight data up to 2 years after surgery and as shown GB remained
effective, resulting in a weight loss of 14.5±3.5 after 1 year and
15.7±4.6 after 2 years ANOVA P - value on ranks < 0.02, Figure 1.

Serum total adiponectin increased following GB P<0.05 and this was
primarily due to an increase of the HMW-subform P<0.001 Figure 2. The
relative increase of the HMW-subform averaged 21.5±4.8, whereas that of
total adiponectin was only 8.1±3.0 P<0.001. The LMW-subform showed only
a modest increase in plasma concentration P<0.01, while the MMW-subform
did not change NS Figure 2. The relative distribution of the
adiponectin-subforms changed after GB as the HMW-fraction increased
P<0.001 and the MMW-fraction decreased P<0.001; Table 1. Before GB,
males and females had statistically similar levels of total adiponectin
5.70±0.39 vs. 6.34±0.56 mg/l, P =NS as well as HMW-adiponectin P>0.14;
Figure 3, and the lack of a gender difference persisted after GB: total
adiponectin males vs. females: 6.10±0.46 vs. 6.95±0.70 mg/l, P=NS; HMW-
adiponectin: P =0.18; Figure 3.

Serum ghrelin was measured at -10, 0, 30 and 60 min, but neither baseline
P < 0.1 nor integrated values P < 0.3 differed significantly when
comparing values obtained pre- and post GB Table 1. In contrast, baseline
levels of leptin decreased after GB Table 1.

When comparing gender-specific changes before vs. after GB, the weight loss
was larger in males

than females 27.1±3.1 vs. 14.5±1.5 kg; P<0.001, and accordingly, the
reduction in BMI differed 2

males vs. females: 8.2±0.8 vs. 5.2±0.5 kg/m ; P<0.01. None of the other
parameters showed any differences when comparing men and women.



Simple, unadjusted linear regression analyzes were performed to investigate
the relationship between circulating adiponectin total levels as well as
absolute and relative subform levels and glucose metabolic parameters
fasting glucose, fasting insulin, integrated insulin and glucose
concentrations, HOMA-B and HOMA-S as well as anthropometric
characteristics like age, BMI and weight. However, only a few significant
correlations were obtained, and they were based on cross-sectional data
after GB: MMW-adiponectin correlated positively with HOMA-B r=0.49;
P<0.02 and inversely with HOMA-S r=-0.43; P<0.05, and total adiponectin
correlated positively with HOMA-B r=0.42; P<0.05.

Neither before nor after GB, we found any significant correlations between
serum ghrelin and total adiponectin, HMW-adiponectin and insulin,
respectively. Serum leptin correlated positively with BMI before r = 0.42;
P < 0.05 as well as after GB r = 0.42; P < 0.05, but as for ghrelin,
leptin correlated with neither adiponectin nor insulin.






The present study aimed to examine the effect of GB on circulating
adiponectin in extremely obese

men and women. We found that the average patient 6 months after GB had lost
more than 20 kg.

This weight loss was accompanied by an elevation in total adiponectin,
which was caused primarily

by an increase in the absolute and relative levels of the HMW-subform.

The prevalence of obesity has increased dramatically during the last
decades and today

obesity is considered a worldwide health problem. Accordingly, different
treatments against obesity

like surgery, exercise or diet are being widely used. The preferred
surgical treatments include

laparoscopic adjustable GB used in the present study and laparoscopic
gastric bypass  23. A

recent prospective study showed that the obtained weight loss was larger
following gastric bypass

than GB  10 year follow-up: 25 vs. 13  24. In the present study, the
weight loss after 6

months, 1 year and 2 years corresponded to 13, 15 and 16 of the
preoperative body weight,

respectively. Other studies have reported a 17 weight loss 6 months after
GB and 20-25 after 1

to 2 years 24. Thus, the weight loss reported here was in the low range
as compared to previously

reported findings and we speculate that this may be explained by the
preoperative BMI of our study

participants, which was higher than in the other studies mean BMI 42.4
24 and 45.3 kg/m

2

33.

Several studies have shown that total adiponectin in plasma increases after
weight loss 13-

18, but the reported changes on the molecular distribution of subforms
have been controversial

Our results demonstrated that a significant weight reduction was
accompanied by increased levels

of total adiponectin and in particular of the HMW-subform that has been
suggested to represent the

primary biologically active form  5. This finding is consistent with
previous studies of the

molecular distribution of adiponectin after weight reductions obtained by
diet 15, biliopancreatic

diversion 13 or gastric bypass 17. To our knowledge only a very limited
number of papers have

studied the molecular distribution of adiponectin subforms after GB.
Englund et al investigated 13

obese individuals 1 year after GB and found that total adiponectin and MMW-
subforms had

increased significantly, whereas HMW-subforms had only tended to
increaseThe relationship between insulin sensitivity, weight loss and
adiponectin is complex, and

changes within these parameters not always go hand in hand. For instance,
thiazolidinediones

stimulates insulin sensitivity and plasma adiponectin levels in particular
the HMW-subform

despite the absence of weight loss  25, 26, and metformin reduces body
weight and increases

insulin sensitivity without changing plasma adiponectin 27-29. This
complexity has been stressed

by the present study, where GB despite positive effects on body weight and
serum adiponectin in

particular the HMW-subform failed to increase HOMA-estimates of insulin
sensitivity and betacell function. Thus, further studies are warranted.

In the present study females had numerically higher levels of total and HMW
adiponectin

than males, but differences were insignificant before as well as after GB.
The absence of a

significant impact of gender on serum adiponectin is consistent with other
studies in obese subjects

10 but contradictory to studies of non-obese patients that have shown
higher total adiponectin

levels in women  21, 30. Previous studies  21, 31, 32 suggest that the
higher levels of total

adiponectin in females are due to an elevation of HMW-subforms. Therefore,
we speculate that the

lack of gender difference observed in the present study is caused by the
obesity-related suppression

of HMW-adiponectin.

There are notable differences between previous studies of HMW-adiponectin
after weight

loss, and both unchanged and elevated levels have been reported. Some of
this discrepancy is most

likely related to different HMW-assays. The method we used for measuring
adiponectin-subforms

was quantitative, based on separation by FPLC and subsequent immunoassay,
whereas semiquantitative methods based on Western Blotting have been used
in the majority of other studies. To

the best of our knowledge, only one other study has used quantitative ELISA
to measure

adiponectin-subforms after weight loss and their results were consistent
with our findings  17.

Furthermore, the study populations have varied in important clinical
characteristics, eg number of

participants, weight, BMI, sex and age.

Levels of ghrelin were numerically, but not statistically higher 6 months
after GB, in

accordance with findings by Hanusch-Enserer et al 33. However, in
another study, plasma ghrelin had increased 12 months after GB, at a time
point where the BMI had decreased from 47.5 to 33.2

kg/m

2

34. The latter observation indicates that our samples were collected too
early to allow for

significant changes in serum ghrelin. On the other hand, we were able to
detect a ~23 reduction in

serum leptin following GB, ie in the same order of magnitude as the
relative increase in HMWadiponectin. However, no inverse associations
between adiponectin and leptin were observed.

Weight loss leads to improved insulin sensitivity and glucose tolerance
13, 14, 18, but the

molecular mechanisms responsible are yet to be fully resolved. In the
present study, we observed

that some metabolic parameters improved after weight loss: for instance
patients had lower

overnight fasting plasma glucose levels and meal-integrated levels of
glucose and insulin following

GB Table 1, whereas HOMA-estimates of insulin sensitivity and pic-cell
function failed to improve.

However, it should be noted that the original HOMA estimate was not
evaluated in morbidly obese

subjects, and therefore the calculated values require some reservations
19. Alternatively, the

discrepancy may be explained by the extreme degree of obesity, which
persisted 6 months after GB.

In agreement with this view, previous studies reporting an improved insulin
sensitivity after weight

loss have involved far less obese individuals mean BMI up to 45.0 kg/m

2

 13, 16.

 Recent data suggest that alterations in several adipokines, including
adiponectin, are

contributing to improvements in insulin sensitivity after weight loss 35.
Accordingly, previous

studies on morbidly obese humans BMI>40 kg/m

2

 showed that total adiponectin as well as HMW subforms correlated strongly
with insulin sensitivity after weight reduction 18, 36. The weight

losses were 15 and 36 and their post-operative BMI 34 and 31 kg/m

2

, respectively. In

comparison, in our study the weight loss averaged 13 and the post-
operative BMI 52 kg/m

2

, and

neither cross-sectionally nor longitudinally, circulating adiponectin
correlated significantly with

weight, BMI or any of the metabolic parameters. One explanation may be the
number of

participants n=23. Alternatively, and more tenable the weight loss was
still insufficient to reveal

the expected associations - even 6 months after GB the participants
remained extremely obese.

In conclusion, we demonstrate that absolute and relative levels of the HMW-
subform of

adiponectin are increased 6 months after GB. However, we were unable to
demonstrate an association between changes in adiponectin and improvements
in body weight, BMI and metabolic

parameters. Hence, we recommend that further studies should be made in
order to clarify whether

or not the elevation of the HMW-subform is causally linked to the
improvement in insulin

sensitivity after weight loss.
