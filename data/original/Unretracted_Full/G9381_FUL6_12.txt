Dodders are the most important group of parasitic
weeds in the world, inhabiting virtually every continent and causing untold damage to both crop and
noncrop species Kujit 1969, Malik and Singh 1979,
Dawson et al 1994. Because dodders are obligate 
parasites, requiring both water and all other nutrients
from the host, these plants must attack the host rapidly
and achieve contact with the host xylem and phloem
before their own seed reserves are depleted. Thus, the
dodder must perform the delicate balancing act of an
aggressive invasion of the host while at the same time
not damaging it sufﬁciently to cause host defense
responses nor wounding it so much as to interfere with
the host’s ability to conduct water or produce photosynthate. Despite the ubiquity of this group of weeds,
relatively little is known of the mechanisms by which
dodder invades the host. In a previous report from my
laboratory Vaughn 2002, the mechanisms by which
dodder attaches to the host are described, whereas 
in this report the mechanisms of host penetration and
host response to this invasion are investigated.
Light and more limited electron microscopic studies
Dörr 1968a, 1968b, 1969; Lee and Lee 1989; and summarized in Kujit and Toth 1976, Dawson et al 1994
have elucidated the basic steps of the dodder invasion.
After attachment to the host, the lower haustorium
Lee and Lee 1989 penetrates the host tissue, making
a ﬁssure in the host by either mechanical or enzymatic
means. Epidermal cells of the lower haustorium begin
to elongate now called searching hyphae and penetrate the host tissue on their way to contact with the
xylem and phloem of the host. These tip-growing hyphal cells may extend up to 800 mm in this searching process. Light microscopic studies of the dodder
hyphae reveal an apparent growth of the hyphae right
through the host tissue eg, Lee and Lee 1989.
However, electron microscopic examination revealed
that the hyphae apparently did not grow through the
host but were coated with a layer of “stretched”
Dawson et al 1994 host wall. This mechanism allows
the dodder to penetrate the host, yet essentially still 
be extracellular, thus minimizing stress to the host.
Because dodder relies on the host totally or nearly so,
see Sherman et al 1999 for both ﬁxed carbon and
water throughout its life cycle, it is imperative that the
dodder does not stress the host by disrupting the ﬂow
of either water or photosynthate.
In this study, the characteristics of the dodder invasion of the host are investigated by microscopic, cytochemical, and immunocytochemical techniques. These
data reveal the varying methods by which the dodder
is able to penetrate the host and the unique composition of the chimeric wall formed in the interaction of
host and parasite as the hyphae move through the host
tissue. Some of these data have been recently presented in abstract form Vaughn et al 2001.

Plants of impatiens Impatiens sultanii were grown as described
previously under constant illumination in a Conviron growth
chamber Vaughn 2002. Dodder Cuscuta pentagona seeds were
scariﬁed with concentrated sulfuric acid, washed extensively in
bicarbonate and water as per Sherman et al 1999, and then placed
on top of the potting medium in which the impatiens plants are
growing. Materials for this study were ﬁxed during the initial stages
of dodder invasion of the host and after a successful parasitic union
had been completed. Haustoria associated with stems, petioles, and
leaves were investigated.
Sections of tissue containing developing and mature pieces of
dodder invading the host tissue were ﬁxed in 3 v/v glutaraldehyde in 0.05 M piperazine-N, N-bis2-ethanesulfonic acid PIPES
buffer pH 7.4 at room temperature for 2 h. The samples were
washed in two exchanges of PIPES buffer, 15 min each, and dehydrated in an ethanol series at 4 °C. After two exchanges of 4 °C
absolute ethanol, the samples were transferred to a -20 °C freezer
and embedded in London Resin White resin, by increasing the concentration of London Resin White by 25 increments each day. The
samples were left in 100 London Resin White resin for 2 days at
-20 °C and then allowed to warm to room temperature and placed
on a rocking shaker for 24 h. The segments were then transferred to
BEEM capsules, the capsules sealed, and placed at 50 °C for 2–3 h
to effect polymerization.
Sections 0.35 mm thick were cut with a diamond histoknife
Delaware Diamond Knives, Bear, Del., U.S.A. and mounted on
clean glass slides coated with chrome-alum to enhance section adhesion. Rings were drawn around the sections with wax pencils to
maintain the ﬂuid around the sections in subsequent steps of the
protocol. The slides were then moved to an incubation chamber with
high relative humidity for the incubation steps. The steps and times
are as follows: 1 w/v bovine serum albumin BSA in phosphatebuffered saline PBS, 30 min; primary antiserum diluted 1 : 8 to 
1 : 80 depending upon the antibody or antiserum, 4 h; three
exchanges of PBS-BSA; 1 : 20 dilution of secondary antibody-gold
EY Labs, San Mateo, Calif., U.S.A. in PBS-BSA; three washes of
PBS. The slides were then rinsed with double distilled water from a
squirt bottle and then three to six short incubations of water alone
to remove residual chloride ions, which would interfere with the
silver intensiﬁcation.
For silver intensiﬁcation, the sections were incubated in freshly
prepared solutions from the Amersham InstenSe Amersham,
Arlington, Ill., U.S.A. silver enhancement kit. A 50–100 ml aliquot
of the solution was pipetted on each slide and the slides returned to
the humid chamber for 15–30 min. After the silver had developed,
the sections were washed thoroughly with distilled water from a
squirt bottle, dried with a stream of compressed air, and then
mounted with Permount. Sections were photographed with a Zeiss
Axioskop light microscope. Sections serial to those used for
immunocytochemistry were stained for 1 min with a 1 w/v Toluidine blue in 1 w/v sodium borate to assess structural preservation and to determine the positions of hyphae in treatments where
hyphae were unlabeled with the particular antibody.
Preparation of samples for immunogold transmission electron
microscopy TEM follows the ﬁxation and embedding protocols
described above for immunogold silver above or for transmission
electron microscopy below. Sections from these block faces were
cut with a Delaware diamond knife at 99 nm pale gold reﬂectance
and mounted on 300-mesh gold grids. The grids were then processed
through the localization protocol exactly as described for the slides
in the immunogold silver protocols before the intensiﬁcation steps
above, except that the grids are ﬂoated on 4 ml drops of the solutions. After the water wash, the samples are dried and then stained
for 2 min in 2 w/v uranyl acetate and 30 s in Reynolds lead
citrate before observation with a Zeiss EM 10 CR electron microscope. Sections from several block faces were used in the examination and 20 random micrographs were counted to determine the
density of gold particles over structures of interest.
Samples were ﬁxed in 6 v/v glutaraldehyde in 0.05 M PIPES
buffer pH 7.2 for 2 h at room temperature and then washed in two
exchanges of 0.10 M cacodylate buffer pH 7.2 for 15 min each.
Postﬁxation was carried out in 2 w/v osmium tetroxide in 0.1 M
cacodylate buffer for 2 h at room temperature. After a brief water
rinse, the samples were incubated in 2 w/v uranyl acetate
overnight at 4 °C. After a water rinse to remove residual uranyl
acetate, the samples were dehydrated in an acetone series and then
transferred to 100 propylene oxide. Embedding was carried out
slowly by inﬁltrating in increasing increments of 25 plastic resin
1 : 1 mixture of Spurr resin and Epon, 2–4 h/step. The propylene
oxide was allowed to evaporate through tiny holes in the lid of the
sample vials from the 75 plastic step overnight, gradually increasing the concentration of resin. After reaching 100 resin, fresh resin
was added to the sample vials and the samples were rocked on a
rotating platform for 24–48 h to enhance plastic inﬁltration. BEEM
tear-away molds were used to ﬂat embed the specimens and poly- merization of the plastic took place at 68 °C. Pieces of tissue were
cut from the polymerized plastic with jeweler’s saws and mounted
on acrylic rods for sectioning.
Thin sections with silver to pale gold reﬂectance colors were cut
with a Delaware diamond knife and collected on 300-mesh copper
grids or Formvar-coated slot grids serial sections. Sections were
stained for 7 min each in 2 w/v uranyl acetate and Reynolds lead
citrate before observation with a Zeiss EM 10 CR transmission electron microscope.
For light microscopy, sections 0.35 mm thick of the same block
faces used for TEM analysis were cut with a histological diamond
knife and mounted on chrome-alum coated slides. Some block faces
containing abundant hyphae were cut and mounted individually in
serial sections so as to trace the paths of individual hyphae as they
grew in the host tissue. Sections were stained with 1 w/v Toluidine blue in 1 w/v sodium borate for 3–5 min. After mounting
in Permount resin, the sections were photographed with a Zeiss
Axioskop light microscope.
Most of the antibodies and antisera used in this study have been
described in other papers using these technologies Sabba et al
1999,Vaughn 2002 and are presented here brieﬂy. To eliminate nonspeciﬁc binding of the secondary antibody sometimes a problem
with walls, some controls were run in the absence of primary antibody. For some of the other antibodies and the cytochemical probe
cellulase-gold, the antibodies or probes were incubated in pachyman
callose, highly esteriﬁed citrus pectin JIM7, or carboxymethyl
cellulose cellulase-gold. All of these preincubations essentially
eliminated any labeling of the sections. For characterization of
pectins, the monoclonal antibodies that recognize chieﬂy an esteri-
ﬁed pectin epitope JIM7 or chieﬂy de-esteriﬁed pectin epitope
JIM5 and a polyclonal antiserum that recognizes the de-esteriﬁed
polygalacturonic acid PGA backbone Moore and Staehelin 1988
were utilized. Recent investigations by Willats et al 1999, 2001
indicate that the assigned speciﬁcities of these two monoclonal antibodies may not be as exact as previously presumed. However, in
material embedded for TEM by standard protocols, JIM5 seems to
recognize only a highly de-esteriﬁed pectin epitope and these labeling patterns are very similar to those obtained with the anti-PGA
backbone polyclonal serum. Pretreatment of the sections with 0.1 M
sodium carbonate prior to the labeling protocol eliminated labeling
by the JIM7 monoclonal antibody but greatly enhanced the labeling of both JIM5 and the PGA backbone serum. The LM5 and 
LM6 monoclonal antibodies recognize the 1Æ4-galactan and 1Æ5-
arabinan side chains of rhamnogalacturonan-I RG-I, respectively
Willats et al 1999, 2001. These monoclonal antibodies were 
raised to the puriﬁed polysaccharide and are highly speciﬁc. The 
arabinogalactan protein AGP rat monoclonal antibody JIM8 
and the CCRCMM7 and the AGP mouse monoclonal BioSystems
Australia antibodies are highly speciﬁc to the plasma membraneassociated AGPs. Similarly, the polyclonal antibodies to xyloglucan
and monoclonal antibody to fucosylated xyloglucan CCRCMM1
are well characterized and speciﬁc for each of these polysaccharides
Moore and Staehelin 1988, Freshour et al 1996.

After the successful attachment of the parasite to the
host, a large mass of dodder tissue known as the inner
or lower haustorium Lee and Lee 1989 invades the
host tissue by creating a ﬁssure in the host stem tissue
Fig 1. From the inner haustorium tissue, epidermal
cells known as searching hyphae begin to elongate dramatically, apparently growing through the host tissue
Fig 1. The searching hyphae may extend up to 
800 mm before contact with the host phloem or xylem
cells Dawson et al 1994, this study. In a single 
0.35 mm thick section, a single hypha can be seen to
traverse several cells, extending about 100–200 mm. In
addition to their “searching” action, their extensive
growth may aid in anchoring the parasite securely to
the host plant.
Searching hyphae are tip-growing cells, in some
ways similar to pollen tubes or fungal hyphae in 
structure. The growing end of the hypha Fig 2 A, B
is enriched in Golgi-derived vesicles with larger
organelles plastids and mitochondria and has a
prominent very lobed and probably polyploid
nucleus, progressively back from the tip. Large strands
of actin are found throughout the ends of the developing hyphae and are in contact with the abundant
vesicles present in the cytoplasm. Although observations at the light microscopic level indicate that the
searching hyphae appear to grow through the host
tissue, electron microscopic observations reveal a signiﬁcantly different mechanism. The hyphae are in
fact not growing through the cells per se but are actually coated with a layer of host cell wall Fig 2 A, B.
Although in most cases the wall of the host makes 
a rather uniform thin coating around the searching
hyphae, occasionally there are appositions of the 
host wall Fig 2 B, similar to those formed in host
tissue during fungal hyphae invasion. Appositions are
less frequently observed in the hyphal wall. A more
electron-opaque zone that corresponds to the middle
lamellae can clearly be discerned separating the two
walls Fig 2 C. Although, in general, the coating host
wall appears to be thinner than the dodder wall,
certain of these walls display a thickness similar to or
greater than the hyphal wall eg, Fig 2 C. Thus, it is
unlikely that the wall is simply stretched during the
invasion process, rather some new cell walls must be
produced to cover a hypha growing through a host cell.
Also the volume of coating wall surface must be rather
extensive to cover a new wall that is nearly equal in
volume to the preexisting host wall. In some cases,
such as through multiple invasions in a single cell, the
volume of the coating walls must greatly exceed that
of the preexisting walls.
Unlike in pollen tube cells, plasmodesmata or
ectodesmata, as they are formed on the surface, not via
cell division are found abundantly along the walls of
the developing hypha Fig 3. Connections between
host and dodder cytoplasms occur frequently in the
areas near the hyphal tip but are relatively rare in later
stages of hyphal differentiation further back from the
tip. Both relatively simple unbranched plasmodesmata
Fig 3 A, C and more complex branched structures
are found between the host and parasite. Examples of
collars, particles Fig 3 B, and radial spokes Fig 3 D
are found in these plasmodesmata, much as has been
noted in other plasmodesmata that do not connect
species to species. Plasmodesmata appear to degenerate in these older portions of the hyphae, with
occlusions and hairpin turns back into the dodder
cytoplasm, indicating their loss of function and possible recycling.
 
What is described above is true for virtually all of the
hyphae in dodder invasions of stem or petiole tissue.
These host tissues have relatively few extracellular air spaces so that cells occupy the majority of the tissue
volume and the hyphae are in intimate contact with
cells during virtually the entire invasion sequence. In
leaf tissue, however, there are large air spaces between
cells, especially in the spongy mesophyll. The dodder
invasion in leaf tissue takes advantage of the air
spaces, so that both extracellular and intracellular
growth of the hypha occur Fig 4 A. Serial sections of
a given hypha reveal that both extra- and intracellular
growth may occur on a single hypha Fig 4 A. The
extracellular growth of the hyphae is similar to that
found in the intracellular growth except that the hyphae are not coated with any host cell wall material
Areas between adjacent cells of the leaf palisade are
normally connected tightly by the middle lamellae. In
those areas where hyphae are present, however, the
area occupied by middle lamellae is now replaced 
by a meshlike electron-opaque network and is spread
through a greater area than the relatively compressed
middle lamella Fig 4 B, C. The growth of the searching hyphae intracellular in the leaf lamina, however, is
identical to that in the petioles or stems, with a coating
wall of the host surrounding the hyphae eg, Fig 2 A.
To determine the composition of the unusual chimeric
hypha and coating host wall, we probed sections of
impatiens stem tissue infected with dodder with a
battery of antibodies and probes to polysaccharides
and wall proteins Table 1.The technique of immunogold-silver-enhanced light microscopy was used for all
of these antibodies to obtain a more “global” picture
of the distribution of these epitopes in the tissue, and
these observations were conﬁrmed by immunogold localizations at the TEM level on sections from these
same block faces. Other antibodies were used on thin
sections prepared for traditional TEM studies to allow
high-resolution detection of their reactive sites. Four
different block faces from four different ﬁxations were
probed with the complete battery of these antibodies
and cytochemical probes at the light microscopic level.
The results described herein are consistent both
between and within a block face. In some cases serial
thick sections were probed with several different antibodies so that the wall composition of a given hypha
could be determined.
Pectins are a diverse group of polysaccharides and
a number of antibodies and antisera are available to
detect speciﬁc epitopes present in the wall. Almost all
of these antibodies and antisera revealed an enhanced
labeling of the chimeric wall compared to the host or
nonhyphal dodder walls. Label with the monoclonal
antibodies JIM5 and JIM7, which recognize primarily
esteriﬁed or de-esteriﬁed pectins, respectively, and the
polyclonal serum to the PGA backbone, strongly label
the chimeric wall Fig 5 A, B. At the TEM level, a
strong reaction at the dodder and host middle lamellae could be detected, especially with the JIM5 and
PGA backbone probes not shown. A strong reaction
was noted throughout this wall when sodium bicarbonate treatment to de-esterify the pectins was utilized and the sections probed with either the JIM5
monclonal or the PGA backbone antiserum not
shown. This treatment eliminated labeling with the
JIM7 monoclonal antibody, however.
RG-I is the major pectic fraction in the primary wall.
Two different modiﬁcations, a galactan and an arabinan can be added as side groups that modify the
properties of the RG-I. Monoclonal antibodies LM5
and LM6 speciﬁcally label RG-I modiﬁed with either
1Æ4-galactan or 1Æ5-arabinan, respectively Willats
et al 1999, 2001.When sections are probed with LM5, the host and nonhyphal dodder walls react strongly,
but the chimeric wall of host and parasite is unlabeled
Fig 5 C. In contrast, labeling with LM6 monoclonal
results in very intense label over the chimeric wall,
but staining is less intense in the surrounding host
walls Fig 5 D. These data indicate that unlike 
other walls in both the host and the dodder, the
chimeric wall has RG-I that is substituted only with
arabinans, not galactans.
AGPs are associated with the plasma membrane,
especially around areas of new cell wall biogenesis
Freshour et al 1996, Gaspar et al 2001. The label
with these antibodies was especially distinctive in that
a double ring formation could be detected when the
chimeric wall was cut at right angles to the planes of
the section, representing label of both the host and
dodder plasma membranes. This pattern was observed
for CCRCMM7 and an AGP monoclonal antibody
raised by Anderson et al 1984 Fig 5 E. An exception to this pattern is the label with the JIM8 antibody,
which labels only the dodder tissue, leaving the host
cells unlabeled Fig 5 F.
The host often responds to pathogen invasion by
producing callose and extensin. Despite the extensive
invasion of the host by the dodder, the host tissue produces relatively little callose and only relatively scattered patches of labeling was detected at the light
microscopic level. Callose is found in association with
the extensive plasmodesmata that are produced along
the searching hyphae Fig 6 A and in all other plasmodesmata throughout both the host and parasite,
conﬁrming that the punctate labeling at the light
microscopic level is indeed due to plasmodesmata.
The tips of the growing searching hyphae are callose enriched and a thin layer of wall containing callose
extends further back in the hyphae, perhaps as remnants of this tip area of callose not shown. Appositions formed on either the host or hyphal walls were
labeled with anticallose Fig 6 B, C. The only host
structures exhibiting intense callose labeling were
those walls that had been crushed in the penetration
of the lower haustorium into the host tissue Fig 6 D.
These remnant cell walls were uniformly labeled. Likewise, extensin, another component of thickened or
hardened walls after pathogen invasion, was probed
for with a polyclonal serum and three different monoclonal antibodies. None of these label the chimeric
walls or any other wall strongly with the exception 
of the vascular systems of both host and dodder 
Table 1.
Although both the fucosylated xyloglucan monoclonal and the xyloglucan polyclonal antibodies label
all the wall surfaces, there was a lower density of labeling over the chimeric wall than in the host walls Table
1. A similar pattern was noted for the distribution of
labeling with the afﬁnity gold probe cellulase-gold.
Because the chimeric walls are enriched in pectins,
these polysaccharides must make up most of the bulk
of the chimeric wall rather than the normal cellulosexyloglucan matrix.
In the growth of hyphae through the air spaces in
leaf tissue, the hyphal tip has a distinctly bilayered wall
with an outer zone enriched in de-esteriﬁed pectins
and an inner zone composed primarily of cellulosexyloglucan Fig 7 A, B. The threadlike material present in the areas where the hypha has grown between
cells reacts only with antibodies to de-esteriﬁed pectin, indicating that these represent degraded areas of
middle lamellae, as predicted by their location in this
extracellular area Fig 7 C, D.

Dodder hyphae have long intrigued botanists and
plant pathologist and even the term “hypha” was ﬁrst
applied to the structure in dodder haustorium, not to
the more familiar and similar tip-growing cell in the
fungi Kujit 1969, Kujit and Toth 1976. Earlier light
microscopic investigations reported that the hyphae
grow through the host cells reviewed in Kujit 1969,
Malik and Singh 1979. However, it is clear that the
hyphae grow through the cells encased in a layer of
host cell wall that either is stretched Tripodi and 
Pizzolongo 1967, Dörr 1968b, Dawson et al 1994 or is
de novo synthesized in concert with the expanding
hyphae. Although the thickness of the coating wall is
often less than that of the hyphae, there are ample
cases of the walls of the host being equal to the thickness of that of the hyphae eg, Fig 2 B, C. Moreover,
the presence of a wall of different composition
between the coating wall and the surrounding host
walls eg, Fig 5 indicates that the new wall must be
made de novo or involves the selective stretching of
certain components of the old host wall.
Because dodders are obligate parasites, they rely on
the host plant for both water and photosynthate to maintain their growth. Thus, the dodder must invade
the plant rapidly but at the same time perturb the host
minimally so that the host continues to function normally to sustain the parasite. In contrast, most pathogenic fungi would not require a functional host after a
successful invasion. Because the host and dodder are
physically separated by a wall during the invasion
process, the host appears to be less perturbed than in
the invasion by fungi, as there are few or no indications of wound-type responses of either callose or
extensin production in the host walls Fig 6. The
occasional wall appositions Figs 2 B and 6 B, C are
the only indication of any perturbation to the host
around the growing hyphae. In contrast, fungal invasion almost always results in the extensive production
of both callose and extensin by the host.
Remarkably, the dodder hypha appears to “sense”
the difference between growth in cells and that
between the cells. When the dodder grows between
cells, the hypha appears to degrade the cell-to-cell
cement in the middle lamellae and is thus more similar
to the growth patterns described for the hyphae of the parasitic weed Orobanche spp. Losner-Goshen et al
1998. Few or no plasmodesmata are formed or are
degraded quickly in the extracellular hypha of the
dodder, even though they are formed extensively in
hyphae that traverse the host cells. The hyphal wall in
this growth pattern resembles the wall composition
and organization of expanding epidermal cells, essentially a bilayered wall with the outer portions enriched
in de-esteriﬁed pectin Vaughn and Turley 1999,
Vaughn 2002. Thus, depending upon whether the
hypha grows through the wall or through air space, the
hypha is able to modify its composition and organization of the wall and the machinery for invasion and
penetration. In the case of intracellular invasion the
host wall is induced to expand and in extracellular
invasion the middle lamella is degraded. Both allow
expansion of the hyphae but by radically different
routes.
The composition of the chimeric wall formed from the
dodder hypha and the coating host cell wall is unique
and distinct from both the host and nonhyphal walls
of the dodder.
Pectins are altered both quantitatively and qualitatively in the chimeric wall formation Fig 5 A–D and
Table 1. Both the hyphal wall and the coating wall 
are enriched in both esteriﬁed and de-esteriﬁed pectins relative to the surrounding host walls Fig 5 A,
B. In addition, there is a dramatic difference in the
modifying side chains of the RG-I pectins in the
chimeric walls. In the preexisting host walls, RG-I is
modiﬁed both by the 1Æ4-galactan and the 1Æ5-arabinan side chains Fig 5 C, D. Label with both the
LM5 and LM6 antibodies is also noted on all other
walls of the dodder not shown. In the hypha and
coating host wall, however, there is label associated
with the arabinan, not the galactan Fig 5 C, D. Arabinan side chains are associated with laticifer walls,
which are ﬂexible and/or invasive in growth Serpe 
et al 2001, and perhaps it is these characteristics 
of the RG-I so modiﬁed that are necessary for the 
circuitous movement of the hypha through the host
tissues. Moreover, the presence of a new host wall 
with differences in RG-I side group composition compared to the surrounding host walls indicates that
these walls are just not merely “stretched” but are also
altered in composition. Those data indicate that the
coating host wall is a de novo synthesized wall or at
least the pectins in that wall in response to hyphal
penetration.
Although AGPs as a group have been suggested to
be involved in signaling processes, indicating where
new walls are to be produced, and other developmental regulation, the functions of a given AGP or
epitopes recognized by an AGP antibody are
unknown Gaspar et al 2001. The presence of strong
reactions of the CCRCMM7 Freshour et al 1996 and
the AGP monoclonal antibody of Anderson et al
1984 in both the dodder and the host, compared to
the exclusive labeling of the dodder by JIM8, indicates
a difference in AGPs between host and parasite.
Recently, Gaspar et al 2001 conﬁrmed that the AGP
recognized by JIM8 recognizes a very lipophilic AGP,
whereas MAC207 and JIM13 antibodies recognize
“general AGP epitopes”. The growing dodder hypha
secretes large numbers of Golgi-derived vesicles and
it may be that a very lipophilic AGP is involved in an
interaction between the lipophilic Golgi vesicles and
the plasma membrane. Certainly the very punctate
labeling of the JIM8 AGP monoclonal antibody may
indicate regions of speciﬁc membrane fusions.
The low levels of labeling with either extensin or
callose antisera in the host indicate that neither of the
wall components frequently associated with wounding
is induced by dodder invasion. Exceptions to this are
the crushed, cytoplasm-less cells and tissues at the site
of lower haustorium formation Fig 6 D, in which the
cytoplasm exists only in the remnant form of degraded
organelles or not at all. These data indicate that the
host does not recognize the dodder hyphal invasion of
its tissues as a pathogenic attack, either through or
between cells. This apparent lack of host response is in
striking contrast to the induction of pathogens is related proteins in the parasitic attack of Orobanche
spp. Joel and Portnoy 1998, Westwood et al 1998, a
parasite that does not induce a coating wall formation
by the host.
The dodder hyphae-to-host plasmodesmata connection is one of the rare instances of direct species-to species cytoplasmic contact in the plant kingdom
Dörr 1968a, 1968b, 1987; Blackman and Overall 2001;
Ehlers and Kollman 2001. At the tip of growing
hyphae, plasmodesmata or ectodesmata are produced in great numbers and the chimeric plasmodesmata are labeled by callose antibodies Fig 6 A, as are
same-species plasmodesmata in cells of both the host
and the dodder. Some of the plasmodesmata establish
cross-species bridges between the host and the parasite, whereas others appear to be degraded or to
meander through the cell walls, sometimes forming
hairpin loops. In areas of the hyphae back from the tip,
these connections become increasingly more rare,
indicating that the connections are ephemeral The
presence of these cross-species plasmodesmata is
unlikely to be mere happenstance and certainly begs
questions as to messages ﬂowing between the species.
For example, the production of a coating host wall of
a composition unique compared to the preexisting
host walls indicates the induction of a new-wall synthesizing machinery in a cell that has long stopped
producing new wall. Whether mRNA, protein, or small
molecules are involved in this reaction is not known,
but it does represent an intriguing aspect of this
unique parasitism, requiring further investigation.
