The safe use of sedative is often important for oncologic
patients. Anticancer chemotherapeutic agents have been reported to reduce cognitive function via hippocampal neuronal toxicity, and increase the vulnerability for development
of depression during certain stressful periods 1—3. Especially
in the case of geriatric patients receiving a palliative care,
cautions should be paid. In brain aging, brain area involved
in the modulation of the hypothalamo-pituitary-adrenal
HPA axis might be significantly changed. Late-life depression and anxiety may lead individuals to be unable to adjust
their physiology and behavior to stressful events and elevate
their circulating cortisol to chronic levels, with detrimental
consequences to adversely altered HPA axis 4,5. Efforts toward stabilization of altered HPA axis via anti-anxiety and antidepressant effects, even in mild or transient cases, may affect favorably on the progress of aged patients having malignancy. It has also been suggested that the drug can play anticancer function by inhibiting proliferation of some cancer
cell lines 6—8. Thus, for the control of anxiety and depression,
it is reasonable to suppose that use of sedative may be one of
choices for the management of malignancies, especially in
geriatric patients. However, prolonged use of synthetic benzodiazepines like diazepam, is limited owing to harmful adverse effects including tolerance.
Diazepam have been known to bind the peripheral-type
benzodiazepine receptors PBRs, and PBR ligands, including PK11195, have been reported to enhance apoptosis and
elicit cell cycle arrest in many types of tumors 9—14. However,
cancer progression has also been implied in cancer cells with
perinclear/nuclear located PBRs, especially certain types of
breast cancer cell, and their proliferative effect linked to a
cholesterol inﬂux into the nuclear membrane 15,16.
Accordingly, to establish scientific information for the safe
use of sedative in the case of malignancies, this study examined the in vitro anticancer characteristics of diazepam and
flavonoids.

The human adenocarcinoma SNU-C4 colorectal cancer cell line was purchased from the Korean 
Cell Line Bank KCLB, Seoul, Korea, while the human
breast adenocarcinoma MDA-MB-231 cell lines were kindly
provided by Dr Sang-Hyun Kim Kyungpook National 
University, School of Medicine. The RPMI medium 1640,
trypsin solution, sodium pyruvate, ethylene glycol-bis-
aminoethylether N,N,N,N-tetraacetic acid, feta bovine
serum FBS and antibiotics penicillin and streptomycin
were all purchased from GIBCO Grand Island, NY, U.S.A.,
the diazepam kindly donated by Roche Switzerland, the petroleum benzin purchased from Fluka Germany, and the
Lovastatin purchased from Tocris Cookson Inc MI, U.S.A..
All the other assay reagents and chemicals, including apigenin were purchased from Sigma St Louis, MO, U.S.A..
The diazepam, PK11195 1-2-chlorophenyl-N-methyl-N-
1-methylpropyl-3-iso-quinolinecarboxamide, apigenin,
and ﬁsetin were prepared as stock solutions in 100 ethanol
or methanol, then diluted with an aqueous medium to the
ﬁnal desired concentrations. The stock solutions of drugs
were sterilized by ﬁltration through 0.22mm disc ﬁlters Gelman Sciences, Ann Arbor, MI, U.S.A. before being applied
to the cells.
The cells were cultured at 37 °C in a humidified incubator under 5 CO2
/95 air in an RPMI 1640
medium supplemented with 10 FBS, 200 IU/ml penicillin,
200mg/ml of streptomycin and 1 mM sodium pyruvate. The
culture medium was replaced every 2—3 d.
The cytotoxicity was measured using an
MTS assay. The cells 5104
 were seeded in wells containing 100ml of the RPMI medium supplemented with 19 FBS, in 96-well plates. After 24 h, various concentrations 
of different materials were added. After 24, 72 and 144 h,
20ml of an MTS 3-4,5-dimethylthiazol-2-yl-5-3-carboxymethoxyphenyl-2-4-sulfophenyl-2H-tetrazolium,
inner salt solution CellTiter 96®
Aqueous One Solution
Cell Proliferation Assay, Promega was added and the plates
were incubated for further 1—4 h. The optical density at
490 nm was measured with a 96-well plate reader, as the
quantity of formazan produced, as measured by the absorbance at 490 nm, is directly proportional to the number of
living cells in the culture. Concentration for 50 inhibition
IC50 values were obtained via nonlinear regression of concentration–response curves.
The cells
were harvested and the pellets collected by centrifugation.
The resulting pellets were then resuspended in a hypotonic
buffer pH7.5 consisting of 1 mM EDTA and 20 mM
Tris–HCl. The sample protein contents were measured using
a BCA assay kit Pierce. 50mg protein, various concentrations of drugs and 20ml of the reaction mixture were then
added to tubes containing 2.5 ml of a potassium phosphate
buffer 100 mM, pH7. The reaction mixture consisted of
2.5 mM NADPH, 1.25 mM acetyl-CoA, 1.25 mM MalonylCoA and 0.02 mM 2-14Cmalonyl-CoA 45 mCi/mmol,
BMS. The tubes were incubated for 15 min, at 37 °C. The
reaction was stopped by adding 3 ml of ice-cold 1 M
HCl/methanol 6 : 4 v/v. The fatty acids were extracted with
petroleum benzin. The incorporation of 2-14Cmalonyl-CoA
activity was measured with a scintillation counter Wallac,
Finland.
After trypsinization process, 3106
cells were placed in the assay wells of culture dishes containing 1 ml of complete RPMI Falcon multiwell 12 wells, Becton Dickinson Labware, U.S.A.. Then, cells were treated
with 106
M concentration of apigenin, fisetin and diazepam
for 6 d, respectively. Human vascular endothelial growth factor kit VEGF Immunoassay, Koma-biotech, Korea and
human granulocyte-macrophage-colony stimulating factor kit
GM-CSF Immunoassay, Komabiotech, Korea were used to
measure VEGF and GM-CSF concentration, respectively.
The optical density of each sample was measured in microplate reader at 450 nm. Release amounts of VEGF and
GM-CSF were normalized by viable cell counts in each well.
The data represent the mean the
standard error S.E.. Inter-group data comparisions were
made using Student’s t-test Systat, Intelligent Software,
Evanston, IL, U.S.A.. Nonlinear regression for obtaining the
IC50 value were made using computer software Prism,
Graphpad, U.S.A..

As shown in Fig 1, treatment for 72 h with apigenin and fisetin, both flavonoids showed concentration-dependent inhibition on the survival of the SNU-C4 and MDA-MB-231
adenocarcinoma cells. In the SNU-C4 cells, IC50 mM of apigenin- and ﬁsetin-treated group was 2.30.2 and 0.80.1,
respectively. In the MDA-MB-231 cells, IC50 mM of apigenin- and ﬁsetin-treated group was 1.20.3 and 3.20.5,
respectively. Same treatment of diazepam, a synthetic benzodiazepine, showed also cancer cell survival concentration-dependently. IC50 mM of diazepam-treated group was 5.51.2
and 0.120.1, in the SNU-C4 and MDA-MB-231 adenocarcinoma cells, respectively.
As shown in Fig 2, in the longer duration of treatment,
micromolar concentration of apigenin and fisetin showed 
the higher inhibitory effect on the survival of the SNU-C4
and MDA-MB-231 adenocarcinoma cells. Like flavonoids,
106
M concentration of diazepam, also showed incubation
time-dependent inhibition of the cell survival In the group
treated for 72 h with the same concentration of apigenin,
fisetin and diazepam, inhibition of cell survival  were increased to 10.31.0, 8.80.9 and 8.91.2, respectively. In
the group treated for 144 h with the same concentration of
apigenin, fisetin and diazepam, inhibition of cell survival 
were increased to 9.21.2, 10.92.1 and 9.60.6, respectively.
As shown in Fig 3, treatment for 24 h with a 106
M concentration of PK11195, a PBR ligand, slightly inhibited the
survival of the SNU-C4 colorectal cancer cells, while for the
MDA-MB-231 breast cancer cells, the same concentration of
PK11195 enhanced the cell survival to 20.12.1. However, the addition of a 106
M concentration of lovastatin, a
HMG CoA reductase inhibitor, reversed the proliferative effects of PK11195 in the MDA-MB-231 cells, and lovastatin
increased the cytotoxic effects of PK11195 in the SNU-C4
cells. Single treatment of 106
M concentration of lovastatin
for 24 h showed weak inhibitory effect on the cancer cell survival, and inhibition of cell survival  were 4.50.1 and 2.30.1 in the SNU-C4 and MDA-MB-231 cancer cells, respectively.
As shown in Fig 4, concentration-dependent inhibitory effect on FAS activity, known as an anticancer mechanism of
flavonoids, was also observed in the various concentrations
105
M, 106
M, 107
M, 108
M of the sedative-treated for
30 min SNU-C4 and MDA-MB-231 cancer cells. In the apigenin, ﬁsetin and diazepam-treated groups, IC50 mM were
3.90.6, 1.00.2, and 5.40.4, respectively for the SNU-C4
cells, and 0.60.1, 4.70.6, and 2.40.3, respectively for
the MDA-MB-231 cancer cells.
As shown in Fig 5, for the SNU-C4 and MDA-MB-231
adenocarcinoma cells, anticancer cytotoxicity induced by
flavonoids or diazepam was not reduced by treatment of
106
M concentration of 5-ﬂuorouracil 5-FU, a chemotherapeutic agent. Their inhibitory action on cancer cell survivals
was further enhanced by the addition of PK11195, a putative
chemosensitizer.
As shown in Fig 6, treatment of micromolar concentration
of apigenin and ﬁsetin reduced the release of VEGF, and examined flavonoids also reduced the release of GM-CSF from
the SNU-C4 and MDA-MB-231 adenocarcinoma cells. In
this study, like flavonoids, micromolar concentration of diazepam inhibited the release of VEGF and GM-CSF into supernatants of cultured SNU-C4 and MDA-MB-231 adenocarcinoma cells.

Flavonoids, as polyphenolic compounds found in plants,
have already been reported to have anti-cancer efficacies 17—19. 
Consistently, in this study, apigenin and ﬁsetin, ﬂavonoids
showed in vitro anticancer cytotoxicity. Like ﬂavonoids, diazepam, a sedative also showed inhibition of cancer cell survival The anticancer cytotoxicity of flavonoids and diazepam showed the concentration-dependency, and micromolar
ranges of IC50. The plasma concentration of diazepam in patients who are taking therapeutic dose of diazepam 5—
10 mg were found to be micromolar range. The inhibition 
of FAS activity, is a known cytotoxic mechanism of ﬂavonoids, 20 and in the present study, micromolar concentration
of diazepam consistently inhibited the FAS activity in the
SNU-C4 and MDA-MB-231 cells.
In highly aggressive breast cancer cells, such as MDAMB-231 cells, a nuclear/perinuclear located PBR have been
reported to mediate the stimulation of cancer cell survival 15,16 Consistently, in this study, PK11195, a specific
PBR ligand remarkably enhanced the proliferation of MDAMB-231 breast adenocarcinoma cells, but diazepam reduced
the cell survival However, PBR as a critical part of the mitochondrial permeability transition pore mPT has been shown
to mediate enhancing the apoptosis 9—14. In a previous study
by the current authors, 21 anticancer cytotoxic flavonoids including apigenin elicited upregulation of PBR mRNA expression in SK-N-Mc human neuroblastoma cells. Moreover,
in this study, treatment with PBR ligands consistently inhibited the SNU-C4 cell survival, and diazepam also produced
anticancer cytotoxic effects, similar to those caused by
flavonoids.
In highly aggressive cancer cells, including MDA-MB-231
breast cancer cells, an increased cholesterol influx into the
nucleus has been reported to be associated with the proliferative action of a perinuclear/nuclear located PBR 16. Consistent with this, in the present study, proliferative effect of
PK11195 on of the MDA-MB-231 cells was reversed by the
co-administration of lovastatin, a HMG-CoA reductase inhibitor. HMG-CoA reductase is an enzyme that catalyzes the
rate-limiting step of the isoprenoid producing mevalonate
pathway, and isoprenoid is involved in the activation of Ras
and cholesterol synthesis. In particular, lipid soluble statins
have been also shown to produce antitumor effects, and reduce intracellular cholesterol level 22—24. In the present study,
single treatment of lovastatin showed weak anticancer cell
cytotoxicity.
Moreover, the micromolar concentration of diazepam- and
flavonoids-induced cytotoxic activity in the cancer cells in
this study was not reduced by the addition of 5-ﬂuorouracil
5-FU, a chemotherapeutic agent. PK11195 has been reported to generate a reversal action on the chemoresistance
in cancer cells, independent of the direct activation of
PBR 25—27. In this study, PK11195, a putative chemosensitizer, showed further enhancement of their anticancer effects.
VEGF and GM-CSF were shown to stimulate malignant
tumor cell growth and migration in vitro and to promote cancer progression in vivo 28,29.
Modulation of angiogenesis via
VEGF system by black tea polyphenols has been proposed as
a mechanism of chemopreventon of rat mammary carcinogenesis 29. Flavonoids, active ingredients isolated from several plants, have been also introduced to inhibit tumor angiogenesis through decreasing VEGF expression and the release
of the angiogenic peptide VEGF from U-343 and U-118
human glioma cells 30,31. Consistently with these, treatment
of micromolar concentration of apigenin and ﬁsetin reduced
the release of VEGF, and examined flavonoids also reduced
the release of GM-CSF from in the SNU-C4 and MDA-MB-
231 cells. In this study, micromolar concentration of diazepam inhibit the release of VEGF and GM-CSF like
flavonoids into supernatants of cultured in the SNU-C4 and
MDA-MB-231 cells.
In cancer cells, increased glucose transporter Glut and
hexokinase HK activities facilitate glucose utilization and
thus continuous intracellular accumulation of ﬂuoro-deoxyglucose FDG. The logic behind delayed FDG PET imaging
is based on the known fact that most types of malignant cells
have significantly increased ratios of HK to glucose-6-phosphatase activities, which allow 18F-deoxyglucose-6-phosphatate to accumulate to a much higher level over time than
in the normal cells. It has been reported that the routine use
of 5 mg diazepam p.o. before intravenous administration of
FDG renders PET imaging ineffective 32. It has also been reported that diazepam inhibit the function of Glut, insulin sensitivity and HK activity 33,34. From above reports, it can be
postulated that sedative may elicit in vivo anticancer efficacy.
Agents that inhibit glucose uptake should decrease the
proliferation of cancer cells. Certain flavonoids inhibit glucose uptake in culture cells 35,36. Flavonoids, polyphnolic
compounds distributed widely in plant-based foods, exert diverse biological effects in cultured cells and in vivo. Certain
flavonoids including apigenin have been reported to show a
selective anxiolysis in mice 10 mg/kg i.p., and a partial agonistic activity at the CBRs in micromolar concentration 37.
In the plasma samples of population taking apigenin-rich
foods regularly, similar amounts of the compound were detected. Yet, the difficulties involved with attaining a sufficient
concentration to activate the central nervous system present
an obstacle. Therefore, the improved activities shown by certain synthetic derivatives, such as methylapigenin, may provide a better challenge. Moreover, caffeic acid butyl ester
and various related synthetic nitroﬂavone derivatives have
exhibited potent anticancer activity, without affecting normal
cell survival 38 Consistently, these options may support a
safer use of sedative in the management of oncologic patients. Plus compounds derived from natural products have been shown to be advantageous over synthetic sedatives, due
to diminished adverse effects, such as muscle relaxation, tolerance, and dependency. 
The results of our study can increase the knowledge about
the inﬂhence of the examined substances on proliferation and
progression of cancer cells. Our observations might give an
insight into the safe use of sedatives in the malignancies.
However, for a prolonged use, natural substance like apigenin might be advantageous over a synthetic drugs. In conclusion, this study provided in vitro information on the use of
flavonoids and sedative in oncologic patients.
