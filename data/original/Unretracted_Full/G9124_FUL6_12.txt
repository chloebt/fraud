The human immunodeficiency virus type 1 HIV-l is the 
etiologic agent of the acquired immune deficiency syndrome AIDS; Barre-Sinoussi et al, 1983; Gallo et al, 
1984. Replication of HIV-1 is an extremely complex process which involves several trans-acting genes and responding c acting sequences Arya et al, 1985; Sodroski et al, 1985, 1986; Rosen et al, 1985, 1986; Peterlin et 
al, 1986. One of the trans-activators that plays a pivotal 
role in virus replication is the tat protein. The mechanism 
by which tar trans-activates HIV-1 gene expression is not 
clear, although evidence suggests both transcriptional 
and posttranscriptional mechanisms eg, Cullen, 1986; 
Rosen et al, 1986; Peterlin et al, 1986; Muesing et al, 
1987; Sadaie et al, 1988; Rice and Mathews, 1988. 
The &-acting sequences responding to rat trans-activation termed -TAR are located downstream of the 
transcription initiation site Rosen et al, 1985. The TAR 
sequence is present in all HIV-1 mRNAs and assumes a 
stable stem-loop structure in vitro as determined by RNA 
nuclease mapping Muesing et al, 1987. 
Excessive secondary structure in the 5’ untranslated region UTR of eukaryotic mRNAs interdicts translation initiation Pelletier and Sonenberg, 1985. Recently, Parkin 
et al 1988 demonstrated that the TAR region, when 
fused to a heterologous mRNA, exhibited a strong inhibitory effect on translation in cell-free extracts and Xenopus 
oocytes. Two factors determined the degree of inhibition 
by the TAR region: secondary structure and accessibility 
of the cap structure Parkin et al, 1988. 
The secondary structure at the 5’ proximal end of a eukaryotic mRNA is believed to be melted during translation 
initiation by the eukaryotic initiation factor 4F elF-4F in 
conjunction with several other initiation factors for a recent review, see Sonenberg, 1988. Consequently, the accessibility of the cap structure to elF-4F is an important 
determinant in controlling translational efficiency Godefroy Colburn et al, 1985; Lawson et al, 1988. Consistent 
with this, increasing the accessibility of the cap structure 
of HIV-1 mRNA using site-directed mutagenesis has mitigated the translational inhibitory effect of the TAR sequence Parkin et al, 1988. 
The presence of double-stranded RNA can potentially 
inhibit protein synthesis in tram by a different mechanism. 
Double-stranded RNA sequences can mediate the auto-phosphorylation and activation of the double-stranded 
RNA-dependent kinase dsl; Farrell et al, 1977. The 
phosphorylated and active dsl specifically phosphorylates the a subunit of eukaryotic initiation factor 2 elF-2, 
rendering it incapable of recycling for reviews, see Safer, 
1983, and London et al, 1987. Although it is not entirely 
clear what the normal physiological function of dsl is, it 
plays an important role during viral infection. As part of the 
cellular response to block viral gene expression, cells synthesize the anti-viral agent interferon reviewed in Pestka 
et al, 1987. Interferons elicit their anti-viral activity, in 
part, by increasing the level of dsl. 
The results in this paper demonstrate that the TAR region can inhibit mRNA translation in trans and that this inhibition is mediated by the presence of the unique TAR 
stem-loop structure. Translational inhibition is correlated 
with phosphorylation of dsl and the a subunit of elF-2. Activation of dsl mediated by the TAR region might have important implications for the control of viral and host gene 
expression after HIV-l infection. From these observations 
we suggest a novel translational regulatory mechanism.
All recombinant DNA techniques were performed by standard 
methods Maniatis et al, 1982. The plasmids CAT and TAR/CAT Figure 2A and the TAR mutants, TARS/CAT, TARSRICAT and TAR3R3/ 
CAT Figure 4. were previously described Parkin et al, 1988. The 
plasmid polio/CAT Figure 2A was described by Pelletier et al, 1988. 
The plasmid PLTARlCAT Figure 2A contains 23 nucleotides between 
the SP8 promoter and the +l nucleotide of the TAR element mostly 
polylinker sequences and was a kind gift from Dr C. Rosen Roche 
Institute, Nutley, NJ. To generate the hybrid plasmids TAR/polio/CAT 
and PLTARlpoliolCAT Figure 2A, plasmid polio/CAT designated A5’- 32O/CAT in Pelletier et al, 1988 was digested with Hindlll and BamHI. 
The released insert containing the poliovirus 5’ UTR and the CAT coding sequence was gel purified, and inserted into the Hindlll-BamHI 
site of the TAR/CAT plasmid, resulting in TAR/polio/CAT The plasmid 
PLTARlpoliolCAT was created by first performing a partial Hindlll 
digestion on the plasmid PLTARKAT. The partially digested plasmids 
were gel purified and incubated with BarnHI. The desired Hindlll-BamHl backbone was purified and ligated to the Hindlll-BamHI insert 
from polio/CAT. All poliovirus clones contain nucleotides 320 to 733 of 
the poliovirus 5’ UTR. 
Plasmids were linearized at the unique BamHl site downstream of the 
CAT coding sequence followed by phenol extraction and ethanol 
precipitation. Linearized plasmids were used as templates for in vitro 
synthesis of m7GpppG-primed mRNA as previously described Pelletier and Sonenberg, 1985 except that the concentrations of GTP and 
m7GpppG were raised to 0.2 and 1 mM, respectively. Yields of transcripts were calculated from the incorporation of 13HCTP into RNA. 
All mRNAs were analyzed for integrity on 1 agarose-formaldehyde 
gels and visualized by fluorography and autoradiography. For each experiment where different mRNAs were directly compared, they were 
all synthesized simultaneously. For the experiment in Figure 78, transcription was performed with a-32PGTP The resulting mRNAs were 
purified by preparative gel electrophoresis using a 40 cm long sequencing gel 7 M urea, 4 acrylamide. 0.2 bis-acrylamide, 1 x 
TBE. The corresponding radiolabeled mRNAs were identified by autoradiography and purified as described in Grabowski et al 1984. The 
quality of the gel-purified mRNAs was determined by analysis on a 1 
agarose-formaldehyde gel and autoradiography. All mRNAs were 
resuspended in sterile water and stored at -70. 
In vitro translations were performed in a nuclease-treated rabbit 
reticutocyte lysate Promega according to the manufacturers recommended procedure. pans-inhibition experiments were done as follows: dried mRNA was resuspended in 4.4 ul of nuclease-treated rabbit 
rehculocyte lysate and preincubated for 10 min at 30 except where 
indicated otherwise, and immediately placed in an ice-water bath. Subsequently, the reaction mixture was supplemented with methio-nine, amino acid mixture minus methionine Promega, and the indicator CAT mRNA or polio/CAT mRNA in Figure 6 and translation was 
done at 30 for 60 min. Radiolabeled CAT protein was analyzed by 
resolution on a 12.5 polyacrylamide-SDS gel followed by fluorography and autoradiography. The autoradiograms were quantitated using 
an LKB scanning densitometer 
3T3-F442A cells were grown to confluence in Dulbecco-Vogt modified 
Eagles minimal essential medium supplemented with 10 fetal or calf 
serum and SIO extracts were prepared as previously described 
Petryshyn et al, 1984. Mouse inteferon-beta IFN8 Lee Bio Molecular Research Laboratories, San Diego, CA was added to cultures 50 
IUlml 18 hr prior to preparation of extracts Petryshyn et al, 1988. 
These SlO extracts were used as a source of crude dsl in the protein 
kinase assays. Highly purified latent dsl Petryshyn et al, 1983 and 
elF-2 Levin et al, 1973 were prepared from rabbit reticulocyte lysates 
as described. Guanine nucleotide exchange factor GEF was a kind 
gift from Dr Robert L. Matts Oklahoma State University and prepared 
as described Matts et al, 1983. 
Protein phosphorylation assays 20 ul using 3T3-SIO extracts 30 ug 
and purified latent dsl were carried out under conditions previously described Petryshyn et al, 1983, 1984. All incubations were for 10 min 
at 30. Other additions or changes are as indicated in the figure. The 
reactions were terminated by the addition of SDS denaturing buffer 
Laemmli, 1970 and heated at 95 for 2 min. Proteins were separated 
on a 7.5 polyacrylamide-SDS gel and analyzed by autoradiography 
Petryshyn et al, 1984. 
Approximately 20 ng of 13HCTP labeled TAR/CAT mRNA was subjected to electrophoresis on a 1 agarose-formaldehyde gel, transferred to nitrocellulose paper Hybond-N, Amersham, and processed 
according to the manufacturer’s recommended procedure. Prehybridization, hybridization, and washes were performed essentially as described in Zinn et al 1983 except that dextran sulfate was omitted. 
Sense and anti-sense probes for the TAR region were transcribed by 
SP6 polymerase to a specific activity of *6 x lo* cpmlug. Hybridization was performed with ~1 x 1Oa cpmlml of either aF3*PTAR RNA 
DrObe.
The trans-inhibitory properties of the TAR region were investigated as a consequence of experiments designed to 
analyze the translational effects of secondary structure 
upstream of the poliovirus 5’ untranslated region UTR. 
There are two possible mechanisms by which 40s ribosomal subunits can initiate translation on eukaryotic mRNAs 
illustrated in Figure 1. It is thought that the majority of 
mRNAs initiate translation in a cap-dependent manner, 
whereby the 40s subunit binds at or near the S’cap structure and scans the 5’ UTR until an appropriate AUG initiation codon is recognized Figure 1, top left; see Kozak, 
1983. Recently, it has been shown that poliovirus and 
EMC virus mRNAs, which are naturally uncapped, initiate 
translation by a different mechanism. The 40s subunit 
can bind directly to an internal region of the picornavirus’ 5’ UTRs, bypassing upstream sequences and the requirement for the cap structure Figure 1, top right; Pelletier and 
Sonenberg, 1988; Jang et al, 1988. The presence of secondary structure in the 5’ UTR of cap-dependent mRNAs 
has been shown to inhibit translation Pelletier and Sonenberg, 1985. This inhibition is presumably caused by the 
reduced ability of the 405 subunit to bind and/or scan the 
5’ UTR Figure 1, bottom left. However, the introduction 
of secondary structure upstream of the poliovirus mRNA 
5’ UTR does not significantly impede protein synthesis since translation can initiate internally Pelletier and Sonenberg, 1988; Figure 1, bottom right. 
We generated hybrid mRNAs consisting of 5’ untranslated sequences derived from HIV-1 and poliovirus mRNA 
fused 5’ to the chloramphenicol acetyl transferase CAT 
coding sequence Figure 2A. The stem-loop structure of 
PLTAR and TAR constructs 2 and 3, Figure 2A was derived from the 5’ proximal 59 nucleotides of HIV-1 mRNAs 
Parkin et al, 1988. PLTAR is identical to TAR except that 
it contains an extra 23 nucleotides upstream of the TAR 
sequence originating from the polylinker region in the 
pSP64 plasmid and a synthetic Pstl linker compare constructs 2 and 3 in Figure 2A. When transcribed into 
mRNA, both PLTAR and TAR sequences are predicted to 
fold into the same stem-loop structure Figure 26. However, it is important that the cap structure is more accessible to the translational machinery in PLTAR than in TAR 
RNA. The parental vector construct 4 that was used to 
synthesize the poliovirus/CAT hybrid mRNAs constructs 
5 and 6 contains poliovirus sequences sufficient for 
promoting internal initiation Pelletier and Sonenberg, 
1988. 
In vitro transcription of the linearized templates Figure 
2A, primed with m7GpppG, yielded capped mRNAs that 
were analyzed for integrity on 1 agarose-formaldehyde 
gels. Autoradiography Figure 3A shows the presence of 
mostly a single RNA species the autoradiograph is overexposed that migrates at the appropriate size for the 
different mRNAs. It is significant that no radioactivity is detected on the top of the gel indicated by arrowheads or 
above the major RNA species where large extraneous 
RNA contaminants are sometimes present Schenborn 
and Mierendorf, 1985. Contamination by extraneous 
double-stranded RNA might have interfered with the interpretation of our data-see below. Messenger RNAs 
were translated in a rabbit reticulocyte lysate Figure 38. 
The presence of PLTAR 5’ to the CAT coding sequence 
significantly inhibited translation Sfold; compare lanes 2 
and 1. In agreement with an earlier study Parkin et al, 
1988, TAR/CAT mRNA translated less efficiently lo-fold 
than CAT mRNA compare lanes 3 and 1. A different 
translational hierarchy was obtained using mRNA that 
had the poliovirus 5’UTR inserted between the secondary 
structure sequence of TAR and the indicator CAT coding 
sequence Figure 2A, constructs 4-6. PLTARlpolioKAT 
mRNA was translated nearly as efficiently 1.3-fold less as 
polio/CAT mRNA Figure 3, compare lane 4 with lane 5, 
consistent with the scenario depicted in Figure 1. However, surprisingly, and at variance with the expected scenario Figure l, TAR/polio/CAT mRNA was poorly translated lo-fold less relative to polio/CAT compare lanes 4 and 6. All mRNAs had similar stabilities data not shown, 
and therefore the effect must have occurred at the translational level. These results led us to investigate a possible 
trans-inhibitory effect of the TAR region on translation. 
Wns-Inhibition: TAR Structural Requirements 
To investigate the possibility of a TAR-mediated trans inhibition of translation, we performed the following experiment: a reticulocyte translation lysate was preincubated with small amounts of TAR/CAT mRNA 350 nglml 
in the absence of protein synthesis, followed by translation 
of the indicator CAT mRNA. The presence of TAR/CAT 
mRNA during the preincubation step caused a marked inhibition lo-fold in the translation of added CAT mRNA 
Figure 4; compare lanes 1 and 2. In a control experiment, preincubation in the absence of added mRNA lane 
1 or in the presence of CAT mRNA lane 3 did not result 
in any detectable reduction in CAT yield. It is important to 
note that the amount of mRNAs present during the preincubation step were approximately 20-fold less than the 
subsequently added indicator CAT mRNA. Consequently, 
the yield of CAT protein synthesized in the different reactions is almost exclusively a result of the indicator CAT 
mRNA. This is demonstrated by the fact that the low levels 
of CAT mRNA present during the preincubation step compare lane 3 with lane 1 did not cause a detectable increase in the total synthesis of CAT protein. To examine 
the possibility that the inhibitory effect mediated by TAR/ 
CAT mRNA is due to its unique stem-loop structure Figure 28 we performed similar experiments in the presence of mRNA containing mutations in the TAR region 
Figure 4; lanes 4-6. These mutants were created by site-directed mutagenesis Figure 4, top, and their translational efficiencies have been previously reported Parkin 
et al, 1988. Two mutants TAR3 and TAR3R exhibiting reduced secondary structure, as compared with wild-type 
TAR, were considerably less potent in eliciting the trans-inhibitory effect Figure 4; compare lanes 4 and 5 with 
lane 2. However, the mRNA generated from the double 
mutant TARSR3/CAT, which restores the wild-type stem 
structure, was as effective as TAR/CAT mRNA in trans-inhibition compare lanes 6 and 2. These results demonstrate that secondary structure, rather than the nucleotide 
sequence of TAR, is important for the inhibitory activity. 
The importance of a stable stem-loop structure notwithstanding, it is not the only criterion necessary for effective 
trans-inhibition by the TAR region. PLTARKAT mRNA, 
which has a secondary structure similar to TAR/CAT 
mRNA, but with an accessible cap structure Figure 2B, 
exhibited low trans-inhibitory activity Figure 4, compare 
lanes 7 and 2. This result suggests that cap accessibility 
is also a strong determinant in effecting trans-inhibition. 
Reversal of Pans-Inhibition by poly I-poly C 
and Guanine Exchange Factor GEF 
TAR/CAT mRNA begins to lose its effect as a trans-inhibitor of protein synthesis at a concentration below approximately 50 nglml Figure 5, lanes l-7. For example, 
at a concentration of 1 pglml TAR/CAT mRNA lane 2 
there is a lo-fold reduction in CAT synthesis compared 
with only a 2.2-fold inhibition in the presence of 50 nglml 
TAR/CAT mRNA lane 5. Considering that the length of 
the TAR/CAT mRNA is ~1600 nucleotides and that of the 
TAR stem-loop structure 59 nucleotides Figure 28 this 
would imply an effective concentration of TAR in the range 
of approximately 2 nglml. It has been observed that low 
levels of double-stranded RNA strongly inhibit protein synthesis Ehrenfeld and Hunt, 1971. This inhibition is due to 
activation of a double-stranded RNA-dependent protein 
kinase dsl, which phosphorylates the a subunit of elF-2 
Farrell et al, 1977; Levin and London, 1978; Samuel, 
1979. One approach to determine that dsl is involved in 
the inhibition of protein synthesis is the reversal of inhibition by high levels of poly I.poly C. In a seemingly 
paradoxical fashion, high levels m25 pglml of double-stranded RNA prevent the activation of dsl by low levels 
of double-stranded RNA Hunter et al, 1975. When 
TAR/CAT mRNA was pre incubated in the presence of 25 
pglml poly I.poly C, &a&s-inhibition was no longer detected Figure 5, compare lanes 8 and 2. The addition of 
poly I.poly C alone during the preincubation step had 
no effect on CAT mRNA translation compare lanes 9 and 
1. The trans-inhibition by TAR/CAT mRNA increased with 
longer preincubation time 1.5fold lane lo, 2.5fold lane 
111, and lo-fold lane 21. This observation is also consistent with the observed lag period required to attain a sufficient level of dsl activation and elF-Pa phosphorylation, 
which precedes inhibition of protein synthesis Hunter et 
al, 1975. 
To further support the contention that dsl is activated by 
the TAR region, we examined the ability of guanine exchange factor GEF to reverse the translational inhibition 
Matts and London, 1984. The mechanisms by which dsl 
mediates protein synthesis inhibition are partly known 
reviewed in London et al, 1987. elF-2 enters the initiation 
cycle as a complex with GTP and, once the cycle is completed, elF-2 is released as an inactive binary complex 
bound to GDP which is exchanged for GTP by GEF for another round of initiation. Phosphorylation of the a subunit 
of elF-2 by dsl diminishes the ability of GEF to catalyze the 
exchange reaction, and leads to inhibition of protein synthesis. The inhibition can be overcome by the addition of 
exogenous GEF Matts et al, 1983. The addition of GEF 
at the end of the preincubation step completely reversed 
the inhibitory effect observed in the presence of TAR/CAT 
mRNA Figure 6; lanes l-3. The addition of GEF to a 
reaction mixture containing CAT mRNA that was not preincubated with TAR/CAT mRNA resulted in only a 10 increase in translational efficiency data not shown. These 
results indicate that phosphorylation of the a subunit of 
elF-2, which prevents the recycling reaction, is the cause 
of Warts-inhibition. TAR/CAT mRNA can also inhibit in 
trans the translation of polio/CAT mRNA Figure 6, compare lane 5 with lane 4, and this inhibition was also completely alleviated by GEF lane 6. These results indicate 
that both cap-dependent and cap-independent lane 7; 
see also Pelletier et al, 1988 translations see Figure 1 
are amenable to trans-inhibition by TAR. This is an expected result, in light of elF-2 requirement for the translation of all mRNAs. The ability of GEF to reverse the TAR-mediated inhibition was specific inasmuch as all the other 
purified initiation factors elF-4A, elF-4B, elF-4F, and elF-3 with the exception of elF-2, could not relieve this inhibition data not shown. 

Direct evidence for TAR-mediated activation of dsl and 
phosphorylation of elF-2a was obtained Figure 7. Protein 
kinase assays containing crude Figure 7A; Petryshyn et 
al, 1984 or highly purified Figure 78; Petryshyn et al, 
1983 preparations of dsl were carried out in the presence 
and absence of the various RNAs and Y-~~PATP All assays were supplemented with elF-2 Petryshyn et al, 
1984. Phosphoproteins were resolved by SDS-poly-acrylamide gel electrophoresis and visualized by auto-radiography. In control assays, the addition of low levels 
of double-stranded reovirus RNA dsReo resulted in the 
phosphorylation of dsl and elF-Pa Figures 7A and 78, 
compare lanes 1 and 2. A similar phosphorylation of dsl 
and elF-Pa was observed in the presence of TAR/CAT 
mRNA Figures 7A and 78, lane 5, but no or little phosphorylation was observed with CAT or PLTARKAT mRNAs 
Figures 7A and 78, lanes 3 and 4, respectively. The 
phosphorylation of dsl and elF-2a mediated by TAR/CAT mRNA was prevented when high concentrations of poly 
Ipoly C 25 fig/ml were included in the assays data not 
shown. The specific activation of dsl by the TAR/CAT 
mRNA and the subsequent phosphorylation of elF-2a observed in these experiments most likely explains the TAR/ 
CAT mRNA-mediated trans-inhibition of translation Figures 4 and 5. In other experiments addition of native polio, 
TMV, or globin mRNAs or tRNA at similar concentrations 
resulted in no phosphorylation of dsl and elF-Pa data not 
shown. 
We wanted to rule out the possibility that small amounts 
of double-stranded RNA might have contaminated our 
mRNA preparations and were responsible for the activation of dsl and subsequent phosphorylation of elF-2a and 
inhibition of protein synthesis. This seemed a priori an unlikely possibility for the following reasons: First, significant 
levels of large extraneous double-stranded RNA transcripts are only produced during in vitro transcription 
when templates digested with restriction enzymes that 
leave 3’ protruding ends are used Schenborn and Mierendorf, 1985. We used BamHl 5’ protruding end to digest the templates and, as expected, did not detect any 
radioactivity above the major RNA species Figures 3A 
and 8A. Second, the mutants, TAR3/CAT and TAR3R/ 
CAT, in which there is only a four nucleotide difference 
in the entire template sequence, did not result in mRNA 
preparations that could trans-inhibit protein synthesis, 
whereas the double mutant TARBRBlCAT did Figure 4. 
Moreover, PLTARlCAT mRNA, which contains the entire 
sequence of TAR/CAT mRNA in addition to 23 extra 
nucleotides, did not trans-inhibit protein synthesis Figure 
4 nor did it activate dsl Figure 7. Notwithstanding the 
strength of these considerations, we performed the following experiments to rule out the possibility of double-stranded RNA contamination. First, we subjected the 
Spa-derived mRNAs to gel purification. Figure 8A shows 
an autoradiograph of TAR and TAR/CAT mRNA before 
purification lanes 1 and 2, respectively and after purification lane 3 and 4, respectively. It is clear from this analysis that no detectable contaminant was removed by the 
purification. Importantly, the gel-purified RNAs exhibited 
the same effect on the phosphorylation of dsl and elF-2, 
in that only the TAR/CAT mRNA mediated the phosphorylation Figure 78. Furthermore, the TAR/CAT mRNA retained its ability to trans-inhibit translation data not 
shown. 
In a second experiment, we probed directly for the presence of anti-sense RNA originating from the TAR/CAT SP6 
construct. Figure 88 shows a Northern blot of non-gel-purified TAR/CAT mRNA probed with anti-sense lane 1 or 
sense lane 2 TAR RNA probe. We could not detect any 
signal when probing with the sense probe, even after a 
long overexposure. We conclude that trans-inhibition and 
phosphorylation of dsl and elF-2a were not the cause 
of contaminating double-stranded RNA in the TAR/CAT 
mRNA preparation.
The results in this paper demonstrate that a unique 
stem-loop structure present at the E/proximal end of HIV-l 
mRNAs can activate the cellular double-stranded RNA-dependent kinase, resulting in frans-inhibition of translation. The presence of secondary structure in the 5’ un- 
translated region of eukaryotic mRNAs can also inhibit 
translation in cis by a different mechanism eg, Pelletier 
and Sonenberg, 1985. We suggest a novel translational 
regulatory phenomenon that can link these separate inhibitory mechanisms. Thus, an intriguing aspect of this 
work is the interdigitation between the translational control pathways involving two initiation factors, elF-4F and 
elF-2. 
 
The role of elF-4F is to bind the S’cap structure, and then, 
in conjunction with two other initiation factors elF-4A and 
elF-4B, to mediate the ATP-dependent unwinding of 5’ 
proximal mRNA secondary structure Ray et al, 1985; 
reviewed in Edery et al, 1987; Sonenberg, 1988. This 
helicase activity underlies the stimulatory effect of the cap 
on 40s subunit binding to mRNA. It is significant that elF-4F is present in limiting amounts in the cell Duncan et al, 
1987 and acts at the rate-limiting step in translation Jagus et al, 1981. As a result, the activity of elF-4F can 
regulate translational efficiency. Therefore, it is conceivable that under conditions whereby elF-4F activity is impaired, an increase in the effective concentration of 
double-stranded RNA will occur. This increase in RNA 
secondary structure could lead to the activation of the latent dsl followed by inactivation of elF-2 recycling. This 
model is shown in Figure 9. 
There are several examples which suggest that this can 
occur in vivo. In cells exposed to elevated temperatures, 
the activities of elF-2 and elF-4F are impaired. Inhibition 
of elF-2 activity is caused by the phosphorylation of its a 
subunit Ernst et al, 1982; Duncan and Hershey, 1984, 
whereas the inhibition of elF-4F activity is correlated with 
dephosphorylation of the 24 kd subunit of elF-4F Duncan 
et al, 1987. Significantly, the addition of elF-4F to extracts 
prepared from heat-shocked Ehrlich cells not only stimulated translation to control levels, but, surprisingly, also 
enhanced the activity of elF-2 Panniers et al, 1985. This 
observation can be explained by our proposed model. Another example of a condition leading to translational 
repression where elF-4F and elF-2 activities are both inhibited is serum deprivation Duncan and Hershey, 1985. 
The control of elF-2a phosphorylation by elF-4F does 
not exclusively implicate a general inhibition of protein 
synthesis. De Benedetti and Baglioni 1984 invoked local 
activation of dsl leading to selective inhibition of mRNA 
translation. This contention is further supported by the 
specific translational increase in mRNAs derived from 
transfected DNA under conditions that antagonize dsl activation Kaufman and Murtha, 1987; Akusjarvi et al, 
1987. Since dsl requires dsRNA for its activation Levin 
and London, 1978, local activation would presumably occur when dsl interacts with the secondary structure elements present on that mRNA. Local effects on translation 
might be maintained in the cell by compartmentalization 
or anchoring of certain factors to the cytoskeletal framework Howe and Hershey, 1984. It is possible that dsl 
binds the TAR region and locally catalyzes elF-Pa phosphorylation, causing a selective downregulation of HIV-1 
expression. Since all HIV-1 mRNAs contain the TAR region, they could be coordinately regulated. 
 
It is presumed that most mRNAs have base-paired 
regions. However, it is unlikely that the majority of mRNAs 
can activate dsl. The results here Figure 4 indicate that 
the presence of a stable stem-loop structure is necessary 
but not sufficient to activate dsl. This is best illustrated by 
comparing TAR/CAT and PLTARlCAT mRNAs. The latter 
mRNA is predicted to have a similar stem-loop structure 
to TAR/CAT except that its cap structure is significantly 
more accessible Figure 28. There are two likely explanations, which are not mutually exclusive, for the inability of 
PLTARlCAT mRNA to activate dsl significantly. First, the 
secondary structure in PLTARlCAT cannot be recognized 
by dsl. This is consistent with the fact that PLTARlCAT 
does not activate dsl, even in the absence of initiation factors that are required to unwind the TAR secondary structure Figure 7B. Similarly, although tRNA, rRNA, and VAI 
RNA Monstein and Philipson, 1981 have extensive regions of secondary structure, they do not activate dsl 
Kitajewski et al, 1986. A second possibility is that increased accessibility of the cap structure in PLTARlCAT 
may increase the efficiency of elF-4F mediated unwinding, thus causing a reduction in dsRNA structure. Cross-linking experiments have shown that the cap structure of 
PLTARlCAT mRNA is more accessible to elF-4F than the cap structure of TAR/CAT mRNA N. Parkin, personal 
communication. HIV-1 may have evolved a highly specialized stem-loop structure in its mRNAs to activate dsl. The 
difficulty in unwinding the TAR region by the initiation factors may contribute to its effectiveness as an activator 
of dsl. 
Double-stranded mRNA regions that activate dsl do not 
have to be contained exclusively in the 5’ UTR, as is the 
case for the TAR element of HIV-l. Baum and Ernst 1983 
reported that a crude preparation of cytoplasmic polyA+ 
mRNA could phosphorylate elF-2 in vitro. Pratt et al 
1988 extended these findings by showing that polysomal 
mRNA could cause elF-Pa phosphorylation. The elements that mediates the phosphorylation of elF-2a was 
not investigated in these reports. It is possible that sequences in the coding or 3’ untranslated region could also 
activate dsl. 
One important aspect of our findings lies in the possible 
implications for the regulation of the life cycle of HIV-l. It 
was postulated that active dsl can specifically induce the 
synthesis of p-interferon Zinn et al, 1988. Thus, phosphorylation of dsl mediated by the TAR secondary structure can have important consequences in HIV-1 infected 
cells. If HIV-l mRNAs have the capacity to induce interferon production in vivo, it could serve in an attempt to establish an anti-viral state in individuals infected with the 
AIDS virus. Moreover, interferon can suppress the production of growth factors required for cell proliferation reviewed in Pestka et al, 1987. The establishment of an 
anti-viral state and/or the inability of cells to proliferate 
could contribute to the repression of virus replication. 
In summary, we show that HIV-l mRNAs have a unique 
structure that can activate dsl, which catalyzes elF-2a 
phosphorylation. Here we report a specific mRNA sequence that has this capacity. This may have profound implications for translational control in general, and HIV-l 
replication in particular.