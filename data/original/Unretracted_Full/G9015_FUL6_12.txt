Various pharmacologic preparations have been prescribed for preservation of the acutely ischemic myocardium, l and increasing attention has been directed to the use of calcium antagonists for this purpose. Administration of diltiazem to dogs during coronary occlusion decreases consumption of adenosine triphosphate in the ischemic region, reduces the inhibition of anaerobic glycolysis and lowers tissue levels of lactic acid and free fatty acids, thus minimizing the consequences of acute ischemic damage 2.
Myocardial ischemic injury is reportedly associated with accumulation of intracellular calcium 3-5. Calcium antagonists are capable of mediating protective effects against the ischemic insult by blocking the inward movement of calcium into the muscle cells. These antagonists are potent vasodilators acting directly on the coronary vascular bed to relax coronary smooth muscle 6,7. Several investigators a1o have observed an increase in regional myocardial blood flow with diltiazem using the radionuclide-labeled microsphere technique in the canine heart subjected to acute or chronic myocardial ischemia. However, these investigators failed to show an obvious improvement of function in the corresponding area, and this discrepancy has been attributed to the inherent negative inotropic effect of the drug on the myocardium as an excitation-contraction uncoupler, which would offset the effect of increase in blood flow. 
This study was designed to elucidate the effect of
diltiazem on the regional myocardial function of conscious
dogs with induced acute myocardial ischemia. We
analyzed the effect of this drug on the time course of the
dissipation of post-stimulation potentiation and the
evolution of the segmental shortening of ischemic
myocardium induced by pacing stress under partial
coronary constriction, the phenomenon related to the transsarcolemmal movement of calcium.

Eleven dogs weighing 16 to
26 kg average 19 kg were anesthetized with sodium pentobarbital
25 mg/kg intravehously and underwent thoracotomy
performed in the left fifth intercostal space. The left
circumflex coronary artery was dissected free near its origin
and a hydraulic cuff was placed around it. A high fidelity
pressure micromanometer Konigsberg P-22 and a Silastic@
fluid-filled catheter outer diameter 1 mm were inserted into
the left ventricular chamber through the apex. A pacing wire
was sutured to the left atria1 appendage and to the right
ventricle. Two pairs of ultrasonic dimension gauges were implanted
within the subendocardium, one in a normally perfused
area of the anterior wall and the other in an area to be
rendered ischemic near the posterior papillary muscle. The
pericardium was left open and the wires were exteriorized to
the back of the dog. The experiments were performed with the
conscious dog lying quietly on its right side, at least 10 days
postoperatively when recovery from the preparative surgery
was complete 11J2.
Recordings were made
during each experiment on an eight channel forced ink oscillograph San-Ei Instruments model 142-8 and also on magnetic
tape with use of a TEAC R-260 data recorder for subsequent
analysis. The signals from the two pairs of segment
lengths and left ventricular pressure derived from both the
micromanometer and the fluid-filled catheter were recorded
simultaneously. The pressure recorded through the tube was
calibrated against a mercury manometer attached to a
Statham P-23Db strain gauge transducer that was calibrated
directly with a mercury manometer; the zero reference point
was at the mid point of the left ventricle. The micromanometer
was calibrated by adjusting its output to the pressure
obtained with the catheter. The first derivative of the left
ventricular pressure dP/dt was obtained using an active
differentiating circuit, and was calibrated against a triangular
wave of a known slope. Measurements of end-diastolic dimension
were taken at the nadir of the pressure tracing after
atria1 contraction at the time dP/dt crossed zero, and measurements
of end-systolic dimension were taken at the nadir
of the segmental tracing or approximately 20 ms before the
nadir of negative dP/dt. The measured values were normalized
to a 10 mm initial dimension by dividing the observed length
by the control end-diastolic length and multiplying by 10. This
method of normalization was necessary to compare data in
different dogs, because the distance between pairs of crystals
was variable and arbitrary and represents the actual values
as a constant fraction of the end-diastolic length. Stroke
segmental excursion from end-diastole to end-systole was
corrected by dividing by end-diastolic length and expressed
as percent shortening.
After a
control recording during the resting state, the coronary artery
was partially constricted with an occluder to establish the
stable hypokinesia in the ischemic segment, according to the
method described by Tomoike et al 13 In brief, the hydraulic
cuff was gradually inflated with water using a power-driven
syringe during pacing at the rate of more than 180 beats/min.
When the shortening of the ischemic segment became reduced, pacing was stopped, leaving the stenosis constant. Once
this state of hypoperfusion was achieved, the occluder was not
manipulated for the remainder of the experiment. After a 10
minute interval, a second recording was obtained during
spontaneous sinus rhythm to confirm a stable state. Then, the
heart rate was suddenly increased by cardiac pacing to 220
beats/min. The high level of vagal tone in the conscious dogs
often precluded rapid atria1 pacing and right ventricular
pacing was substituted in 8 of 11 dogs studied. The pacemaker
was turned off after 2 minutes of pacing and measurements
of hemodynamic variables and regional myocardial function
were continued for another 5 minutes.
After confirmation that all the measures had
returned to the prepacing state, diltiazem, 0.4 mg/kg, was
injected intravenously over 5 minutes. Twenty minutes later
cardiac pacing was again carried out exactly as in the first run.
If there was any instability or excitement in the dog during
the procedure, the control data were discarded and studies
were repeated the next day. The effects of diltiazem on the
evolution of post-stimulation potentiation was assessed in
comparison with the change in the first pacing run. For this
comparison, the values of dP/dt and percent shortening of the
two myocardial segments were normalized so that the values
during stable stenosis were always 100 percent.
The data comparing the resting value
during coronary stenosis with serial changes after cardiac
pacing and the control pacing data with the data obtained
after administration of diltiazem were analyzed using a two
way analysis of variance with repeated measure and a Newman-
Keuls multiple comparison test using a DEC PDP-11
Computer i4. Statistical comparisons between the control value
in the basal resting state and during coronary stenosis were
made using a paired t test. A probability p value of less than
0.05 was considered significant in each procedure. All results were expressed as mean + standard deviation.

With partial
coronary constriction, the heart rate increased by an
average of 15 beats/min. There was no significant
change in left ventricular pressure or in peak derivative
of ventricular pressure dP/dt. The end-diastolic length
of both the control and ischemic segments tended to
increase but these differences were not statistically
significant, Percent shortening remained the same in
the control segment but decreased from 19.9 to 16.4
percent p <0.05 in the ischemic segment Fig 1 to 3.
The values for peak dP/dt and percent shortening of
both segments during stable coronary stenosis were then
normalized so that control values before any intervention
were always 100 percent Table I, Fig 4 and 5. With left atrial or right ventricular
pacing, the heart rate was increased from 99 to 222
beats/min. In three dogs in which left atria1 pacing was
performed, ischemic segment shortening was further
reduced by 38, 40 and 47 percent, respectively, during
pacing Fig 2. In the remaining eight dogs, hemodynamic
and dimensional data during pacing were not
analyzed because of significant aberration of the contraction
pattern with ventricular pacing 1. the first contraction was
accompanied by a potentiation of dP/dt and of control
segmental shortening by an average of 39 and 22 percent,
respectively, over the control value during coronary
stenosis. The ischemic segmental shortening returned
to control value in the first postpacing beat;
however, it was gradually depressed in the subsequent
several beats, percent shortening being reduced to 61
percent of the control value before pacing in the second
postpacing beat and to 40 percent at 5 seconds, and returning
to the prepacing level in approximately 5 minutes.
End-diastolic length was not appreciably altered
in the postpacing beats in either control or ischemic
segments, whereas there was a significant increase in
end-diastolic pressure relative to dimension, indicating
the upward shift in the left ventricular diastolic pressure-
length curve during pacing-induced ischemia
Fig 1, Table I. At 20
minutes after intravenous administration of diltiazem during coronary stenosis, the heart rate remained the
same, although there was a marked delay in atrioventricular
conduction as shown in the prolonged atria1
kick in Fig 3. Peak dP/dt tended toward a decrease 95
percent of the control value, but this change was not
statistically significant. There was no significant change
in segmental dimension and function of either the
control or the ischemic segments during the resting state
and during coronary stenosis after administration of
diltiazem. Hypokinesia induced during pacing was
lessened in all three dogs receiving atria1 pacing Fig
2. The potentiation of peak dPldt in the first postpacing
beat was significantly lower than in the control
period 21 percent over the control value. In the ischemit
segment, the shortening of the first postpacing
beat was similar to that seen in the control pacing period;
however, the subsequent deterioration during the
initial 5 seconds was greatly lessened. The percent
shortening was increased from 11.1 to 13.7 percent in
the second postpacing beat and from 7.4 to 10.6 percent
in the beats at 5 seconds after termination of pacing.
The ischemic segmental shortening returned to the
prepacing level within 1 minute. Changes in postpacing
beats in the control segments were not affected by diltiazem
Fig 3 and 4. The pacing-induced changes in
left ventricular diastolic pressure-length relation were
not significantly different from those in the control
period Table I. In five dogs, the experiment was repeated on
a different day with the same protocol, but saline solution
was injected instead of diltiaxem Fig 5. The dP/dt
and percent shortening of both the control and ischemic
segments did not differ significantly in these successive
pacing studies, thus verifying the stability of the induced
coronary stenosis.

An experimental model relevant to clinical coronary
insufficiency has been developed by partially constricting
the coronary artery in conscious dogs. Cardiac
pacing during limited coronary reserve effectively induced
a localized disturbance in myocardial function
as a result of an imbalance between myocardial oxygen
supply and demand. Although we have not yet performed
measurements of regional myocardial blood flow
using radioactive microspheres, others 17 demonstrated
that myocardial blood flow failed to increase in deeper
myocardial layers in response to the increased myocardial
oxygen demand of atria1 pacing in the region
supplied by the critically stenosed vessels. In this study,
hypokinesia induced during pacing lessened after administration
of diltiazem in three dogs with atria1 pacing.
However, the high level of vagal tone of the awake
dogs necessitated right ventricular pacing to increase
heart rate in the remaining eight dogs. Thus the analysis
was made only in the postpacing beats because the
detrimental effect of the ventricular pacing on cardiac
performance influenced the true response during rapid
cardiac pacing. Mahler et
alls reported that there was early potentiation of dP/dt after cessation of rapid cardiac pacing, which appeared
to decay in an exponential manner over subsequent
several beats. The shortening characteristics of normal
myocardium were also shown to follow the typical dissipation
pattern of this potentiation, whereas in the
ischemic segment, although shortening was improved
with this postpacing potentiation in the initial beats,
there followed a subsequent severe deterioration of
shortening associated with dissipation of this effect 13.
Our study demonstrated that diltiazem greatly lessened
the sustained postpacing deterioration in shortening of
the ischemic myocardium, while it exerted no significant
effect on the ischemic segmental shortening of the first
postpacing beat despite a substantial decrease in dP/
dt. Two major mechanisms have been postulated to
explain the post-stimulation potentiation. One is the
Bowditch force-treppe, which facilitates the transmembrane
calcium influx or accumulation into the
myocardial cell with increase in the frequency of contraction 
1g-21. The other is the Woodworth phenomenon,
the “recuperative effect of long pause” which has been
suggested to represent increased calcium release from
bound sources around or within the fiber itself 22-24.
In the experiment with isolated cardiac muscle,
diltiazem was shown to block this transmembrane calcium
influx and to inhibit a certain intracellular calcium-
releasing mechanism. As a result of inhibition of
excitation-contraction coupling, the drug exerts a negative
inotropic effect in vitro, 25 whereas Himori et al 26
demonstrated that diltiazem exerts a dual action depending
on the dose namely, it caused a slight but
dose-related increase in contractility in a smaller dose.
They concluded that an increase in blood flow might
produce a positive inotropic effect that was masked by
the negative inotropic action of a large dose. In this study, the amplification of dPldt of the first
postpacing beat was substantially decreased after
diltiazem, thereby indicating that the dosage we used
was relatively large and that the intrinsic depressant
effect predominated the latter divergent action. The
lack of any significant effect of diltiazem on the ischemic
segmental shortening of the same initial postpacing beat
may be caused by the protective effect of the drug on
the ischemic injury by lowering the myocardial oxygen
requirement in proportion to the reduced contractility.
However, the severe sustained deterioration of ischemit
segmental shortening that rapidly ensued after
termination of the pacing was greatly alleviated by
diltiazem, although there was no appreciable change in
the sequence of control segmental function. This postpacing
deterioration may be related to the interaction
of several phenomena: 1 the rapid decay of poststimulation
potentiation, 2 sustained wall tension in
the ischemic myocardium due to a substantial increase
in the end-diastolic length with the disappearance of the
positive inotropic effect of post-stimulation potentiation,
or 3 the slow recovery from ischemia due to the
sustained coronary stenosis 13.
Several mechanisms by which calcium antagonists
could mediate protective effects in the presence
of myocardial ischemia have been postulated.
Calcium antagonists inhibit excitation-contraction
coupling and reduce mechanical tension associated with
resultant accumulation of high energy phosphates in the
cardiac muscle. As a consequence of a decrease in the
consumption of adenosine triphosphate, oxygen requirement
is also reduced 27. These agents have a potent
vasodilating effect on the coronary artery by producing
“musculotropic” relaxation of smooth muscle cells
through an inhibition of the transmembrane calcium
supply to the contractile system of the vascular wa11 28.
The increased perfusion of the ischemic myocardium
after administration of diltiazem has directly been
demonstrated by the studies with radioactive microspheres.
s’O Unlike nifedipine, which produces dilation
of a small restrictive artery, the vasodilating effect of
diltiazem involves the large conductance artery as
we11, 2g and the retrograde flow in the chronically stenosed
coronary artery is increased and causes a redistribution
of intramyocardial blood flow, as is the case
with nitroglycerin 30.
Shen et ah 3 indicated that the primary event leading
to irreversibility of myocardial ischemic injury is the
sarcolemmal defect that allows excess calcium to enter
the injured cell, or development of a structural defect
in the plasma membrane such as that occurring when
the sarcoplasmic calcium concentration increases appreciably
after reflow. Henry et al 4 observed in isolated
hearts perfused at reduced flow a progressive contracture
associated with an accumulation of calcium in
mitochondrial cell fraction prepared from ventricular
myocardium. This myocardial contracture and accumulation
of calcium were shown to be prevented by the
calcium-antagonizing effect of nifedipine with promotion
of mechanical recovery after reperfusion 4. Similarly,
diltiazem was reported to lessen the ischemic injury
by diminishing the breakdown of adenosine triphosphate,
and lowering the tissue levels of lactic acid
and free fatty acids, which reduced the tissue acidosis
with a resultant increase in the enzymatic activity
necessary for the metabolism of the tissue 2. All of these
combined effects of diltiazem promote rapid recovery
from ischemia and may account for the lessening of
post-stimulation deterioration of the ischemic myocardial
shortening. 
