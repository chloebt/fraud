Systems for the typing and grading of cancerous and 

precancerous lesions can only be clinically useful <\connective form=if text=if id=F1535_FUL6_12.txt.raw_196 relation=Contingency\> they 

are reproducible between separate observers 1. In 

addition, the parameters considered in the histological 

assessment should be biologically meaningful, reflecting 

the malignant potential of the lesions 2. White patches 

of the oral mucous membrane leukoplakia have a 

well-documented potential to develop into squamous 

cell carcinomas OSCCs 3,4 and in this respect, the 

histological finding of dysplasia is of particular prognostic importance 5–7. Approximately one in ten 

leukoplakias are histologically classified as dysplasias 

6,7, but no current marker reliably predicts the clinical 

outcome of dysplastic white patches 8. With current 

therapeutic options, when OSCC presents clinically, it 

is too late for a cure in the majority of patients 9. The 

challenge is <\connective form=therefore text=therefore id=F1535_FUL6_12.txt.raw_242 relation=Contingency\> to identify those white patches 

that have a potential to develop into OSCC and which 

accordingly demand particular attention 10. 

Several studies have been published that demonstrate low intra- and inter- observer agreement in the 

subjective grading of oral preneoplastic lesions 11–13. 

Attempts have been made to improve the prognostic 

value of histological grading, using simplifying grading 

systems that have proved superior to the Broders’ 

grading, WHO and UICC classifications 11,14,15. 

Studies on the prognostic impact of grading have 

generally included a standard tutorial and prediagnostic calibration of participating pathologists, 

<\connective form=but text=but id=F1535_FUL6_12.txt.raw_80 relation=Comparison\> this is seldom possible in diagnostic practice. 

Diagnostic shortcomings make reliable prognostication 

of oral preneoplastic lesions difficult, which in turn 

makes treatment planning inefficient. 

Growing evidence reveals molecular mechanisms by 

which alterations in large-scale genomic status DNA 

ploidy may constitute early and causative events of 

carcinogenesis 16–21. These findings all point to 

large-scale genomic status as a prognostic factor of 

major significance in cancer development. The present study quantifies the inter-observer 

agreement among experienced but uncalibrated pathologists in the grading of oral dysplasia. In previous 

efforts to investigate large-scale genomic status as a 

prognostic marker in oral preneoplastic lesions 22–25, 

either low-resolution image cytometric systems were 

employed, few cases were investigated, overt carcinomas were included in the study, or no follow-up time 

was stated. The present study compares the prognostic 

value of large-scale genomic status by a high-resolution 

image cytometric system with that of simplified 

histological grading, in a large series of patients with 

oral dysplastic lesions followed for a considerable time 

on average, close to 9 years. 


One hundred and ninety-six cases of leukoplakia 120 

males, mean age 70.1 years, range 32.4–81.0 years; 76 

females, mean age 64.7 years, range 44.3–80.1 years 

from oral lesions histologically defined as dysplasia 

precancerous lesion of squamous cell epithelium 

characterized by cellular atypia and loss of maturation 

short of carcinoma in situ 5 were obtained from the 

archives of the Department of Pathology, University of 

Bergen and the Department of Pathology and Forensic 

Odontology, University of Oslo, collected between 

1976 and 1995. 

To avoid skewing of data, care was taken not to 

include patients with previous or concomitant clinical 

features of erythroplakia in addition to leukoplakia, as 

erythroplakias are generally associated with a particularly high occurrence of cancer at least 90 7. In 

Norway, all oral white lesions histologically typed as 

any grade of dysplasia are reported to the Norwegian 

Cancer Registry; the use of personal identification 

numbers and updated national registers for premalignant and malignant lesions, as well as place of 

residency, offers the opportunity to monitor patients 

with complete data on morbidity and mortality. 

Of the 196 cases with dysplastic leukoplakia, 36 

cases previously or simultaneously diagnosed with 

carcinoma-in-situ CIS or carcinoma of the oral 

cavity or upper aerodigestive tract were excluded 

from the survival analysis, as these patients are prone 

to develop a second primary carcinoma 26. Of the 

remaining 160, there existed ample tissue blocks for 

150 cases 81 males, mean age 68.4 years, range 

32.4–80.6 years; 69 females, mean age 64.9 years, 

range 45.7–73.4 years, as ten cases were from archival 

material prior to 1981, when unbuffered formaldehyde 

was used as fixative. The blocks were <\connective form=therefore text=therefore id=F1535_FUL6_12.txt.raw_243 relation=Contingency\> not 

suitable for monolayer preparations. SNOMED codes 

27 for the localization of the biopsies and the 

subsequent carcinomas were used to verify close spatial 

relationships between the dysplasias and the subsequent carcinomas. The SNOMED codings were 

confirmed by a standardized drawing provided with 

each patient file. The mean follow-up time for the 196 

cases that were analyzed only with respect to interobserver variation was 107 months range 4–178 

months. The mean follow-up time for the 150 cases 

in which both histological grading and large-scale 

genomic status were assessed for survival analysis was 

103 months range 4–165 months. 


All histological sections were reviewed by four separate 

pathologists working at three different institutions 

Department of Pathology, Haukeland Hospital, 

University of Bergen; The Norwegian Radium Hospital; and Department of Oral Pathology, University 

of Oslo. As there is no existing scheme for the 

histological grading of dysplasia that gives consistent 

and reproducible results, the dysplasias were subjectively graded as mild, moderate or severe, taking into 

consideration the criteria listed by WHO 5. According to this scheme, mild dysplasia is characterized by 

slight nuclear abnormalities, most marked in the basal 

third of the epithelial thickness and minimal in the 

upper layers, where the cells show maturation and 

stratification. A few, but no abnormal mitoses may be 

present, usually accompanied by keratosis and chronic 

inflammation. Moderate dysplasias display more 

marked nuclear abnormalities and nucleoli tend to be 

present, with changes most marked in the basal two-thirds of the epithelium. Nuclear abnormalities may 

persist up to the surface, but cell maturation and 

stratification are evident in the upper layers. Mitoses 

are present in the parabasal and intermediate layers, 

but none is abnormal Severe dysplasias show marked 

nuclear abnormalities and loss of maturation involving 

more than two-thirds of the epithelium, with some 

stratification of the most superficial layers. Mitoses, 

some of which are abnormal, may be present in the 

upper layers. The presence of some maturation and 

stratification of the cells in the most superficial parts of 

the tumor distinguishes the lesion from CIS 5. 


From every biopsy analyzed, two 50 mm sections were 

cut and enzymatically digested SIGMA protease, type 

XXIV, Sigma Chemical Co, St Louis, MO, USA for 

the preparation of the monolayers, according to a 

modified Hedley method 28. The cell nuclei were <\connective form=then text=then id=F1535_FUL6_12.txt.raw_67 relation=Temporal\> 

stained by the Feulgen-Schiff method 29. Measurements of large-scale genomic status were done by 

image cytometry, according to an established protocol 

30 figure 1. 

Monolayers were analyzed using a Zeiss Axioplan II 

microscope Zeiss, Oberkochen, Germany 40r/0.65 

with a 546 nm green filter and modified for computer 

control of the stage Prior HI52V2, Prior Scientific 

Instruments, UK. The microscope was equipped with 

a single chip digital camera C4742-95, Hamamatsu 

Photonics K.K., Hamamatsu, Japan with high resolution. The magnification was 1600r at a resolution of 170 nm 0.2 mm per pixel, 1024r1024 pixels with 

10-bit resolution 1024 grey levels per visual field. As a 

rule, 300–600 cell nuclei were measured and stored in 

galleries for each case; lymphocytes lower right 

corner, panel 7, figure 1 were included as internal 

controls. In oral epithelium, the size of epithelial nuclei 

far exceeds that of stromal and inflammatory cells, 

allowing reliable identification. All specimens were 

coded and the histograms were classified in a blinded 

manner by three different persons trained in the 

classification of DNA histograms. A lesion was 

classified as diploid <\connective form=if text=if id=F1535_FUL6_12.txt.raw_197 relation=Contingency\> only one G0/G1 2c peak was 

present, or <\connective form=if text=if id=F1535_FUL6_12.txt.raw_198 relation=Contingency\> the number of nuclei in the G2 4c peak 

did not exceed 10 of the total number of epithelial 

nuclei, or <\connective form=if text=if id=F1535_FUL6_12.txt.raw_199 relation=Contingency\> the number of nuclei with a DNA content 

greater than 5c did not exceed 1 of the total number 

of epithelial cells. A lesion was defined as tetraploid 

<\connective form=when text=when id=F1535_FUL6_12.txt.raw_94 relation=Temporal\> its G0/G1 4c peak was present together with its 

G2 peak 8c, or when the fraction of nuclei in the 

tetraploid region exceeded 10 of the total number of 

nuclei, without a corresponding S-phase. A lesion was 

defined as aneuploid <\connective form=if text=if id=F1535_FUL6_12.txt.raw_200 relation=Contingency\> non-euploid peaks were present, 

or <\connective form=if text=if id=F1535_FUL6_12.txt.raw_201 relation=Contingency\> the number of nuclei with a DNA content greater 

than 5c or 9c exceeded 1. 



The statistical analysis was done using the SPSS for 

Windows software SPSS release 9.0, Chicago, 1999. 

Inter-observer agreement was computed using Cohen’s 

kappa values and was calculated for the typing of 

all 196 cases and for the grading of the 150 cases in 

which assessment of large-scale genomic status was 

<\connective form=also text=also id=F1535_FUL6_12.txt.raw_246 relation=Expansion\> possible to perform. A kappa value of 0 means 

no agreement and a kappa value of 1 means total 

agreement. Bivariate correlation analysis was used to 

compute the correlation between grading and DNA 

ploidy for all the pathologists. The prognostic values 

of histological grading and DNA ploidy were calculated and compared with use of Kaplan-Meier estimates and log-rank tests. A case was censored <\connective form=if text=if id=F1535_FUL6_12.txt.raw_202 relation=Contingency\> death 

resulted from unrelated disease, or <\connective form=if text=if id=F1535_FUL6_12.txt.raw_203 relation=Contingency\> death occurred in 

the absence of progression to overt carcinoma. All 

statistical tests are two-sided and p values less than 

0.05 were considered to be statistically significant. For computing the degree of agreement among the four 

observers, all 196 dysplastic lesions were included in the grading study, but for the comparison of histological grading and large-scale genomic status in the 

survival analysis, only those 150 cases that were not 

preceded by or accompanied by CIS or OSCC were 

included. Of these, 38 cases altogether 25 <\connective form=later text=later id=F1535_FUL6_12.txt.raw_77 relation=Temporal\> 

developed OSCC and 29 19 later died of their 

cancer. 



The kappa values in the gradings performed by the 

four observers were in the range 0.17–0.33 when 196 

dysplastic lesions were graded as mild, moderate or 

severe Table 1A. The kappa values for the total of 

196 cases were in the range 0.21–0.31 when only two 

diagnostic groups were considered mild and moderate 

dysplasias grouped as low grade and severe dysplasias 

as high grade, constituting a presumptive favorable 

and poor prognostic group, respectively. Simplifying 

the grading scheme from three categories mild, 

moderate, and severe to two categories low grade 

and high grade did not significantly improve the 

kappa values p=0.41. The kappa values in the 150 

cases that were assessed both by histological grading 

and by assessment of large-scale genomic status by 

ICM were in the range 0.21–0.33 when three diagnostic 

groups were considered and in the range 0.27–0.34 

<\connective form=when text=when id=F1535_FUL6_12.txt.raw_99 relation=Temporal\> only two diagnostic groups were considered 

Table 1B. As was observed for the total of 196 

cases, there was no significant increase in the kappa 

values in this subgroup <\connective form=when text=when id=F1535_FUL6_12.txt.raw_100 relation=Temporal\> the number of diagnostic 

groups was reduced from three to two  p=0.47. 



Table 2 shows the correlation between the histological 

findings of the four pathologists and measurements 

of large-scale genomic status in 150 cases of oral 

dysplastic leukoplakia. For none of the observers was 

there any significant correlation between the large-scale genomic status and the grading done by the 

four pathologists Tables 2 and 3 p values in the range 

0.07–0.46. 


Figure 2 shows the disease-free survival curves for the 

gradings done by the four different observers. For 

none of the observers was there any significant 

prognostic value of grading observer 1: p=0.33; 

observer 2: p=0.28; observer 3: p=0.44; observer 4: 

p=0.31. There were no significant differences in the 

prognostic values of the gradings done by the four 

observers  p=0.39. 

Prognostic value of large-scale genomic status 

Consensus was reached in all observations by all three 

persons classifying the histograms depicting the 

distribution of large-scale genomic status panel 7, 

figure 1. The mean coefficient of variation CV of 

the diploid peak for all the 150 cases diploid, 

tetraploid, and aneuploid was 5.7 range 3.3–7.9. 

The assessment of large-scale genomic status produces 

significant prognostic data. <\connective form=when text=When id=F1535_FUL6_12.txt.raw_101 relation=Temporal\> the cases with 

diploid, tetraploid, and aneuploid lesions were compared, the diploid cases had a clearly favorable 

prognosis, with only three out of 103 cases subsequently developing a carcinoma, after follow-up times 

of 35, 46, and 76 months, respectively figure 3. The 

prognosis of cases with aneuploid lesions was significantly less favorable than those with tetraploid 

and diploid lesions  p=0.001, <\connective form=while text=while id=F1535_FUL6_12.txt.raw_194 relation=Comparison\> the prognosis 

of the cases with tetraploid lesions was significantly better that those with aneuploid lesions  p=0.005, 

data not shown and significantly less favorable than 

those with diploid lesions  p=0.01, data not shown. 

Mounting evidence now indicates that large-scale 

genomic changes constitute early and causative events 

in the carcinogenic process 16,17. Mutations in genes 

controlling chromosome segregation during mitosis and 

centrosome abnormalities have been shown to play a 

critical role in causing chromosome instability in cancer 

18–20, including oral cancer cells 21. Chromosomal 

aberrations consistent with disruption of normal chromosome segregation during mitosis have been shown to 

occur exclusively in aneuploid tumor cell lines 17. 

We have employed a simplified grading scheme for 

oral epithelial dysplasias according to the guidelines 

given by WHO 5 and compared the prognostic value of this with the single feature of large-scale genomic 

status. Our findings indicate that assessing large-scale 

genomic status by high-resolution image cytometry has 

a significant prognostic impact in oral cavity lesions 

that precede overt malignancies. 

Our findings contrast with those of another group 

31, who found that large-scale genomic status correlated well with the histological grading of preneoplastic 

lesions. <\connective form=however text=However id=F1535_FUL6_12.txt.raw_102 relation=Comparison\>, this was an experimental cancer 

model in mice that <\connective form=also text=also id=F1535_FUL6_12.txt.raw_247 relation=Expansion\> included frank carcinomas and 

the number of observations was limited. In addition, 

dysplasias were graded as low or high grade. <\connective form=although text=Although id=F1535_FUL6_12.txt.raw_36 relation=Comparison\> 

such a simplification did not improve the inter-observer 

agreement or prognostic impact in our own study, it 

could conceivably have contributed to the results of 

Munck-Wikland et al 31. 

The present study confirms the relatively high 

malignant transformation rate in dysplasias approximately 1 in 5 observed by other authors 6,7 and 

underscores the relevance of closely investigating the 

malignant potential of oral white patches. Other 

authors have demonstrated that DNA analysis by 

image cytometry may be a useful adjunct to histopathological evaluation 22–24,32. This contrasts with 

our findings, which clearly indicate that assessment of 

large-scale genomic status is superior to histological 

assessment as a prognostic marker. <\connective form=however text=However id=F1535_FUL6_12.txt.raw_103 relation=Comparison\>, in two 

of the studies 22,32, the resolution of the image 

cytometric system employed was considerably lower 

and the number of cases investigated was limited. In 

one study 23, invasive carcinomas were <\connective form=also text=also id=F1535_FUL6_12.txt.raw_248 relation=Expansion\> included. 

Conceivably, the grading of overt carcinomas may 

not be subject to the same degree of inter-observer 

variability as the grading of dysplastic lesions. The 

number of cases investigated in our study is considerably larger than in any previous efforts to compare 

DNA ploidy assessment and histological grading. In 

addition, in the present study, the assessment of 

large-scale genomic status was performed by a high-resolution image cytometry system and the number of 

cells sampled for evaluation >300 was larger than in 

any previous study. 

One possible objection to the design of our study is 

that we have not taken care to calibrate the observers 

before assessing their inter-observer agreement. No 

qualifications were required other than that the 

observers should have long experience in the typing 

and grading of oral mucosal lesions. Calibrating the 

pathologists through a pre-diagnostic briefing would 

most likely improve the observed kappa values. 

Whether it would <\connective form=also text=also id=F1535_FUL6_12.txt.raw_249 relation=Expansion\> improve the prognostic impact 

of grading in dysplasia is an open question. In our 

opinion, the approach of not briefing the pathologist 

prior to grading is reasonable, as it reflects the reality 

of diagnostic pathology. 

Our results may <\connective form=also text=also id=F1535_FUL6_12.txt.raw_250 relation=Expansion\> be adversely affected by the 

fact that biopsies of dysplastic mucosa taken adjacent 

to an invasive tumor may not contain tumor cells 

and may <\connective form=consequently text=consequently id=F1535_FUL6_12.txt.raw_74 relation=Contingency\> be classified as diploid in the 

DNA ploidy analysis. Given that most OSCCs start 

out as visible changes in the oral mucosa either 

leukoplakia or, more rarely, erythroplakia <\connective form=and text=and id=F1535_FUL6_12.txt.raw_178 relation=Expansion\> that 

biopsies for the most part include the bulk of the 

affected area, the chance of missing a malignant clone 

is small, <\connective form=but text=but id=F1535_FUL6_12.txt.raw_86 relation=Comparison\> the possibility of missing an aneuploid cell 

clone cannot be ruled out. 

The ability of image cytometry to detect small 

genomic changes is limited. The coefficient of variance 

CV of the diploid peak for all 150 cases diploid, 

tetraploid, and aneuploid was 5.7 range 3.3–7.9. 

This roughly corresponds to two chromosomes. 

Accordingly, small but significant changes point 

mutations, deletions, balanced translocations pertinent to carcinogenesis may go undetected. <\connective form=however text=However id=F1535_FUL6_12.txt.raw_104 relation=Comparison\>, 

compelling evidence indicates that large-scale genomic 

changes, crude as they may be, constitute early events 

in the carcinogenic process 16,17. Our results support 

such findings. 

White patches of the oral cavity commonly present 

as multiple synchronous or metachronous lesions. 

Biopsies may be obtained from only a subset of the 

lesions detected, which could result in missed aberrant 

lesions. <\connective form=however text=However id=F1535_FUL6_12.txt.raw_105 relation=Comparison\>, as the DNA diploid classification 

shows, this is most probably not the case, as 97 100 

of 103 of the diploid cases did not show malignant 

transformation during a follow-up time of almost 9 

years. Alternatively, our findings could be explained by 

a clonal origin of oral precancer 33. Conceivably, 

intraoral dispersion and subsequent implantation of 

viable tumor cells, or lateral intraepithelial migration, 

could give rise to autonomously growing cell clones. 

Accumulation of further genetic changes in daughter 

cells, providing growth advantages, could give rise to 

overt carcinomas 34. Hence, multiple lesions would have a common clonality and large-scale genomic 

status could predict the outcome for the patient, 

regardless of which white patch was biopsied. 

The size of the patches most likely influences the 

prognosis of white lesions 35, <\connective form=but text=but id=F1535_FUL6_12.txt.raw_88 relation=Comparison\> we have not been 

able reliably to reconstruct the size of the lesions, 

<\connective form=either or text=either id=F1535_FUL6_12.txt.raw_253 relation=Expansion\> from the biopsy blocks <\connective forme=either or text=or id=F1535_FUL6_12.txt.raw_253 relation=Expansion\> from the medical 

records. Accordingly, the prognostic impact of this 

parameter has not been investigated in the present 

study. Whether or how large-scale genomic status 

varies within a white patch is a matter that needs to 

be further investigated, <\connective form=but text=but id=F1535_FUL6_12.txt.raw_89 relation=Comparison\> as our results show, we are 

able to predict the clinical outcome of aneuploid 

lesions with a considerable degree of certainty 23/27, 

<\connective form=while text=while id=F1535_FUL6_12.txt.raw_195 relation=Temporal\> the negative predictive value of a diploid 

classification is even greater 100/103. This would 

indicate that <\connective form=if text=if id=F1535_FUL6_12.txt.raw_204 relation=Contingency\> variation in large-scale genomic status 

is present in cells of the white patches, the variation is 

spatially represented as a uniformly dispersed mosaic. 

The identification of epithelial cell nuclei from a 

monolayer preparation may be said to pose a problem, 

with hampered morphological control, as the native 

tissue structure is disrupted and the cell nuclei are not 

surrounded by cytoplasm. <\connective form=however text=However id=F1535_FUL6_12.txt.raw_106 relation=Comparison\>, in oral epithelium, 

the size of epithelial cell nuclei far exceeds that of 

stromal and inflammatory cells, with a diameter in the 

range of 10–15 mm unpublished data. We <\connective form=therefore text=therefore id=F1535_FUL6_12.txt.raw_244 relation=Contingency\> 

feel confident that it is mainly epithelial cells that are 

analyzed. <\connective form=however text=However id=F1535_FUL6_12.txt.raw_107 relation=Comparison\>, should stromal cells have been 

included, they would most likely be diploid and would 

<\connective form=therefore text=therefore id=F1535_FUL6_12.txt.raw_245 relation=Contingency\> dilute a possible aneuploid population, 

reducing the sensitivity of detection of aneuploid 

lesions. Again, the high positive predictive value of 

aneuploidy and the high negative predictive value of 

diploidy indicate that the possible adverse effect of including stromal or inflammatory cells is negligible. 

finally, in 20 of 150 cases 13 that were classified 

as tetraploid, the clinical outcome could not be predicted with any certainty figure 3. In these lesions, 

further cytogenetic investigation is deemed necessary. 

Alterations in chromosome 3p are possible candidates 

for further cytogenetic research 36. <\connective form=alternatively text=Alternatively id=F1535_FUL6_12.txt.raw_193 relation=Expansion\>, 

these lesions could be regarded as having a poor 

prognosis and treated <\connective form=accordingly text=accordingly id=F1535_FUL6_12.txt.raw_72 relation=Temporal\>. Given that 8 of 20 

cases 40 did not develop a carcinoma, this would 

imply an overtreatment of no more than 8 of 150 5 

patients. Since non-invasive treatment modalities for 

superficial premalignant processes are now available 

37,38, this may be considered acceptable. 

Despite the search for prognostic markers in oral 

precancers 8 and the introduction of new treatment 

modalities 37–42, the 5-year disease-free survival rate 

of OSCC has not improved considerably over the last 

40 years and is poor: 20–50, depending on clinical 

stage 9,43. The most likely explanation for this is 

that the malignancy is disseminated by the time of 

diagnosis, making curative measures less effective 9. 

We clearly need improved tools for identifying, at an 

early stage, lesions that will develop into OSCC. The 

assessment of large-scale genomic status in oral white 

patches seems to reveal biological information of 

prognostic significance in a group of lesions that 

precede overt malignancies and whose clinical outcome 

has been hard to predict by traditional pathological 

methods.