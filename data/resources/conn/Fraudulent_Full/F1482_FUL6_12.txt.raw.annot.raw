Oral cancer is a disfiguring disease that continues 

to increase in incidence, even among the young, 

and to an extent that cannot be fully explained by 

increased exposure to known risk factors. 



Despite extensive research on treatment modalities towards oral cancer, the 5-year survival 

rate of this disease has not been improved over 

the last 4—5 decades. 


This situation may change 

drastically during the next decade. Molecular targeting research has brought about a revolution in understanding disease processes. In oncology this is 

blurring the distinction between malignancy and 

premalignancy, as previously defined from clinicopathological findings. 


<\connective form=consequently text=Consequently id=F1482_FUL6_12.txt.raw_73 relation=Contingency\>, the distinction between cancer therapy and prevention 

will <\connective form=also text=also id=F1482_FUL6_12.txt.raw_257 relation=Expansion\> become less clear. 

With increased understanding of cancer biology, 

and exact knowledge of molecular pathways critical for carcinogenesis, a basis for improved handling of malignant disorders has evolved. 


Treatment of leukemia, AIDS and tuberculosis 

shows that combinatorial strategies afford 

enhanced clinical response with diminished probabilities of developing drug resistance. 



One may 

likewise conjecture that combinatorial preventive 

measures could impinge upon or compensate for 

several genetic and biochemical alterations that 

initiate and sustain neoplasia. 

This review will highlight some important advances in targeted treatment, and emphasize the ways 

in which molecular biology is likely to affect future 

therapies for oral cancer. 



Chemoprevention is the use of pharmacologic or 

natural agents that inhibit the development of 

invasive cancer. These work either by blocking the 

DNA damage that initiates carcinogenesis or by 

arresting or reversing the progression of premalignant cells in which such damage has already 

occurred. 



With the rapid advances in understanding of what causes cancer and the consequent 

ability to provide genetic diagnosis of susceptibility, 

there is a need to find agents that can effectively 

revert, stop or at least delay the carcinogenic 

process. 


For cancer chemoprevention, the 

definitive question—whether an agent reduces the 

incidence of invasive cancer—typically takes many 

years, thousands of phase III trial participants, and 

substantial financial effort to answer. 

In search of quicker, preliminary answers, clinical prevention researchers are increasingly focusing their efforts on intraepithelial neoplasia IEN, 

a group of cellular and genetic abnormalities that 

have been shown to be associated with invasive 

cancer at nearly every solid tumor site. 


A large 

number of putative intermediate biomarkers of 

prognosis or treatment effect have been investigated, but none have been put into clinical use. 

Ultimately, the long-term effect of a chemopreventive strategy will need to be evaluated 

through a prospective randomized trial and evaluated by the only definitive end point for prevention 

of cancer, the incidence rates of new carcinomas. 

3. Oral cancer—rationale for prevention 

Annually, more than 300,000 new cases of oral 

cancer occur worldwide. Of these, approximately 

50,000 cases occur in the United States and Europe. In the Nordic countries Norway, Sweden, 

Denmark, Finland approximately 1500 cases are 

being diagnosed with oral squamous cell carcinoma 

annually. 


This aggressive type of carcinoma is 

associated with severe morbidity and high mortality with less than 50 long- term survival Furthermore, the treatment results of this devastating 

disease have not significantly improved over the 

last 4—5 decades, 


despite advances in treatment with surgery, radiation and chemotherapy. 

One probable explanation for the dismal treatment 

results is the fact that oral cancer is a field process, 

which leaves local treatment modalities inefficient. 



The concept of field cancerization was 

introduced by Slaughter in 1953, 



and was based 

on the fact that the epithelial surface of the aerodigestive tract is likely to be exposed to many of 

the common carcinogens such as tobacco and alcohol and <\connective form=thus text=thus id=F1482_FUL6_12.txt.raw_214 relation=Contingency\> has an increased risk of oral cancer 

development. Second primary tumors occur in the 

‘‘field of cancerization’’ at a rate of 20—30 

despite improvements in loco regional control. 



Strategies for prevention of head and neck cancers 

should be investigated intensively in order to 

reduce the incidence of first and second primary 

malignancies and the morbidity and mortality 

associated with them. One way to approach the 

prevention of head and neck cancer is to identify 

individuals who are at a high risk to develop such 

cancers eg, individuals with oral premalignant 

lesions and to treat them with agents that can 

suppress the development of additional premalignant lesions and inhibit the development of oral 

cancer. Oral carcinogenesis is a multi-step process, 

which is characterized by genetic, epigenetic and 

phenotypic changes. Many of these changes involve 

activation of signaling or metabolic pathways that 

give the cells favorable growth and survival characteristics. <\connective form=therefore text=Therefore id=F1482_FUL6_12.txt.raw_254 relation=Contingency\>, agents that can inhibit or 

reverse these changes by targeting molecularly 

defined pathways receive increased attention as 

novel candidates for cancer prevention and therapy. 

One of the lessons learned from biomedical 

research is that disease processes evolving over a 

long time-span are rarely treated in an efficient 

manner using single therapeutic agents. <\connective form=thus text=Thus id=F1482_FUL6_12.txt.raw_215 relation=Contingency\>, a 

chemopreventive approach to head and neck cancer, which gradually develops over a 10—20 year 

period of time, most likely will have to target more 

than one metabolic pathway in order to be efficient. 

These targets include cyclooxygenase-2 COX-2 fig 1, peroxisome proliferator-activated receptor PPAR-delta, PPAR-gamma, transforming 

growth factor-beta receptor type II, epidermal 

growth factor receptor, and several subtypes of 

lipoxygenases LOXfig 2. 


Two of these targets that appear to be relevant 

for preventing head and neck cancer development 

are the arachidonic acid metabolizing enzyme 

cyclooxygenase-2 COX-2 and the epidermal 

growth factor receptor, both of which are upregulated in premalignant lesions eg, oral leukoplakia and in head and neck cancers. 


The 

significance of these two targets is underscored by 

the fact that inhibitors of them are commercially 

available. The inhibitors of COX-2 <\connective form=also text=also id=F1482_FUL6_12.txt.raw_258 relation=Expansion\> have Food 

and Drug Administration approval as chemopreventive agents for neoplasia in the large bowel. There 

are <\connective form=also text=also id=F1482_FUL6_12.txt.raw_259 relation=Expansion\> interactions between EGFR and COX-2, 

which makes a combined targeting of the two pathways an attractive therapeutic approach fig 3. 


4. Oral leukoplakia as a model for 

studying cancer chemoprevention 

Leukoplakia as a medical term has been encumbered by a hodgepodge of synonyms and interpretations. 


Oral leukoplakia is a clinical lesion 

that presents as a white patch or plaque of the oral 

mucosa that cannot be characterized clinically or 

pathologically as any other diagnosable oral disease. It is generally regarded as a precancerous 

condition with increased risk of developing into 

invasive cancer. 


<\connective form=however text=However id=F1482_FUL6_12.txt.raw_88 relation=Comparison\>, the risk varies substantially in different studies. It is generally believed 

that oral leukoplakias with dysplastic content carry a 

higher risk of developing oral cancer. 



This view has, 

<\connective form=however text=however id=F1482_FUL6_12.txt.raw_89 relation=Comparison\>, been challenged by several recent studies. 

Studies conducted by our group show that DNA content rather than histology predicts the risk of the 

lesions in terms of their likelihood to develop into 

invasive cancer. 


In fact, cancers developed in 

histologically normal tissue, provided it contained 

tissue with evidence of genomic instability. 



Invasive cancers of the oral cavity are not necessarily at the sites of preceding leukoplakia. This 

indicates that leukoplakia in smokers reflects the 

presence of a defected field with multiple premalignant lesions. It is difficult to assess the 

malignant potential of leukoplakia, which may 

partially explain the wide range of oral cancer 

rates in patients with oral leukoplakia in the literature. 


<\connective form=nevertheless text=Nevertheless id=F1482_FUL6_12.txt.raw_85 relation=Comparison\>, leukoplakia is an excellent model in chemoprevention studies because of 

its cancer risk, easy access for clinical observation, 

and possibility to obtain tissues for histology and 

molecular analysis without the use of costly and 

sophisticated procedures. In previous chemoprevention studies, Lotan et al have shown that 

retinoids based agents may induce major responses 

of oral leukoplakia in both the size of the lesions 

and severity in derangements of the histology. 


<\connective form=however text=However id=F1482_FUL6_12.txt.raw_90 relation=Comparison\>, they were unable to determine whether 

such responses may actually result in a decreased 

incidence of invasive oral cancer. Additionally, the 

toxicity of retinoids further limits its potential use 

as preventive agents. 



A proposed phase III trial 

will provide for the first time a high-risk population 

as well as sufficient statistic power to determine 

whether the preventive agents used can reduce the 

oral cancer risk in patients with oral leukoplakia. 

5. Molecular alterations and their significance in oral chemoprevention 

Oral leukoplakia is a heterogeneous group of 

lesions, and demonstrates a varying degree of risk 

for cancer progression. 

Histology presence 

and degree of dysplasia, the current gold standard 

for assessing this risk, is a poor predictor for lesions 

without dysplasia, or with minimal dysplasia, as 

only a few of these lesions will progress to cancer. 

Molecular abnormalities identified in early oral 

tumorigenesis are often mutations of tumor sup pressor genes such as deletion of chromosome 3p 

which harbor multiple tumor suppressor genes, 

chromosome 9p21 which harbors the p16 tumor 

suppressor gene, and chromosome 17p13 which 

harbors the p53 tumor suppressor gene. 

Recent studies have shown that a loss of chromosome regions loss of heterozygosity, LOH containing known or presumptive tumor suppressor 

genes is predictive of the cancer risk of oral premalignant lesions. In a recent study by Rosin et al, 

most lesions that later progressed to oral cancer 

exhibited LOH at 3p and/or 9p regions, whereas 

lesions that lacked any LOH at the two regions did 

not progress. 


<\connective form=however text=However id=F1482_FUL6_12.txt.raw_91 relation=Comparison\>, a significant number of 

lesions with LOH at the two chromosome regions 

did not develop oral cancer, which indicates oral 

lesions with different clonal origin may have a 

varying malignant potential The determination of 

clonal relationship between oral premalignant 

lesions and subsequent cancers or new premalignant lesions may clarify how chemopreventive agents act to prevent development of cancers 




Previous evidence has firmly linked the arachidonic acid metabolism pathways and prostaglandin 

synthesis with the promotion and progression of 

human cancers. 


Multiple lines of evidence—in 

vitro, in vivo, observational, and clinical—now 

confirm that the antineoplastic effect of NSAID is 

dependent on COX inhibition, reducing prostaglandin production. 



Reduced prostaglandin activity in tissues as an antineoplastic principle has 

extensive documentation for malignancies of the 

large bowel. <\connective form=however text=However id=F1482_FUL6_12.txt.raw_92 relation=Comparison\>, there is less clinical evidence to support its role in preventing oral cancer. 

Growth factors are important mediators of cell 

proliferation. 



The interaction between these 

growth factors with their respective receptors 

primes the signal transduction pathway. The intracellular domains of these receptor proteins function 

as protein tyrosine kinases, <\connective form=and text=and id=F1482_FUL6_12.txt.raw_150 relation=Expansion\> it is this property 

that allows them to effect signaling. Under certain conditions, such as overexpression, mutation, or co 

expression of the ligand and the receptor, these 

receptors can become constitutively active, 

resulting in uncontrolled cell proliferation. 


We 

have recently demonstrated that EGFR is up regulated in all oral squamous cell carcinomas, and in 

most lesions histologically defined to be dysplastic, 

but not in histologically normal oral mucosa. All 

cases with COX-2 expression <\connective form=also text=also id=F1482_FUL6_12.txt.raw_260 relation=Expansion\> had EGFR expression unpublished data. A selective and combinatorial treatment using a selective COX-2 inhibitor 

and EGFR inhibitors in lesions with aneuploidy may 

represent a suitable limitation for targeted combinatorial treatment of high-risk persons. 




Cyclooxygenase 2 COX-2 is overexpressed in 

most oral carcinomas as well as in oral dysplastic 

lesions suggesting a role of COX-2 in oral tumorigenesis. COX-2 inhibitors have shown to be able to 

suppress cell proliferation and induce apoptosis in 

cancer cell lines including oral cancer cell lines. In 

animal models, inhibition of COX-2 by celecoxib 

results in reduced tumor prostaglandin 2 PGE2 

levels and a dose-dependent reduction in tumor 

growth. Celecoxib-treated tumors <\connective form=also text=also id=F1482_FUL6_12.txt.raw_261 relation=Expansion\> show 

reduced proliferation and increased apoptosis. 


Interestingly, inhibition of PGE2 activity by a neutralizing antibody can mimic the effect of celecoxib treatment suggesting that a major antitumor 

mechanism of celecoxib is inhibition of COX-2- 

derived prostaglandins, particularly PGE2, in the 

model. <\connective form=however text=However id=F1482_FUL6_12.txt.raw_93 relation=Comparison\>, whether findings in the animal 

model apply to humans remains unclear. Recent 

data support the hypothesis that COX-2 is downregulated by adenomatosis polyposis coli APC and 

upregulated by nuclear b-catenin accumulation. 

This implicates the Wnt signal transduction pathway in epithelial cancers fig 3. <\connective form=thus text=Thus id=F1482_FUL6_12.txt.raw_216 relation=Contingency\>, in a recent 

study, Araki et al demonstrate that COX-2 is upregulated by the Wnt signaling transduction pathway and can cooperate with the ras signaling 

transduction pathway. 

EGFR and a related family member, c-erbB-2, are 

receptor tyrosine kinases with increased activity in 

a wide range of solid tumors, including oral cancers 

and precancers. 


Upon binding a ligand, EGFR 

undergoes homo- or hetero-dimerization with 

c-erbB-2. 


This results in an autophosphorylation, 

which activates numerous intracellular signal 

transduction pathways and ultimately, results in 

uncontrolled cell proliferation. 


Several 

approaches have been developed to achieve EGFR 

blockade as an anticancer treatment strategy. 



Monoclonal antibodies may competitively bind to 

the extracellular EGFR site and prevent binding by 

the natural ligands EGF and transforming growth 

factor-alpha. Inhibition of cell cycle progression, 

induction of apoptosis, inhibition of angiogenesis, 

inhibition of metastasis, and enhancement of the 

response to chemotherapy and radiation therapy 

may <\connective form=then text=then id=F1482_FUL6_12.txt.raw_67 relation=Temporal\> occur. 



Studies <\connective form=also text=also id=F1482_FUL6_12.txt.raw_262 relation=Expansion\> show that ErbB-2 

are overexpressed in oral tumors. 


The selective 

EGFR inhibitor 3-cyanoquinoline, designated 

EKB-569, binds covalently to EGFR, and is a potent 

inhibitor of a recombinant form of EGFR kinase and 

phosphorylation of EGFR in cells. 


In a higher 

dose EKB-569 can <\connective form=also text=also id=F1482_FUL6_12.txt.raw_263 relation=Expansion\> inhibit other tyrosine kinases, including c-erbB-2. 


Despite the differences 

in dose requirement, EKB-569 is approximately 

equipotent at inhibiting the growth of cells that 

overexpressed <\connective form=either or text=either id=F1482_FUL6_12.txt.raw_271 relation=Expansion\> EGFR <\connective forme=either or text=or id=F1482_FUL6_12.txt.raw_271 relation=Expansion\> c-erbB-2. The effect 

is specific <\connective form=because text=because id=F1482_FUL6_12.txt.raw_209 relation=Contingency\> 10- to 50-fold more drug is needed to inhibit the growth of cell lines that did not 

overexpress EGFR or c-erbB-2. 


In the mouse xenograft model, EKB-569 given 

orally 10 mg/kg/day, daily for 10 days inhibits the 

growth of tumors that overexpress EGFR. 


Effect is <\connective form=also text=also id=F1482_FUL6_12.txt.raw_264 relation=Expansion\> observed in cells that overexpress 

c-erbB-2. These effects are specifically linked to 

the EGFR or c-erbB-2 receptors, <\connective form=because text=because id=F1482_FUL6_12.txt.raw_210 relation=Contingency\> dosage 

increase of EKB-569 does not inhibit the growth of 

tumors that had low levels of EGFR or c-erbB-2. 

Consistent with its ability to irreversibly bind to 

EGFR, inhibition of phosphorylation of EGFR in 

tumors is sustained much longer than plasma levels 

of active compound. 


Torrance et al, in a 

recent study, show that combining a NSAID, sulindac, with EKB-569 effectively protects against 

intestinal neoplasia in a mouse model. 



In the 

study, mice with a predisposition to developing 

intestinal polyps were treated for 60 days, after 

which their intestinal tracts were removed and the 

number of intestinal polyps counted. High doses of 

sulindac 20 mg/kg/day reduced intestinal polyps 

by about 70 <\connective form=while text=while id=F1482_FUL6_12.txt.raw_206 relation=Temporal\> low doses 5 mg/kg/day had no 

effect. EKB-569, when given alone, reduced polyp 

formation by 87. <\connective form=however text=However id=F1482_FUL6_12.txt.raw_94 relation=Comparison\>, a combination of 

EKB-569 and 5 mg/kg/day sulindac reduced the 

number of polyps formed by over 95. In the control group, all of the mice developed tumors, 

whereas only half of the animals in the combination group were free of tumor. The combined 

administration of sulindac and EKB-569 allowed a 

75 reduction in the dose of sulindac. This indicates at least an additive, if not synergistic effect 

between sulindac and EKB-569, <\connective form=and text=and id=F1482_FUL6_12.txt.raw_173 relation=Expansion\> that reduction 

in doses of the agents, with minimized long-term 

toxicity, may be possible. Combining an EGFR 

kinase inhibitor with a COX-2 selective inhibitor 

might achieve similar or better results for prevention of tumor formation. Indeed, recent data of 

combining EKB-569 and a COX-2 specific inhibitor, 

celecoxib, are promising in preventing development of polyps and tumors in colorectum. 

effect of chemopreventive agents 

One of the major difficulties in assessing chemopreventive agents is the lack of full understanding 

of the biological effects of most of the chemopreventive agents. It is no exception for celecoxib and 

EKB-569 <\connective form=although text=although id=F1482_FUL6_12.txt.raw_37 relation=Comparison\> their roles as COX-2 inhibitor and 

EGFR inhibitor have been firmly demonstrated. A 

number of studies have shown that celecoxib 

induced apoptosis may be achieved through COX-2 

independent pathways <\connective form=because text=because id=F1482_FUL6_12.txt.raw_211 relation=Contingency\> many of the cancer 

cell lines sensitive to celecoxib lack of COX-2 

activity. Similarly, many cancer cell lines with high 

level EGFR are resistant to certain EGFR inhibitors. 

It is not uncommon that the major mechanism of a 

drug is found eventually different as originally 

proposed. <\connective form=therefore text=Therefore id=F1482_FUL6_12.txt.raw_255 relation=Contingency\>, it is important to identify a 

molecular basis for those who will benefit for a 

particular chemopreventive strategy. 

It is important to keep in mind that most visible 

lesions will be excised as part of diagnostic work-up 

and treatment, and there may not be any lesions 

available for further examination. It may be that 

not all oral cancers are preceded by visible changes 

of the oral mucosa. Conceivably, brushings or 

scrapings from clinically normal oral mucosa in 

persons with high-risk behavior for oral cancer 

could identify premalignant cell clones. 


Telomerase is a rib nucleoprotein enzyme has 

been related to cancer development by extending 

chromosome ends that have been shortened during successive cycles of cell division. 


Telomerase 

activation plays a critical role in tumorigenesis by 

sustaining cellular immortality, 

and is composed of an RNA component hTERC, a catalytic 

protein subunit hTERT, and other telomeraseassociated proteins such as TEP1 and hsp90. Telomerase is expressed in up to 90 of oral cancers 

and oral premalignancies. 


However, telomerase 

is not expressed in the corresponding benign tissues. 



Notably, telomerase activity is closely 

linked to the expression pattern of hTERT, the telomerase catalytic subunit gene. 



The development of in situ hybridization techniques that can 

reliably detect hTERT messenger RNA mRNA has 

made it possible to examine the expression of this 

critical telomerase component at the single-cell 

level. 


N-4-Hydroxyphenyl retinamide 4-HPR 

can down-regulate hTERT expression in bronchial 

epithelial cells for former smokers, suggesting a 

possible use of the molecule as an intermediate 

biomarker for evaluation of these chemopreventive 

agents. 

Several in vitro and in vivo studies on 

tumor models of head and neck squamous cell carcinoma have shown that NSAIDs such as indomethacin which <\connective form=also text=also id=F1482_FUL6_12.txt.raw_265 relation=Expansion\> inhibits COX-2 can inhibit 

telomerase activity and tumor growth, suggesting 

that reduction of telomerase activity may play a role 

in COX-2 mediated cancer preventive action. A 

recent study demonstrated that EGF rapidly upregulates telomerase activity in cells that express EGFR 

after the activation of hTERT mRNA expression. This 

appears to be a direct effect of EGF on hTERT transcription. Further experiments suggest that EGF 

mediated hTERT expression is through MAP kinase 

signaling pathway. 



These data support the hypothesis that COX-2 inhibitors and EGFR inhibitors 

down-regulate telomerase activity leading to cell 

arrest or elimination of premalignant clones. <\connective form=therefore text=Therefore id=F1482_FUL6_12.txt.raw_256 relation=Contingency\>, measuring hTERT expression status may provide 

an indicator of whether the agents have biologically 

relevant effect in the target tissues and predict a 

potential therapeutic outcome. It <\connective form=also text=also id=F1482_FUL6_12.txt.raw_266 relation=Expansion\> points to the 

use of telomerase peptide vaccines 


as a therapeutic approach in oral premalignant lesions. 


An additional biomarker, which may directly 

relate to treatment with an EGFR inhibitor such as 

EKB-569, is the expression of EGFR. In a recent 

study, Bei et al demonstrated a significant association of EGFR or ErbB2 overexpression with oral 

squamous cell carcinoma when compared with 

benign lesions and apparently normal epithelium. 



tumor-specific overexpression of ErbB receptors 

and their co-expression, most frequently involving 

EGFR and ErbB2, in the same cell layer of neoplastic oral epithelium, implicate receptor heterodimers in the pathogenesis of oral squamous cell 

carcinoma. Furthermore, TGF-alpha stimulation of 

EGFR has been found to be associated with constitutive signal transducer and activator of transcription 3 Stat3 activation in oral cancer. 


Abrogation of constitutive Stat3 activation was seen 

after treatment a potent RXR-selective retinoic acid 

LGD1069. The results indicate that proliferation in 

oral squamous cell carcinomas decreases due to 

interference with TGF-alpha/EGFR autocrine signaling. These observations have important implications for the targeting of individual ErbB receptors 

with diagnostic or therapeutic objectives in oral 

carcinoma. They <\connective form=also text=also id=F1482_FUL6_12.txt.raw_267 relation=Expansion\> points to EGFR expression as a 

plausible intermediate biomarker for monitoring 

treatment effects during chemopreventive intervention, even by other agents than those that 

directly act on the EGFR receptor. 


Establishment of an early and reliable biomarker 

for oral carcinogenesis whose expression can be 

monitored during treatment will enable assessment 

of treatment response and early progression to 

cancer. The expression of COX-2 during malignant 

transformation of oral epithelium makes this a 

useful target for monitoring treatment response. 


It is <\connective form=however text=however id=F1482_FUL6_12.txt.raw_96 relation=Comparison\>, not the COX-2 levels by itself that 

drive the malignant transformation of the tissue. 

More likely this is related to the levels of the main 

metabolites of the enzyme, the prostaglandins. 

Prostaglandin E2 has been most closely related to 

malignant transformation into oral squamous cell 

carcinomas. 



Expression of other markers of tumor 

progression related to apoptosis and angiogenesis 

pathway genes shows relatively low level of changes in oral premalignant tissue. 




DNA aneuploidy as a marker of genomic instability has been demonstrated to be a significant 

prognostic factor in oral premalignant lesions. 


Cytogenetic response in oral premalignant lesions 

could possibly be used as a marker of treatment 

response. Cytogenetic response by DNA ploidy 

assessment is a feasible and low cost method. 



Despite their promise, anti-inflammatory drugs 

and antibodies towards dysfunctional growth receptors are not yet recommended for the prevention or treatment of any cancers. Fundamental 

questions remain concerning their molecular and 

cellular targets of action, efficacy, safety, treatment regimen, indications, and the balance of risks 

and benefits from treatment in designated patient 

populations. <\connective form=however text=However id=F1482_FUL6_12.txt.raw_97 relation=Comparison\>, as the concept of individualized therapy gradually is becoming accepted in 

the field of medical oncology regarding manifest 

cancers, we should <\connective form=also text=also id=F1482_FUL6_12.txt.raw_268 relation=Expansion\> consider tailoring chemopreventive measures towards persons that do not 

yet have a cancer, but are at considerable risk of 

acquiring one. <\connective form=if text=If id=F1482_FUL6_12.txt.raw_213 relation=Contingency\> a chemopreventive strategy can 

reduce a particular cancer incidence by 50, we 

should ideally target those who are likely to benefit 

from such regimen <\connective form=while text=while id=F1482_FUL6_12.txt.raw_207 relation=Comparison\> sparing other from therapy. <\connective form=thus text=Thus id=F1482_FUL6_12.txt.raw_217 relation=Contingency\>, as knowledge of the processes leading 

up to oral cancer increase, it is likely that the 

paradigm of treatment will move from ‘‘seek and 

destroy’’ to ‘‘target and prevent’’.