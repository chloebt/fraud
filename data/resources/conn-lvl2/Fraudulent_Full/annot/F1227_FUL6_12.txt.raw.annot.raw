Hypercholesterolemia HC is a primary risk factor for the development of cardiovascular 

disease, which remains a leading cause of morbidity and mortality in the United States 34. Like 

other risk factors, HC exerts both short- and long-term effects  on the vascular system, including 

endothelial cell dysfunction 4,  inflammation 19, and thrombosis 35, setting the stage for 

atherosclerotic plaque formation 5. The endothelial cell dysfunction that accompanies HC is 

usually manifested as an increased production of ROS 22, increased adhesion molecule expression 

Li 1993, and impaired EDV 3. These changes occur early in the course of HC and the 

endothelial cell activation appears  to be a systemic vascular response, manifested in both the 

microvasculature 31,32,33 and large arteries 38. <\connective form=while text=While id=F1227_FUL6_12.txt.raw_183 relation=Contrast\> a variety of mechanisms have been 

proposed to explain the inflammatory phenotype that occurs in blood vessels subjected to HC, 

immune cell activation has gained considerable attention as a major underlying cause of both the 

short- vasomotor dysfunction and long- atherosclerosis  term vascular responses to HC 

20,25,30,42,43. For example, recent reports have implicated CD4+ T- lymphocytes as key 

modulators of the increased leukocyte-endothelial cell adhesion, enhanced ROS production, and 

impaired EDV elicited in the microvasculature 31,33 and large arteries during HC 38. However 

despite the recent advances, it remains unclear how the circulating T- lymphocytes are able to exert 

their modulating influence on endothelial cells and subsequently elicit the deleterious responses 

associated with HC. 

There are several lines of evidence in the literature that support a potential role for IFN-γ in 

the immune cell-mediated responses associated  with HC: 1 activated Th1 CD4+ T-cells from 

apoE 

-/- 

mice exhibit an accelerated secretion of IFN-γ 41, 2 mice that are genetically deficient in both apoE and IFN-γ do not develop the large aortic plaques seen in apoE-/- mice 8, and 3 the 

increased adhesion and emigration of leukocytes, and enhanced ROS production induced by HC in 

postcapillary venules of WT mice are significantly blunted in IFN-γ 

-/- 

mice 31.  Furthermore, we 

have recently reported that aortic rings from HC-IFN-γ 

-/- 

mice do not exhibit the increased 

gp91 

phox 

Nox-2 mRNA expression, enhanced ROS production, and impaired EDV that are 

observed in WT mice placed on the same HC diet 38. <\connective form=while text=While id=F1227_FUL6_12.txt.raw_184 relation=Contrast\> the latter study strongly implicates 

IFN-γ in the HC-induced endothelial cell activation and vasomotor dysfunction, a number of 

important issues regarding the involvement of IFN-γ remain unresolved. Since a variety of cells 

residing <\connective form=either or text=either id=F1227_FUL6_12.txt.raw_220 relation=Conjunction\> within <\connective forme=either or text=or id=F1227_FUL6_12.txt.raw_220 relation=Conjunction\> outside the vascular wall or circulating in blood can produce IFN-γ 36, 

the cellular source of this regulatory cytokine is not known. Similarly, it remains unclear whether 

the vascular responses mediated by IFN-γ result from direct engagement of IFN-γ receptors on the 

vessel wall or indirectly from engagement of receptors on circulating blood cells. <\connective form=although text=Although id=F1227_FUL6_12.txt.raw_29 relation=Contrast\> signal 

transducer and activator of transcription-1 STAT-1 and IFN-γ regulatory factor-1 IRF-1 have 

been implicated as signals that mediate cellular responses to IFN-γ receptor 

activation12,14,16,17,23,24,26,37, the contribution of  these signaling molecules to the HC-induced, IFN-γ-mediated superoxide production and impaired vasomotor function has not been 

previously addressed. These important unresolved issues related to IFN-γ involvement in HCinduced vasomotor dysfunction and gp91 

phox 

-mediated superoxide production were addressed in 

the present study. Our findings are consistent with a mechanism wherein HC elicits increased IFN- 

γ secretion from CD4+ T-cells, resulting in increased gp91 

phox 

-dependent ROS production and 

EDV dysfunction through a STAT-1/IRF-1-independent signaling pathway that involves 

activation of IFN-γ receptors on circulating blood cells. 


Animals and experimental groups:  C57BL/6 wild type or WT, CD4+ T- lymphocyte 

deficient, CD4 

-/- 

, CD8+ T-lymphocyte deficient, CD8 

-/- 

, IFN-γ-deficient IFN-γ 

-/- 

, IFN-γ 

receptor-deficient IFN-γR 

-/- 

, and IFN-γ regulatory factor 1-deficient IRF-1 

-/- 

mice mutant mice 

on a C57BL/6 background were obtained from Jackson Laboratories Bar Harbor, Me. STAT-1- 

deficient STAT-1 

-/- 

, on a B129 background and B129 the wild type control for STAT-1 

-/- 

were 

purchased from Taconic Hudson, NY. Mice 5-6 weeks old in this study were placed on either a 

normal diet ND or cholesterol-enriched HC diet 1.25 cholesterol, 0.125 choline chloride, 

15.8 fat; Harlan Teklad for 2 weeks n= 5/group as previously described 38. 

Surgical protocols:  Mice were anesthesized with xylazine 7.5 mg/kg body wt IP and 

ketamine chloride 150 mg/kg body wt i.p. The thoracic and abdominal cavities of the mouse were 

opened through a midline incision. After hemostasis was achieved, the thoracoabdominal aorta was 

carefully dissected, removed and placed in an oxygenated petri dish filled with ice cold Kreb’s 

physiologic salt saline PSS. The composition of PSS is mM: NaCl 119, KCl 4.5, NaHCO3 

25, KH2PO4 1.2, MgSO4 1.2, L-glucose 11 and CaCl2 2.5. All animal procedures employed 

in this study were approved by the LSUHSC-Shreveport Institutional Animal Care and Use 

Committee. 

Wire myography: The freshly dissected aorta was cut into 3 sections, each 2 mm in length, 

and mounted on an eight-channel wire myograph Radnoti Glass Monrovia, Ca. Aortic rings were 

maintained in 25 ml organ baths with oxygenated PSS 95 O2 and 5 CO2 at 37.1°C. The PSS in 

each organ bath was changed every 15 minutes throughout the experiment. Pretension 1400 mg was placed on each aortic ring appropriate starting tension for optimal vasomotor function as 

determined in previous experiments. An eight-channel octal bridge Powerlab and data acquisition 

software Chart version 5.2.2 were used to record all force measurements. After equilibration for 1 

hour, the aortic rings were rinsed with a 120  mM KCl solution for vascular smooth muscle 

activation and to determine the maximal contractile response. This was repeated two additional 

times with increasing 200 mg increments in tension. Vessel contractility to KCl at each tension 

setting was analyzed DMT normalization module, AD instruments and the optimal resting tension 

was determined 38. Following the final KCl rinse, the aortic rings were adjusted to the optimal 

tension and allowed to equilibrate for a further 60 min before generation of dose-response curves. 

Experimental protocols: Each aortic ring was used to  generate a dose- response curve to 

test for: 1 endothelium-independent contraction to phenylephrine PE, 2 endothelium-independent dilation to sodium  nitroprusside SNP, and 3 endothelium-dependent dilation to 

acetylcholine Ach. The vasoactive agent under study was added to the organ bath in increasing 

concentration from 10 

-9 

M to 10 

-4 

M. For the determination of SNP and Ach dose-response 

relationships, aortic rings were pre-contracted with 10 

-5 

M PE, and <\connective form=then text=then id=F1227_FUL6_12.txt.raw_54 relation=Asynchronous\> SNP or Ach was added in 

increasing concentrations from 10 

-9 

M to 10 

-4 

M. Data is presented in histograms depicting the 

dilation responses noted at 10 

-4 

M Ach. Endothelium-dependent dilation was expressed as  max dilation. Cytochrome C reduction assay: Superoxide production by aortic tissue was quantified 

using the cytochrome C reduction assay as described in previous studies 6,38. The specificity of 

the assay for superoxide was verified by measuring the difference in absorbance at 550 nm in the 

presence and absence of superoxide dismutase SOD using a Hitachi U-2000 spectrophotometer. 

Cytometric bead array CBA: Serum taken from WT-ND, WT-HC, CD4 

-/- 

-HC, CD8 

-/- 

- 

HC, IFN-γ 

-/- 

-HC, and CD4+→IFN-γ 

-/- 

-HC HC IFN-γ 

-/- 

-mice reconstituted with WT-HC CD4+ Tcells isolated from splenocytes  as previously described 38 mice was frozen for subsequent 

measurements of IFN-γ levels using a CBA BD Biosciences, as described in previous studies 33. 

Real-time PCR RT-PCR measurements of Nox-1, Nox-2 gp91 

phox 

, and Nox-4: 

Quantification of mRNA expression of the NADPH oxidase subunits, Nox-1, gp91 

phox 

, and Nox-4 

was performed on aortic tissue derived from mice n=5/group using a pre- developed assay for RTPCR Applied Biosystems Foster City, CA. Samples of cDNA 10 ng derived from aorta of each 

animal were assayed in duplicate using an ABI PRISM 7500 bioanalyzer and gene expression was 

quantified using a comparative critical threshold CT method according to the manufacturers 

suggestions. The CT value reflects the cycle number at which the DNA amplification is first 

detected. For each sample, a CT value was obtained by subtracting GAPDH CT values from those 

of each target gene, <\connective form=thereby text=thereby id=F1227_FUL6_12.txt.raw_30 relation=Cause\> allowing the expression of each target gene to be normalized to 

GAPDH content. Chimeras: Four groups of bone marrow chimeras were produced, as previously described 

38. WT→WT chimeras were created by transferring bone marrow cells from CD45 congenic mice 

CD45.1 positive leukocytes into C57BL/6 CD45.2 positive leukocytes recipient mice. IFN-γ 

-/- 

→WT chimeras were produced by transplanting bone marrow from IFN-γ 

-/- 

mice CD45.2 positive 

leukocytes into congenic WT recipients CD45.1 positive leukocytes. IFN- γR 

-/- 

→WT chimeras 

were produced by transplanting bone marrow from IFN-γR 

-/- 

mice CD45.2 positive leukocytes into 

congenic WT mice CD45.1 positive leukocytes. WT→IFN-γR 

-/- 

chimeras were produced by 

transplanting bone marrow from congenic WT mice CD45.1 positive leukocytes into IFN-γR 

-/- 

mice CD45.2 positive leukocytes. Flow cytometry was used to verify the degree of chimerization 

in all recipient mice by staining for CD45.1 and CD45.2 expression on leukocytes with a FITClabeled anti-CD45.1 antibody and a biotinylated  anti- CD45.2 antibody with  a Streptavidin-PerCP 

secondary antibody PharMingen. Only chimeras with >90 conversion of leukocyte antigen to 

the donor phenotype were used in the studies. 

Statistical Analysis: All values are reported as mean ± SE. ANOVA Scheffe was used for 

statistical comparison of the experimental groups with statistical significance set at p<0.05. 


Mice placed on two weeks of cholesterol-enriched diet exhibited a  greater than 3-fold 

increase in serum cholesterol concentration 188.0 mg/dl  ± 4.1 when compared to normal diet 

controls 61.1 mg/dl  ± 1.9. Additionally, mutant mice on average exhibited similar elevation in 

serum cholesterol 189.8 mg/dl ± 4.7 as wild type controls 186.3 mg/dl ± 9.6 when placed on HC 

diet. There was <\connective form=also text=also id=F1227_FUL6_12.txt.raw_216 relation=Conjunction\> no statistically significant difference in serum cholesterol concentration <\connective form=when text=when id=F1227_FUL6_12.txt.raw_79 relation=Synchronous\> 

comparing mutant 57.3 mg/dl ± 4.7 and wild type 60.8 mg/dl ± 3.6 normal diet controls. 

A comparison of dose-response curves for endothelium-independent contraction PE dose-response curve and -dilation SNP dose-response curve from aortic rings of mice placed on ND 

versus HC diets revealed no statistically significant difference  data not shown. Similarly, 

comparison of dose-response curves for endothelium-independent contraction and -dilation from 

aortic rings of mutant mice exhibited no significant difference when compared to same diet WT 

controls data not shown. EDV acetylcholine dose-response curve was significantly attenuated in 

the aortic rings of WT→WT-HC mice, when compared to WT→WT-ND controls Figure 1A. 

EDV and superoxide production from WT→WT chimera mice did not statistically differ from same 

diet WT mice data not shown.  This EDV impairment in WT→WT-HC mice was accompanied by 

an increased superoxide production Figure 1B. The HC-induced vasomotor dysfunction and 

increased superoxide production was not seen in IFN-γ 

-/- 

→WT-HC chimera mice Figure 1A/B. 

The latter observations are consistent with our previously published findings in aortic rings from 

IFN-γ 

-/- 

mice 38 and extend these observations to demonstrate that the IFN-γ mediating the HC-induced vascular responses is produced by circulating blood cells. To better define the circulating cell populations that produces the IFN-γ involved in the 

HC-induced vascular responses, a cytometric bead array was used to measure plasma IFN-γ in IFN- 

γ in WT-ND, WT-HC, CD4 

-/- 

-HC, CD8 

-/- 

-HC, IFN-γ 

-/- 

-HC, and CD4+→IFN-γ 

-/- 

-HC mice Figure 

2. As previously reported 33, a significantly increased IFN-γ concentration was detected in 

plasma of WT-HC mice, compared to WT mice  on a normal diet. The HC-induced increase in 

plasma IFN-γ concentration was noted in CD8+ T-cell-deficient CD8 

-/- 

, but not in CD4+ T-cell-deficient CD4 

-/- 

and IFN-γ-deficient IFN-γ 

-/- 

mice, suggesting that CD4+ T-lymphocytes are 

largely responsible for the increased plasma IFN-γ levels associated with HC. To confirm these 

findings, we adoptively transferred  WT-HC CD4+ T-cells into HC IFN-γ 

-/- 

recipient mice. The 

transfer of CD4+ T-cells  reconstituted HC-induced IFN-γ production in IFN- γ 

-/- 

recipient mice, 

establishing CD4+ T-cells as the major source of IFN-γ secretion with HC. 

Figure 3 compares the HC-induced changes vasomotor function panel A and superoxide 

production panel B that were detected in WT and IFN-γ receptor-deficient IFN-γR 

-/- 

mice. In 

contrast to the responses elicited in aortic tissue from WT-HC mice, IFN-γR 

-/- 

placed on the same 

HC diet did not exhibit the impaired vasomotor function and increased superoxide production, 

implicating the IFN-γ receptor in the IFN-γ-mediated vascular responses. 

In order to determine whether the vascular responses mediated by IFN-γ result from direct 

engagement of IFN-γ receptors on the vessel wall or indirectly from engagement of receptors on 

circulating blood cells, we compared the HC-induced vascular responses in WT mice to bone marrow chimeras generated from IFN-γR 

-/- 

mice Figure 4. These experiments revealed that the 

HC-induced vasomotor dysfunction and increased superoxide formation are elicited in aortic tissue 

from WT→IFN-γR 

-/- 

chimeras, but not in IFN-γR 

-/- 

→WT mice placed on the same HC diet. These 

findings indicate that the HC-induced  vascular responses mediated by IFN-γ result from 

engagement of cytokine with its receptor on circulating blood cells. 

STAT-1 and IRF-1 are two signaling molecules  that have been implicated in the cellular 

responses to IFN-γ receptor activation 12,17,26,37. In order  to assess the contribution of these 

signaling molecules to the HC-induced IFN-γ receptor mediated vascular responses, we compared 

the vascular responses to HC in mice deficient in either STAT-1 or IRF-1 to the responses in their 

corresponding WT counterparts Figure 5. Our findings indicate that deficiency of either signaling 

molecule did not alter the HC-induced impairment of endothelium-dependent vasodilation panel 

A or the increased superoxide formation panel B, suggesting that IFN-γ receptor-mediated 

signaling in HC involves pathways that are independent of both IRF-1 and STAT-1. 

Previous work in our laboratory has demonstrated that aortic tissue from WT-HC mice 

exhibits an increased expression of mRNA for gp91 

phox 

, but not for Nox-1 or Nox-4, compared to 

tissue from WT-ND mice 38. We  have <\connective form=also text=also id=F1227_FUL6_12.txt.raw_217 relation=Conjunction\> reported that IFN-γ 

-/- 

mice do not exhibit the 

upregulation of gp91 

phox 

mRNA in response to HC 38.  Table 1 summarizes the changes in 

gp91 

phox 

mRNA expression detected in aortic tissue derived from the different experimental groups 

evaluated in this  study. WT mice and WT→WT chimeras placed on the HC diet exhibited a 

significantly increased gp91 

phox 

mRNA expression, compared to their normocholesterolemic 

counterparts. The HC-induced increase in gp91 

phox 

mRNA expression was not detected in IFN- γ→WT-HC chimeras, IFN-γR 

-/- 

mice and in IFN-γR 

-/- 

→WT chimeras, <\connective form=while text=while id=F1227_FUL6_12.txt.raw_185 relation=Contrast\> aortic tissue from 

WT→IFN-γR 

-/- 

-HC mice exhibited the increased gp91 

phox 

mRNA expression noted in WT-HC 

controls. 


It is now well recognized that the established risk factors for cardiovascular disease, including 

hypertension, diabetes, and hypercholesterolemia  are all associated with endothelial cell 

dysfunction that is manifested as an increased production of ROS and an impaired ability of arterial 

vessels to respond to endothelium-dependent  vasodilators 1,2,13,18,44. <\connective form=while text=While id=F1227_FUL6_12.txt.raw_186 relation=Contrast\> a variety of 

mechanisms have been proposed to explain the endothelial cell dysfunction elicited by risk factors, 

recent attention has focused on inflammatory cells and their activation products. Several recent 

reports have implicated T-lymphocytes and T-cell-derived cytokines in the oxidative stress, 

inflammatory cell recruitment, and impaired vasomotor dysfunction observed in both microvessels 

and large arteries during either diet-induced hypercholesterolemia 33,38 or angiotensin-induced 

hypertension 9. Studies on hypercholesterolemic mice suggest that IFN-γ may be a critical soluble 

mediator that links T-lymphocytes to the leukocyte-endothelial cell adhesion, oxidative stress, and 

vasomotor dysfunction elicited by this risk factor 31,38. The overall objective of the present study 

was to more clearly define the role of IFN-γ as a mediator of the altered gp91 

phox 

regulation, 

increased ROS production, and impaired EDV that are detected in aortic rings from mice placed on 

a cholesterol-enriched diet. 

A major aim of this study was to determine the cellular source of the IFN- γ that mediates the 

altered arterial vessel responses induced by HC.  A variety of cells residing <\connective form=either or text=either id=F1227_FUL6_12.txt.raw_225 relation=Conjunction\> within <\connective forme=either or text=or id=F1227_FUL6_12.txt.raw_225 relation=Conjunction\> outside 

the vascular wall or circulating in blood are known produce IFN-γ 10. Recent work on the 

inflammatory responses in postcapillary venules during HC has  implicated T- lymphocytes as a 

major cellular source of IFN-γ that mediates the leukocyte-endothelial cell adhesion and increased ROS production 31,33. In the present study, two different approaches, ie, IFN-γ 

-/- 

bone marrow 

chimeras and T-cell deficient mice were used to address whether circulating T-lymphocytes are 

responsible for the IFN-γ that participates in the HC-induced arterial vessel responses. The changes 

in EDV, gp91 

phox 

mRNA, and superoxide production observed in IFN-γ 

-/- 

bone marrow chimeras are 

consistent with the view that a circulating blood cell population is largely responsible for the HC-induced, IFN-γ-mediated responses to HC.  Our observation that the HC-induced elevated plasma 

IFN-γ concentration normally detected in wild type mice is <\connective form=also text=also id=F1227_FUL6_12.txt.raw_218 relation=Conjunction\> noted in CD8+ T-cell deficient 

mice, but not in CD4+ T-cell deficient mice, suggests that CD4+ T-cells are the likely source of 

IFN-γ in this model. These observations are consistent with reports implicating CD4+ T-cells as a 

major source of the IFN-γ that contributes to the development of atherosclerotic lesions in apoE 

-/- 

mice 28,42,43 as well as our previous work that  implicates CD4+ T-cells as mediators of the 

gp91 

phox 

upregulation, increased ROS production, and impaired EDV that are detected in aortic 

rings from mice placed on a cholesterol-enriched diet 38. 

Another major aim of this study was to determine whether the vascular responses mediated 

by IFN-γ result from direct engagement of IFN-γ receptors on the vessel wall or indirectly from 

engagement of receptors on circulating blood cells. Our findings in IFN-γ receptor-deficient IFN- 

γR 

-/- 

mice reveal that the HC-induced, IFN-γ-mediated effects on aortic tissue require the 

engagement of this cytokine with its receptor.  This involvement of IFN-γ receptors in the early 2 

weeks of cholesterol-enriched diet HC-induced vascular dysfunction is consistent with results 

showing that the later development of atherosclerotic lesions during HC is significantly blunted in 

apoE-/- mice that are crossed with IFN-γR 

-/- 

mice 8. Furthermore, our observation that IFN-γR 

-/- 

mice exhibit an attenuated HC-induced gp91 

phox 

upregulation and superoxide production is consistent with studies reporting that the engagement of IFN-γ with its receptor on osteoclasts leads 

to gp91 

phox 

-dependent production of superoxide 40. 

Since IFN-γ receptors are expressed on nearly all cells, including endothelial cells, vascular 

smooth muscle, T-cells, and macrophages 27, we used IFN-γR 

-/- 

bone marrow chimeras to 

determine whether the cytokine is exerting its primary action on a circulating blood cell or on a 

vascular wall/extravascular cell  population in HC mice. Our findings are consistent with the 

involvement of IFN-γ receptors on a populations of circulating blood cells, whose activation by 

IFN-γ leads to the HC-induced upregulation of gp91 

phox 

, increased superoxide production and 

impaired EDV in mouse aorta.  <\connective form=while text=While id=F1227_FUL6_12.txt.raw_187 relation=Synchronous\> the nature of this blood cell receptor- mediated response to 

HC remains unclear, it is possible that IFN-γ is acting on receptors either in an autocrine fashion to 

further stimulate CD4+ T-cells which appear to produce the cytokine in this model or as a 

paracrine agent to stimulate another blood cell  population eg, platelets, NK cells to liberate a 

soluble factor that ultimately mediates the oxidative and vasomotor responses to HC.  Additional 

work is needed to more clearly identify the IFN-γR-bearing cells that are activated during HC and 

how these cells mediate the vascular dysfunction. 

IFN-γ signaling is known to occur through both STAT-1 signal transducer and activator of 

transcription-1-dependent and –independent pathways 23,29. We considered  STAT-1  to  be  a 

likely candidate for the signaling pathway that  mediates the HC-induced vascular responses 

following engagement of IFN-γ to its receptor <\connective form=because text=because id=F1227_FUL6_12.txt.raw_188 relation=Cause\> this signaling pathway has been linked to 

IFN-γ-induced transcription of NADPH oxidase 14. <\connective form=however text=However id=F1227_FUL6_12.txt.raw_84 relation=Contrast\>, our results from STAT-1 deficient 

mice placed on a cholesterol-enriched diet are not  consistent with a major role for this signaling pathway in the IFN-γ mediated changes in gp91 

phox 

expression, increased superoxide production 

and impaired EDV. IFN-γ regulatory factor-1 IRF-1 is another intracellular signal that can mediate 

the cellular responses to IFN-γ receptor activation 23, <\connective form=and text=and id=F1227_FUL6_12.txt.raw_167 relation=Conjunction\> this signaling molecule is <\connective form=also text=also id=F1227_FUL6_12.txt.raw_219 relation=Conjunction\> known to 

mediate the transcription of gp91 

phox 

in cells stimulated with IFN-γ 14,16. In addition, IFN-γ 

induced nitric oxide production by vascular endothelial cells appears to result from IRF-1 activation 

11. Our analysis of the contribution of IRF-1 to the HC-induced, IFN-γ mediated vascular 

responses using IRF-1 deficient mice does not provide support for this signaling pathway in our 

model. Since our findings suggest that neither STAT-1 nor IRF-1 is involved in the IFN-γ-mediated 

upregulation of gp91 

phox 

, increased superoxide production, and impaired EDV, then other signaling 

pathways should be considered. It is known that IFN-γ can activate other signaling pathways 

including IkB kinase IKK and the MAP kinases, ERKs and Src 21,24.  The MAP kinase and 

nuclear factor kappa-B pathways appear to be reasonable candidate mediators of the HC-induced, 

IFN-γ-mediated responses since both have been implicated in the transcriptional regulation of 

NADPH oxidase 7,40. Additional work is needed to assess the potential contribution of these 

signaling molecules to the IFN-γ-mediated vascular responses to hypercholesterolemia. 



In a recent report from our laboratory 39, evidence is provided to implicate platelelet-derived RANTES as the final mediator of HC-induced, CD4+ T-cell dependent oxidative stress and 

vasomotor dysfunction. These findings, coupled to data summarized in the present study, are 

consistent with the sequence of events depicted in Figure 6, where HC causes CD4+ T-cells to 

release IFN-γ. The cytokine <\connective form=then text=then id=F1227_FUL6_12.txt.raw_56 relation=Asynchronous\> engages with receptors directly on platelets or another blood cell 

population that subsequently activates platelets to release the chemokine, RANTES. This 

chemokine <\connective form=then text=then id=F1227_FUL6_12.txt.raw_57 relation=Asynchronous\> binds avidly to glycosaminoglycans on the endothelial cell surface, which greatly increases the concentration of RANTES in the vicinity of its receptors. RANTES receptor activation 

<\connective form=then text=then id=F1227_FUL6_12.txt.raw_58 relation=Asynchronous\> results in the upregulation of Nox-2 via  yet undefined mechanisms that do not appear to 

involve either STAT-1 or IRF-1 but may include MAP kinase and/or G-protein coupled pathways. 

The subsequent enhanced production of superoxide leads to inactivation of endothelial nitric oxide 

and a consequent impairment of acetylcholine-induced vasodilation. Additional work is needed to 

test the validity of the scheme outlined in Figure 6 and to more precisely define the cellular and 

molecular mechanisms that underlie immune cell mediated vasomotor dysfunction.