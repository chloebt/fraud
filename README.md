# README #

Code and data for fraud detection.

### Code ###

* classif/ code to run the experiments, look at bash scripts e.g. *run-all.sh*. The experiments are, for one type of features (i.e. the name of the directory of the in data):
    * LOO
    * Nested cross-validation with a K-fold as inner and outer loop
    * Nested cross-validation with a LOO as outer loop and a K-fold as inner loop
This directory also contains *plot_scores.py* to plot the scores of all the experiments (*classify_fraud.py* output files with the scores for each experiments for the different trials).
* feats/ code to build the feature files (format *svmlight*) from the original data. Look at *feat.sh*.
This directory contains the code to: 
    * build the feature files (*feat_fraud.py*), 
    * compute the Randomized stability selection (*selec_feats.py*), 
    * re-write the feature files keeping only the selected features (*purge_features.py*), 
    * compute the chi2 scores for each features for analysis (*chi2.py*).
To obtain a dataset with different sets of features, use *concat_svmlight.py*.
* preprocess/ contains the code to pre-process the original data

### Data ###

* features/ data used for classification, one subdirectory per feature set
* original/ original data 
* conllu-utf8/ data parsed, used to build the features
* resources/ contains conn/ with the data automatically annotated with connectives and explicit relations (PDTB level 1), and some lexicon (MPQA, Inquirer, Levin classes)

### Expe ###

expe/ contains the scores for the different expe for each trial and each validation scheme. These files are used to build the plots.