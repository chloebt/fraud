#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e



DATA=$1 #data/fraud/
FEAT=$2 #conn or bow ...
ALGO=$3 #maxent, pa ...
MODE=$4 #loo, opt


INPATH=${DATA}/${FEAT}
MODEL=${INPATH}/model_${ALGO}_${MODE}
PRED=${INPATH}/pred_${ALGO}_${MODE}



python navi/other_projects/fraud/code/classify_fraud.py --inpath ${INPATH} --outmodel ${MODEL} --outpred ${PRED} --algo ${ALGO} --feat ${FEAT}
