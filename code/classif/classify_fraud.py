#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Requires scikit 18 (python3)
(on Obi: peachtree conda env!)
'''

# from __future__ import print_function
import matplotlib as mpl
mpl.use('Agg')
import argparse, os, sys, subprocess, shutil, codecs, random
import numpy as np


from sklearn import linear_model, svm
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, make_scorer

from sklearn.model_selection import GridSearchCV, cross_val_score, KFold, LeaveOneOut

# from sklearn.grid_search import GridSearchCV #change in v20
from sklearn.datasets import load_svmlight_file
from sklearn.externals import joblib
from scipy.sparse import hstack, vstack
# from sklearn.cross_validation import LeaveOneOut,KFold,cross_val_score #model_selection in v18

from sklearn.utils import shuffle

clf2params = {
        "nb":[{'alpha':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]}],
        "maxent":[{'penalty':['l1'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]},
            {'penalty':['l2'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]}],
        "pa":[{'loss':['hinge'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100], 'n_iter':[1, 5, 50, 100, 200, 500]},
            {'loss':['squared_hinge'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100], 'n_iter':[1, 5, 50, 100, 200, 500]}],
        "svc":[{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                     'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
        }
# 0.001, 0.005, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 5, 10, 20, 30, 40, 50, 100

def main( ):
    parser = argparse.ArgumentParser(
            description='Classification based on cross-validation: only a train file is required.')
    parser.add_argument('--inpath', dest='inpath', action='store',
            help='Input directory containing the train file (or directly give the train file)')

    parser.add_argument('--feat', dest='feat', action='store', help='Feature used (for naming files)')
    parser.add_argument('--train', dest='train', action='store', help='train')

    parser.add_argument('--outpath', dest='outpath', action='store', help='Output directory')
    parser.add_argument('--outmodel', dest='outmodel', action='store', default=None, help='Output file for model')
    parser.add_argument('--outpred', dest='outpred', action='store', default=None, help='Output file for pred')# TODO for each trial ??

    parser.add_argument('--algo', dest='algo', action='store', default='maxent', choices=['maxent', 'nb','svc', 'pa'],
            help='Algo to use: maxent, nb, pa, svc')

    # TODO other scores not implemented yet
    parser.add_argument('--score', dest='score', action='store', default='accuracy_score',
            help='Score to optimize: accuracy_score, precision_score, f1_score')
    parser.add_argument('--avg', dest='avg', action='store', default='micro',
            help='Average strategy for score to optimize: macro, micro, weighted?')
    args = parser.parse_args()

    if args.outpath and not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    if args.inpath:
        train = os.path.join( vars(args)['inpath'], "train.svmlight" )
    else:
        train = args.train

    X_train, y_train,n_features = load( train )
    print( "Train", X_train.shape )

    nested_crossval( X_train, y_train, args.outpath, args.feat, algo=args.algo )


def nested_crossval( X, y, outpath, feat, algo='maxent' ):
    print( "\n\nFEATURE:", feat, file=sys.stderr )
    # Number of random trials
    NUM_TRIALS = 10

    # Initialize classifier and set up possible values of parameters to optimize
    me, tuned_parameters = define_update( algo )

    # Array to store scores
    nested_scores_loo = np.zeros(NUM_TRIALS)
    nested_scores_kf = np.zeros(NUM_TRIALS)
    loo_scores = np.zeros(NUM_TRIALS)

    # Loop for each trial
    for i in range(NUM_TRIALS):
        print( "\nTrial", i, file=sys.stderr )

        # First shuffle the data (ie no shuffling option in new LOO)
        Xs,ys = shuffle( X, y, random_state=i)

        # 1) LOO on all data (ie reproduce prev work)
        loo_cv = LeaveOneOut(  )
        clf = GridSearchCV(estimator=me, param_grid=tuned_parameters, cv=loo_cv, n_jobs=4)
        clf.fit(Xs, ys)
        loo_scores[i] = clf.best_score_

        # Choose cross-validation techniques for the inner and outer loops, independently of the dataset.
        # E.g "LabelKFold", "LeaveOneOut", "LeaveOneLabelOut", etc.

        # Nested with KF as inner and outer
        outer_cv = KFold( n_splits=5 )
        inner_cv_kf = KFold( n_splits=5 )

        # Nested with KF as inner, LOO as outer
        outer_loo = LeaveOneOut()
        inner_cv_kf_loo = KFold( n_splits=5 ) # not the same indices as the other, use more data
        # inner_cv_loo = LeaveOneOut(  )

        cv_loo_scores, cv_kf_scores = [], []

#         # 2) KF outer, KF inner
        kf_true, kf_pred = [], []
#         print( "Running outer KF: ", file=sys.stderr, end='' )
#         for k, (train_index, test_index) in enumerate( outer_cv.split(Xs) ):# Do k times
#             print( k, sep='', end=', ', flush=True)
#             X_train, X_test = Xs[train_index], Xs[test_index]
#             y_train, y_test = ys[train_index], ys[test_index]
#             kf_true.extend( y_test )
#
#             # -- KF as inner
#             clf = GridSearchCV(estimator=me, param_grid=tuned_parameters, cv=inner_cv_kf, n_jobs=4)
#             clf.fit( X_train, y_train )
#             preds = clf.predict( X_test ) # eval on the fold left out
#             kf_pred.extend( preds )
#
        # 3) LOO outer, KF inner
        loo_true, loo_pred = [], []
        print( "\nRunning outer LOO: ", file=sys.stderr, end='' )
        for j, (train_index, test_index) in enumerate( outer_loo.split( Xs ) ): # Do lenght times
            if j%100 == 0:
                print( j, sep='', end=', ', flush=True)
            X_train, X_test = Xs[train_index], Xs[test_index]
            y_train, y_test = ys[train_index], ys[test_index]
            loo_true.extend( y_test )

            # -- KF as inner
            clf = GridSearchCV(estimator=me, param_grid=tuned_parameters, cv=inner_cv_kf_loo, n_jobs=4)
            clf.fit( X_train, y_train )
            preds = clf.predict( X_test ) # eval on the fold left out
            loo_pred.extend( preds )


#         # -- LOO as inner TODO?
#             clf = GridSearchCV(estimator=me, param_grid=tuned_parameters, cv=inner_cv_loo, n_jobs=4)
#             clf.fit(X_train, y_train) # grid search on the train part
#             preds = clf.predict( X_test ) # eval on the fold left out

#         nested_scores_loo[i] = accuracy_score( kf_true, kf_pred )
#         nested_scores_kf[i] = accuracy_score( loo_true, loo_pred )

        nested_scores_loo[i] = accuracy_score( loo_true, loo_pred )

#         print( "\nRound", i,'N-LOO / N-KF / LOO', nested_scores_loo[i], nested_scores_kf[i], loo_scores[i] )

        print( "\nRound", i,'N-LOO / LOO', nested_scores_loo[i], loo_scores[i] )

    joblib.dump( loo_scores, os.path.join( outpath, 'loo_scores_'+algo+'_'+feat) )
    joblib.dump( nested_scores_loo, os.path.join( outpath, 'nested_scores_loo_'+algo+'_'+feat) )
#     joblib.dump( nested_scores_kf, os.path.join( outpath, 'nested_scores_kf_'+algo+'_'+feat) )

    score_difference_loo = loo_scores - nested_scores_loo
#     score_difference_kf = nested_scores_loo - nested_scores_kf

    print("\nLOO vs CV-LOO - Average difference of {0:6f} with std. dev. of {1:6f}."
          .format(score_difference_loo.mean(), score_difference_loo.std()))

#     print("CV-LOO vs CV-KF - Average difference of {0:6f} with std. dev. of {1:6f}."
#           .format(score_difference_kf.mean(), score_difference_kf.std()))

    print( "Non nested scores with LOO:", loo_scores, loo_scores.mean() )
    print( "Nested scores with LOO:", nested_scores_loo, nested_scores_loo.mean() )
#     print( "Nested scores with KF:", nested_scores_kf, nested_scores_kf.mean() )


def define_score_fct( score, average='macro' ):
    # DEFINE SCORE FUNCTION
    if score == 'accuracy_score':
        return make_scorer(accuracy_score)
    elif score == 'precision_score':
        return  make_scorer(precision_score, average=average)
    elif score == 'f1_score':
        return  make_scorer(f1_score, average=average)
    else:
        sys.exit( "Unknwon score "+score )

def define_update( update ):
    # DEFINE TUNED PARAMETERS AND UPDATE
    if update == 'nb':
        return MultinomialNB(), clf2params["nb"]
    elif update == 'maxent':
        return linear_model.LogisticRegression( n_jobs=1 ), clf2params["maxent"]
    elif update == 'pa':
        return linear_model.PassiveAggressiveClassifier(), clf2params["pa"]
    elif update == "svc":
        return svm.SVC(), clf2params["svc"]
    else:
        sys.exit( "Unknown algo "+update )


def load( _file, n_features=-1 ):
    if n_features<0:
        X,y = load_svmlight_file( _file )
    else:
        X,y = load_svmlight_file( _file, n_features=n_features )
    return X, y, X.shape[1]


def scores( gold, pred, score='acc' ):
    print( "Acc:", accuracy_score( gold, pred ) )
    print( classification_report( gold, pred ) )
    print( confusion_matrix( gold, pred ) )
    if score == 'acc':
        return accuracy_score( gold, pred )


if __name__ == '__main__':
    main()



