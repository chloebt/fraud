#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e


SRC=discoursecph/fraud/code/classif/
DATA=$1 #data/fraud/
OUT_EXPE=$2


# One feature
for FEAT in conn relexpl polarity causal # ngram treelet bow
do 
    echo
    echo -------------------------------------------------------
    echo ${FEAT}
    OUT=${DATA}/${FEAT}/clf
    mkdir -p ${OUT}
    for ALGO in maxent #pa svc nb
    do  
        TRAIN=${DATA}/${FEAT}/train.svmlight #.selec 
#         MODEL=${OUT}/model_selec_${ALGO}_${MODE} #_selec
#         PRED=${OUT}/pred_selec_${ALGO}_${MODE} #_selec
        OUTDIR=${OUT_EXPE}/plot_scores/


        echo ${FEAT} ${ALGO}  
        #--outmodel ${MODEL} --outpred ${PRED} 
        python ${SRC}classify_fraud.py --train ${TRAIN} --algo ${ALGO} --outpath ${OUTDIR} --feat ${FEAT}

    done
done




# #for FEAT in bow+ngram conn+relexpl conn+treelet conn+relexpl+bow+treelet+ngram
# for FEAT in bow+ngram conn+relexpl bow+ngram+treelet bow+ngram+conn+relexpl bow+ngram+treelet+conn+relexpl bow+ngram+treelet+conn+relexpl+polarity 
# do
#     echo
#     echo -------------------------------------------------------
#     echo ${FEAT}
#     OUT=${DATA}/${FEAT}/clf
#     mkdir -p ${OUT}
#     for ALGO in maxent #pa svc nb
#     do
# 
#         for MODE in ncv #opt loo 
#         do
#         echo MODE ${MODE}
# #         INPATH=${DATA}/${FEAT}
#         TRAIN=${DATA}/${FEAT}/train.selec.svmlight #.selec 
# #         MODEL=${OUT}/model_selec_${ALGO}_${MODE} #_selec
# #         PRED=${OUT}/pred_selec_${ALGO}_${MODE} #_selec
#         OUTDIR=${OUT_EXPE}/plot_scores/
# 
# 
#         echo ${FEAT} ${ALGO}  
#         # --outmodel ${MODEL} --outpred ${PRED} 
#         python ${SRC}classify_fraud.py --train ${TRAIN} --algo ${ALGO} --outpath ${OUTDIR} --feat ${FEAT}
# 
#     done
# done
# done
# 
