#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib as mpl
mpl.use('Agg')

import argparse, os, sys, subprocess, shutil, codecs, random
import numpy as np
from sklearn.externals import joblib

from matplotlib import pyplot as plt

def main( ):
    parser = argparse.ArgumentParser(
            description='Read fcking file')

    # If you want to print for all the experiments, use the inpath option (will read for all features)
    parser.add_argument('--inpath', dest='inpath', action='store', default='experiments/fraud_workshopEMNLP17/plot_scores/', help='Input directory')
    parser.add_argument('--outpath', dest='outpath', action='store', default='experiments/fraud_workshopEMNLP17/plot_scores/', help='Output directory')

    # If you want to print for one experiment only, use the following options
    parser.add_argument( '--loo', dest='loo', action='store', help='LOO scores' )
    parser.add_argument( '--nloo', dest='nloo', action='store', help='Nested LOO scores' )
    parser.add_argument( '--nkf', dest='nkf', action='store', help='Nested KF scores' )


    args = parser.parse_args()

    if args.loo and args.nloo and args.nkf:
        loo_sc, nested_loo_sc, nested_kf_sc, feat = args.loo, args.nloo, args.nkf, args.loo.split('_')[-1]
        print( "\nReading results for features: ", feat )
        loo_sc, nested_loo_sc, nested_kf_sc = read_scores( loo_f, nested_loo_f, nested_kf_f )
        print_mean_scores( loo_sc, nested_loo_sc, nested_kf_sc )
        plot_mean_scores_over_trials( loo_sc, nested_loo_sc, nested_kf_sc, args.outpath, feat=feat )


    else:
        feats = set()
        for f in [_f for _f in os.listdir( args.inpath ) if not _f.startswith( '.') and not _f.startswith( 'plot' )]:
            feats.add( f.split('_')[-1] )
        feats = list(feats)

        for f in feats:
            if not os.path.isfile( os.path.join( args.outpath, 'plot_fraud_'+f+'.png' ) ):

                # Error in results files:
                # - experiments/fraud_workshopEMNLP17_2/plot_scores/ the N LOO scores are the N KF scores
                # - experiments/fraud_workshopEMNLP17/plot_scores/ the N LOO are the N LOO
                loo_f = os.path.join( args.inpath, 'loo_scores_maxent_'+f )
#                nested_loo_f = os.path.join( args.inpath, 'nested_scores_loo_maxent_'+f )
                nested_kf_f = None
#                 nested_kf_f = os.path.join( args.inpath, 'nested_scores_kf_maxent_'+f )
#
#                 nested_kf_f = os.path.join( args.inpath, 'nested_scores_loo_maxent_'+f )
                nested_loo_f = os.path.join( args.inpath, 'nested_scores_kf_maxent_'+f )

                print( "\nReading results for features: ", f )
                loo_sc, nested_loo_sc, nested_kf_sc = read_scores( loo_f, nested_loo_f, nested_kf_f )
                print_mean_scores( loo_sc, nested_loo_sc, nested_kf_sc )
    #             plot_mean_scores_over_trials( loo_sc, nested_loo_sc, nested_kf_sc, args.outpath, feat=f )

                plot_mean_scores_over_trials_bar( loo_sc, nested_loo_sc, nested_kf_sc, args.outpath, feat=f )

#                 plot_mean_scores_over_trials_loo_kf( loo_sc, nested_loo_sc, nested_kf_sc, args.outpath, feat=f )


def read_scores( loo_f, nested_loo_f, nested_kf_f=None ):
    if nested_kf_f:
        return joblib.load( loo_f ), joblib.load( nested_loo_f), joblib.load( nested_kf_f)
    else:
        return joblib.load( loo_f ), joblib.load( nested_loo_f), None
def print_mean_scores( loo, nloo, nkf ):
    print( "LOO", np.mean( loo ) )
    print( 'NLOO', np.mean( nloo ) )
#     print( 'NKF', np.mean( nkf ) )

def plot_mean_scores_over_trials( loo, nloo, nkf, outpath, feat='unk' ):
    if feat == 'bow':
        feat = "unigrams"
    if feat == "ngram":
        feat = "2-3-grams"

    plot_file = os.path.join( outpath, 'plot_fraud_'+feat+'.png' )

    plt.figure()
    plt.subplot(211)
    loo_line, = plt.plot(loo, color='r')
    nloo_line, = plt.plot(nloo, color='b')
    nkf_line, = plt.plot(nkf, color='green')


    plt.axhline( y=np.mean( nloo ), color='b',alpha=0.3)
    plt.axhline( y=np.mean( nkf ), color='g',alpha=0.3)

    plt.ylabel("score", fontsize="11")
    plt.legend([loo_line, nloo_line, nkf_line],
               ["LOO", "NCV LOO", "NCV 4F"],
               bbox_to_anchor=(0, 0.4, 1, -0.1), fontsize="9")

    plt.savefig( plot_file, bbox_inches='tight' )


def plot_mean_scores_over_trials_bar( loo, nloo, nkf, outpath, feat='unk' ):
    if feat == 'bow':
        feat = "unigrams"
    if feat == "ngram":
        feat = "2-3-grams"

    score_difference = loo - nloo

    print("Average difference of {0:6f} with std. dev. of {1:6f}."
      .format(score_difference.mean(), score_difference.std()))

    plot_file = os.path.join( outpath, 'plot_fraud_loo-nloo'+feat+'.png' )



    # Plot bar chart of the difference.
    plt.figure()
    plt.subplot(211)
    difference_plot = plt.bar(range(10), score_difference)
    plt.xlabel("Individual Trial #")
    plt.legend([difference_plot],
               ["Non-Nested LOO - Nested LOO"],
               bbox_to_anchor=(0, 1, .8, 0))
    plt.ylabel("score difference", fontsize="14")

    plt.savefig( plot_file, bbox_inches='tight' )


def plot_mean_scores_over_trials_loo_kf( loo, nloo, nkf, outpath, feat='unk' ):
    if feat == 'bow':
        feat = "unigrams"
    if feat == "ngram":
        feat = "2-3-grams"

    score_difference = nkf - nloo

    print("Average difference of {0:6f} with std. dev. of {1:6f}."
      .format(score_difference.mean(), score_difference.std()))

    plot_file = os.path.join( outpath, 'plot_fraud_nkf-nloo'+feat+'.png' )



    # Plot bar chart of the difference.
    plt.figure()
    plt.subplot(211)
    difference_plot = plt.bar(range(10), score_difference)
    plt.xlabel("Individual Trial #")
    plt.legend([difference_plot],
               ["Nested KF - Nested LOO"],
               bbox_to_anchor=(0, 1, .8, 0))
    plt.ylabel("score difference", fontsize="14")

    plt.savefig( plot_file, bbox_inches='tight' )






if __name__ == '__main__':
    main()





