#!/usr/bin/python
# -*- coding: utf-8 -*-

# Python 3 !


from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random
from nltk.parse.dependencygraph import *


from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.datasets import dump_svmlight_file
from sklearn.externals import joblib

def main( ):
    parser = argparse.ArgumentParser(
            description='Read fraud data, generate features files.')
    parser.add_argument('--inpath', dest='inpath', action='store', help='Input directory (conllu-utf8/)')
    parser.add_argument('--outpath', dest='outpath',action='store', help='Output directory')
    parser.add_argument('--feat', dest='feat', action='store', default="bow", help='Type of features (bow, ngram..)')
    parser.add_argument('--labels', dest='labels', action='store', help='File associating a label (pos or neg) to each document')
    # External resources/annotations
    parser.add_argument('--conn', dest='conn', action='store', default=None, help='Connective annotation')
    parser.add_argument('--subj', dest='subjmpqa', action='store', help='Lexicon with polarity')
    parser.add_argument('--inq', dest='inquirer', action='store', help='Lexicon with sem info')
    parser.add_argument('--levin', dest='levin', action='store', help='Lexicon with Levin classes')
    args = parser.parse_args()

    if not args.labels:
        sys.exit( 'Please provide a label file (--labels)' )

    outpath = args.outpath
    if not outpath:
        outpath = os.path.join( args.inpath, args.feat )
    if not os.path.isdir( outpath ):
        os.mkdir( outpath )

    # No train/dev/test, only need to retrieve the label of each document
    fname2label, infiles = retrieve_files( args.inpath, args.labels,
            label=True, ext=".txt.conllu" )

    min_df=1 # minimum frequence of the features
    if args.feat == "bow":
        X, vectorizer = bag_of_word( infiles, min_df=min_df )
    if args.feat == "ngram":# 1, 2 and 3 grams
        X, vectorizer = ngram_words( infiles, min_df=min_df ) # TODO tfidf?
    elif args.feat == "treelet":
        X, vectorizer = treelet( infiles, min_df=min_df )
    elif args.feat == "conn":
        X, vectorizer = explicit_conn( infiles, args.conn, min_df=min_df )
    elif args.feat == "relexpl":
        X, vectorizer = explicit_rel( infiles, args.conn, min_df=min_df )
    elif args.feat == "polarity":
        lex_pol = read_lexicon_pol( args.subjmpqa )
        X, vectorizer = polarity( infiles, lex_pol, min_df=min_df )
    elif args.feat == "inquirer": # keep all the categories
        lex_inq, cats_inq = read_lexicon_inq( args.inquirer )
        X, vectorizer = inquirer( infiles, lex_inq, cats_inq, min_df=min_df )
    elif args.feat == "causal": # keeponly causal terms
        lex_inq, cats_inq = read_lexicon_inq( args.inquirer ) # keep only causality
        X, vectorizer = causal( infiles, lex_inq, cats_inq, min_df=min_df )
    elif args.feat == "pronounsep":
        X, vectorizer = pronoun( infiles, min_df=min_df )
    elif args.feat == "hedges":
        X, vectorizer = hedges( infiles, min_df=min_df )

    # -- Get labels
    y = get_labels( infiles, fname2label )

    # -- Write data
    write( X, y, os.path.join( outpath, "train.svmlight" ) )

    # -- Save vocabulary
    #print( "#Vocab", len(vectorizer.vocabulary_), vectorizer.vocabulary_ )
    joblib.dump( vectorizer.vocabulary_, os.path.join( args.outpath, 'vocab-'+args.feat ), compress=3 )



# ----------------------------------------------------------------------------------
# --- FEATURES
# ----------------------------------------------------------------------------------


def bag_of_word( train, min_df=1 ):
    # represent each corpus by a list of words
    train_wds = get_words( train )
    vectorizer, X_train = build_vectorizer(train_wds, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train_wds ), "\nX_train:", X_train.shape )
    return X_train, vectorizer


def ngram_words( train, min_df=1 ):
    train_wds = get_words( train )
    vectorizer = CountVectorizer(ngram_range=(2, 3), min_df=min_df, stop_words='english')
    X_train = vectorizer.fit_transform(train_wds)
    print( '#Train Docs', len( train_wds ), "\nX_train:", X_train.shape )
    return X_train, vectorizer


def explicit_conn( train, conn_annot, min_df=1 ):
    conn_files = getFiles( conn_annot, ext='.txt.raw.annot' )
    train_conn = get_conn( train, conn_files )
    vectorizer, X_train = build_vectorizer(train_conn, min_df=min_df)
    print( '#Train Docs', len( train_conn ), "\nX_train:", X_train.shape )
    return X_train, vectorizer


def explicit_rel( train, conn_annot, min_df=1 ):
    conn_files = getFiles( conn_annot, ext='.txt.raw.annot' )
    train_rel = get_rel( train, conn_files )
    vectorizer, X_train = build_vectorizer(train_rel, min_df=min_df)
    print( '#Train Docs', len( train_rel ), "\nX_train:", X_train.shape )
    return X_train, vectorizer

def treelet( train, min_df=1 ):
    train_treelets = get_dependencies( train ) # list of doc treelets (# feature name = str treelet)
    # Need a tokenizer based on spaces
    vectorizer, X_train = build_vectorizer(train_treelets, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train_treelets ), "\nX_train:", X_train.shape )
    return X_train, vectorizer

def polarity( infiles, lex_pol, min_df=1 ):
    train = get_polarity( infiles, lex_pol )
    vectorizer, X_train = build_vectorizer(train, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train ), "\nX_train:", X_train.shape )
    return X_train, vectorizer

def inquirer( infiles, lex_inq, cats_inq, min_df=1, check_pos=True, causal_only=False ):
    train = get_inquirer( infiles, lex_inq, cats_inq, check_pos=check_pos, causal_only=False )
    vectorizer, X_train = build_vectorizer(train, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train ), "\nX_train:", X_train.shape )
    return X_train, vectorizer

def causal( infiles, lex_inq, cats_inq, min_df=1, check_pos=True ):
    train = get_inquirer( infiles, lex_inq, cats_inq, check_pos=check_pos, causal_only=True )
    vectorizer, X_train = build_vectorizer(train, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train ), "\nX_train:", X_train.shape )
    return X_train, vectorizer

def pronoun( infiles, min_df=1 ):
    train = get_pronoun( infiles )
    vectorizer, X_train = build_vectorizer(train, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train ), "\nX_train:", X_train.shape )
    return X_train, vectorizer

def hedges( infiles, min_df=1 ):
    train = get_hedges( infiles )
    vectorizer, X_train = build_vectorizer(train, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train ), "\nX_train:", X_train.shape )
    return X_train, vectorizer


# ----------------------------------------------------------------------------------
# --- READ
# ----------------------------------------------------------------------------------

# -- Words
def get_words( files ):
    '''
    List of words for each document
    List of string representing each document, to be passed to countVect
    '''
    words = []
    count_doc = 0
    for _file in files:
        with open( _file ) as f:
            doc_wds = []
            sentences = [s.strip() for s in f.read().split('\n\n') if s.strip() != '']
            for sent in sentences:
                tokens = [s.strip() for s in sent.split('\n') if not s.strip() == '']
                for t in tokens:
                    id_tok, form, lemma, pos, posx, morph, head, fct, _, _ = t.split('\t')
                    doc_wds.append( form )
            words.append( ' '.join( doc_wds ) )
            count_doc += 1
    print( "\#Documents", count_doc, len( words ) )
    return words


def get_hedges( files ):
    '''
    List of hedge words
    List of string representing each document, to be passed to countVect
    '''
    hedges = []
    count_doc = 0
    for _file in files:
        with open( _file ) as f:
            doc_hedges = []
            sentences = [s.strip() for s in f.read().split('\n\n') if s.strip() != '']
            for sent in sentences:
                tokens = [s.strip() for s in sent.split('\n') if not s.strip() == '']
                for t in tokens:
                    id_tok, form, lemma, pos, posx, morph, head, fct, _, _ = t.split('\t')

                    # Many pos tagging errors, so based on the form
                    if form.lower() in HEDGES:
                        doc_hedges.append( form )
            hedges.append( ' '.join( doc_hedges ) )
            count_doc += 1
    print( "\#Documents", count_doc, len( hedges ) )
    return hedges


def get_pronoun( files ):
    '''
    List of pronouns for each document, group per person?
    List of string representing each document, to be passed to countVect
    '''
    pron = []
    count_doc = 0
    for _file in files:
        with open( _file ) as f:
            doc_pron = []
            sentences = [s.strip() for s in f.read().split('\n\n') if s.strip() != '']
            for sent in sentences:
                tokens = [s.strip() for s in sent.split('\n') if not s.strip() == '']
                for t in tokens:
                    id_tok, form, lemma, pos, posx, morph, head, fct, _, _ = t.split('\t')

                    # Many pos tagging errors, so based on the form
                    if form.lower() in ['i', 'you', 'we', 'he', 'she', 'it', 'they']:
                        doc_pron.append( form )
#                     if form.lower() in ['i', 'my', 'we', 'our', 'ours', 'myself', 'ouselves', 'us', 'me', 'mine']:
# #                         doc_pron.append( 'pro1st' )
#                         doc_pron.append( form )
#                     elif form.lower() in ['you', 'yours', 'yourself', 'yourselves']:
# #                         doc_pron.append( 'pro2nd' )
#                         doc_pron.append( form )
#                     elif form.lower() in ['she', 'her', 'he', 'his', 'they', 'their', 'them', 'himself', 'herself', 'themselves', 'it', 'itself', 'its', 'hers']:
# #                         doc_pron.append( 'pro3rd' )
#                         doc_pron.append( form )
            pron.append( ' '.join( doc_pron ) )
            count_doc += 1
    print( "\#Documents", count_doc, len( pron ) )
    return pron

def get_polarity( files, lex_pol ):
    polarities = []
    count_doc = 0
    not_found = set()
    for _file in files:
        with open( _file ) as f:
            doc_pol = []
            sentences = [s.strip() for s in f.read().split('\n\n') if s.strip() != '']
            for sent in sentences:
                tokens = [s.strip() for s in sent.split('\n') if not s.strip() == '']
                for t in tokens:
                    id_tok, form, lemma, pos, posx, morph, head, fct, _, _ = t.split('\t')
                    if form in lex_pol:
                        found = False
                        for t in lex_pol[form]:
                            if t['pos1'] == "anypos" or t['pos1'].lower() == pos.lower() or t['pos1'].lower()[:3] == pos.lower():
                                doc_pol.append( t['priorpolarity'] )
                                found = True
                                #print( form, pos, t['priorpolarity'] )
                                break
                        if not found:
                            for t in lex_pol[form]:
                                if (t['pos1'] == "adj" and pos == "VERB") or (t['pos1'] == "adj" and pos == "AUX"):
                                    doc_pol.append( t['priorpolarity'] )
                                    #print( form, pos, t['priorpolarity'] )
                                    found = True
                                    break
                        if not found:
                            if len( lex_pol[form] ) == 1:
                                doc_pol.append( lex_pol[form][0]['priorpolarity'] )
#                             else: # 65 forms where we can t easily decide
#                                 print( "\n Dealing with", form )
#                                 print( form, pos, lex_pol[form] )
#                                 not_found.add( form )
            # Filter ? keep only positive/negative ?
            polarities.append( ' '.join( doc_pol ) )
    return polarities

def get_inquirer( files, lex_inq, cats_inq, check_pos=True, causal_only=False ):
    inq_categories = []
    not_found = set()
    for _file in files:
        with open( _file ) as f:
            doc_inq = []
            sentences = [s.strip() for s in f.read().split('\n\n') if s.strip() != '']
            for s,sent in enumerate(sentences):
                tokens = [s.strip() for s in sent.split('\n') if not s.strip() == '']
                for t in tokens:
                    id_tok, form, lemma, pos, posx, morph, head, fct, _, _ = t.split('\t')
                    pos = pos.lower()
                    if lemma in lex_inq:
                        if causal_only:


                            doc_inq.extend( [lemma for c in range( len(cats_inq) )
                                if lex_inq[lemma][c].lower() == 'causal' or (cats_inq[c].lower() == 'causal' and lex_inq[lemma][c] != 'UNK') ] )

#                             doc_inq.extend( [lex_inq[lemma][c] for c in range( len(cats_inq) )
#                                 if lex_inq[lemma][c].lower() == 'causal' or (cats_inq[c].lower() == 'causal' and lex_inq[lemma][c] != 'UNK') ] )
                        else:
                            if check_pos == False or (check_pos and _check_pos_inq( lex_inq[lemma][-1], pos )):
                                doc_inq.extend( [lex_inq[lemma][c] for c in range( len(cats_inq) )
                                    if lex_inq[lemma][c] != 'UNK' and not cats_inq[c] in ['Entry', 'Othtags', 'FormLw', 'POS'] ] ) #lemma+'/'+cats_inq[c]+'/'+
            inq_categories.append( ' '.join( doc_inq ) )
    return inq_categories



def _check_pos_inq( lex_pos, pos ):
    # same pos or not ambiguous, keep it
    if lex_pos == pos or lex_pos == 'UNK':
        return True
    else:
        # correspondence
        if (lex_pos == 'verb' and pos == 'aux') or (lex_pos[-1] in ['noun', 'pronoun'] and pos == 'propn'):
            return True
        # strange things: eg primarily is an ADJ, safe is an ADV, dependent is a verb
        if (lex_pos == 'adverb' and pos == 'adj') or (lex_pos == 'adj' and pos == 'adv') or (lex_pos == 'verb' and pos == 'adj'):
            return True
    return False


# -- Connectives and explicit relations
def get_conn( dset_files, conn_files ):
    dset_conn = []
    for f in dset_files:
        doc_conn = []
        cf = findFile( f, conn_files )
        if cf == None:
            sys.exit( "Connective file not found "+f )
        lines = open( cf ).readlines()
        for l in lines:
            l = l.strip()
            _,_,connective,use,_,_,_,relation,_,_ = l.split('\t')
            if use == 'positive':
                doc_conn.append( connective.lower() )#only keep the connective in discourse reading
        dset_conn.append( ' '.join( doc_conn ) )
    #id	base_file	form	discourse_use	score_emploi	position	score_position	relation	score_relation	span_conn
    return dset_conn

def get_rel( dset_files, conn_files ):
    dset_rel = []
    for f in dset_files:
        doc_rel = []
        cf = findFile( f, conn_files )
        if cf == None:
            sys.exit( "Connective file not found "+f )
        lines = open( cf ).readlines()
        for l in lines:
            l = l.strip()
            _,_,connective,use,_,_,_,relation,_,_ = l.split('\t')
            if use == 'positive':
                doc_rel.append( relation.lower() )#only keep the connective in discourse reading
        dset_rel.append( ' '.join( doc_rel ) )
    #id	base_file	form	discourse_use	score_emploi	position	score_position	relation	score_relation	span_conn
    return dset_rel



# -- Syntactic dependencies
def get_dependencies( files ):
    corpus_treelet = []
    for i,_file in enumerate(files):
        doc_treelet = ''
        doc_one_treelets, doc_two_treelets, doc_three_treelets = [],[],[]
        with open( _file ) as f:
            # print( "Reading file", _file, i, '/', len( files ) )
            sentences = [s.strip() for s in f.read().split('\n\n') if s.strip() != '']
            # -- Transform to MALT TAB format to be able to use dependencygraph nltk class
            for i,sent in enumerate( sentences ):
                dg, one_tok_treelets = _build_graph( sent )#graph and one token treelets
                doc_one_treelets.extend( one_tok_treelets )

                two_tok_treelets = []
                for (h, r, d) in dg.triples():#(head_wd,head_tag), relation, (dep_wd, dep_tag)
                    two_tok_treelets.append( h[1]+'->'+r+'->'+d[1] )
                doc_two_treelets.extend( two_tok_treelets )

                three_tok_treelets = get_three_tok_treelets( dg )
                doc_three_treelets.extend( three_tok_treelets )
#             print( "Reading file", _file, i, '/', len( files ), len( sentences ), len( doc_three_treelets ) )
            doc_treelet += ' '.join( doc_one_treelets )+' '
            doc_treelet += ' '.join( doc_two_treelets )+' '
            doc_treelet += ' '.join( doc_three_treelets )
            corpus_treelet.append( doc_treelet )
    return corpus_treelet

def get_three_tok_treelets( dg ):
    '''
    Three tokens treelets are either:
    - one node dominating two nodes  DEP1 -> r1 -> NODE <- r2 <- Dep2
    - a chain: one node dominating another node that dominates another node NODE -> r1 -> Dep1 -> r2 -> Dep2
    '''
    three_tok_treelets = []
    for n in dg.nodes:
        node = dg.get_by_address(n)
        node_word = node['word']
        node_pos = node['tag']
        node_deps = node['deps']
        node_rel = node['rel']
        new_deps = flat_dep_list( node_deps )

        if len( new_deps ) > 1 and node_word != None:#not taking into account the fake root added
            new_deps = sorted( new_deps, key=lambda x:x[1] )
            # --> head dominating two nodes
            for i, (fct1, dep_add1) in enumerate( new_deps[:-1] ):
                for (fct2, dep_add2) in new_deps[i+1:]:
                    three_tok_treelets.append( dg.get_by_address(dep_add1)['tag']+'->'+fct1+'->'+node_pos+'<-'+fct2+'<-'+dg.get_by_address(dep_add2)['tag'] )

            # --> chains: head -> dep1 -> dep2
            for i, (fct1, dep_add1) in enumerate(new_deps):
                dependents = get_deps( dg, dep_add1 )# Look for dependents of dep1
                if len( dependents ) > 0:
                    for (fct2, dep_add2) in dependents:
                        three_tok_treelets.append( node_pos+'->'+fct1+'->'+dg.get_by_address(dep_add1)['tag']+'->'+fct2+'->'+dg.get_by_address(dep_add2)['tag'] )
    return three_tok_treelets

def flat_dep_list( node_deps ):
    new_deps = []
    for (fct, dep_add) in node_deps.items():
        for a in dep_add:
            new_deps.append( (fct,a) )
    return new_deps

def get_deps( dg, add ):
    '''
    Look for dependents of node at the address add
    '''
    deps = []
    for n in dg.nodes:
        if n == add:
            node = dg.get_by_address(n)
            node_deps = node['deps']
            new_deps = flat_dep_list( node_deps )
            return new_deps
    return []

def _build_graph( sent, tmpf='tmpdep.txt' ):
    '''
    Build a graph for the sentence sent using the class DependencyGraph from NLTK
    Return also the list of pos occurring in the doc ie the one token treelets
    '''
    pos_treelets = []
    tokens = [s.strip() for s in sent.split('\n') if not s.strip() == '']
    o = open( tmpf, 'w' )#required to read data from a file
    for t in tokens:
        id_tok, form, lemma, pos, posx, morph, head, fct, _, _ = t.split('\t')
        pos_treelets.append( pos )
        if int(head) == 0:
            fct = 'root'
        o.write( '\t'.join( [form, pos, head, fct] )+'\n' )
    o.close()

    depgraph = DependencyGraph()
    g = depgraph.load (tmpf, cell_separator='\t', top_relation_label='root' )

    if len( g ) != 1:
        print( open( tmp ).read() )
        sys.exit( "More than one graph?" )
    #dotg = g[0].to_dot()#should allow to visualize the graph with graphivz
    return g[0], pos_treelets





# ----------------------------------------------------------------------------------
# --- UTILS
# ----------------------------------------------------------------------------------

# --- WRITE
def write( X, y, _file ):
    dump_svmlight_file( X, y, _file )


# --- LABELS
def get_labels( infiles, file2label ):
    return _get_labels( infiles, file2label )


def _get_labels( files, file2label ):
    labels = []
    for f in files:
        f = os.path.basename(f).split('.')[0]
        if file2label[f] == 'pos':
            labels.append( 1 )
        elif file2label[f] == 'neg':
            labels.append( -1 )
        else:
            sys.exit( 'Unknown label', file2label[f] )
    return labels

# --- VECTORIZE
def build_vectorizer( words, tokenizer=None, min_df=1 ):
    vectorizer = CountVectorizer(min_df=min_df, tokenizer=tokenizer)
    X = vectorizer.fit_transform(words)
    return vectorizer, X

def tfidf( X_train, X_dev, X_test ):
    tf_transformer, X_train_tf = build_tfidf_transformer( X_train )
    X_dev_tf = tf_transformer.transform( X_dev )
    X_test_tf = tf_transformer.transform( X_test )
    return X_train_tf, X_dev_tf, X_test_tf

def build_tfidf_transformer( X_train ):
    tf_transformer = TfidfTransformer(use_idf=False).fit(X_train)
    X_train_tf = tf_transformer.transform(X_train)
    return tf_transformer, X_train_tf

# --- VARIA
def my_tokenizer(s):
    ''' because the text is already tokenized '''
    return s.split()

def findFile( tf, _Files ):
    basename = os.path.basename( tf ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' ).replace( '.conllu', '').replace( '.annot', '').replace( '.raw', '').replace( '.xml', '')
    for f in _Files:
        b = os.path.basename( f ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' ).replace( '.conllu', '').replace( '.annot', '').replace( '.raw', '').replace( '.xml', '')
#         print( "\t", b )
        if b == basename:
            return f
    return None

def getFiles( tbpath, ext=".edus" ):
    _files = []
    for p, dirs, files in os.walk( tbpath ):
        subdir = os.path.basename(p)
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for file in files:
            if not file.startswith('.') and file.endswith(ext):
                #and not 'ANOTADOR_B' in os.path.join( p,file )
                _files.append( os.path.join( p, file ) )
    return _files


def retrieve_files( inpath, labels, label=True, ext=".txt.conllu" ):
    fname2label = {_t.split('\t')[0].split('.')[0]:_t.split('\t')[1] for _t in [l.strip() for l in open( labels ).readlines()]}
    conlluFiles = getFiles( inpath, ext=ext )
    return fname2label, conlluFiles

def getFilesLabel( ):
    file2label = {}
    for f in [_f for _f in os.listdir( split_files ) if not _f.startswith( '.' )]:
        dset = os.path.basename( f.replace( '.txt', '' ) )
        set2files[dset] = []
        with open( os.path.join( split_files, f ) ) as myfile:
            for l in myfile.readlines():
                if not label:
                    file2set[l.strip()] = dset
                    set2files[dset].append( l.strip() )
                else:
                    file2set[l.split('\t')[0].strip()] = dset
                    set2files[dset].append( l.split('\t')[0].strip() )
                    basef = os.path.basename( l.split('\t')[0].strip() ).split('.')[0]
                    if basef in file2label:
                        sys.exit( 'Two files with the same basename ?', basef )
                    file2label[basef] = l.split('\t')[1].strip()
                if dset in set2count:
                    set2count[dset] += 1
                else:
                    set2count[dset] = 1
    print( ", ".join( [dset+':'+str(set2count[dset]) for dset in sorted(set2count.keys())] ) )
    return file2set, set2files, file2label







# ------ READERS FOR EXTERNAL LEXICONS
def read_lexicon_pol( subjmpqa ):
    '''
    Return a dictionary associating one word to a list of dictionary for each sense
    For each sense, the dict contains the follonwing info:
        - 'len'?? = 1
        - 'pos1' among ['verb', 'adj', 'anypos', 'noun', 'adverb']
        - 'priorpolarity' among ['negative', 'weakneg', 'neutral', 'both', 'positive']
        (weakneg only for one word: impassive)
        - 'type' among ['strongsubj', 'weaksubj']
        - 'stemmed1' ?? among ['y', 'n', '1']
    '''
    lex = {}
    lines = open( subjmpqa ).readlines()
    for l in lines:
        l = l.strip()
        info = {k.split('=')[0]:k.split('=')[1] for k in l.split()}
        word = info['word1']
        if not word in lex:
            lex[word] = [ {k.split('=')[0]:k.split('=')[1] for k in l.split() if not k.split('=')[0]=='word1'} ]
        else:
            lex[word].append( {k.split('=')[0]:k.split('=')[1] for k in l.split() if not k.split('=')[0]=='word1'} )
    return lex

def read_lexicon_inq( inquirer ):
    lex = {}
    with open( inquirer) as myf:
        lines = myf.readlines()
        cats = [e.strip() for e in lines[0].split('\t')]
#         print( cats, len( cats ) )
#         print( cats.index("Othtags") )
        for l in lines[1:]:
            l = l.strip()
            lemma, values = l.split('\t')[0].lower(), l.split('\t')[1:]
            if len( values ) < len( cats ):
                for i in range( (len( cats )-len( values )) ):
                    values.append( "" )
            if '#' in lemma: # ambiguity, ie one entry per meaning
                lemma = lemma.split('#')[0]
            lex[lemma] = []
            for v in values:
                if v == "":
                    lex[lemma].append( 'UNK' )
                else:
                    lex[lemma].append( v )
            # Find pos of the entry
            pos = _define_pos( lex, lemma, cats )
            lex[lemma].append( pos )
    cats.append( 'POS' )


    cats2val = {cats[i]:set() for i in range(len(cats))}
    for l in lex:
        for i in range( len( cats ) ):
            cats2val[cats[i]].add( lex[l][i] )

    return lex, cats


inq_tagset = ['prep', 'prep-adv', 'verb-modal', 'idiom-verb', 'idiom', 'noun-adj-adv', 'idiom-adv', 'idiom-adj', 'adj', 'adv', 'verb-adj', 'verb', 'noun', 'noun-adj', 'idiom-prep','adjective', 'adverb', 'idiom-noun', 'pron-adv', 'adj-adv', 'prep-adverb', 'verb-(aux)', 'pron', 'pronoun', 'adv-adj', 'conj-prep', 'idiom-conj', 'idiom-adverb', 'adjective-adv','conj', 'preposition', 'prep-conj-adv', 'modal', 'verb-idiom', 'noun-adv', 'noun-idiom', 'idiom-noun-adj', 'idiom-adj-adv', 'interjec', 'idiom-verb-noun', 'verb-adj-noun', 'verb-noun', 'adj-noun', 'particle', 'adv-adjective', 'adj-pron', 'adv-idiom', 'adjective-noun', 'idiom-adj-pron', 'interj-adv', 'idiom-modal', 'noun-adj-adv-intj', 'idiom-adv-prep', 'adj-idiom', 'idiom-adv-adj', 'idiom-interj', 'idiom-interject', 'adj-prep-conj', 'adv-adj-noun', 'idiom-pron', 'noun-adv-adj', 'adj-adv-noun', 'adv-adj-prep', 'noun-adjective', 'pron-adj-adv', 'conj-prep-adv', 'adverb-idiom', 'idiom-intj', 'infinitive', 'conjunction']

def _define_pos( lex, lemma, cats ):
    if lex[lemma][-1] != "" and lex[lemma][-1] != "|":
        l = lex[lemma][-1].split()
        if len(l)>=3:
            pos1, pos2 = l[1], l[2] # try to find the pos tag
            if pos1[-1] == ":":
                pos1 = pos1[:-1]
            if pos2[-1] == ":":
                pos2 = pos2[:-1]
            if pos1[-1] == '.':
                pos1 = pos1[:-1]
            if pos2[-1] == ".":
                pos2 = pos2[:-1]

            if pos1 in inq_tagset:
                return pos1
            if pos2 in inq_tagset:
                return pos2
#         else:
#             print( l ) # always ['UNK']
    # If it didn t work, find other potential sources of info
    if lex[lemma][cats.index("IAV")] != 'UNK' or lex[lemma][cats.index("DAV")] != 'UNK' or lex[lemma][cats.index("SV")] != 'UNK':
        return 'verb'
    if lex[lemma][cats.index("IPadj")] != 'UNK' or lex[lemma][cats.index("IndAdj")] != 'UNK':
        return 'adj'
    if lex[lemma][cats.index("Othtags")] != 'UNK':
        for e in lex[lemma][cats.index("Othtags")].split( " " ):
            if e.lower() in inq_tagset:
                return e.lower()
            if e == "SUPV":
                return "verb"
    return "UNK"


HEDGES=[
  "a bit",
  "about",
  "actually",
  "allege",
  "alleged",
  "almost",
  "almost never",
  "always",
  "and all that",
  "and so forth",
  "apparent",
  "apparently",
  "appear",
  "appear to be",
  "appeared",
  "appears",
  "approximately",
  "around",
  "assume",
  "assumed",
  "assumes",
  "assumption",
  "at least",
  "basically",
  "be sure",
  "believe",
  "believed",
  "believes",
  "bunch",
  "can",
  "certain",
  "certainly",
  "clear",
  "clearly",
  "conceivably",
  "consider",
  "considered",
  "considers",
  "consistent with",
  "could",
  "couple",
  "definite",
  "definitely",
  "diagnostic",
  "don't know",
  "doubt",
  "doubtful",
  "effectively",
  "estimate",
  "estimated",
  "estimates",
  "et cetera",
  "evidently",
  "fairly",
  "few",
  "find",
  "finds",
  "found",
  "frequently",
  "generally",
  "guess",
  "guessed",
  "guesses",
  "hopefully",
  "if i'm understanding you correctly",
  "improbable",
  "in general",
  "in my mind",
  "in my opinion",
  "in my understanding",
  "in my view",
  "inconclusive",
  "indicate",
  "kind of",
  "largely",
  "like",
  "likely",
  "little",
  "look like",
  "looks like",
  "mainly",
  "many",
  "may",
  "maybe",
  "might",
  "more or less",
  "most",
  "mostly",
  "much",
  "must",
  "my impression",
  "my thinking is",
  "my understanding is",
  "necessarily",
  "occasionally",
  "often",
  "overall",
  "partially",
  "perhaps",
  "possibility",
  "possible",
  "possibly",
  "practically",
  "presumable",
  "presumably",
  "pretty",
  "probability",
  "probable",
  "probably",
  "quite",
  "quite clearly",
  "rare",
  "rarely",
  "rather",
  "read",
  "really",
  "roughly",
  "say",
  "says",
  "seem",
  "seemed",
  "seems",
  "seldom",
  "several",
  "should",
  "so far",
  "some",
  "somebody",
  "somehow",
  "someone",
  "something",
  "something or other",
  "sometimes",
  "somewhat",
  "somewhere",
  "sort of",
  "speculate",
  "speculated",
  "speculates",
  "suggest",
  "suggested",
  "suggestive",
  "suggests",
  "suppose",
  "supposed",
  "supposedly",
  "supposes",
  "surely",
  "tend",
  "their impression",
  "think",
  "thinks",
  "thought",
  "understand",
  "understands",
  "understood",
  "unlikely",
  "unsure",
  "usually",
  "virtually",
  "will",
  "would"
]



if __name__ == '__main__':
    main()

