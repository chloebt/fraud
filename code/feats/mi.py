#!/usr/bin/python
# -*- coding: utf-8 -*-


import argparse, os, sys, subprocess, shutil, codecs, random

# from statsmodels.sandbox.stats.runs import mcnemar as mcnemar

from sklearn.datasets import load_svmlight_file
from sklearn.feature_selection import chi2, mutual_info_classif
from sklearn.externals import joblib

def main( ):
    parser = argparse.ArgumentParser(
            description='Study features.')
    parser.add_argument('--train',
            dest='train',
            action='store',
            help='train')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output file with kept features')
    parser.add_argument('--vocab',
            dest='vocab',
            action='store',
            help='Mapping feat indice and name')
    args = parser.parse_args()

#     if not os.path.isdir( args.outpath ):
#         os.mkdir( args.outpath )

    train = args.train
    X_train, y_train, n_features = load( train )
    print( "Data size:", X_train.shape )


    vocab = joblib.load( args.vocab )
    vocab = {v:k for (k,v) in vocab.items()}
#     print( vocab )
    study_feats( X_train, y_train, args.outpath, vocab )



def study_feats( X, y, outpath, vocab ):
    ml = mutual_info_classif( X.toarray(), y, discrete_features=False, n_neighbors=5, copy=True, random_state=None)
    res = sorted( [( vocab[i], round(v,2) ) for i,v in enumerate( ml ) ], key=lambda x:x[1], reverse=True )
    with open( outpath, 'w' ) as f:
        for r in res:
            f.write( '{0[0]}\t{0[1]}\n'.format(r) )
            print( '{0[0]}: {0[1]}'.format(r) )

#             if r[2] < 0.02:
#                 print( '{0[0]}: {0[1]}, p={0[2]}'.format(r) )



#     for i,v in enumerate( c ):
#         if p[i] < 0.02:
#             print( vocab[i], v, p[i] )
#     print( c.chi2 )
#     print( c.pval )



def load( _file, n_features=-1 ):
    if n_features<0:
        X,y = load_svmlight_file( _file )
    else:
        X,y = load_svmlight_file( _file, n_features=n_features )
    return X, y, X.shape[1]


if __name__ == '__main__':
    main()


