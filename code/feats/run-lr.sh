
# Exit immediately if a command exits with a non-zero status.
set -e



DATA=$1 # data/
SRC=$2 # code/feats/
# OUTPATH=$3 # data/features/


#OUTPATH=${DATA}/features/
# mkdir -p ${OUTPATH}


for FEAT in hedges pronounsep causal polarity inquirer pronoun conn relexpl
do
    echo
    echo --------------------------------------------
    echo ${FEAT}
    python ${SRC}run-lr.py --train ${DATA}${FEAT}/train.svmlight --feat ${FEAT} --vocab ${DATA}${FEAT}/vocab-${FEAT} --outpath log_fraud_lr_coeff_010617.txt

done

for FEAT in relexpl-lvl2 
do 
    echo
    echo --------------------------------------------  
    echo ${FEAT}
    python ${SRC}run-lr.py --train ${DATA}${FEAT}/train.svmlight --feat ${FEAT} --vocab ${DATA}${FEAT}/vocab-relexpl --outpath log_fraud_lr_coeff_010617.txt
done

for FEAT in bow ngram treelet  
do
    echo
    echo --------------------------------------------
    echo ${FEAT}
    python ${SRC}run-lr.py --train ${DATA}${FEAT}/train.selec.svmlight --feat ${FEAT} --vocab ${DATA}${FEAT}/vocab-selec --outpath log_fraud_lr_coeff_010617.txt
     
done





