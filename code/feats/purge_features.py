#!/usr/bin/python
# -*- coding: utf-8 -*-

# from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random
# from nltk.parse.dependencygraph import *
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.datasets import dump_svmlight_file
from sklearn.externals import joblib


def main( ):
    parser = argparse.ArgumentParser(
            description='Purge existing train/test files from unselected features.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input directory')
    parser.add_argument('--selec',
            dest='selec',
            action='store',
            help='Files with the selected features')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory')
    args = parser.parse_args()

    train_file = os.path.join( args.inpath, 'train.svmlight' )
#     test_file = os.path.join( args.inpath, 'test.svmlight' )

    selected = read( args.selec ) # Feature id:feature name
#     write( train_file, test_file, selected, args.outpath )# Write new files + new vocab
    write( train_file, selected, args.outpath )

def write( train_file, selected, outpath ):
    print( "\nReading Train" )
    train = _read( train_file )
    X,y,vectorizer = _purge( train, selected,vectorizer=None )
    _write( X, y, os.path.join( outpath, 'train.selec.svmlight' ) )
    print( "Train shape", X.shape )
#     print( "\nReading Test" )
#     test = _read( test_file )
#     X,y,vectorizer = _purge( test, selected, vectorizer=vectorizer )
#     _write( X, y, os.path.join( outpath, 'test.selec.svmlight' ) )
#     print( "Test shape", X.shape )
    print( "\n#Vocab", len(vectorizer.vocabulary_) )
    joblib.dump( vectorizer.vocabulary_, os.path.join( outpath, 'vocab-selec' ), compress=3 )

#     print( vectorizer.vocabulary_ )

def _write( X, y, _file ):
    dump_svmlight_file( X, y, _file )

def _purge( data, selected, vectorizer=None ):
    kept, removed = set(),set()
    x,y = [],[]
    for label, feats in data:
        y.append( int(label) )
        xs = '' # doc representation with original name feats selected
        for (k,v) in feats.items():
            if k in selected:
#                 print( k,selected[k], v )
                for i in range(0,int(v)):
                    xs += selected[k].replace( ' ', '__' )+' '
                kept.add( k )
            else:
                removed.add( k )
        x.append( xs )
#     print([selected[k] for k in kept])
    if len( data )!=len(y) or len( data )!=len(x):
        print( 'len( data )', len( data ), 'len(y)', len(y),'len(x)', len(x) )
        sys.exit( "Not the same number of examples")
    if vectorizer == None:
        vectorizer, X = build_vectorizer(x, tokenizer=my_tokenizer)
    else:
        X = vectorizer.transform( x )
    print( "#Feats kept", len(kept), "#Feats removed", len(removed) )
    print( "#Doc", len( data ), "X_train:", X.shape )
    return X,y,vectorizer

def build_vectorizer( words, tokenizer=None, min_df=1 ):
    vectorizer = CountVectorizer(min_df=min_df, tokenizer=tokenizer)
    X = vectorizer.fit_transform(words)
    return vectorizer, X

def my_tokenizer(s):
    ''' because the text is already tokenized '''
    return s.split()



def _read( _file ):
    # get the file as a list of [label, list of feature ids:feature value]
    feats = []
    with open( _file ) as f:
        lines = f.readlines()
        for l in lines:
            l = l.strip()
            feats.append( [l.split()[0], {k.split(':')[0]:k.split(':')[1] for k in l.split()[1:]} ] )
    return feats

def read( select_feats ):
    selected = {}
    with open( select_feats ) as f:
        lines = f.readlines()
        for l in lines:
            l = l.strip()
            id_feat, name_feat, _ = l.split('\t')
            selected[id_feat] = name_feat
    return selected



if __name__ == '__main__':
    main()



