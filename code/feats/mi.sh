
# Exit immediately if a command exits with a non-zero status.
set -e



DATA=$1 # data/
SRC=$2 # code/feats/
OUTPATH=$3 # data/features/

CONLLU=${DATA}/conllu-utf8/
#CONN=${DATA}/resources/conn-lvl2/
CONN=${DATA}/resources/conn/
SUBJ=${DATA}/resources/subjclueslen1-HLTEMNLP05.tff
INQ=${DATA}/resources/inquirerbasicttabsclean
#LEVIN=${DATA}/resources/levin_class.txt 

#OUTPATH=${DATA}/features/
mkdir -p ${OUTPATH}


for FEAT in conn hedges inquirer polarity pronoun pronounsep relexpl causal # cat_causal relexpl-lvl2 
do 
    echo
    echo --------------------------------------------
    echo -- MI ${FEAT} 
    python ${SRC}mi.py --train ${DATA}/${FEAT}/train.svmlight --vocab ${DATA}/${FEAT}/vocab-${FEAT} --outpath ${OUTPATH}/mi_${FEAT}.txt
done


for FEAT in cat_causal 
do 
    echo
    echo --------------------------------------------
    echo -- MI ${FEAT} 
    python ${SRC}mi.py --train ${DATA}/${FEAT}/train.svmlight --vocab ${DATA}/${FEAT}/vocab-causal --outpath ${OUTPATH}/mi_${FEAT}.txt
done

for FEAT in relexpl-lvl2 
do 
    echo
    echo --------------------------------------------
    echo -- MI ${FEAT} 
    python ${SRC}mi.py --train ${DATA}/${FEAT}/train.svmlight --vocab ${DATA}/${FEAT}/vocab-relexpl --outpath ${OUTPATH}/mi_${FEAT}.txt
done






for FEAT in bow ngram treelet  
do
    echo
    echo --------------------------------------------
    echo -- MI ${FEAT}
     
    python ${SRC}mi.py --train ${DATA}/${FEAT}/train.selec.svmlight --vocab ${DATA}/${FEAT}/vocab-selec --outpath ${OUTPATH}/mi_${FEAT}.txt
done



# Concatenate some features
# conn+relexpl
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/conn/train.selec.svmlight ${OUTPATH}/relexpl/train.selec.svmlight --outpath ${OUTPATH}/conn+relexpl/

# # bow+ngram
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram/
# 
# # bow+ngram+treelet
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight ${OUTPATH}/treelet/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram+treelet/
# 
# # bow+ngram+conn+relexpl
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight ${OUTPATH}/conn/train.selec.svmlight ${OUTPATH}/relexpl/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram+conn+relexpl/
# 
# # bow+ngram+treelet+conn+relexpl
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight ${OUTPATH}/treelet/train.selec.svmlight ${OUTPATH}/conn/train.selec.svmlight ${OUTPATH}/relexpl/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram+treelet+conn+relexpl/
# 
# # bow+ngram+treelet+conn+relexpl+polarity
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight ${OUTPATH}/treelet/train.selec.svmlight ${OUTPATH}/conn/train.selec.svmlight ${OUTPATH}/relexpl/train.selec.svmlight ${OUTPATH}/polarity/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram+treelet+conn+relexpl+polarity/




