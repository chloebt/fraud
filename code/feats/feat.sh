
# Exit immediately if a command exits with a non-zero status.
set -e



DATA=$1 # data/
SRC=$2 # code/feats/
OUTPATH=$3 # data/features/

CONLLU=${DATA}/conllu-utf8/
#CONN=${DATA}/resources/conn-lvl2/
CONN=${DATA}/resources/conn/
SUBJ=${DATA}/resources/subjclueslen1-HLTEMNLP05.tff
INQ=${DATA}/resources/inquirerbasicttabsclean
#LEVIN=${DATA}/resources/levin_class.txt 

#OUTPATH=${DATA}/features/
mkdir -p ${OUTPATH}


# for FEAT in hedges # pronounsep causal ngram treelet bow polarity inquirer pronoun conn relexpl
# do
#     echo
#     echo --------------------------------------------
#     echo ${FEAT}
# 
#     mkdir -p ${OUTPATH}/${FEAT}
# 
#     # Extract features from the data
#     echo -- BUILD FEAT
#     python ${SRC}feat_fraud.py --inpath ${CONLLU} --outpath ${OUTPATH}/${FEAT}/ --feat ${FEAT} --conn ${CONN} --subj ${SUBJ} --inq ${INQ} --labels ${DATA}labels-doc.txt 
# 
# 
#     # Compute Randomized stability thing, keep features selected at least 50% of the time
#     echo -- STABLE SELEC
#     mkdir -p ${OUTPATH}/${FEAT}/selected_feats/
#     python ${SRC}selec_feats.py --inpath ${OUTPATH}/${FEAT}/ --outpath ${OUTPATH}/${FEAT}/selected_feats/ --vocab ${OUTPATH}/${FEAT}/vocab-${FEAT}
# 
#     # Re write the svmlight files keeping only the feats selected
#     echo -- PURGE
#     python ${SRC}purge_features.py --inpath ${OUTPATH}/${FEAT}/ --selec ${OUTPATH}/${FEAT}/selected_feats/kept_features.txt --outpath ${OUTPATH}/${FEAT}/
#     
#     # Compute chi2
#     echo -- CHI2
#     python ${SRC}chi2.py --train ${OUTPATH}/${FEAT}/train.selec.svmlight --vocab ${OUTPATH}/${FEAT}/vocab-selec --outpath ${OUTPATH}/${FEAT}/selected_feats/chi2.txt
# done



# Concatenate some features
# bow+ngram+treelet+conn+relexpl+polarity
# python ${SRC}concat_svmlight.py --train ${DATA}/bow/train.selec.svmlight ${DATA}/ngram/train.selec.svmlight ${DATA}/treelet/train.selec.svmlight ${DATA}/conn/train.svmlight ${DATA}/relexpl/train.svmlight ${DATA}/polarity/train.svmlight ${DATA}/causal/train.svmlight ${DATA}/relexpl-lvl2/train.svmlight ${DATA}/inquirer/train.svmlight ${DATA}/pronoun/train.svmlight ${DATA}/pronounsep/train.svmlight ${DATA}/hedges/train.svmlight --outpath ${OUTPATH}/allfeats/


# conn+relexpl
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/conn/train.selec.svmlight ${OUTPATH}/relexpl/train.selec.svmlight --outpath ${OUTPATH}/conn+relexpl/

# # bow+ngram
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram/
# 
# # bow+ngram+treelet
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight ${OUTPATH}/treelet/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram+treelet/
# 
# # bow+ngram+conn+relexpl
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight ${OUTPATH}/conn/train.selec.svmlight ${OUTPATH}/relexpl/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram+conn+relexpl/
# 
# # bow+ngram+treelet+conn+relexpl
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight ${OUTPATH}/treelet/train.selec.svmlight ${OUTPATH}/conn/train.selec.svmlight ${OUTPATH}/relexpl/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram+treelet+conn+relexpl/
# 
# # bow+ngram+treelet+conn+relexpl+polarity
# python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight ${OUTPATH}/treelet/train.selec.svmlight ${OUTPATH}/conn/train.selec.svmlight ${OUTPATH}/relexpl/train.selec.svmlight ${OUTPATH}/polarity/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram+treelet+conn+relexpl+polarity/

# # bow+ngram+conn+relexpllvl2
python ${SRC}concat_svmlight.py --train ${OUTPATH}/bow/train.selec.svmlight ${OUTPATH}/ngram/train.selec.svmlight ${OUTPATH}/conn/train.selec.svmlight ${OUTPATH}/relexpl-lvl2/train.selec.svmlight --outpath ${OUTPATH}/bow+ngram+conn+relexpl-lvl2/



