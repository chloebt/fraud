#!/usr/bin/python
# -*- coding: utf-8 -*-


import argparse, os, sys, subprocess, shutil, codecs, random

from scipy.stats import wilcoxon
from sklearn.datasets import load_svmlight_file
from sklearn.feature_selection import chi2
from sklearn.externals import joblib

import numpy as np

def main( ):
    parser = argparse.ArgumentParser(
            description='Study features.')
    parser.add_argument('--train',
            dest='train',
            action='store',
            help='train')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output file with kept features')
    parser.add_argument('--vocab',
            dest='vocab',
            action='store',
            help='Mapping feat indice and name')
    args = parser.parse_args()

#     if not os.path.isdir( args.outpath ):
#         os.mkdir( args.outpath )

    train = args.train
    X_train, y_train, n_features = load( train )
    print( "Data size:", X_train.shape )


    vocab = joblib.load( args.vocab )
    vocab = {v:k for (k,v) in vocab.items()}
    print( vocab )
    study_feats( X_train, y_train, args.outpath, vocab )



def study_feats( X, y, outpath, vocab ):
    m = len(vocab)
    pmax = 0.05/m
    res = []
    # for each feature
    for i in range( len( vocab ) ):
#         feat = vocab[i]
        # create an array with the count for this feat for each doc
        B = np.zeros( len( y ) )
        for d in range( len( y ) ):
#             print( i, d, X.shape )
            B[d] += X[d,i]
#
#         print( feat, i )
#         print( B, B.shape )

        T,p = wilcoxon( y, B )

        res.append( (vocab[i], T,p) )

#         print( feat, T, p )

#     c,p = chi2( X, y )
#     res = sorted( [( vocab[i], round(v,2), p[i] ) for i,v in enumerate( c ) ], key=lambda x:x[2]  )
    with open( outpath, 'w' ) as f:
        for r in sorted( res, key=lambda x:x[2]  ):
            f.write( '{0[0]}\t{0[1]}\t{0[2]}\n'.format(r) )
            if r[2] < pmax:
                print( '{0[0]}: {0[1]}, p={0[2]}'.format(r) )



#     for i,v in enumerate( c ):
#         if p[i] < 0.02:
#             print( vocab[i], v, p[i] )
#     print( c.chi2 )
#     print( c.pval )



def load( _file, n_features=-1 ):
    if n_features<0:
        X,y = load_svmlight_file( _file )
    else:
        X,y = load_svmlight_file( _file, n_features=n_features )
    return X, y, X.shape[1]


if __name__ == '__main__':
    main()


