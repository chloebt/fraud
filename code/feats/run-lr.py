#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Requires scikit 18 (python3)
(on Obi: peachtree conda env!)
'''

# from __future__ import print_function
import matplotlib as mpl
mpl.use('Agg')
import argparse, os, sys, subprocess, shutil, codecs, random
import numpy as np


from sklearn import linear_model, svm
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, make_scorer

from sklearn.model_selection import GridSearchCV, cross_val_score, KFold, LeaveOneOut

# from sklearn.grid_search import GridSearchCV #change in v20
from sklearn.datasets import load_svmlight_file
from sklearn.externals import joblib
from scipy.sparse import hstack, vstack
# from sklearn.cross_validation import LeaveOneOut,KFold,cross_val_score #model_selection in v18

from sklearn.utils import shuffle

clf2params = {
        "nb":[{'alpha':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]}],
        "maxent":[{'penalty':['l1'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]},
            {'penalty':['l2'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]}],
        "pa":[{'loss':['hinge'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100], 'n_iter':[1, 5, 50, 100, 200, 500]},
            {'loss':['squared_hinge'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100], 'n_iter':[1, 5, 50, 100, 200, 500]}],
        "svc":[{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                     'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
        }
# 0.001, 0.005, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 5, 10, 20, 30, 40, 50, 100

def main( ):
    parser = argparse.ArgumentParser(
            description='Run LR and save coeff for the features.')
    parser.add_argument('--inpath', dest='inpath', action='store',
            help='Input directory containing the train file (or directly give the train file)')

    parser.add_argument('--feat', dest='feat', action='store', help='Feature used (for naming files)')
    parser.add_argument('--train', dest='train', action='store', help='train')
    parser.add_argument('--vocab', dest='vocab', action='store', help='vocab')

    parser.add_argument('--outpath', dest='outpath', action='store', help='Output directory')
    parser.add_argument('--outmodel', dest='outmodel', action='store', default=None, help='Output file for model')
    parser.add_argument('--outpred', dest='outpred', action='store', default=None, help='Output file for pred')# TODO for each trial ??

    parser.add_argument('--algo', dest='algo', action='store', default='maxent', choices=['maxent', 'nb','svc', 'pa'],
            help='Algo to use: maxent, nb, pa, svc')

    # TODO other scores not implemented yet
    parser.add_argument('--score', dest='score', action='store', default='accuracy_score',
            help='Score to optimize: accuracy_score, precision_score, f1_score')
    parser.add_argument('--avg', dest='avg', action='store', default='micro',
            help='Average strategy for score to optimize: macro, micro, weighted?')
    args = parser.parse_args()

#     if args.outpath and not os.path.isdir( args.outpath ):
#         os.mkdir( args.outpath )

    if args.inpath:
        train = os.path.join( vars(args)['inpath'], "train.svmlight" )
    else:
        train = args.train

    X_train, y_train,n_features = load( train )
    print( "Train", X_train.shape )

    vocab = joblib.load( args.vocab )
    vocab = {v:k for (k,v) in vocab.items()}

#     o = open( args.outpath, 'w' )
#     o.close()

    run_lr( X_train, y_train, args.outpath, args.feat, vocab )

def run_lr( X, y, outpath, feat, vocab ):
    print( "\n\nFEATURE:", feat, file=sys.stderr )
    print( "\n\nFEATURE:", feat )

    clf = linear_model.LogisticRegression( n_jobs=4 )
    random.seed = 1234
    Xs,ys = shuffle( X, y, random_state=1234)

    clf.fit(Xs, ys)

    preds = clf.predict( Xs )
    print( 'ACC', accuracy_score( ys, preds ) )

    res = []
    print( vocab )
    print( clf.coef_ )
#     print( vocab[i] )
#     print( clf.coef_[0][i] )
    res = [ (vocab[i], clf.coef_[0][i]) for i in range( len( vocab ) ) ]

    with open( outpath, 'a' ) as f:
        f.write( '\n-- '+feat+'\n' )
        for r in sorted( res, key=lambda x:x[1]  ):
            f.write( '{0[0]}\t{0[1]}\n'.format(r) )



def load( _file, n_features=-1 ):
    if n_features<0:
        X,y = load_svmlight_file( _file )
    else:
        X,y = load_svmlight_file( _file, n_features=n_features )
    return X, y, X.shape[1]


if __name__ == '__main__':
    main()



