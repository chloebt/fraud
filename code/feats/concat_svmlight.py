#!/usr/bin/python
# -*- coding: utf-8 -*-

# from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random

from sklearn.datasets import load_svmlight_file, dump_svmlight_file
from scipy.sparse import hstack, vstack
import numpy as np

def main( ):
    parser = argparse.ArgumentParser(
            description='Concat files containing features, format svmlight.')
    parser.add_argument('--train',
            nargs='+',
            dest='train',
            action='store',
            help='Train files.')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    print( "\nReading TRAIN" )
    out_train = os.path.join( args.outpath, os.path.basename( args.train[0] ) )
    X_tr, y_tr = concat( args.train )
    dump_svmlight_file( X_tr, y_tr, out_train )

def concat( train_files ):
    Xl,y = [],[]
    for tr in train_files:
        _X,_y = load_svmlight_file( tr )
        Xl.append( _X )
        if len(y) == 0:
            y = _y

    X = Xl[0]

    print( 0, '-', train_files[0], " ; Shape:", Xl[0].shape )
    print( "\t--> Merged", X.shape )

    for i,_X in enumerate( Xl ):
        if i != 0: #already merged
            print( i, '-', train_files[i], " ; Shape:", _X.shape )

            X = hstack( [X, _X], format="csr", dtype='float64' )

            print( "\t--> Merged", X.shape )
    print( "Final", X.shape, " ; #Inst", y.shape )
    return X,y

if __name__ == '__main__':
    main()

