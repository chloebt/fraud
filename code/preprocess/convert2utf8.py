#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random

'''
Convert the documents into the input format for the UDPipe parser, ie raw documents
(some documents have line breaks within sentences, remove these line breaks)
'''


def main( ):
    parser = argparse.ArgumentParser(
            description='Write in utf8.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input directory')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output dir')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    convert( args.inpath, args.outpath )

def convert( inpath, outpath ):
    for sdir in [d for d in os.listdir( inpath ) if not d.startswith( '.' )]:
        print( 'Reading', sdir )
        outdir = os.path.join( outpath, sdir )
        if not os.path.isdir( outdir ):
            os.mkdir( outdir )
        for inf in [f for f in os.listdir( os.path.join( inpath, sdir ) ) if not f.startswith( '.' )]:
            print( '\tReading', inf )
            try:
                conllu = open( os.path.join( inpath, sdir, inf ) ).read()
                shutil.copy( os.path.join( inpath, sdir, inf ), outdir )
            except:
                conllu = open( os.path.join( inpath, sdir, inf ), encoding='latin1' ).read()
                o = open( os.path.join( outdir, inf ), 'w', encoding='utf8' )
                o.write( conllu )
                o.close()




if __name__ == '__main__':
    main()


