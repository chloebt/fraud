#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e

export PYTHONPATH=$PYTHONPATH:/home/bplank/tools/cnn/pycnn/


OUTDIR=data/v1_fraud/seg-dis-pred/Unretracted_Full/ #Unretracted_Full Fraudulent_Full
mkdir -p ${OUTDIR}
TEST=data/v1_fraud/seg-dis/Unretracted_Full/*.txt.seg

SRC=discoursecph/segmentation/code/
MODEL=experiments/eacl17_multiling-domRSTsegmenter/en/rsten-doc-wdpos/model-disco-doc.task-rsten-doc-wdpos.ite-30.lay-2.hdim-100.sigma-0.2.indim-500.model

ITER=30
INDIM=500
HDIM=100
SIGMA=0.2
HLAYERS=2
TASK=rsten-doc-wdpos
# -- Fixed parameters
SEED=3068836234
#MEM=1024
MEM=2000 #1536

# OUTDIR=$1 #directory for pred and score files
# SRC=$2
# #TRAIN=$3
# MODEL=$3
# TEST=$4
# INDIM=$5
# HDIM=$6
# #HDIM=100
# SIGMA=$7
# HLAYERS=$8
# TASK=$9


echo TASK ${TASK}
echo MODEL ${MODEL}

PY=/home/bplank/anaconda/envs/p3k/bin #need python3 with the right packages


for f in $TEST
do
    OUTFILE=${OUTDIR}pred-${f##*/}
    if [ ! -f ${OUTFILE} ]; then
#     echo $f
#     echo ${OUTDIR}pred-${f##*/}
    echo READING ${f##*/}
    TEMP=temp/
    mkdir -p $TEMP 
    cp $f $TEMP
    
    
    echo OUTFILE ${OUTFILE} 
    ${PY}/python ${SRC}/disco.py --cnn-mem ${MEM} --cnn-seed ${SEED} --in_dim ${INDIM} --iters ${ITER} --h_dim ${HDIM} --sigma ${SIGMA} --h_layers ${HLAYERS} --pred_layer ${HLAYERS} --train --model ${MODEL} --test ${TEMP} --output ${OUTFILE}

    rm $TEMP/${f##*/}
    fi
done


# OUTFILE=${OUTDIR}/pred-test.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA}.indim-${INDIM}
# 
# echo ${OUTFILE}
# 
# echo task:${TASK} ite:${ITER} lay:${HLAYERS} hdim:${HDIM} sigma:${SIGMA} indim:${INDIM}
# ${PY}/python ${SRC}/disco.py --cnn-mem ${MEM} --cnn-seed ${SEED} --in_dim ${INDIM} --iters ${ITER} --h_dim ${HDIM} --sigma ${SIGMA} --h_layers ${HLAYERS} --pred_layer ${HLAYERS} --train --model ${MODEL} --test ${TEST} --output ${OUTFILE}
# 
# 
# 
# # Compute scores
# OUTSC=${OUTDIR}/scores-test-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA}.indim-${INDIM}.txt
# echo Scores Bi-LSTM >${OUTSC}
# 
# python ${SRC}/segmenter_scorer.py --preds ${OUTFILE} --params iter:${ITER},hlay:${HLAYERS},hdim:${HDIM},sig:${SIGMA},indim:${INDIM}>>${OUTSC}


