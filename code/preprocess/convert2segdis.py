#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random

'''
Convert the documents into the input format for the discourse segmenter
'''


def main( ):
    parser = argparse.ArgumentParser(
            description='Study features.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input directory (conllu)')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output dir')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    convert( args.inpath, args.outpath )

def convert( inpath, outpath ):
    for sdir in [d for d in os.listdir( inpath ) if not d.startswith( '.' )]:
        print( 'Reading', sdir )
        outdir = os.path.join( outpath, sdir )
        if not os.path.isdir( outdir ):
            os.mkdir( outdir )
        for inf in [f for f in os.listdir( os.path.join( inpath, sdir ) ) if not f.startswith( '.' )]:
            print( '\tReading', inf )
            with open( os.path.join( inpath, sdir, inf ) ) as myfile:
                outf = open( os.path.join( outdir, inf ).replace( '.conllu', '.seg'), 'w' )

                sentences = [s.strip() for s in myfile.read().split('\n\n') if s.strip() != '']
                for sent in sentences:
                    tokens = [s.strip() for s in sent.split('\n') if not s.strip() == '']
                    for t in tokens:
                        id_tok, form, lemma, pos, posx, morph, head, fct, _, _ = t.split('\t')
                        outf.write( form+'\tI\n' )
                        outf.write( pos+'\tI\n' )
                outf.close()



if __name__ == '__main__':
    main()


