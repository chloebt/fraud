#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random


def main( ):
    parser = argparse.ArgumentParser(
            description='Study features.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input directory')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output dir')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    convert( args.inpath, args.outpath )

def convert( inpath, outpath ):
    for f in [_f for _f in os.listdir( inpath ) if not _f.startswith( '.' )]:
        print( f )
        lines = open( os.path.join( inpath, f ) ).readlines()
        random.shuffle( lines )
        o = open( os.path.join( outpath, f ), 'w' )
        print( os.path.join( outpath, f ) )
        for l in lines:
            l = l.strip()
            o.write( l+'\n' )
        o.close()

if __name__ == '__main__':
    main()


