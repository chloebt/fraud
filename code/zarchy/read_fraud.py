#!/usr/bin/python
# -*- coding: utf-8 -*-




from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random
from corenlp_xml import document

from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

def main( ):
    parser = argparse.ArgumentParser(
            description='Read fraud data, generate features files.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            default="data/corpora/others/Fraud_data/parsed/",
            help='Input directory')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            default="data/fraud_data/",
            #default="data/corpora/others/Fraud_data/samples/",
            help='Output directory')
    args = parser.parse_args()

#     train, dev, test = split( vars(args)['inpath'], vars(args)['outpath'] )

    train_files = sorted( [os.path.join( vars(args)['outpath'], 'train', f )
        for f in os.listdir( os.path.join( vars(args)['outpath'], 'train' ) )
        if not f.startswith( '.')] )
    dev_files = sorted( [os.path.join( vars(args)['outpath'], 'dev', f )
        for f in os.listdir( os.path.join( vars(args)['outpath'], 'dev' ) )
        if not f.startswith( '.')] )
    test_files = sorted( [os.path.join( vars(args)['outpath'], 'test', f )
        for f in os.listdir( os.path.join( vars(args)['outpath'], 'test' ) )
        if not f.startswith( '.')] )

    # -- Get labels
    y_train, y_dev, y_test = get_labels( train_files, dev_files, test_files )

    # -- Feats data
    X_train, X_dev, X_test = bag_of_word( train_files, dev_files, test_files )
    X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )

    # -- Train/eval
    clf = train_clf( X_train, y_train )
    eval( clf,  X_dev, y_dev )




def train_clf( X_train, y_train ):
    clf = LogisticRegression()
    clf.fit( X_train, y_train )
    return clf

def eval( clf, X_test, y_test ):
    preds = clf.predict( X_test )
    print( "Acc:", accuracy_score( y_test, preds ) )
    print( classification_report( y_test, preds ) )
    print( confusion_matrix( y_test, preds ) )



def get_labels( train_files, dev_files, test_files ):
    return _get_labels( train_files ), _get_labels( dev_files ), _get_labels( test_files )

def _get_labels( files ):
    labels = []
    for f in files:
        if f.endswith( 'pos' ):
            labels.append( 1 )
        else:
            labels.append( -1 )
    return labels



def bag_of_word( train, dev, test ):
    # represent each corpus by a list of words
    train_wds = get_words( train )
    vectorizer, X_train = build_vectorizer(train_wds)
    print( '#Train Docs', len( train_wds ), "\nX_train:", X_train.shape )

    dev_wds = get_words( dev )
    X_dev = vectorizer.transform( dev_wds )
    print( '#Dev Docs', len( dev_wds ), "\nX_dev:", X_dev.shape )

    test_wds = get_words( test )
    X_test = vectorizer.transform( test_wds )
    print( '#Dev Docs', len( test_wds ), "\nX_dev:", X_test.shape )
    return X_train, X_dev, X_test

def build_vectorizer( words ):
    vectorizer = CountVectorizer(min_df=1)
    X = vectorizer.fit_transform(words)
    return vectorizer, X


def tfidf( X_train, X_dev, X_test ):
    tf_transformer, X_train_tf = build_tfidf_transformer( X_train )
    X_dev_tf = tf_transformer.transform( X_dev )
    X_test_tf = tf_transformer.transform( X_test )
    return X_train_tf, X_dev_tf, X_test_tf

def build_tfidf_transformer( X_train ):
    tf_transformer = TfidfTransformer(use_idf=False).fit(X_train)
    X_train_tf = tf_transformer.transform(X_train)
    return tf_transformer, X_train_tf





def get_words( files ):
    ''' List of words for each document (in fact a string, to be passed to countVect  '''
    words = []
    for _file in files:
        with open( _file ) as f:
            doc = document.Document( f.read() )
            doc_wds = []
            for sentence in doc.sentences:
                for token in sentence.tokens:
                    doc_wds.append( token.word )
        words.append( ' '.join( doc_wds ) )
    return words
#                         #print( sentence.semantic_head )
#                         #print( sentence.basic_dependencies )
#                     #print( doc.sentences )
#                     #print( doc.sentiment )

# def get_dependencies( path ):








def split( inpath, outpath ):
    percentage = 10
    random.seed = 1234

    paths = []
    for dataset in ['train', 'dev', 'test']:
        path = os.path.join( outpath, dataset )
        paths.append( path )
        if not os.path.isdir( path ):
            os.mkdir( path )

    pos_files = [os.path.join( inpath, 'fraudulent_parsed', f ) for f in os.listdir( os.path.join( inpath, 'fraudulent_parsed' ) ) if not f.startswith('.') ]
    neg_files = [os.path.join( inpath, 'unretracted_parsed', f ) for f in os.listdir( os.path.join( inpath, 'unretracted_parsed' ) ) if not f.startswith('.') ]

    print( "Total files:", len(pos_files)+len(neg_files) )

    dev_test_size = ( len( pos_files )*percentage )/100
    random.shuffle( pos_files )


    pos_dev_files = pos_files[:dev_test_size]
    copy_files( pos_dev_files, paths[1], '.pos' )
    pos_test_files = pos_files[dev_test_size:dev_test_size*2]
    copy_files( pos_test_files, paths[2], '.pos' )
    pos_train_files = pos_files[dev_test_size*2:]
    copy_files( pos_train_files, paths[0], '.pos' )

    neg_dev_files = neg_files[:dev_test_size]
    copy_files( neg_dev_files, paths[1], '.neg' )
    neg_test_files = neg_files[dev_test_size:dev_test_size*2]
    copy_files( neg_test_files, paths[2], '.neg' )
    neg_train_files = neg_files[dev_test_size*2:]
    copy_files( neg_train_files, paths[0], '.neg' )

    print( "TRAIN:", len( pos_train_files )+len( neg_train_files ), " ; DEV:", len( pos_dev_files )+len( neg_dev_files ), " ; TEST:", len( pos_test_files )+len( neg_test_files ) )

    return paths

def copy_files( list_files, out_path, ext ):
    for f in list_files:
        shutil.copy( f, os.path.join( out_path, os.path.basename( f )+ext ) )



if __name__ == '__main__':
    main()



