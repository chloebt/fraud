#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Requires scikit 18
'''


# from __future__ import print_function
import matplotlib as mpl
mpl.use('Agg')
import argparse, os, sys, subprocess, shutil, codecs, random
import numpy as np


from sklearn import linear_model, svm
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, make_scorer

from sklearn.model_selection import GridSearchCV, cross_val_score, KFold, LeaveOneOut


# from sklearn.grid_search import GridSearchCV #change in v20
from sklearn.datasets import load_svmlight_file
from sklearn.externals import joblib
from scipy.sparse import hstack, vstack
# from sklearn.cross_validation import LeaveOneOut,KFold,cross_val_score #model_selection in v18
from matplotlib import pyplot as plt

from sklearn.utils import shuffle

def main( ):
    parser = argparse.ArgumentParser(
            description='Classification.')
    parser.add_argument('--inpath',dest='inpath',action='store',help='Input directory')
    parser.add_argument('--outpath',dest='outpath',action='store',help='Output directory')
    parser.add_argument('--feat',dest='feat',action='store',help='Feature used (for naming files)')
    args = parser.parse_args()

    if args.outpath and not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    loo_scores = joblib.load( os.path.join( args.inpath,
        'loo_scores_maxent_'+args.feat ) )
    nested_loo_scores = joblib.load( os.path.join( args.inpath,
        'nested_scores_loo_maxent_'+args.feat ) )
    nested_kf_scores = joblib.load( os.path.join( args.inpath,
        'nested_scores_kf_maxent_'+args.feat ) )

    print( loo_scores.shape, nested_loo_scores.shape, nested_kf_scores.shape )

    # Plot scores on each trial for nested and non-nested CV
    plt.figure()
    plt.subplot(211)
    non_nested_scores_line, = plt.plot(loo_scores, color='r')
    nested_line, = plt.plot(nested_loo_scores, color='b')
    nested_line_kf, = plt.plot(nested_kf_scores, color='green')
    plt.ylabel("score", fontsize="14")
    plt.legend([non_nested_scores_line, nested_line, nested_line_kf],
               ["Non-Nested LOO", "Nested LOO", "Nested KF"],
               bbox_to_anchor=(0, .4, .5, 0))
    plt.title("Non-Nested and Nested Cross Validation",
              x=.5, y=1.1, fontsize="15")
#
#
    score_difference = score_difference_loo
    # Plot bar chart of the difference.
    plt.subplot(212)
    difference_plot = plt.bar(range(NUM_TRIALS), score_difference)
    plt.xlabel("Individual Trial #")
    plt.legend([difference_plot],
               ["Non-Nested LOO - Nested LOO Score"],
               bbox_to_anchor=(0, 1, .8, 0))
    plt.ylabel("score difference", fontsize="14")
#
    # Plot bar chart of the difference.
    plt.subplot(212)
    difference_plot = plt.bar(range(NUM_TRIALS), score_difference_kf)
    plt.xlabel("Individual Trial #")
    plt.legend([difference_plot],
               ["Nested LOO - Nested KF Score"],
               bbox_to_anchor=(0, 1, .8, 0))
    plt.ylabel("score difference", fontsize="14")
#
    plot_file = os.path.join( outpath, 'fraud_nested_crossval_'+algo+'_'+feat+'.png' )
#
    plt.savefig( plot_file, bbox_inches='tight' )
#     plt.show()



if __name__ == '__main__':
    main()



