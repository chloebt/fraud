#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib as mpl
mpl.use('Agg')

import argparse, os, sys, subprocess, shutil, codecs, random
import numpy as np

from matplotlib import pyplot as plt

def main( ):
    parser = argparse.ArgumentParser(
            description='Read fcking file')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
#             default='/Users/chloebraud/projects/navi/other_projects/fraud/expe/new_expe/classif_fraud_NCV_020217.txt',
            default='/Users/chloebraud/projects/navi/other_projects/fraud/expe/new_expe/classif_fraud_combineFeats_NCV_050217.txt',
            help='Input directory')
    args = parser.parse_args()

    read_scores( args.inpath )


def read_scores(inpath ):
    lines = open( inpath ).readlines()
    cur_feat = None
    feat2scores_loo, feat2scores_inloo, feat2scores_inkf, feat2size = {},{},{},{}
    feat2scores_nloo, feat2scores_nkf = {},{}

    cur_trial = 0

    i = 0
    while i != len( lines ):
        l = lines[i].strip()
        if l == '-------------------------------------------------------':
            cur_feat = lines[i+1].strip()
            i += 1
            print( '\n', cur_feat )
        if l.startswith( 'Train '):
            if not cur_feat in feat2size:
                cur_trial = 0
                tmp = ''.join( l.split()[1:] ).replace('(','').replace(')','').split(',')
                feat2size[cur_feat] = ( int(tmp[0]),int(tmp[1]) )
                print( feat2size[cur_feat] )

        if l.startswith( 'Running outer CV fold '):
            outer_cv_fold = int( l.split()[-1] )
            if outer_cv_fold == 0:
                cur_trial += 1


        if l.startswith( 'Inner scores with' ):
            strat = l.strip().split()[-1]


        if l.startswith( 'Acc:'):
            score = float( l.split()[1] )

            if strat == 'LOO':
                # fill dict for LOO
#                 print( cur_feat, strat, cur_trial, outer_cv_fold, score )
                if cur_feat in feat2scores_inloo:
                    if cur_trial in feat2scores_inloo[cur_feat]:
                        feat2scores_inloo[cur_feat][cur_trial].append( score )
                    else:
                        feat2scores_inloo[cur_feat][cur_trial] = [score]
                else:
                    feat2scores_inloo[cur_feat] = {cur_trial:[score]}

            if strat == 'KF':
                # fill dict for KF
#                 print( cur_feat, strat, cur_trial, outer_cv_fold, score )
                if cur_feat in feat2scores_inkf:
                    if cur_trial in feat2scores_inkf[cur_feat]:
                        feat2scores_inkf[cur_feat][cur_trial].append( score )
                    else:
                        feat2scores_inkf[cur_feat][cur_trial] = [score]
                else:
                    feat2scores_inkf[cur_feat] = {cur_trial:[score]}

        if l.startswith( 'Round ' ):
            scores = [float(s) for s in l.split()[-3:]]
            if cur_feat in feat2scores_loo:
                feat2scores_loo[cur_feat].append( scores[-1] )
                feat2scores_nloo[cur_feat].append( scores[0] )
                feat2scores_nkf[cur_feat].append( scores[1] )
            else:
                feat2scores_loo[cur_feat] = [scores[-1]]
                feat2scores_nloo[cur_feat] = [scores[0]]
                feat2scores_nkf[cur_feat] = [scores[1]]

        i += 1


    for f in feat2scores_loo:
        print( "LOO", f, np.mean( feat2scores_loo[f] ) )

#         for t in feat2scores_nloo[f]:
        print( '\tNLOO', np.mean( feat2scores_nloo[f] ) )
        print( '\tNKF', np.mean( feat2scores_nkf[f] ) )


#     for f in feat2scores_nloo:
#         for t in feat2scores_nloo[f]:
#             print( f, t, feat2scores_nloo[f][t] )



    # Plot the acc for each system against the trial
    outpath = '/Users/chloebraud/projects/navi/other_projects/fraud/expe/new_expe/plot_scores/'
    if not os.path.isdir( outpath ):
        os.mkdir( outpath )

    plot_mean_scores_over_trials( feat2scores_loo, feat2scores_nloo, feat2scores_nkf, outpath )

#     plot_overfitting( feat2scores_inloo, feat2scores_inkf, feat2scores_nloo, feat2scores_nkf )


def plot_overfitting( feat2scores_inloo, feat2scores_inkf, feat2scores_nloo, feat2scores_nkf, outpath ):
    for f in feat2scores_inloo:
        print( "\n", f )
        feat = f
        if f == 'bow':
            feat = "unigrams"
        if f == "ngram":
            feat = "2-3-grams"

        print( feat2scores_nloo[f] )

        for t in feat2scores_inloo[f]:

            print( t, feat2scores_inloo[f][t] )

#         loo = feat2scores_loo[f]
#         nloo = feat2scores_nloo[f]
#         nkf = feat2scores_nkf[f]
# #
# #
#         plt.figure()
#         plt.subplot(211)
#         loo_line, = plt.plot(loo, color='r')
#         nloo_line, = plt.plot(nloo, color='b')
#         nkf_line, = plt.plot(nkf, color='green')
# #
# #
#         plt.axhline( y=np.mean( feat2scores_nloo[f] ), color='b',alpha=0.3)
#         plt.axhline( y=np.mean( feat2scores_nkf[f] ), color='g',alpha=0.3)
# #
#         plt.ylabel("score", fontsize="11")
#         plt.legend([loo_line, nloo_line, nkf_line],
#                    ["LOO", "NCV LOO", "NCV 4F"],
#                    bbox_to_anchor=(0, 0.4, 1, -0.1), fontsize="9")
# #
#         plot_file = os.path.join( outpath, 'fraud_nested_crossval_'+f+'.png' )
#     #
#         plt.savefig( plot_file, bbox_inches='tight' )
#
def plot_mean_scores_over_trials( feat2scores_loo, feat2scores_nloo, feat2scores_nkf, outpath ):
    for f in feat2scores_loo:
        feat = f
        if f == 'bow':
            feat = "unigrams"
        if f == "ngram":
            feat = "2-3-grams"

        loo = feat2scores_loo[f]
        nloo = feat2scores_nloo[f]
        nkf = feat2scores_nkf[f]


        plt.figure()
        plt.subplot(211)
        loo_line, = plt.plot(loo, color='r')
        nloo_line, = plt.plot(nloo, color='b')
        nkf_line, = plt.plot(nkf, color='green')


        plt.axhline( y=np.mean( feat2scores_nloo[f] ), color='b',alpha=0.3)
        plt.axhline( y=np.mean( feat2scores_nkf[f] ), color='g',alpha=0.3)

        plt.ylabel("score", fontsize="11")
        plt.legend([loo_line, nloo_line, nkf_line],
                   ["LOO", "NCV LOO", "NCV 4F"],
                   bbox_to_anchor=(0, 0.4, 1, -0.1), fontsize="9")

        plot_file = os.path.join( outpath, 'fraud_nested_crossval_'+f+'.png' )
    #
        plt.savefig( plot_file, bbox_inches='tight' )



if __name__ == '__main__':
    main()

