#!/usr/bin/python

# -*- coding: utf-8 -*-




from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random
from corenlp_xml import document

from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.datasets import dump_svmlight_file
from sklearn.externals import joblib

def main( ):
    parser = argparse.ArgumentParser(
            description='Read fraud data, generate features files.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            default="data/fraud_data/",
            help='Input directory')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory')
    parser.add_argument('--feat',
            dest='feat',
            action='store',
            default="bow",
            help='Type of features (bow, ngram..)')
    parser.add_argument('--lexsent',
            dest='lexsent',
            action='store',
            help='Lexique with info attached to words (eg polarity sentiment etc)')
    parser.add_argument('--conn',
            dest='conn',
            action='store',
            help='Connective annotation')
    args = parser.parse_args()

    outpath = vars(args)['outpath']
    if not outpath:
        outpath = os.path.join( vars(args)['inpath'], vars(args)['feat'] )
    if not os.path.isdir( outpath ):
        os.mkdir( outpath )


    train_files = sorted( [os.path.join( vars(args)['inpath'], 'train', f )
        for f in os.listdir( os.path.join( vars(args)['inpath'], 'train' ) )
        if not f.startswith( '.')] )
    dev_files = sorted( [os.path.join( vars(args)['inpath'], 'dev', f )
        for f in os.listdir( os.path.join( vars(args)['inpath'], 'dev' ) )
        if not f.startswith( '.')] )
    test_files = sorted( [os.path.join( vars(args)['inpath'], 'test', f )
        for f in os.listdir( os.path.join( vars(args)['inpath'], 'test' ) )
        if not f.startswith( '.')] )

    # -- Get labels
    y_train, y_dev, y_test = get_labels( train_files, dev_files, test_files )

    # -- Feats data
    min_df=1

    if args.feat == "bow":
        X_train, X_dev, X_test, vectorizer = bag_of_word( train_files, dev_files,
                test_files, min_df=min_df )
        X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )
    if args.feat == "ngram":# 1, 2 and 4 grams
        X_train, X_dev, X_test, vectorizer = ngram_words( train_files, dev_files,
                test_files, min_df=min_df )
        #X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )
    elif args.feat == "treelet":
        X_train, X_dev, X_test, vectorizer = treelet( train_files, dev_files,
                test_files, min_df=min_df )
        X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )
    elif args.feat == "conn":
        X_train, X_dev, X_test, vectorizer = explicit_conn( train_files, dev_files,
                test_files, args.conn, min_df=min_df )
    elif args.feat == "relexpl":
        X_train, X_dev, X_test, vectorizer = explicit_rel( train_files, dev_files,
                test_files, args.conn, min_df=min_df )
    elif args.feat == "bow+treelet":
        X_train, X_dev, X_test, vectorizer = bw_treelet( train_files, dev_files,
                test_files, min_df=min_df )
        X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )

    # -- Write data
    write( X_train, y_train, os.path.join( outpath, "train.svmlight" ) )
    write( X_dev, y_dev, os.path.join( outpath, "dev.svmlight" ) )
    write( X_test, y_test, os.path.join( outpath, "test.svmlight" ) )

    # -- Save dictionnary
    joblib.dump( vectorizer.vocabulary_, os.path.join( args.outpath, 'vocab-'+args.feat ), compress=3 )



# --- WRITE
def write( X, y, _file ):
    dump_svmlight_file( X, y, _file )


# --- LABELS
def get_labels( train_files, dev_files, test_files ):
    return _get_labels( train_files ), _get_labels( dev_files ), _get_labels( test_files )

def _get_labels( files ):
    labels = []
    for f in files:
        if f.endswith( 'pos' ):
            labels.append( 1 )
        else:
            labels.append( -1 )
    return labels


# --- FEATURES
def bag_of_word( train, dev, test, min_df=1 ):
    # represent each corpus by a list of words
    train_wds = get_words( train )
    vectorizer, X_train = build_vectorizer(train_wds, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train_wds ), "\nX_train:", X_train.shape )

    dev_wds = get_words( dev )
    X_dev = vectorizer.transform( dev_wds )
    print( '#Dev Docs', len( dev_wds ), "\nX_dev:", X_dev.shape )

    test_wds = get_words( test )
    X_test = vectorizer.transform( test_wds )
    print( '#Test Docs', len( test_wds ), "\nX_dev:", X_test.shape )
    return X_train, X_dev, X_test, vectorizer

def ngram_words( train, dev, test, min_df=1 ):
    train_wds = get_words( train )
    vectorizer = CountVectorizer(ngram_range=(1, 3), min_df=min_df, stop_words='english')
    X_train = vectorizer.fit_transform(train_wds)
    print( '#Train Docs', len( train_wds ), "\nX_train:", X_train.shape )

    dev_wds = get_words( dev )
    X_dev = vectorizer.transform( dev_wds )
    print( '#Dev Docs', len( dev_wds ), "\nX_dev:", X_dev.shape )

    test_wds = get_words( test )
    X_test = vectorizer.transform( test_wds )
    print( '#Test Docs', len( test_wds ), "\nX_dev:", X_test.shape )
    return X_train, X_dev, X_test, vectorizer

# def explicit_conn( train_files, dev_files, test_files, conn_annot, min_df=min_df ):
#     train_files_names = [os.path.basename( f )
#     for fin in [f for f in os.listdir( conn_annot )
#             if not f.startswith( '.' ) and f.endswith( '.txt.raw.annot' )]:
#         #id	base_file	form	discourse_use	score_emploi	position	score_position	relation	score_relation	span_conn



def build_vectorizer( words, tokenizer=None, min_df=1 ):
    vectorizer = CountVectorizer(min_df=min_df, tokenizer=tokenizer)
    X = vectorizer.fit_transform(words)
    return vectorizer, X


def tfidf( X_train, X_dev, X_test ):
    tf_transformer, X_train_tf = build_tfidf_transformer( X_train )
    X_dev_tf = tf_transformer.transform( X_dev )
    X_test_tf = tf_transformer.transform( X_test )
    return X_train_tf, X_dev_tf, X_test_tf

def build_tfidf_transformer( X_train ):
    tf_transformer = TfidfTransformer(use_idf=False).fit(X_train)
    X_train_tf = tf_transformer.transform(X_train)
    return tf_transformer, X_train_tf



def treelet( train, dev, test, min_df=1 ):
    # list of doc treelets (# feature name = str treelet)
    train_treelets = get_dependencies( train )
    # Need a tokenizer based on spaces
    vectorizer, X_train = build_vectorizer(train_treelets, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train_treelets ), "\nX_train:", X_train.shape )
#     print( vectorizer.vocabulary_ )
#     print( len( vectorizer.vocabulary_ ) )

    dev_treelets = get_dependencies( dev )
    X_dev = vectorizer.transform(dev_treelets)
    print( '#Dev Docs', len( dev_treelets ), "\nX_dev:", X_dev.shape )

    test_treelets = get_dependencies( test )
    X_test = vectorizer.transform(test_treelets)
    print( '#Test Docs', len( test_treelets ), "\nX_test:", X_test.shape )

    return X_train, X_dev, X_test, vectorizer



def bw_treelet( train, dev, test, min_df=1 ):
    train_treelets = get_dependencies( train )
    train_wds = get_words( train )
    train_merged = merge( train_treelets, train_wds )
    # Need a tokenizer based on spaces
    vectorizer, X_train = build_vectorizer(train_merged, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train_merged ), "\nX_train:", X_train.shape )
#     print( vectorizer.vocabulary_ )
#     print( len( vectorizer.vocabulary_ ) )

    dev_treelets = get_dependencies( dev )
    dev_wds = get_words( dev )
    dev_merged = merge( dev_treelets, dev_wds )
    X_dev = vectorizer.transform(dev_merged)
    print( '#Dev Docs', len( dev_merged ), "\nX_dev:", X_dev.shape )

    test_treelets = get_dependencies( test )
    test_wds = get_words( test )
    test_merged = merge( test_treelets, test_wds )
    X_test = vectorizer.transform(test_merged)
    print( '#Test Docs', len( test_merged ), "\nX_test:", X_test.shape )
    return X_train, X_dev, X_test, vectorizer


# --- UTILS
def merge( train_treelets, train_wds ):
    # each doc repr by a str
    train_merged = []
    for i,doc in enumerate( train_treelets ):
        str_ = doc+' '+train_wds[i]
        train_merged.append( str_ )
    return train_merged


def my_tokenizer(s):
    return s.split()

# --- READ
def get_words( files ):
    ''' List of words for each document (in fact a string, to be passed to countVect  '''
    words = []
    count_doc = 0
    for _file in files:
        with open( _file ) as f:
            print( "Reading file", _file, count_doc, '/', len( files ) )
            doc = document.Document( f.read() )
            doc_wds = []
            for sentence in doc.sentences:
                for token in sentence.tokens:
                    doc_wds.append( token.word )
        words.append( ' '.join( doc_wds ) )
        count_doc += 1
    print( "\#Documents", count_doc, len( words ) )
    return words
#                         #print( sentence.semantic_head )
#                         #print( sentence.basic_dependencies )
#                     #print( doc.sentences )
#                     #print( doc.sentiment )




def get_dependencies( files ):
    corpus_treelet = []
    three_tok_treelet = []
    two_tok_treelet = []
    one_tok_treelet = []
    count_doc = 0
    for _file in files:
        with open( _file ) as f:
            print( "Reading file", _file, count_doc, '/', len( files ) )
            doc_three_tok_treelet = []
            doc_two_tok_treelet = []
            doc_one_tok_treelet = []

            doc_treelet = ''

            doc = document.Document( f.read() )
            count_sent = 0
            for sentence in doc.sentences:
                sent_three_tok_treelet = []
                graph = sentence.basic_dependencies
                for link in graph.links:
#                     print( link.governor.text, link.type, link.dependent.text )
                    gov_id = link.governor.idx
                    dep_id = link.dependent.idx
                    gov_pos = find_pos( sentence, gov_id )
                    dep_pos = find_pos( sentence, dep_id )
                    # - 2 tok Treelet
                    treelet = [ (dep_id, dep_pos, gov_id, gov_pos, link.type) ]
                    treelet_str = dep_pos+'-'+link.type+'->'+gov_pos
#                     treelet_str = dep_pos+'-'+link.type+'->'+gov_pos if gov_id > dep_id else gov_pos+'<-'+link.type+'-'+dep_pos
                    doc_two_tok_treelet.append( treelet_str )
                    # - 3 tok Treelet
                    # for each link, find token attached to the gov or dep: three token treelet
                    #   NOUN<-nsubk-VERB-dobj->NOUN or PRON<-poss-NOUN<-nsubj-VERB
                    # for each link between a GOV1 and a DEP1
                    #  ?-- another DEP for the GOV, DEP2
                    #  ?-- a DEP for the DEP, DEP3
                    #three_treelet = [] # GOV1, DEP1, DEP2, DEP3
                    sent_three_tok_treelet.extend( find_threetreelet( gov_id, dep_id, graph, sentence ) )

                # three tok : remove duplicates, transform to string repr
                three_treelet_s = []
                no_duplicate_three_tok_treelet = []
                for t in sent_three_tok_treelet:
                    if not sorted( t ) in three_treelet_s:
                        three_treelet_s.append( sorted( t ) )
                        no_duplicate_three_tok_treelet.append( t )
                # Add the three let for the sentence to the doc repr
                doc_three_tok_treelet.extend( tostr_three_treelet( no_duplicate_three_tok_treelet, graph, sentence ) )

                # one tok = POS
                for token in sentence.tokens:
                    doc_one_tok_treelet.append( token.pos )

                count_sent += 1
            print( "\t\#Sentences", count_sent )
            count_doc += 1

#             three_tok_treelet.append( doc_three_tok_treelet )
#             two_tok_treelet.append( doc_two_tok_treelet )
#             one_tok_treelet.append( doc_one_tok_treelet )

#             print( len( doc_three_tok_treelet )+len( doc_two_tok_treelet )+len( doc_one_tok_treelet ) )

#             # simulate a document with these treelets
#             print( "\tneq doc_one_tok_treelet:", len( set( doc_one_tok_treelet ) ) )
#             print( "\tneq two_tok_treelet:", len( set( doc_two_tok_treelet ) ) )
#             print( "\tneq three_tok_treelet:", len( set( doc_three_tok_treelet ) ) )
            doc_treelet += ' '.join( doc_three_tok_treelet )
            doc_treelet += ' '+' '.join( doc_two_tok_treelet )
            doc_treelet += ' '+' '.join( doc_one_tok_treelet )
#             print( doc_treelet )
            corpus_treelet.append( doc_treelet )
#
#             print( len( doc_treelet.split() ) )
#             sys.exit()

    print( "\#Documents", count_doc, len( corpus_treelet ) )
    #, "(", len( three_tok_treelet ), len( two_tok_treelet ), len( one_tok_treelet ), ")" )
    return corpus_treelet


def tostr_three_treelet( no_duplicate_three_tok_treelet, graph, sentence ):
    str_three_treelet = []
    for (gov1, dep1, dep2, dep3) in no_duplicate_three_tok_treelet:
        if dep3 == None:
            # GOV1 has 2 DEP : DEP1-link->GOV1<-link-DEP2
            link_g1d1 = find_link( gov1, dep1, graph )
            link_g1d2 = find_link( gov1, dep2, graph )
            if link_g1d1 == None:
                print( "1-No link found for ", gov1, dep1 )
                sys.exit()
            if link_g1d2 == None:
                print( "2-No link found for ", gov1, dep2 )
                sys.exit()
            str_three_treelet.append(
                    find_pos( sentence, dep1 )+'-'+link_g1d1+'->'+find_pos( sentence, gov1 )+'<-'+link_g1d2+'-'+find_pos( sentence, dep2 ) )
        elif dep2 == None:
            # GOV1 governs DEP1, and DEP1 governs DEP3 : DEP2-LINK->DEP1-LINK->GOV1
            link_g1d1 = find_link( gov1, dep1, graph )
            link_d1d3 = find_link( dep1, dep3, graph )
            if link_g1d1 == None:
                print( "3-No link found for ", gov1, dep1 )
                sys.exit()
            if link_d1d3 == None:
                print( "4-No link found for ", dep1, dep3 )
                sys.exit()
            str_three_treelet.append(
                    find_pos( sentence, dep3 )+'-'+link_d1d3+'->'+find_pos( sentence, dep1 )+'-'+link_g1d1+'->'+find_pos( sentence, gov1 ) )
        else:
            print( "WTF?", gov1, dep1, dep2, dep3 )
            sys.exit()
    return str_three_treelet

def find_link( gov1, dep1, graph ):
    for link in graph.links:
        gov_id = link.governor.idx
        dep_id = link.dependent.idx
        if gov_id == gov1 and dep_id == dep1:
            return link.type
    return None


# - for each link between a GOV1 and a DEP1
#  -- another DEP for the GOV, DEP2
#  -- a DEP for the DEP, DEP3
def find_threetreelet( gov1_id, dep1_id, graph, sentence ):
    three_treelet = []
    for token in sentence.tokens:
        if token.id != gov1_id and token.id != dep1_id:
            gov_token = get_gov( token.id, graph )
            # if token is another dep of gov ie gov id in gov token
#             print( gov1_id, token.id, gov_token )
            if gov1_id in gov_token:
#                 print( "LINK", link_type )
#                 sys.exit()
                three_treelet.append( tuple([gov1_id, dep1_id, token.id, None]) )
#                 print( "TOKEN another DEP of GOV", find_pos( sentence, gov1_id ), find_pos( sentence, dep1_id ), find_pos( sentence, token.id ) )
            # if token if a dep of dep ie dep id in gov token
            elif dep1_id in gov_token:
                three_treelet.append( tuple([gov1_id, dep1_id, None, token.id]) )
#                 print( "TOKEN is DEP of DEP", find_pos( sentence, gov1_id ), find_pos( sentence, dep1_id ), find_pos( sentence, token.id ) )
    return three_treelet


def get_dep( token_id, graph ):
    deps = []
    for link in graph.links:
        gov_id = link.governor.idx
        dep_id = link.dependent.idx
        if token_id == gov_id:
            deps.append( dep_id )
#     print( token_id, deps )
    return deps

def get_gov( token_id, graph ):
    govs = []
    for link in graph.links:
        gov_id = link.governor.idx
        dep_id = link.dependent.idx
        if token_id == dep_id:
            govs.append( gov_id )
    return govs


def has_link( graph, idx, idd ):
    for link in graph.links:
#                     print( link.governor.text, link.type, link.dependent.text )
        gov_id = link.governor.idx
        dep_id = link.dependent.idx
        # the token idx is the governor
        if (idx == gov_id and idd == dep_id):
            return link, idx
        if (idx == dep_id and idd == gov_id):
            return link, idd
    return None, None


def find_pos( sentence, idx ):
    if idx == 0:
        return "root"
    for i, token in enumerate( sentence.tokens ):
        if token.id == idx:
#             print( token.word, token.pos )
            return token.pos
    return None

if __name__ == '__main__':
    main()



