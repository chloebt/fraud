#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random

'''
Convert the documents into the input format for the UDPipe parser, ie raw documents
(some documents have line breaks within sentences, remove these line breaks)
'''


def main( ):
    parser = argparse.ArgumentParser(
            description='Study features.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input directory')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output dir')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    convert( args.inpath, args.outpath )

def convert( inpath, outpath ):
    for sdir in [d for d in os.listdir( inpath ) if not d.startswith( '.' )]:
        print( 'Reading', sdir )
        outdir = os.path.join( outpath, sdir )
        if not os.path.isdir( outdir ):
            os.mkdir( outdir )
        for inf in [f for f in os.listdir( os.path.join( inpath, sdir ) ) if not f.startswith( '.' )]:
            print( '\tReading', inf )
            with open( os.path.join( inpath, sdir, inf ) ) as myfile:
                outf = open( os.path.join( outdir, inf )+'.raw', 'w' )
                lines = myfile.readlines()
                curdoc = ''
                for i,l in enumerate( lines ):
                    l = l.strip()
                    newl = ''
                    if l == '':
                        newl = '\n'
                        if i == 0 or lines[i-1].strip() != '':#keep original line breaks
                            newl += '\n'
                    else:
                        newl = l+' '
                    curdoc += newl
                outf.write( curdoc.strip() )
                outf.close()



if __name__ == '__main__':
    main()


