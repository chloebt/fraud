#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e

export PYTHONPATH=$PYTHONPATH:/home/bplank/tools/cnn/pycnn/


TEST=$1
OUTDIR=$2

SRC=discoursecph/segmentation/code/
MODEL=experiments/eacl17_multiling-domRSTsegmenter/en/rsten-doc-wdpos/model-disco-doc.task-rsten-doc-wdpos.ite-30.lay-2.hdim-100.sigma-0.2.indim-500.model

ITER=30
INDIM=500
HDIM=100
SIGMA=0.2
HLAYERS=2
TASK=rsten-doc-wdpos
# -- Fixed parameters
SEED=3068836234
#MEM=1024
MEM=1536


echo TASK ${TASK}
echo MODEL ${MODEL}

PY=/home/bplank/anaconda/envs/p3k/bin #need python3 with the right packages


OUTFILE=${OUTDIR}pred-${TEST##*/}

echo READING ${TEST##*/}


TEMP=temp2/
mkdir -p $TEMP 
cp $TEST $TEMP

echo OUTFILE ${OUTFILE} 
${PY}/python ${SRC}/disco.py --cnn-mem ${MEM} --cnn-seed ${SEED} --in_dim ${INDIM} --iters ${ITER} --h_dim ${HDIM} --sigma ${SIGMA} --h_layers ${HLAYERS} --pred_layer ${HLAYERS} --train --model ${MODEL} --test ${TEMP} --output ${OUTFILE}



