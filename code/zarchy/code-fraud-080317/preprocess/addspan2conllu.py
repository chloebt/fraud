#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random

'''
Convert the documents into the input format for the UDPipe parser, ie raw documents
(some documents have line breaks within sentences, remove these line breaks)
'''


def main( ):
    parser = argparse.ArgumentParser(
            description='Study features.')
    parser.add_argument('--conllu',
            dest='conllu',
            action='store',
            help='Input directory')
    parser.add_argument('--raw',
            dest='raw',
            action='store',
            help='Output dir')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output dir, augmented conllu')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    convert( args.conllu, args.raw, args.outpath )

def convert( conllu, raw, outpath ):
    for sdir in [d for d in os.listdir( conllu ) if not d.startswith( '.' )]:
        print( 'Reading', sdir )
        outdir = os.path.join( outpath, sdir )
        if not os.path.isdir( outdir ):
            os.mkdir( outdir )
        for inf in [f for f in os.listdir( os.path.join( conllu, sdir ) ) if not f.startswith( '.' )]:
            print( '\tReading', inf )
            # Find corresponding connective file
            #if inf == 'F752_FULL6_12.txt.conllu':
            rawf = find_file( inf, os.path.join( raw, sdir), ext='.txt.raw' )
            if rawf == None:
                sys.exit( "File with raw text not found for file "+inf )
            token2conllu, token2span = addSpan( os.path.join( conllu, sdir, inf ), rawf )
            write( token2conllu, token2span, os.path.join( outdir, inf.replace( '.conllu', '.span.conllu' ) ) )

def write( token2conllu, token2span, outf ):
    with open( outf, 'w' ) as myf:
        for i, (tok,l) in enumerate( token2conllu ):
#             print( "==> TOK", tok, l )
            if i != 0 and int( l.split('\t')[0] ) == 1:
                myf.write( '\n' )
            newl = l.strip()+'\t'+str(token2span[i][1][0])+'..'+str(token2span[i][1][1])
            myf.write( newl+'\n' )


def addSpan( conllu, rawf ):
    # original conllu files are in latin1 Oo
    token2span, token2conllu = [], []
    try:
        token2conllu = [ [l.split('\t')[1], l] for l in open( conllu ).readlines() if l.strip() != '']
    except:
        token2conllu = [ [l.split('\t')[1], l] for l in open( conllu, encoding='latin1' ).readlines() if l.strip() != '']

    try:
        raw = open( rawf ).read()
    except:
        raw = open( rawf, encoding='latin1' ).read()#some raw files in latin1 ??
#     print( type(raw) )
#     print( raw )
    pos = 0
    tok = 0
    curt = ''
    while pos < len( raw ):
        c = raw[pos]
        curt += c
        if curt.strip() == token2conllu[tok][0]:
            token2span.append( [token2conllu[tok][0], [pos-len(token2conllu[tok][0])+1, pos+1] ] )#exact span needed for having the word by calling raw[beg:end]
#             print( token2conllu[tok][1].split('\t')[0], token2conllu[tok][0], [pos-len(token2conllu[tok][0])+1, pos+1], raw[pos-len(token2conllu[tok][0])+1:pos+1], len(raw), '-->', curt )
            tok += 1
            curt = ''
        pos += 1
    return token2conllu, token2span

def find_file( originf, conn, ext='.txt.raw.annot.raw' ):
    for inf in [f for f in os.listdir( conn ) if not f.startswith( '.' )]:
        if inf.endswith( ext ) and os.path.basename( inf ).split('.')[0] == os.path.basename( originf ).split('.')[0]:
            return os.path.join( conn, inf )
    return None


if __name__ == '__main__':
    main()


