
# discoursecph/multilingual/expe/en-rstdt/
models=$1
# discoursecph/multilingual/data/en-rtdt/
test=$2 #rawtest=${data}/test.raw
# name of the directory where the models are kept
#modelname=$3
# template file
tpls=$3
dims=$4
outpath=$5


### parameters
it=8       # iterations
rlr=0.03     # learning_rate
rdc=1e-6     # decrease constant
rh=128
rbeam=16

# tpls=discoursecph/multilingual/expe/discourse_ttd_s0s1q0.tpl # templates
# dims=discoursecph/multilingual/expe/dimensions_w50        # dimensions
#dims=discoursecph/multilingual/expe/dimensions_w16

threads=18

## lots of exps !!
#
for lr in $rlr 
do 
for dc in $rdc 
do
for h in $rh 
do
    ((j=j%threads)); ((j++==0)) && wait
    (

    # model name
    model=${models}/tpls_2H_h${h}_lr${lr}_dc${dc}
#     mkdir ${model}
    
    # training command line, log printed in model/trainer_log.txt
    #echo ./discoursecph/multilingual/hyparse_fork/build/nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -a -f 2 ${train} ${dev}
    #./discoursecph/multilingual/hyparse_fork/build/nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -a -f 2 ${train} ${dev} 2> ${model}/trainer_log.txt

    # for each model (i.e. the model dumped at each iteration
    for i in `seq 1 ${it}`
    do 
        # get missing pieces
#         for f in encoder ttd embed_dims templates
#         do
#             cat ${model}/${f} > ${model}/iteration${i}/${f}
#         done
        
        # parse with beam = 32 16 8 4 2 1 (larger beam is possible)
        for b in $rbeam
        do
            # modifies the model/beam file to choose a custom beamsize
#             echo ${b} > ${model}/iteration${i}/beam
            # parse dev and test
#             ./discoursecph/multilingual/hyparse_fork/build/nnp -I ${rawdev} -O ${model}/dev_it${i}_beam${b} -m ${model}/iteration${i}
            ./discoursecph/multilingual/hyparse_fork/build/nnp -I ${rawtest} -O ${outpath}/test_it${i}_beam${b} -m ${model}/iteration${i}
            
#             # evaluate dev and test output
#             python discoursecph/multilingual/expe/scripts/eval_parser.py --preds ${model}/dev_it${i}_beam${b} --gold ${devref} --params set=dev_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores}
#             #> ${model}/eval_dev_it${i}_beam${b}
#             python discoursecph/multilingual/expe/scripts/eval_parser.py --preds ${model}/test_it${i}_beam${b} --gold ${testref} --params set=test_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores}
#             #> ${model}/eval_test_it${i}_beam${b}


        done
    done
    
    ) &
done
done
done

#Cocophotos: should wait for all jobs to finish before finishing itself
for job in $(jobs -p); do
    wait $job;
done



