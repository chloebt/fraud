#!/usr/bin/python
# -*- coding: utf-8 -*-




from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random
from corenlp_xml import document
from sklearn.model_selection import KFold

def main( ):
    parser = argparse.ArgumentParser(
            description='Read fraud data, generate features files.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            default="data/corpora/others/Fraud_data/parsed/",
            help='Input directory')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            default="data/fraud_data/",
            #default="data/corpora/others/Fraud_data/samples/",
            help='Output directory')
    parser.add_argument('--seed',
            dest='seed',
            action='store',
            help='Random seed')
    parser.add_argument('--mode',
            dest='mode',
            action='store',
            choices = ['rand', 'cv'],
            help='Mode to make the split, either random (rand) or by cv (cv)')
    args = parser.parse_args()


#     train, dev, test = split( args.inpath, args.outpath )
    split( args.inpath, args.outpath, seed=args.seed, mode=args.mode )


def split( inpath, outpath, seed=1234, mode='rand' ):
    test_size = 50


    paths = []
    for dataset in ['train', 'test']:
        path = os.path.join( outpath, dataset )
        paths.append( path )
        if not os.path.isdir( path ):
            os.mkdir( path )

    pos_files = [os.path.join( inpath, 'fraudulent_parsed', f )
            for f in os.listdir( os.path.join( inpath, 'fraudulent_parsed' ) ) if not f.startswith('.') ]
    neg_files = [os.path.join( inpath, 'unretracted_parsed', f )
            for f in os.listdir( os.path.join( inpath, 'unretracted_parsed' ) ) if not f.startswith('.') ]

    print( "Total files:", len(pos_files)+len(neg_files) )

    print( 'test size per class:', test_size )


    if mode == 'rand':
        _split_random( pos_files, neg_files, outpath, seed=seed )
    elif mode == 'cv':
        _split_cv( pos_files, neg_files, outpath, seed=seed )

def _split_cv( pos_files, neg_files, outpath, seed=1234 ):
#     kf = KFold(n_splits=2)
    raise NotImplementedError

def _split_random( pos_files, neg_files, outpath, seed=1234 ):
    random.seed = seed
    random.shuffle( pos_files )

    test_list = os.path.join( outpath, 'test_list.txt' )
    d = open( test_list, 'w' )
    d.close()
    train_list = os.path.join( outpath, 'train_list.txt' )
    d = open( train_list, 'w' )
    d.close()

    pos_test_files = pos_files[:test_size]
    copy_files( pos_test_files, paths[1], '.pos', test_list )
    pos_train_files = pos_files[test_size:]
    copy_files( pos_train_files, paths[0], '.pos', train_list )

    neg_test_files = neg_files[:test_size]
    copy_files( neg_test_files, paths[1], '.neg', test_list )
    neg_train_files = neg_files[test_size:]
    copy_files( neg_train_files, paths[0], '.neg', train_list )

    print( "TRAIN:", len( pos_train_files )+len( neg_train_files ),
            " ; TEST:", len( pos_test_files )+len( neg_test_files ) )
#     return paths



def __split( inpath, outpath ):
    percentage = 10
    random.seed = 1234

    paths = []
    for dataset in ['train', 'dev', 'test']:
        path = os.path.join( outpath, dataset )
        paths.append( path )
        if not os.path.isdir( path ):
            os.mkdir( path )

    pos_files = [os.path.join( inpath, 'fraudulent_parsed', f )
            for f in os.listdir( os.path.join( inpath, 'fraudulent_parsed' ) ) if not f.startswith('.') ]
    neg_files = [os.path.join( inpath, 'unretracted_parsed', f )
            for f in os.listdir( os.path.join( inpath, 'unretracted_parsed' ) ) if not f.startswith('.') ]

    print( "Total files:", len(pos_files)+len(neg_files) )

    dev_test_size = int(( len( pos_files )*percentage )/100)
    print( 'test size:', dev_test_size*2 )
    random.shuffle( pos_files )

#     dev_list = os.path.join( outpath, 'dev_list.txt' )
#     d = open( dev_list, 'w' )
#     d.close()
    test_list = os.path.join( outpath, 'test_list.txt' )
    d = open( test_list, 'w' )
    d.close()
    train_list = os.path.join( outpath, 'train_list.txt' )
    d = open( train_list, 'w' )
    d.close()

#     pos_dev_files = pos_files[:dev_test_size]
#     copy_files( pos_dev_files, paths[1], '.pos', dev_list )
#     pos_test_files = pos_files[dev_test_size:dev_test_size*2]
    pos_test_files = pos_files[:dev_test_size]
    copy_files( pos_test_files, paths[2], '.pos', test_list )
#     pos_train_files = pos_files[dev_test_size*2:]
    pos_train_files = pos_files[dev_test_size:]
    copy_files( pos_train_files, paths[0], '.pos', train_list )

#     neg_dev_files = neg_files[:dev_test_size]
#     copy_files( neg_dev_files, paths[1], '.neg', dev_list )
#     neg_test_files = neg_files[dev_test_size:dev_test_size*2]
    neg_test_files = neg_files[:dev_test_size]
    copy_files( neg_test_files, paths[2], '.neg', test_list )
    neg_train_files = neg_files[dev_test_size:]
#     neg_train_files = neg_files[dev_test_size*2:]
    copy_files( neg_train_files, paths[0], '.neg', train_list )

    print( "TRAIN:", len( pos_train_files )+len( neg_train_files ),
            " ; TEST:", len( pos_test_files )+len( neg_test_files ) )
    #" ; DEV:", len( pos_dev_files )+len( neg_dev_files ),

    return paths

def copy_files( list_files, out_path, ext, file_list ):
    o = open( file_list, 'a' )
    for f in list_files:
        shutil.copy( f, os.path.join( out_path, os.path.basename( f )+ext ) )
        o.write( os.path.basename( f )+'\t'+ext.replace('.', '')+'\n' )
    o.close()



if __name__ == '__main__':
    main()



