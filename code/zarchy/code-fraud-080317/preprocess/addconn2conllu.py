#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random

'''
Add connective information to conllu files
'''


def main( ):
    parser = argparse.ArgumentParser(
            description='Add connnectives to conllu files.')
    parser.add_argument('--conllu',
            dest='conllu',
            action='store',
            help='Input directory, containing conllu files with span info from raw')
    parser.add_argument('--conn',
            dest='conn',
            action='store',
            help='Input directory, read the .annot files')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output dir, augmented conllu')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    convert( args.conllu, args.conn, args.outpath )

def convert( conllu, conn, outpath ):
    for sdir in [d for d in os.listdir( conllu ) if not d.startswith( '.' )]:
        print( 'Reading', sdir )
        outdir = os.path.join( outpath, sdir )
        if not os.path.isdir( outdir ):
            os.mkdir( outdir )
        for inf in [f for f in os.listdir( os.path.join( conllu, sdir ) ) if not f.startswith( '.' )]:
            print( '\tReading', inf )
            # Find corresponding connective file
            connf = find_file( inf, os.path.join( conn, sdir), ext='.txt.raw.annot' )
            if connf == None:
                sys.exit( "File with connective info not found for file "+inf )
            addConn( os.path.join( conllu, sdir, inf ), connf )

def addConn( conllu, conn ):
    conn2info = [ (l.split('\t')[2].strip(), l.split('\t')[3].strip(), l.split('\t')[7].strip(), l.split('\t')[-1].strip()) for l in open( conn ).readlines()[1:]]#form / discourse use / relation / span
#     connforms = [ form for (form, use, relation, span) in conn2info ]
#
#
    token2conllu = [ (l.split('\t')[1].strip(), l.split('\t')[-1].strip(), l.strip()) for l in open( conllu ).readlines() if l.strip() != '']#token / span / the whole line
#
#     token2conllu_conn = []
#     pos = 0
#     for i, (tok,span,l) in enumerate( token2conllu ):
#         if tok == connforms[pos]:
#             token2conllu_conn.append( tok,span,l,pos )
#         if i < len( token2conllu ) and connforms[pos] == tok+' '+token2conllu[i+1][0]:
#             token2conllu_conn.append( tok,span,l,pos )




    #id	base_file	form	discourse_use	score_emploi	position	score_position	relation	score_relation	span_conn

    span2conn = { span:[form,use,relation] for (form, use, relation, span) in conn2info }

    span2tok = { span:[form,span,l] for (form, span, l) in token2conllu }


    for span in sorted( span2conn.keys(), key=lambda x:int(x.split('..')[0]) ):
        found = False
        if span in span2tok and span2conn[span][0].strip().lower() == span2tok[span][0].strip().lower():
            found = True
            print( "\tOK", span,span2conn[span], span2tok[span][:2] )
        else:
            newspan = str( int( span.split('..')[0] )-len(span2conn[span][0])+1 )+'..'+str( int( span.split('..')[1] )-len(span2conn[span][0])+1 )
            if newspan in span2tok and span2conn[span][0].strip().lower() == span2tok[newspan][0].strip().lower():
                found = True
                print( "\tOK", span,span2conn[span], span2tok[newspan][:2] )
            else:
                begc = int( span.split('..')[0] )
                endc = int( span.split('..')[1] )

                for (token, spant, l) in token2conllu:
                    begt = int( spant.split('..')[0] )
                    endt = int( spant.split('..')[1] )
                    if token.lower() == span2conn[span][0].lower() and begt in range( begc-10, begc+10 ) and endt == begt+len( token ):
                        print( "\tOK ?", span,span2conn[span], span2tok[str(begt)+'..'+str(endt)][:2] )
                        found = True


        if not found:
            print( 'PB', span,span2conn[span] )


#     print( "\#Conn", len(span2conn) )
#     conn_found = []
#     token2conllu_conn = []
#     for (tok, span, l) in token2conllu:
#         if span in span2conn:
#             token2conllu_conn.append( (tok, span, l, span2conn[span] ) )
#             conn_found.append( span )
#         else:
#             token2conllu_conn.append( (tok, span, l, None ) )
#     print( "\#Conn found", len( conn_found ) )



    sys.exit()

def find_file( originf, conn, ext='.txt.raw.annot' ):
    for inf in [f for f in os.listdir( conn ) if not f.startswith( '.' )]:
        if inf.endswith( ext ) and os.path.basename( inf ).split('.')[0] == os.path.basename( originf ).split('.')[0]:
            return os.path.join( conn, inf )
    return None

if __name__ == '__main__':
    main()


