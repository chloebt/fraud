#!/usr/bin/python
# -*- coding: utf-8 -*-

# Python 3 !


from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random
#from corenlp_xml import document
from nltk.parse.dependencygraph import *


from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
# from sklearn.linear_model import LogisticRegression
# from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.datasets import dump_svmlight_file
from sklearn.externals import joblib

def main( ):
    parser = argparse.ArgumentParser(
            description='Read fraud data, generate features files.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input directory')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory')
    parser.add_argument('--feat',
            dest='feat',
            action='store',
            default="bow",
            help='Type of features (bow, ngram..)')
    parser.add_argument('--conn',
            dest='conn',
            action='store',
            default=None,
            help='Connective annotation')
    parser.add_argument('--subj',
            dest='subjmpqa',
            action='store',
            help='Lexicon with polarity')
    parser.add_argument('--inq',
            dest='inquirer',
            action='store',
            help='Lexicon with sem info')
    parser.add_argument('--levin',
            dest='inquirer',
            action='store',
            help='Lexicon with Levin classes')
    args = parser.parse_args()

    outpath = args.outpath
    if not outpath:
        outpath = os.path.join( vars(args)['inpath'], vars(args)['feat'] )
    if not os.path.isdir( outpath ):
        os.mkdir( outpath )


    # Retrieve the files
    infiles, file2label = retrieve_files( args.inpath, label=True, ext=".txt.conllu" )

    min_df=1
    if args.feat == "bow":
        X_train, X_dev, X_test, vectorizer = bag_of_word( train_files, dev_files,
                test_files, min_df=min_df )
#         X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )#TF IDF
    if args.feat == "ngram":# 1, 2 and 3 grams
        X_train, X_dev, X_test, vectorizer = ngram_words( train_files, dev_files,
                test_files, min_df=min_df )
#         X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )#TF IDF
    elif args.feat == "treelet":
        X_train, X_dev, X_test, vectorizer = treelet( train_files, dev_files,
                test_files, min_df=min_df )
#         X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )#TF IDF
    elif args.feat == "conn":
        X_train, X_dev, X_test, vectorizer = explicit_conn( train_files, dev_files,
                test_files, args.conn, min_df=min_df )
#         X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )#TF IDF
    elif args.feat == "relexpl":
        X_train, X_dev, X_test, vectorizer = explicit_rel( train_files, dev_files,
                test_files, args.conn, min_df=min_df )
#         X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )#TF IDF
    elif args.feat == "polarity":
        lex_pol = read_lexicon_pol( args.subjmpqa )
        X_train, X_dev, X_test, vectorizer = polarity( train_files, dev_files,
                test_files, lex_pol, min_df=min_df )
    elif args.feat == "conn+relexpl":
        X_train, X_dev, X_test, vectorizer = explicit_conn_rele( train_files, dev_files,
                test_files, args.conn, min_df=min_df )
    elif args.feat == "bow+treelet":
        X_train, X_dev, X_test, vectorizer = bw_treelet( train_files, dev_files,
                test_files, min_df=min_df )
#         X_train, X_dev, X_test = tfidf( X_train, X_dev, X_test )

#     elif args.feat == "all":
#         X_train, X_dev, X_test, vectorizer = allfeats( train_files, dev_files,
#                 test_files, args.conn, min_df=min_df )



    # -- Get labels
    y_train, y_dev, y_test = get_labels( train_files, dev_files, test_files, file2label )


#     conn_files = getFiles( args.conn, ext='.txt.raw.annot' )
#     train_conn = get_conn( train_files, conn_files )
#     train_wds = get_words( train_files )
#
#     # nb de mots qui st des conn / nb de mots
#     class2count = {}
#     for i,d in enumerate( train_conn ):
#         if y_train[i] in class2count:
#             class2count[y_train[i]].append( [len( train_wds[i].split() ), len( d.split() )] )
#             #class2count[y_train[i]].append( [len( train_wds[i].split() ), len( [w for w in train_wds[i].split() if w=='but'] )] )
#         else:
#             class2count[y_train[i]] = [ [len( train_wds[i].split() ), len( d.split() )] ]
#             #class2count[y_train[i]] = [ [len( train_wds[i].split() ), len( [w for w in train_wds[i].split() if w=='but'] )] ]
#
#     for c in class2count:
#         allw = sum( [w for (w,c) in class2count[c]] )
#         allc = sum( [c for (w,c) in class2count[c]] )
#         #allc = sum( [c for (w,c) in class2count[c]] )
#         print( allw, allc )
#         print( c, allc/allw )
#         #print( c, (allc/allw)*100 )

    # -- Write data
    write( X_train, y_train, os.path.join( outpath, "train.svmlight" ) )
    if len( y_dev ) != 0:
        write( X_dev, y_dev, os.path.join( outpath, "dev.svmlight" ) )
    write( X_test, y_test, os.path.join( outpath, "test.svmlight" ) )
#
    # -- Save dictionnary
    print( "#Vocab", len(vectorizer.vocabulary_) )
    joblib.dump( vectorizer.vocabulary_, os.path.join( args.outpath, 'vocab-'+args.feat ), compress=3 )

#     print( vectorizer.vocabulary_ )



# ----------------------------------------------------------------------------------
# --- FEATURES
# ----------------------------------------------------------------------------------

def allfeats( train, dev, test, conn_annot, min_df=1 ):
    # bow
    train_wds = get_words( train )
    # conn
    conn_files = getFiles( conn_annot, ext='.txt.raw.annot' )
    train_conn = get_conn( train, conn_files )
    # expl rel
    train_rel = get_rel( train, conn_files )
    # treelet
    train_treelets = get_dependencies( train )
    # ngrams
    ngram_vectorizer = CountVectorizer(ngram_range=(2, 3), min_df=min_df, stop_words='english')
    analyze = ngram_vectorizer.build_analyzer()
    train_ngrams = []
    for i,w in enumerate( train_wds ):
        train_ngrams.append( ' '.join( [f.replace(' ', '_') for f in analyze( train_wds[i] )] ) )
#         print( train_ngrams )
#         sys.exit()
    #X_train = vectorizer.fit_transform(train_wds)
    # merge
    train_merged = merge( train_wds, train_conn )
    train_merged = merge( train_merged, train_rel )
    train_merged = merge( train_merged, train_treelets )
    train_merged = merge( train_merged, train_ngrams )

    vectorizer, X_train = build_vectorizer(train_merged, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train_merged ), "\nX_train:", X_train.shape )

    test_wds = get_words( test )
    test_conn = get_conn( test, conn_files )
    test_rel = get_rel( test, conn_files )
    test_treelets = get_dependencies( test )
    # merge
    test_merged = merge( test_wds, test_conn )
    test_merged = merge( test_merged, test_rel )
    test_merged = merge( test_merged, test_treelets )
    X_test = vectorizer.transform( test_merged )
    print( '#Test Docs', len( test_merged ), "\nX_test:", X_test.shape )

    return X_train, None, X_test, vectorizer

def bag_of_word( train, dev, test, min_df=1 ):
    # represent each corpus by a list of words
    train_wds = get_words( train )
    vectorizer, X_train = build_vectorizer(train_wds, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train_wds ), "\nX_train:", X_train.shape )

    X_dev = None
    if len(dev ) != 0:
        dev_wds = get_words( dev )
        X_dev = vectorizer.transform( dev_wds )
        print( '#Dev Docs', len( dev_wds ), "\nX_dev:", X_dev.shape )

    test_wds = get_words( test )
    X_test = vectorizer.transform( test_wds )
    print( '#Test Docs', len( test_wds ), "\nX_test:", X_test.shape )
    return X_train, X_dev, X_test, vectorizer


def ngram_words( train, dev, test, min_df=1 ):
    train_wds = get_words( train )
    vectorizer = CountVectorizer(ngram_range=(2, 3), min_df=min_df, stop_words='english')
    X_train = vectorizer.fit_transform(train_wds)
    print( '#Train Docs', len( train_wds ), "\nX_train:", X_train.shape )

    X_dev = None
    if len(dev ) != 0:
        dev_wds = get_words( dev )
        X_dev = vectorizer.transform( dev_wds )
        print( '#Dev Docs', len( dev_wds ), "\nX_dev:", X_dev.shape )

    test_wds = get_words( test )
    X_test = vectorizer.transform( test_wds )
    print( '#Test Docs', len( test_wds ), "\nX_dev:", X_test.shape )
    return X_train, X_dev, X_test, vectorizer


def explicit_conn( train, dev, test, conn_annot, min_df=1 ):
    conn_files = getFiles( conn_annot, ext='.txt.raw.annot' )

    train_conn = get_conn( train, conn_files )
    vectorizer, X_train = build_vectorizer(train_conn, min_df=min_df)
    print( '#Train Docs', len( train_conn ), "\nX_train:", X_train.shape )

    X_dev = None
    if len(dev ) != 0:
        dev_conn = get_conn( dev, conn_files)
        X_dev = vectorizer.transform( dev_conn )
        print( '#Dev Docs', len( dev_conn ), "\nX_dev:", X_dev.shape )

    test_conn = get_conn( test, conn_files )
    X_test = vectorizer.transform( test_conn )
    print( '#Test Docs', len( test_conn ), "\nX_test:", X_test.shape )
    return X_train, X_dev, X_test, vectorizer


def explicit_rel( train, dev, test, conn_annot, min_df=1 ):
    conn_files = getFiles( conn_annot, ext='.txt.raw.annot' )

    train_rel = get_rel( train, conn_files )
    vectorizer, X_train = build_vectorizer(train_rel, min_df=min_df)
    print( '#Train Docs', len( train_rel ), "\nX_train:", X_train.shape )

    X_dev = None
    if len(dev ) != 0:
        dev_rel = get_rel( dev, conn_files)
        X_dev = vectorizer.transform( dev_rel )
        print( '#Dev Docs', len( dev_rel ), "\nX_dev:", X_dev.shape )

    test_rel = get_rel( test, conn_files )
    X_test = vectorizer.transform( test_rel )
    print( '#Test Docs', len( test_rel ), "\nX_test:", X_test.shape )
    return X_train, X_dev, X_test, vectorizer


def explicit_conn_rele( train, dev, test, conn_annot, min_df=1 ):
    conn_files = getFiles( conn_annot, ext='.txt.raw.annot' )

    train_rel = get_rel( train, conn_files )
    train_conn = get_conn( train, conn_files )
    train_merged = merge( train_rel, train_conn )
    vectorizer, X_train = build_vectorizer(train_merged, min_df=min_df)
    print( '#Train Docs', len( train_rel ), "\nX_train:", X_train.shape )

    X_dev = None
    if len(dev ) != 0:
        dev_rel = get_rel( dev, conn_files)
        dev_conn = get_conn( dev, conn_files)
        dev_merged = merge( dev_rel, dev_conn )
        X_dev = vectorizer.transform( dev_merged )
        print( '#Dev Docs', len( dev_rel ), "\nX_dev:", X_dev.shape )

    test_rel = get_rel( test, conn_files )
    test_conn = get_conn( test, conn_files)
    test_merged = merge( test_rel, test_conn )
    X_test = vectorizer.transform( test_merged )
    print( '#Test Docs', len( test_rel ), "\nX_test:", X_test.shape )
    return X_train, X_dev, X_test, vectorizer


def treelet( train, dev, test, min_df=1 ):
    # list of doc treelets (# feature name = str treelet)
    train_treelets = get_dependencies( train )
    # Need a tokenizer based on spaces
    vectorizer, X_train = build_vectorizer(train_treelets, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train_treelets ), "\nX_train:", X_train.shape )
#     print( vectorizer.vocabulary_ )
#     print( len( vectorizer.vocabulary_ ) )

    X_dev = None
    if len(dev ) != 0:
        dev_treelets = get_dependencies( dev )
        X_dev = vectorizer.transform(dev_treelets)
        print( '#Dev Docs', len( dev_treelets ), "\nX_dev:", X_dev.shape )

    test_treelets = get_dependencies( test )
    X_test = vectorizer.transform(test_treelets)
    print( '#Test Docs', len( test_treelets ), "\nX_test:", X_test.shape )

    return X_train, X_dev, X_test, vectorizer



def bw_treelet( train, dev, test, min_df=1 ):
    train_treelets = get_dependencies( train )
    train_wds = get_words( train )
    train_merged = merge( train_treelets, train_wds )
    # Need a tokenizer based on spaces
    vectorizer, X_train = build_vectorizer(train_merged, tokenizer=my_tokenizer, min_df=min_df)
    print( '#Train Docs', len( train_merged ), "\nX_train:", X_train.shape )
#     print( vectorizer.vocabulary_ )
#     print( len( vectorizer.vocabulary_ ) )

    X_dev = None
    if len(dev ) != 0:
        dev_treelets = get_dependencies( dev )
        dev_wds = get_words( dev )
        dev_merged = merge( dev_treelets, dev_wds )
        X_dev = vectorizer.transform(dev_merged)
        print( '#Dev Docs', len( dev_merged ), "\nX_dev:", X_dev.shape )

    test_treelets = get_dependencies( test )
    test_wds = get_words( test )
    test_merged = merge( test_treelets, test_wds )
    X_test = vectorizer.transform(test_merged)
    print( '#Test Docs', len( test_merged ), "\nX_test:", X_test.shape )
    return X_train, X_dev, X_test, vectorizer






# ----------------------------------------------------------------------------------
# --- READ
# ----------------------------------------------------------------------------------

# -- Words
def get_words( files ):
    '''
    List of words for each document
    List of string representing each document, to be passed to countVect
    '''
    words = []
    count_doc = 0
    for _file in files:
        with open( _file ) as f:
            doc_wds = []
            sentences = [s.strip() for s in f.read().split('\n\n') if s.strip() != '']
            for sent in sentences:
                tokens = [s.strip() for s in sent.split('\n') if not s.strip() == '']
                for t in tokens:
                    id_tok, form, lemma, pos, posx, morph, head, fct, _, _ = t.split('\t')
                    doc_wds.append( form )
            words.append( ' '.join( doc_wds ) )
            count_doc += 1
    print( "\#Documents", count_doc, len( words ) )
    return words



# -- Connectives and explicit relations
def get_conn( dset_files, conn_files ):
    dset_conn = []
    for f in dset_files:
        doc_conn = []
        cf = findFile( f, conn_files )
        if cf == None:
            sys.exit( "Connective file not found "+f )
        lines = open( cf ).readlines()
        for l in lines:
            l = l.strip()
            _,_,connective,use,_,_,_,relation,_,_ = l.split('\t')
            if use == 'positive':
                doc_conn.append( connective.lower() )#only keep the connective in discourse reading
        dset_conn.append( ' '.join( doc_conn ) )
    #id	base_file	form	discourse_use	score_emploi	position	score_position	relation	score_relation	span_conn
    return dset_conn

def get_rel( dset_files, conn_files ):
    dset_rel = []
    for f in dset_files:
        doc_rel = []
        cf = findFile( f, conn_files )
        if cf == None:
            sys.exit( "Connective file not found "+f )
        lines = open( cf ).readlines()
        for l in lines:
            l = l.strip()
            _,_,connective,use,_,_,_,relation,_,_ = l.split('\t')
            if use == 'positive':
                doc_rel.append( relation.lower() )#only keep the connective in discourse reading
        dset_rel.append( ' '.join( doc_rel ) )
    #id	base_file	form	discourse_use	score_emploi	position	score_position	relation	score_relation	span_conn
    return dset_rel



# -- Syntactic dependencies
def get_dependencies( files ):
    corpus_treelet = []
    for i,_file in enumerate(files):
        doc_treelet = ''
        doc_one_treelets, doc_two_treelets, doc_three_treelets = [],[],[]
        with open( _file ) as f:
            # print( "Reading file", _file, i, '/', len( files ) )
            sentences = [s.strip() for s in f.read().split('\n\n') if s.strip() != '']
            # -- Transform to MALT TAB format to be able to use dependencygraph nltk class
            for i,sent in enumerate( sentences ):
                dg, one_tok_treelets = _build_graph( sent )#graph and one token treelets
                doc_one_treelets.extend( one_tok_treelets )

                two_tok_treelets = []
                for (h, r, d) in dg.triples():#(head_wd,head_tag), relation, (dep_wd, dep_tag)
                    two_tok_treelets.append( h[1]+'->'+r+'->'+d[1] )
                doc_two_treelets.extend( two_tok_treelets )

                three_tok_treelets = get_three_tok_treelets( dg )
                doc_three_treelets.extend( three_tok_treelets )
#             print( "Reading file", _file, i, '/', len( files ), len( sentences ), len( doc_three_treelets ) )
            doc_treelet += ' '.join( doc_one_treelets )+' '
            doc_treelet += ' '.join( doc_two_treelets )+' '
            doc_treelet += ' '.join( doc_three_treelets )
            corpus_treelet.append( doc_treelet )
    return corpus_treelet

def get_three_tok_treelets( dg ):
    '''
    Three tokens treelets are either:
    - one node dominating two nodes  DEP1 -> r1 -> NODE <- r2 <- Dep2
    - a chain: one node dominating another node that dominates another node NODE -> r1 -> Dep1 -> r2 -> Dep2
    '''
    three_tok_treelets = []
    for n in dg.nodes:
        node = dg.get_by_address(n)
        node_word = node['word']
        node_pos = node['tag']
        node_deps = node['deps']
        node_rel = node['rel']
        new_deps = flat_dep_list( node_deps )

        if len( new_deps ) > 1 and node_word != None:#not taking into account the fake root added
            new_deps = sorted( new_deps, key=lambda x:x[1] )
            # --> head dominating two nodes
            for i, (fct1, dep_add1) in enumerate( new_deps[:-1] ):
                for (fct2, dep_add2) in new_deps[i+1:]:
                    three_tok_treelets.append( dg.get_by_address(dep_add1)['tag']+'->'+fct1+'->'+node_pos+'<-'+fct2+'<-'+dg.get_by_address(dep_add2)['tag'] )

            # --> chains: head -> dep1 -> dep2
            for i, (fct1, dep_add1) in enumerate(new_deps):
                dependents = get_deps( dg, dep_add1 )# Look for dependents of dep1
                if len( dependents ) > 0:
                    for (fct2, dep_add2) in dependents:
                        three_tok_treelets.append( node_pos+'->'+fct1+'->'+dg.get_by_address(dep_add1)['tag']+'->'+fct2+'->'+dg.get_by_address(dep_add2)['tag'] )
    return three_tok_treelets

def flat_dep_list( node_deps ):
    new_deps = []
    for (fct, dep_add) in node_deps.items():
        for a in dep_add:
            new_deps.append( (fct,a) )
    return new_deps

def get_deps( dg, add ):
    '''
    Look for dependents of node at the address add
    '''
    deps = []
    for n in dg.nodes:
        if n == add:
            node = dg.get_by_address(n)
            node_deps = node['deps']
            new_deps = flat_dep_list( node_deps )
            return new_deps
    return []

def _build_graph( sent, tmpf='tmpdep.txt' ):
    '''
    Build a graph for the sentence sent using the class DependencyGraph from NLTK
    Return also the list of pos occurring in the doc ie the one token treelets
    '''
    pos_treelets = []
    tokens = [s.strip() for s in sent.split('\n') if not s.strip() == '']
    o = open( tmpf, 'w' )#required to read data from a file
    for t in tokens:
        id_tok, form, lemma, pos, posx, morph, head, fct, _, _ = t.split('\t')
        pos_treelets.append( pos )
        if int(head) == 0:
            fct = 'root'
        o.write( '\t'.join( [form, pos, head, fct] )+'\n' )
    o.close()

    depgraph = DependencyGraph()
    g = depgraph.load (tmpf, cell_separator='\t', top_relation_label='root' )

    if len( g ) != 1:
        print( open( tmp ).read() )
        sys.exit( "More than one graph?" )
    #dotg = g[0].to_dot()#should allow to visualize the graph with graphivz
    return g[0], pos_treelets





# ----------------------------------------------------------------------------------
# --- UTILS
# ----------------------------------------------------------------------------------

# --- WRITE
def write( X, y, _file ):
    dump_svmlight_file( X, y, _file )


# --- LABELS
def get_labels( train_files, dev_files, test_files, file2label ):
    return _get_labels( train_files, file2label ), _get_labels( dev_files, file2label ), _get_labels( test_files, file2label )

def _get_labels( files, file2label ):
    labels = []
    for f in files:
        f = os.path.basename(f).split('.')[0]
        if file2label[f] == 'pos':
            labels.append( 1 )
        elif file2label[f] == 'neg':
            labels.append( -1 )
        else:
            sys.exit( 'Unknown label', file2label[f] )
    return labels

# --- VECTORIZE
def build_vectorizer( words, tokenizer=None, min_df=1 ):
    vectorizer = CountVectorizer(min_df=min_df, tokenizer=tokenizer)
    X = vectorizer.fit_transform(words)
    return vectorizer, X


def tfidf( X_train, X_dev, X_test ):
    tf_transformer, X_train_tf = build_tfidf_transformer( X_train )
    X_dev_tf = tf_transformer.transform( X_dev )
    X_test_tf = tf_transformer.transform( X_test )
    return X_train_tf, X_dev_tf, X_test_tf

def build_tfidf_transformer( X_train ):
    tf_transformer = TfidfTransformer(use_idf=False).fit(X_train)
    X_train_tf = tf_transformer.transform(X_train)
    return tf_transformer, X_train_tf


# --- VARIA
def merge( train_treelets, train_wds ):
    # each doc repr by a str
    train_merged = []
    for i,doc in enumerate( train_treelets ):
        str_ = doc+' '+train_wds[i]
        train_merged.append( str_ )
    return train_merged


def my_tokenizer(s):
    ''' because the text is already tokenized '''
    return s.split()

def findFile( tf, _Files ):
    basename = os.path.basename( tf ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' ).replace( '.conllu', '').replace( '.annot', '').replace( '.raw', '').replace( '.xml', '')
    for f in _Files:
        b = os.path.basename( f ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' ).replace( '.conllu', '').replace( '.annot', '').replace( '.raw', '').replace( '.xml', '')
#         print( "\t", b )
        if b == basename:
            return f
    return None

# def getSplit( split_files, label=True ):
#     file2set = {}
#     set2files = {}
#     set2count = {}
#     file2label = {}
#     for f in [_f for _f in os.listdir( split_files ) if not _f.startswith( '.' )]:
#         dset = os.path.basename( f.replace( '.txt', '' ) )
#         set2files[dset] = []
#         with open( os.path.join( split_files, f ) ) as myfile:
#             for l in myfile.readlines():
#                 if not label:
#                     file2set[l.strip()] = dset
#                     set2files[dset].append( l.strip() )
#                 else:
#                     file2set[l.split('\t')[0].strip()] = dset
#                     set2files[dset].append( l.split('\t')[0].strip() )
#                     basef = os.path.basename( l.split('\t')[0].strip() ).split('.')[0]
#                     if basef in file2label:
#                         sys.exit( 'Two files with the same basename ?', basef )
#                     file2label[basef] = l.split('\t')[1].strip()
#                 if dset in set2count:
#                     set2count[dset] += 1
#                 else:
#                     set2count[dset] = 1
#     print( ", ".join( [dset+':'+str(set2count[dset]) for dset in sorted(set2count.keys())] ) )
#     return file2set, set2files, file2label

def getFiles( tbpath, ext=".edus" ):
    _files = []
    for p, dirs, files in os.walk( tbpath ):
        subdir = os.path.basename(p)
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for file in files:
            if not file.startswith('.') and file.endswith(ext):
                #and not 'ANOTADOR_B' in os.path.join( p,file )
                _files.append( os.path.join( p, file ) )
    return _files


def retrieve_files( inpath, label=True, ext=".txt.conllu" ):
    _files, file2label = getFilesLabel( )
    conlluFiles = getFiles( inpath, ext=ext )

    dset2conllufiles = {}
    for dset in set2files:
        dset2conllufiles[dset] = []
        for i,bf in enumerate( set2files[dset] ):
            ef = findFile( bf, conlluFiles )
            if ef == None:
                print( "File not found: "+bf )
                sys.exit()
            dset2conllufiles[dset].append( ef )

    train_files = dset2conllufiles['train_list']
    test_files = dset2conllufiles['test_list']
    dev_files = []
    if 'dev' in dset2conllufiles:
        dev_files = dset2conllufiles['dev_list']
    return train_files,test_files,dev_files, file2label


def getFilesLabel( ):
    file2label = {}
    for f in [_f for _f in os.listdir( split_files ) if not _f.startswith( '.' )]:
        dset = os.path.basename( f.replace( '.txt', '' ) )
        set2files[dset] = []
        with open( os.path.join( split_files, f ) ) as myfile:
            for l in myfile.readlines():
                if not label:
                    file2set[l.strip()] = dset
                    set2files[dset].append( l.strip() )
                else:
                    file2set[l.split('\t')[0].strip()] = dset
                    set2files[dset].append( l.split('\t')[0].strip() )
                    basef = os.path.basename( l.split('\t')[0].strip() ).split('.')[0]
                    if basef in file2label:
                        sys.exit( 'Two files with the same basename ?', basef )
                    file2label[basef] = l.split('\t')[1].strip()
                if dset in set2count:
                    set2count[dset] += 1
                else:
                    set2count[dset] = 1
    print( ", ".join( [dset+':'+str(set2count[dset]) for dset in sorted(set2count.keys())] ) )
    return file2set, set2files, file2label







# ------ READERS FOR EXTERNAL LEXICONS

def read_lexicon_pol( subjmpqa ):
    lines = open( subjmpqa ).readlines()
    cats = lines[0].strip().split('\t' )
    print( len( cats ) )



# def retrieve_files_dsets( inpath, split, label=True, ext=".txt.conllu" ):
#     file2set, set2files, file2label = getSplit( split, label=label )
#     conlluFiles = getFiles( inpath, ext=ext )
#
#     dset2conllufiles = {}
#     for dset in set2files:
#         dset2conllufiles[dset] = []
#         for i,bf in enumerate( set2files[dset] ):
#             ef = findFile( bf, conlluFiles )
#             if ef == None:
#                 print( "File not found: "+bf )
#                 sys.exit()
#             dset2conllufiles[dset].append( ef )
#
#     train_files = dset2conllufiles['train_list']
#     test_files = dset2conllufiles['test_list']
#     dev_files = []
#     if 'dev' in dset2conllufiles:
#         dev_files = dset2conllufiles['dev_list']
#     return train_files,test_files,dev_files, file2label



if __name__ == '__main__':
    main()


