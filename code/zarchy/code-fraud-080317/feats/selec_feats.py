#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, random


import warnings

# import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg
from sklearn.linear_model import RandomizedLogisticRegression
from sklearn.linear_model import (RandomizedLasso, lasso_stability_path,
                                  LassoLarsCV)
from sklearn.feature_selection import f_regression
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import auc, precision_recall_curve
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.utils.extmath import pinvh
from sklearn.utils import ConvergenceWarning

from sklearn.datasets import load_svmlight_file

from sklearn.externals import joblib

from scipy.sparse import hstack, vstack

warnings.filterwarnings("ignore", category=DeprecationWarning)

# Default param  RandomizedLogisticRegression
# C=1, scaling=0.5, sample_fraction=0.75, n_resampling=200, selection_threshold=0.25, tol=0.001, fit_intercept=True, verbose=False, normalize=True, random_state=None, n_jobs=1, pre_dispatch='3*n_jobs', memory=Memory(cachedir=None)


def main( ):
    parser = argparse.ArgumentParser(
            description='Study features.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input directory')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output file with kept features')
    parser.add_argument('--vocab',
            dest='vocab',
            action='store',
            help='Mapping feat indice and name')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    train = os.path.join( args.inpath, "train.svmlight" )
#     dev = os.path.join( vars(args)['inpath'], "dev.svmlight" )
    X_train, y_train, n_features = load( train )
    print( "Data size:", X_train.shape )
#     X_dev, y_dev,_ = load( dev, n_features=n_features )

#     X_train = vstack( [X_train, X_dev], format="csr", dtype='float64' )
#     y_train = np.concatenate( (y_train, y_dev ) )

    vocab = joblib.load( args.vocab )
    vocab = {v:k for (k,v) in vocab.items()}
#     print( vocab )
    study_feats( X_train, y_train, args.outpath, vocab )

# def study_feats( inpath, outpath ):
#     randLR = RandomizedLogisticRegression( )



def study_feats( X, y, outpath, vocab ):
#     ###########################################################################
#     # Plot stability selection path, using a high eps for early stopping
#     # of the path, to save computation time
#     alpha_grid, scores_path = lasso_stability_path(X.toarray(), y, random_state=42, eps=0.05)
#
#     plt.figure()
#     # We plot the path as a function of alpha/alpha_max to the power 1/3: the
#     # power 1/3 scales the path less brutally than the log, and enables to
#     # see the progression along the path
#     hg = plt.plot(alpha_grid[1:] ** .333, scores_path[coef != 0].T[1:], 'r')
#     hb = plt.plot(alpha_grid[1:] ** .333, scores_path[coef == 0].T[1:], 'k')
#     ymin, ymax = plt.ylim()
#     plt.xlabel(r'$(\alpha / \alpha_{max})^{1/3}$')
#     plt.ylabel('Stability score: proportion of times selected')
#     plt.title('Stability Scores Path - Mutual incoherence: %.1f' % mi)
#     plt.axis('tight')
#     plt.legend((hg[0], hb[0]), ('relevant features', 'irrelevant features'),
#                loc='best')
#
#     ###########################################################################
#     # Plot the estimated stability scores for a given alpha
#
#     # Use 6-fold cross-validation rather than the default 3-fold: it leads to
#     # a better choice of alpha:
#     # Stop the user warnings outputs- they are not necessary for the example
#     # as it is specifically set up to be challenging.
#     with warnings.catch_warnings():
#         warnings.simplefilter('ignore', UserWarning)
#         warnings.simplefilter('ignore', ConvergenceWarning)
#         lars_cv = LassoLarsCV(cv=6).fit(X, y)
#
#     # Run the RandomizedLasso: we use a paths going down to .1*alpha_max
#     # to avoid exploring the regime in which very noisy variables enter
#     # the model
#     alphas = np.linspace(lars_cv.alphas_[0], .1 * lars_cv.alphas_[0], 6)

    randLR = RandomizedLogisticRegression(random_state=42)
    clf = randLR.fit(X, y)
#     print( randLR.all_scores_ )
    joblib.dump( randLR.all_scores_, os.path.join( outpath, "all_scores" ) )
    joblib.dump( randLR.scores_, os.path.join( outpath, "scores" ) )

    non_zeros = [(vocab[i],v) for i,v in enumerate( randLR.all_scores_.flatten() ) if v != 0]

    # highest score
    print( '\n'.join( [str(i)+'\t'+str(v) for (i,v) in sorted( non_zeros, key=lambda x:x[1], reverse=True )][:10] ) )


    # Keep the features selected more than 50% of the time
    c = 0
    with open( os.path.join( outpath, "kept_features.txt" ) , 'w' ) as f:
        for i,v in enumerate( randLR.all_scores_.flatten() ):
            if v > 0.5:
                f.write( str(i)+'\t'+vocab[i]+'\t'+str(v)+'\n' )
                c += 1
    print( "#Features kept", c )

    # Now we need to re write the input files keeping only these features


#     clf = RandomizedLasso(alpha=alphas, random_state=42).fit(X, y)
#     trees = ExtraTreesRegressor(100).fit(X, y)
#     # Compare with F-score
#     F, _ = f_regression(X, y)
#
#     plt.figure()
#     for name, score in [('F-test', F),
#                         ('Stability selection', clf.scores_),
#                         ('Lasso coefs', np.abs(lars_cv.coef_)),
#                         ('Trees', trees.feature_importances_),
#                         ]:
#         precision, recall, thresholds = precision_recall_curve(coef != 0,
#                                                                score)
#         plt.semilogy(np.maximum(score / np.max(score), 1e-4),
#                      label="%s. AUC: %.3f" % (name, auc(recall, precision)))
#
#     plt.plot(np.where(coef != 0)[0], [2e-4] * n_relevant_features, 'mo',
#              label="Ground truth")
#     plt.xlabel("Features")
#     plt.ylabel("Score")
#     # Plot only the 100 first coefficients
#     plt.xlim(0, 100)
#     plt.legend(loc='best')
#     plt.title('Feature selection scores - Mutual incoherence: %.1f'
#               % mi)
#
#     plt.show()


def mutual_incoherence(X_relevant, X_irelevant):
    """Mutual incoherence, as defined by formula (26a) of [Wainwright2006].
    """
    projector = np.dot(np.dot(X_irelevant.T, X_relevant),
                       pinvh(np.dot(X_relevant.T, X_relevant)))
    return np.max(np.abs(projector).sum(axis=1))

def load( _file, n_features=-1 ):
    if n_features<0:
        X,y = load_svmlight_file( _file )
    else:
        X,y = load_svmlight_file( _file, n_features=n_features )
    return X, y, X.shape[1]


if __name__ == '__main__':
    main()


