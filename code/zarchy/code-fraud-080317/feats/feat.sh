
# Exit immediately if a command exits with a non-zero status.
set -e



DATA=$1 #sandbox/fraud/ #data/fraud/
SRC=$2 #projects/navi/other_projects/fraud/code/feats/ #projects/navi/other_projects/fraud/code/




for FEAT in ngram treelet conn relexpl bow 
do
    echo
    echo --------------------------------------------
    echo ${FEAT}

    mkdir -p ${DATA}${FEAT}

    # Extract features from the data
    echo -- BUILD FEAT

    python ${SRC}feat_fraud.py --inpath ${DATA}base_files/conllu-utf8/ --conn ${DATA}base_files/conn/ --outpath ${DATA}${FEAT}/ --feat ${FEAT}

    
    # Compute Randomized stability thing, keep features selected at least 50% of the time
    echo -- STABLE SELEC
#     mkdir -p ${DATA}feat/${FEAT}/selected_feats/
    python ${SRC}selec_feats.py --inpath ${DATA}${FEAT}/ --outpath ${DATA}${FEAT}/selected_feats/ --vocab ${DATA}/${FEAT}/vocab-${FEAT}

    # Re write the svmlight files keeping only the feats selected
    echo -- PURGE
    python ${SRC}purge_features.py --inpath ${DATA}/${FEAT}/ --selec ${DATA}/${FEAT}/selected_feats/kept_features.txt --outpath ${DATA}/${FEAT}/
    # Compute chi2
    echo -- CHI2
    python ${SRC}chi2.py --train ${DATA}/${FEAT}/train.selec.svmlight --vocab ${DATA}/${FEAT}/vocab-selec --outpath ${DATA}/${FEAT}/selected_feats/chi2.txt
done



# for FEAT in ngram treelet conn relexpl bow 
# do
#     echo
#     echo --------------------------------------------
#     echo ${FEAT}
# #     mkdir -p ${DATA}feat/
#     mkdir -p ${DATA}${FEAT}
# 
#     # Extract features from the data
#     echo -- BUILD FEAT
# #     python ${SRC}feat_fraud.py --inpath ${DATA}conllu-utf8/ --split ${DATA}files_list/ --conn ${DATA}conn/ --outpath ${DATA}feat/${FEAT}/ --feat ${FEAT}
#     python ${SRC}feat_fraud.py --inpath ${DATA}base_files/conllu-utf8/ --split ${DATA}files_list/ --conn ${DATA}base_files/conn/ --outpath ${DATA}${FEAT}/ --feat ${FEAT}
# 
#     
#     # Compute Randomized stability thing, keep features selected at least 50% of the time
#     echo -- STABLE SELEC
# #     mkdir -p ${DATA}feat/${FEAT}/selected_feats/
#     python ${SRC}selec_feats.py --inpath ${DATA}${FEAT}/ --outpath ${DATA}${FEAT}/selected_feats/ --vocab ${DATA}/${FEAT}/vocab-${FEAT}
# 
#     # Re write the svmlight files keeping only the feats selected
#     echo -- PURGE
#     python ${SRC}purge_features.py --inpath ${DATA}/${FEAT}/ --selec ${DATA}/${FEAT}/selected_feats/kept_features.txt --outpath ${DATA}/${FEAT}/
#     # Compute chi2
#     echo -- CHI2
#     python ${SRC}chi2.py --train ${DATA}/${FEAT}/train.selec.svmlight --vocab ${DATA}/${FEAT}/vocab-selec --outpath ${DATA}/${FEAT}/selected_feats/chi2.txt
# done
