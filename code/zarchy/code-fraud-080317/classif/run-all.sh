#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e



DATA=$1 #data/fraud/
OUT_EXPE=$2
# FEAT=$2 #conn or bow ...
# ALGO=$3 #maxent, pa ...
# MODE=$4 #loo, opt



# FEAT=all-ngram
# 
# ALGO=maxent
# 
# 
# # MODE=loo
# # INPATH=${DATA}/${FEAT}
# # MODEL=${INPATH}/model_${ALGO}_${MODE}
# # PRED=${INPATH}/pred_${ALGO}_${MODE}
# # 
# # echo ${FEAT} ${ALGO} ${MODE}
# # python navi/other_projects/fraud/code/classify_fraud.py --inpath ${INPATH} --outmodel ${MODEL} --outpred ${PRED} --algo ${ALGO} --mode ${MODE}
# 
# 
# MODE=opt
# INPATH=${DATA}/${FEAT}
# MODEL=${INPATH}/model_${ALGO}_${MODE}
# PRED=${INPATH}/pred_${ALGO}_${MODE}
# 
# echo ${FEAT} ${ALGO} ${MODE}
# python navi/other_projects/fraud/code/classify_fraud.py --inpath ${INPATH} --outmodel ${MODEL} --outpred ${PRED} --algo ${ALGO} --mode ${MODE}



 
# for FEAT in relexpl conn treelet bow ngram bow+ngram conn+relexpl conn+treelet conn+relexpl+bow+treelet+ngram
#     #all all-ngram bow conn ngram conn+relexpl relexpl treelet
#     #relexpl conn 
# do
#     echo
#     echo -------------------------------------------------------
#     echo ${FEAT}
#     OUT=${DATA}/${FEAT}/clf
#     mkdir -p ${OUT}
#     for ALGO in maxent #pa svc nb
#     do
# 
#         for MODE in ncv #opt loo 
#         do
#         echo MODE ${MODE}
# #         INPATH=${DATA}/${FEAT}
#         TRAIN=${DATA}/${FEAT}/train.selec.svmlight #.selec
#         TEST=${DATA}/${FEAT}/test.selec.svmlight #.selec
#         MODEL=${OUT}/model_selec_${ALGO}_${MODE} #_selec
#         PRED=${OUT}/pred_selec_${ALGO}_${MODE} #_selec
#         OUTDIR=${OUT_EXPE}/plot_scores/
# 
# 
#         echo ${FEAT} ${ALGO} ${MODE}
#         #--inpath ${INPATH}
#         python navi/other_projects/fraud/code/classif/classify_fraud.py --train ${TRAIN} --test ${TEST} --outmodel ${MODEL} --outpred ${PRED} --algo ${ALGO} --mode ${MODE} --outpath ${OUTDIR} --feat ${FEAT}
# 
#     done
# done
# done


#for FEAT in bow+ngram conn+relexpl conn+treelet conn+relexpl+bow+treelet+ngram
for FEAT in conn+bow+treelet+ngram conn+bow+ngram bow+treelet+ngram
    #all all-ngram bow conn ngram conn+relexpl relexpl treelet
    #relexpl conn 
do
    echo
    echo -------------------------------------------------------
    echo ${FEAT}
    OUT=${DATA}/${FEAT}/clf
    mkdir -p ${OUT}
    for ALGO in maxent #pa svc nb
    do

        for MODE in ncv #opt loo 
        do
        echo MODE ${MODE}
#         INPATH=${DATA}/${FEAT}
        TRAIN=${DATA}/${FEAT}/train.svmlight #.selec
        TEST=${DATA}/${FEAT}/test.svmlight #.selec
        MODEL=${OUT}/model_selec_${ALGO}_${MODE} #_selec
        PRED=${OUT}/pred_selec_${ALGO}_${MODE} #_selec
        OUTDIR=${OUT_EXPE}/plot_scores/


        echo ${FEAT} ${ALGO} ${MODE}
        #--inpath ${INPATH}
        python navi/other_projects/fraud/code/classif/classify_fraud.py --train ${TRAIN} --test ${TEST} --outmodel ${MODEL} --outpred ${PRED} --algo ${ALGO} --mode ${MODE} --outpath ${OUTDIR} --feat ${FEAT}

    done
done
done

