#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Requires scikit 18
'''


# from __future__ import print_function
import matplotlib as mpl
mpl.use('Agg')
import argparse, os, sys, subprocess, shutil, codecs, random
import numpy as np


from sklearn import linear_model, svm
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, make_scorer

from sklearn.model_selection import GridSearchCV, cross_val_score, KFold, LeaveOneOut


# from sklearn.grid_search import GridSearchCV #change in v20
from sklearn.datasets import load_svmlight_file
from sklearn.externals import joblib
from scipy.sparse import hstack, vstack
# from sklearn.cross_validation import LeaveOneOut,KFold,cross_val_score #model_selection in v18
from matplotlib import pyplot as plt

from sklearn.utils import shuffle

clf2params = {
        "nb":[{'alpha':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]}],
        "maxent":[{'penalty':['l1'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]},
            {'penalty':['l2'], 'C':[0.001, 0.005, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 5, 10, 20, 30, 40, 50, 100]}],
        "pa":[{'loss':['hinge'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100], 'n_iter':[1, 5, 50, 100, 200, 500]},
            {'loss':['squared_hinge'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100], 'n_iter':[1, 5, 50, 100, 200, 500]}],
        "svc":[{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                     'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
        }


#"maxent":[{'penalty':['l1'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]},
#            {'penalty':['l2'], 'C':[0.001, 0.005, 0.01, 0.1, 0.5, 1, 5, 10, 100]}],

def main( ):
    parser = argparse.ArgumentParser(
            description='Classification.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input directory')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory')
    parser.add_argument('--feat',
            dest='feat',
            action='store',
            help='Feature used (for naming files)')
    parser.add_argument('--train',
            dest='train',
            action='store',
            help='train')
    parser.add_argument('--test',
            dest='test',
            action='store',
            help='test')
    parser.add_argument('--dev',
            dest='dev',
            action='store',
            help='dev')
    parser.add_argument('--outmodel',
            dest='outmodel',
            action='store',
            default=None,
            help='Output file for model')
    parser.add_argument('--outpred',
            dest='outpred',
            action='store',
            default=None,
            help='Output file for predictions on test')
    parser.add_argument('--algo',
            dest='algo',
            action='store',
            default='maxent',
            choices=['maxent', 'nb','svc', 'pa'],
            help='Algo to use: maxent, nb, pa, svc')
    parser.add_argument('--score',
            dest='score',
            action='store',
            default='accuracy_score',
            help='Score to optimize: accuracy_score, precision_score, f1_score')
    parser.add_argument('--avg',
            dest='avg',
            action='store',
            default='micro',
            help='Average strategy for score to optimize: macro, micro, weighted?')
    parser.add_argument('--mode',
            dest='mode',
            choices=['opt', 'def', 'loo', 'ncv'],
            action='store',
            default='opt',
            help='Optimize using grid search (opt) or train with default param (def) or estimate from a leave one out strategy (loo) or use nested cross validation (ncv)')
    args = parser.parse_args()

    if args.outpath and not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    if args.inpath:
        train = os.path.join( vars(args)['inpath'], "train.svmlight" )
        dev = os.path.join( vars(args)['inpath'], "dev.svmlight" )
        test = os.path.join( vars(args)['inpath'], "test.svmlight" )
    else:
        train = args.train
        dev = args.dev
        test = args.test

    X_train, y_train,n_features = load( train )
    print( "Train", X_train.shape )
    X_dev, y_dev = None, None #No dev set, not enough data
    if args.dev and os.path.isfile( dev ):
        X_dev, y_dev,_ = load( dev, n_features=n_features )
    X_test, y_test,_ = load( test, n_features=n_features )

    if args.mode == 'loo': #only LOO scores
        loo( X_train, y_train,  X_dev, y_dev, X_test, y_test, algo=args.algo )
    elif args.mode == 'ncv': #nested cross-validation
        nested_crossval( X_train, y_train,  X_test, y_test, args.outpath,
                args.feat, algo=args.algo )
    elif args.mode == 'opt': #optimization using cv on train + eval on test
        clf = learn( X_train, y_train, X_dev, y_dev, args.mode, algo=args.algo )
#     clf = train_clf( X_train, y_train, args.mode )
#     clf = optimize_clf( X_train, y_train, X_test, y_test, algo=args.algo )

        # -- Eval on the Test set
        preds = eval( clf,  X_test, y_test )

        if vars(args)['outmodel']:
            joblib.dump( clf, vars(args)['outmodel'], compress=3 )

        if vars(args)['outpred']:
            joblib.dump( preds, vars(args)['outpred'] )




def nested_crossval( X_train, y_train,  X_test, y_test, outpath, feat, algo='maxent' ):
    print( "\n\nFEATURE:", feat, file=sys.stderr )
    # Number of random trials
    NUM_TRIALS = 10
    # Merge test and train
    X = vstack( [X_train, X_test], format="csr", dtype='float64' )
    y = np.concatenate( (y_train, y_test ) )
    # Initialize classifier and set up possible values of parameters to optimize
    me, tuned_parameters = define_update( algo )
    # Array to store scores
#     non_nested_scores = np.zeros(NUM_TRIALS)
    nested_scores_loo = np.zeros(NUM_TRIALS)
    nested_scores_kf = np.zeros(NUM_TRIALS)
    loo_scores = np.zeros(NUM_TRIALS)

    # Loop for each trial
    for i in range(NUM_TRIALS):
        print( "Trial", i, file=sys.stderr )
        # Choose cross-validation techniques for the inner and outer loops,
        # independently of the dataset.
        # E.g "LabelKFold", "LeaveOneOut", "LeaveOneLabelOut", etc.
        #inner_cv = KFold(n_splits=10, shuffle=True, random_state=i)
#         inner_cv = KFold(X.shape[0], n_folds=10, shuffle=True, random_state=i)
#         outer_cv = KFold(X.shape[0], n_folds=2, shuffle=True, random_state=i)


#         inner_cv = KFold(n_splits=5, shuffle=True, random_state=i)


        # First shuffle the data (ie no shuffling option in new LOO)
        Xs,ys = shuffle( X, y, random_state=i)

        # Try a LOO on all data
        loo_cv = LeaveOneOut(  )
        clf = GridSearchCV(estimator=me, param_grid=tuned_parameters, cv=loo_cv, n_jobs=4)
        clf.fit(Xs, ys)
        loo_scores[i] = clf.best_score_


        # Nested with LOO/KF as inner, 100 kepts as test (ie 5 fold)
        # -- begin outer loop
        outer_cv = KFold( n_splits=5 )
        inner_cv_loo = LeaveOneOut(  )
        inner_cv_kf = KFold( n_splits=4 )
        cv_loo_scores = []
        cv_kf_scores = []
        for k, (train_index, test_index) in enumerate( outer_cv.split(X) ):# Do k times
            print( "Running outer CV fold", k )
            print( "Running outer CV fold", k, file=sys.stderr )
            X_train, X_test = Xs[train_index], Xs[test_index]
            y_train, y_test = ys[train_index], ys[test_index]

            # -- LOO as inner
            print( "\tInner scores with LOO" )
            clf = GridSearchCV(estimator=me, param_grid=tuned_parameters, cv=inner_cv_loo, n_jobs=4)
            clf.fit(X_train, y_train) # grid search on the train part
            preds = clf.predict( X_test ) # eval on the fold left out
            sc = scores( y_test, preds, score='acc' )
            cv_loo_scores.append( sc )
#             nested_scores_loo[i] = clf.best_score_

            # -- KF as inner
            print( "\tInner scores with KF" )
            clf = GridSearchCV(estimator=me, param_grid=tuned_parameters, cv=inner_cv_kf, n_jobs=4)
            clf.fit( X_train, y_train )
            preds = clf.predict( X_test ) # eval on the fold left out
            sc = scores( y_test, preds, score='acc' )
            cv_kf_scores.append( sc )

#             nested_scores_kf[i] = clf.best_score_

        nested_scores_loo[i] = np.mean( cv_loo_scores )
        nested_scores_kf[i] = np.mean( cv_kf_scores )



#
#         # Keep 100 inst per test set -> 5-fold CV
#         outer_cv = KFold(n_splits=5) #, shuffle=True, random_state=i)
#         ns = outer_cv.get_n_splits(Xs)# number of data sample per KFold
#         print( 'number of data sample per KFold', ns )
#
#         # Try to use only a Leave one out strategy (as inner CV or only CV)
#         inner_cv = LeaveOneOut(  )
#
#         # Try to use 4-kfold CV
#         inner_cv_kf = KFold(n_splits=4)
#
#         # Non_nested parameter search and scoring - LOO
#         clf = GridSearchCV(estimator=me, param_grid=tuned_parameters, cv=inner_cv)
#         clf.fit(Xs, ys)
#         non_nested_scores[i] = clf.best_score_
#
#         # Nested CV with parameter optimization - LOO
#         nested_score = cross_val_score(clf, X=Xs, y=ys, cv=outer_cv)
#         nested_scores[i] = nested_score.mean()

        print( "\tRound", i,'N-LOO / N-KF / LOO', nested_scores_loo[i], nested_scores_kf[i], loo_scores[i] )

#     score_difference = non_nested_scores - nested_scores

    joblib.dump( loo_scores, os.path.join( outpath, 'loo_scores'+algo+'_'+feat) )
    joblib.dump( nested_scores_loo, os.path.join( outpath, 'nested_scores_loo'+algo+'_'+feat) )
    joblib.dump( nested_scores_kf, os.path.join( outpath, 'nested_scores_kf'+algo+'_'+feat) )

    score_difference_loo = loo_scores - nested_scores_loo
    score_difference_kf = nested_scores_loo - nested_scores_kf

    print("LOO - Average difference of {0:6f} with std. dev. of {1:6f}."
          .format(score_difference_loo.mean(), score_difference_loo.std()))

    print("KF - Average difference of {0:6f} with std. dev. of {1:6f}."
          .format(score_difference_kf.mean(), score_difference_kf.std()))


    print( "Non nested scores with LOO:", loo_scores, loo_scores.mean() )
    print( "Nested scores with LOO:", nested_scores_loo, nested_scores_loo.mean() )
    print( "Nested scores with KF:", nested_scores_kf, nested_scores_kf.mean() )

#     print( "Non nested scores:", non_nested_scores, non_nested_scores.mean() )
#     print( "Nested scores:", nested_scores, nested_scores.mean() )
#

#     non_nested_scores = loo_scores
#     nested_scores = nested_scores_loo
#
#     # Plot scores on each trial for nested and non-nested CV
#     plt.figure()
#     plt.subplot(211)
#     non_nested_scores_line, = plt.plot(non_nested_scores, color='r')
#     nested_line, = plt.plot(nested_scores, color='b')
#     nested_line_kf, = plt.plot(nested_scores_kf, color='green')
#     plt.ylabel("score", fontsize="14")
#     plt.legend([non_nested_scores_line, nested_line, nested_line_kf],
#                ["Non-Nested LOO", "Nested LOO", "Nested KF"],
#                bbox_to_anchor=(0, .4, .5, 0))
#     plt.title("Non-Nested and Nested Cross Validation on Iris Dataset",
#               x=.5, y=1.1, fontsize="15")
#
#
#     score_difference = score_difference_loo
#     # Plot bar chart of the difference.
#     plt.subplot(212)
#     difference_plot = plt.bar(range(NUM_TRIALS), score_difference)
#     plt.xlabel("Individual Trial #")
#     plt.legend([difference_plot],
#                ["Non-Nested LOO - Nested LOO Score"],
#                bbox_to_anchor=(0, 1, .8, 0))
#     plt.ylabel("score difference", fontsize="14")
#
#     # Plot bar chart of the difference.
#     plt.subplot(212)
#     difference_plot = plt.bar(range(NUM_TRIALS), score_difference_kf)
#     plt.xlabel("Individual Trial #")
#     plt.legend([difference_plot],
#                ["Nested LOO - Nested KF Score"],
#                bbox_to_anchor=(0, 1, .8, 0))
#     plt.ylabel("score difference", fontsize="14")
#
#     plot_file = os.path.join( outpath, 'fraud_nested_crossval_'+algo+'_'+feat+'.png' )
#
#     plt.savefig( plot_file, bbox_inches='tight' )
# #     plt.show()


# Traceback (most recent call last):
#   File "navi/other_projects/fraud/code/classify_fraud.py", line 428, in <module>
#     main()
#   File "navi/other_projects/fraud/code/classify_fraud.py", line 131, in main
#     args.feat, algo=args.algo )
#   File "navi/other_projects/fraud/code/classify_fraud.py", line 288, in nested_crossval
#     difference_plot = plt.bar(range(NUM_TRIALS), score_difference_kf)
#   File "/home/chloe/anaconda2/envs/peachtree/lib/python3.6/site-packages/matplotlib/pyplot.py", line 2705, in bar
#     **kwargs)
#   File "/home/chloe/anaconda2/envs/peachtree/lib/python3.6/site-packages/matplotlib/__init__.py", line 1892, in inner
#     return func(ax, *args, **kwargs)
#   File "/home/chloe/anaconda2/envs/peachtree/lib/python3.6/site-packages/matplotlib/axes/_axes.py", line 2079, in bar
#     "must be length %d or scalar" % nbars)
# ValueError: incompatible sizes: argument 'height' must be length 20 or scalar


def loo( X, y,  X_dev, y_dev, X_test, y_test, algo='maxent' ):
    clf, tuned_parameters = define_update( algo )
    #score_fct = define_score_fct( score )

    # use all the data
    if X_dev:
        X = vstack( [X, X_dev], format="csr", dtype='float64' )
        y = np.concatenate( (y, y_dev ) )
    X = vstack( [X, X_test], format="csr", dtype='float64' )
    y = np.concatenate( (y, y_test ) )
    print( "Data size:", X.shape[0] )
    #loo = LeaveOneOut( X.shape[0] )
    loo = LeaveOneOut( )
    res = {}
#     #for scikit v18
#     loo = LeaveOneOut( )
#     loo.get_n_splits(X)
#     for train_index, test_index in loo.split(X):
    for train_index, test_index in loo:
        #print("TRAIN:", train_index, "TEST:", test_index)
        X_tr, X_t = X[train_index], X[test_index]
        y_tr, y_t = y[train_index], y[test_index]
        clf.fit(X_tr, y_tr)
        p = clf.predict( X_t )
        #print( 'preds', preds )
        res[test_index[0]] = p[0]
#     print( y )
#     print( res )
#     print( sorted( res.keys() ) )
    preds = [res[k] for k in sorted( res.keys() )]
#     print( preds )
    print( "Acc:", accuracy_score( y, preds ) )
    print( classification_report( y, preds ) )
    print( confusion_matrix( y, preds ) )

def learn( X_train, y_train, X_dev, y_dev, mode, algo ):
    if mode == "opt":
        # no dev, use CV to estimate best parameters
        if X_dev:
            X_train = vstack( [X_train, X_dev], format="csr", dtype='float64' )
            y_train = np.concatenate( (y_train, y_dev ) )
        return optimize_clf( X_train, y_train, algo=algo )
    elif mode == "def":
        # no opt, so no dev
        if X_dev:
            X_train = vstack( [X_train, X_dev], format="csr", dtype='float64' )
            y_train = np.concatenate( (y_train, y_dev ) )
        return train_clf( X_train, y_train, algo=algo )
    else:
        sys.exit( "Mode undefined: "+mode )


def optimize_clf( X_train, y_train, algo="maxent", score='accuracy_score' ):

    base_clf, tuned_parameters = define_update( algo )
    score_fct = define_score_fct( score )

    print("# Tuning hyper-parameters for %s" % score)
    print()

    clf = GridSearchCV( base_clf, tuned_parameters, cv=10,
                       scoring=score_fct)
    clf.fit(X_train, y_train)

    print("Best parameters set found on development set:")
    print()
    print(clf.best_params_)
    print()
    print("Grid scores on development set:")
    print()
    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r"
              % (mean_score, scores.std() * 2, params))
    print()

    return clf

def define_score_fct( score, average='macro' ):
    # DEFINE SCORE FUNCTION
    if score == 'accuracy_score':
        return make_scorer(accuracy_score)
    elif score == 'precision_score':
        return  make_scorer(precision_score, average=average)
    elif score == 'f1_score':
        return  make_scorer(f1_score, average=average)
    else:
        sys.exit( "Unknwon score "+score )

def define_update( update ):
    # DEFINE TUNED PARAMETERS AND UPDATE
    if update == 'nb':
        return MultinomialNB(), clf2params["nb"]
    elif update == 'maxent':
        return linear_model.LogisticRegression( n_jobs=1 ), clf2params["maxent"]
    elif update == 'pa':
        return linear_model.PassiveAggressiveClassifier(), clf2params["pa"]
    elif update == "svc":
        return svm.SVC(), clf2params["svc"]
    else:
        sys.exit( "Unknown algo "+update )


def load( _file, n_features=-1 ):
    if n_features<0:
        X,y = load_svmlight_file( _file )
    else:
        X,y = load_svmlight_file( _file, n_features=n_features )
    return X, y, X.shape[1]


def train_clf( X_train, y_train, algo="maxent" ):
    clf,_ = define_update( algo )
#     clf = linear_model.LogisticRegression()
    clf.fit( X_train, y_train )
    return clf

def eval( clf, X_test, y_test ):
    preds = clf.predict( X_test )
    print( "Acc:", accuracy_score( y_test, preds ) )
    print( classification_report( y_test, preds ) )
    print( confusion_matrix( y_test, preds ) )

    return preds

def scores( gold, pred, score='acc' ):
    print( "Acc:", accuracy_score( gold, pred ) )
    print( classification_report( gold, pred ) )
    print( confusion_matrix( gold, pred ) )
    if score == 'acc':
        return accuracy_score( gold, pred )


if __name__ == '__main__':
    main()



